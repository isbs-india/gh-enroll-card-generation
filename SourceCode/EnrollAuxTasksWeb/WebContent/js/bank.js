function ddlBankStatechange(bankstates)
{
	  var selBankCityId = bankstates.value.split("~");
	 //alert(selBankCityId[0]);
	 var stateIdSelected = selBankCityId[0];
	
	 var xmlhttp;
	 var strAjUrlData="CitySearch?city="+stateIdSelected;
	 if (window.XMLHttpRequest)
	 {
		 // code for IE7+, Firefox, Chrome, Opera, Safari
	     xmlhttp=new XMLHttpRequest();
	 }
	 else
	 {
	  // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	   {
		 // alert(xmlhttp.responseText);	
		  document.getElementById('txtbankcity').innerHTML=xmlhttp.responseText;
		  document.getElementById('ddlbankname').innerHTML="<option value='-1'>-Select-</option>";
		  document.getElementById('txtbankBranch').innerHTML="<option value='-1'>-Select-</option>";
		  document.getElementById('txtifsccode').value="";
		  document.getElementById('txtMicrNo').value="";
		  document.getElementById('txtBankAddress').value="";
	   }
	   else
	   {
	   //alert(xmlhttp.status);
	   }
	  }
	   xmlhttp.open("POST",strAjUrlData,true);
	   xmlhttp.send();
}
function ddlBankCitychange(cityddl){
	 var selCityId = cityddl.value.split("~");
	 var cityIdSelected = selCityId[0]; 
		 var states = document.getElementById("txtbankstate");
		 var ss = states.options[states.selectedIndex].value;
		  var stateId= ss.split("~");
		 var xmlhttp;
		 var strAjUrlData="CitySearch?bank="+cityIdSelected+"&stateId="+stateId[0];
		 if (window.XMLHttpRequest)
		 {
			 // code for IE7+, Firefox, Chrome, Opera, Safari
		     xmlhttp=new XMLHttpRequest();
		 }
		 else
		 {
		  // code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		   {
			  //alert(xmlhttp.responseText);	
			  document.getElementById('ddlbankname').innerHTML=xmlhttp.responseText;
			  document.getElementById('txtbankBranch').innerHTML="<option value='-1'>-Select-</option>";
			  document.getElementById('txtifsccode').value="";
			  document.getElementById('txtMicrNo').value="";
			  document.getElementById('txtBankAddress').value="";
		   }
		   else
		   {
		   //alert(xmlhttp.status);
		   }
		  }
		   xmlhttp.open("POST",strAjUrlData,true);
		       xmlhttp.send();
}

function ddlBankNamechange(cityddl){
	 var selCityId = cityddl.value.split("~");
		 var cityIdSelected = selCityId[0]; 
		 var states = document.getElementById("txtbankstate");
		 var ss = states.options[states.selectedIndex].value;
		  var stateId= ss.split("~");
		  
		  var citis = document.getElementById("txtbankcity");
		  var cc = citis.options[citis.selectedIndex].value;
		  var ghplcityId= cc.split("~");
		 var xmlhttp;
		 var strAjUrlData="CitySearch?branch="+cityIdSelected+"&stateId="+stateId[0]+"&cityId="+ghplcityId[1];
		 if (window.XMLHttpRequest)
		 {
			 // code for IE7+, Firefox, Chrome, Opera, Safari
		     xmlhttp=new XMLHttpRequest();
		 }
		 else
		 {
		  // code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		   {
			  //alert(xmlhttp.responseText);	
			  document.getElementById('txtbankBranch').innerHTML=xmlhttp.responseText;
			  document.getElementById('txtifsccode').value="";
			  document.getElementById('txtMicrNo').value="";
			  document.getElementById('txtBankAddress').value="";
		   }
		   else
		   {
		   //alert(xmlhttp.status);
		   }
		  }
		   xmlhttp.open("POST",strAjUrlData,true);
		   xmlhttp.send();
}

function ddlBankBranchchange(cityddl){
	 var selCityId = cityddl.value.split("~");
		 var cityIdSelected = selCityId[0]; 
		 var xmlhttp;
		 var strAjUrlData="CitySearch?details="+cityIdSelected;
		 var branchdetobj="";
		 if (window.XMLHttpRequest)
		 {
			 // code for IE7+, Firefox, Chrome, Opera, Safari
		     xmlhttp=new XMLHttpRequest();
		 }
		 else
		 {
		  // code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		   {
			   branchdetobj= xmlhttp.responseText;
			 // alert(" first "+branchdetobj);	
			  var detailsobj = branchdetobj.split("~");
			  //alert("sec "+detailsobj);	
			  var detailsifsc = detailsobj[0];
			 // alert("ifsc "+detailsifsc);
			  var detailsMicr = detailsobj[1];
			 // alert("micr "+detailsMicr);
			  var detailsAddress = detailsobj[2];
			 // alert("add "+detailsAddress);
			  document.getElementById('txtifsccode').value=detailsifsc;
			  document.getElementById('txtMicrNo').value=detailsMicr;
			  document.getElementById('txtBankAddress').value=detailsAddress;
		   }
		   else
		   {
		   //alert(xmlhttp.status);
		   }
		  }
		   xmlhttp.open("POST",strAjUrlData,true);
		   xmlhttp.send();
}


function banksubmit(){
	var message = "VALIDATION MESSAGE";
	var regexp_Name1 = new RegExp(/^[a-zA-Z0-9 .-]+$/);	
var bankstate = document.getElementById("txtbankstate");
 var bankstateId = bankstate.options[bankstate.selectedIndex].value;
 var bankcity = document.getElementById("txtbankcity");
 var bankcityId = bankcity.options[bankcity.selectedIndex].value;
 var bankname = document.getElementById("ddlbankname");
 var banknameId = bankname.options[bankname.selectedIndex].value;
 var accno=myTrim(document.getElementById("txtaccno").value);
 var bankBranch = document.getElementById("txtbankBranch");
 var bankBranchId = bankBranch.options[bankBranch.selectedIndex].value;
 var ifsccode=myTrim(document.getElementById("txtifsccode").value);
 var actype = document.getElementById("ddlactype");
 var actypeId = actype.options[actype.selectedIndex].value;
 var txtMicrNo=myTrim(document.getElementById("txtMicrNo").value);
 var chqInFavorName=myTrim(document.getElementById("txtChqInFavorName").value);
 var bankAddress=myTrim(document.getElementById("txtBankAddress").value);
 
 /*if(bankstateId==0){
	 CustomConfirmation('Please select bank state',message);
	 return false; 	 
 }
 if(bankcityId==0){
	 CustomConfirmation('Please select bank city',message);
	 return false; 	 
 }
 if(banknameId==0){
	 CustomConfirmation('Please select bank name',message);
	 return false; 	 
 }
 if((accno==null) || (accno == ""))
  {
		CustomConfirmation('Please enter valid account no',message);
		return false;
  } */
 if((accno!=null) && (accno != "") )
  {
	 for (var i = 0; i < myTrim(accno).length; i++) {
			if(!regexp_Name1.test(accno)){
				CustomConfirmation('Special Characters are not allowed in accno',message);
				return false;
			}
		}
  }
 /*if(bankBranchId==0){
	 CustomConfirmation('Please select bank branch',message);
	 return false; 	 
 }
 if((ifsccode==null) || (ifsccode == ""))
  {
		CustomConfirmation('Please enter valid IFSC code ',message);
		return false;
  } */
 if((ifsccode!=null) && (ifsccode != "") )
  {
	
	 for (var i = 0; i < myTrim(ifsccode).length; i++) {
			if(!regexp_Name1.test(ifsccode)){
				CustomConfirmation('Special Characters are not allowed in IFSC code',message);
				return false;
			}
		}
  }
 /*if(actypeId==0){
	 CustomConfirmation('Please select Account Type',message);
	 return false; 	 
 }
 if((txtMicrNo==null) || (txtMicrNo == ""))
  {
		CustomConfirmation('Please enter valid MCR no ',message);
		return false;
  }*/ 
 if((txtMicrNo!=null) && (txtMicrNo != "") )
  {
	 for (var i = 0; i < myTrim(txtMicrNo).length; i++) {
			if(!regexp_Name1.test(txtMicrNo)){
				CustomConfirmation('Special Characters are not allowed in MCR no',message);
				return false;
			}
		}
  }
 /*if((chqInFavorName==null) || (chqInFavorName == ""))
  {
		CustomConfirmation('Please enter valid Chq In Favour Name ',message);
		return false;
  }*/ 
 if((chqInFavorName!=null) && (chqInFavorName != "") )
  {
	 for (var i = 0; i < myTrim(chqInFavorName).length; i++) {
			if(!regexp_Name1.test(chqInFavorName)){
				CustomConfirmation('Special Characters are not allowed in chq In Favor Name',message);
				return false;
			}
		}
  }
 /*if((bankAddress==null) || (bankAddress == ""))
  {
		CustomConfirmation('Please enter valid Bank Address  ',message);
		return false;
  }*/ 
 return true;
 /* if((bankAddress!=null) && (bankAddress != "") )
  {
	 for (var i = 0; i < myTrim(bankAddress).length; i++) {
			if(!regexp_Name1.test(bankAddress)){
				CustomConfirmation('Special Characters are not allowed in chq In Favor Name',message);
				return false;
			}
		}
  } */
  }


 
 
	 
