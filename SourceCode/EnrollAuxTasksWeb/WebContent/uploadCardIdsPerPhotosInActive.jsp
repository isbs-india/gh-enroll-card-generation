
<%@include file="header.jsp"%>

<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>
<link href="./js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="./js/jquery-ui.theme.min.css" rel="stylesheet"
	type="text/css">
<script type="text/javascript" src="./js/cycle_plugin.js"></script>

<link rel="stylesheet" href="./css/jquery-ui-timepicker-addon.css">
<script src="./js/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery-ui-sliderAccess.js"></script>
<script src="js/bank.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>

<script type="text/javascript">
var message = "VALIDATION MESSAGE";

function ddlModulechange(moduleddl)
{
	
	  var selPolicyId = moduleddl.value.split("~");
	  //alert(selPolicyId);
	 var moduleIdSelected = selPolicyId[0]; 
	  //alert(moduleIdSelected);
	 var xmlhttp;
	 var strAjUrlData="InActivePolicySearch?key="+moduleIdSelected;
	 if (window.XMLHttpRequest)
	 {
		 // code for IE7+, Firefox, Chrome, Opera, Safari
	     xmlhttp=new XMLHttpRequest();
	 }
	 else
	 {
	  // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	
	  xmlhttp.onreadystatechange=function()
	  {
		 
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	   {
		
		  document.getElementById('policy').innerHTML=xmlhttp.responseText;
		  
	   }
	   else
	   {
	   }
	  }
	   xmlhttp.open("POST",strAjUrlData,true);
	   xmlhttp.send();
	 
}
  


function myTrim(x) {
	return x.replace(/^\s+|\s+$/gm, '');
}
function CustomConfirmation(msg, title_msg){
	if ($('#ConfirmMessage').length == 0) {
	        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
	    } else {
	        $('#ConfirmMessage').html(msg);
	    }

	    $("#ConfirmMessage").dialog({
	        autoOpen: false,
			title: title_msg,
	        show: "blind",
	        hide: "explode",  
			resizable: false,      
	        height: 200,
	        width: 300,
	        modal: true,
			buttons: {
	            "Ok": function() 
	            {
	                $( this ).dialog( "close" );
	            }
			}
	    });
	  
	    $( "#ConfirmMessage" ).dialog("open");
	}

function validateForm(){
	
	
	   var moduleId = document.getElementById("moduleId");
	 var moduleIdId = moduleId.options[moduleId.selectedIndex].value;
	 
	 var policyobj = document.getElementById("policy");
	 var policyArray = policyobj.options[policyobj.selectedIndex].value;
	 
	 var policyIdId = policyArray.split("~")[0];
	
	 if(moduleIdId == 0)
	 {
		 CustomConfirmation('Please select valid Module Id',message);
		 return false; 
	 }
	 if(policyIdId == 0)
	 {
		 CustomConfirmation('Please select valid policy',message);
		 return false; 
	 } 
	
	  var xlfile = document.getElementById("xlfile").value;
	
	if(xlfile==""){
		CustomConfirmation("please browse excel file only",message);
		document.getElementById("xlfile").focus();
		return false;
	}
	
	if(xlfile!=""){
		 if(xlfile.lastIndexOf(".xlsx")==-1 && xlfile.lastIndexOf(".xls")==-1){
			 CustomConfirmation("Only xls, xlsx  files can be uploaded",message);
			 return false;
		 }
	 } 
		
	//return false;
}

</script>
 
<h3 align="center">Upload CardId's per Photos</h3>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp"%>
		<td align="center" width="100%" valign="top">
			<table width="60%" border="0" cellpadding="1" cellspacing="1"
				class="tablebg">
				<tr>
					<td>
						<fieldset>
							<legend>Upload CardId's per Photos</legend>		
							<form name="uploadCards" method="post" action="./UploadCardIDsPhotoInActiveAction" enctype="multipart/form-data" onSubmit="return validateForm();"> 
								<table width="100%" border="1" cellpadding="0" cellspacing="0" class="tablebg">
									
									 <tr>
										<td align="left">Module Id <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 <select name="moduleId" id="moduleId" onchange="ddlModulechange(this)">
											<option id="0" value="0">Select</option>
											<option id="1" value="1">Corp</option> 
											<option id="2" value="2">Bank Assurance</option>
										</select> 
										
										</td><td>&nbsp; </td>
									</tr>
									<tr>
										<td align="left">Select Policy <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 	<select name="policy" id="policy"
												style="width: 200px;" >
												<option selected="selected" value="0">-Select -</option>
											</select>
										</td>
										<td>&nbsp; </td>
									</tr> 
									<tr>
										<td align="left">Upload <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 <input type="file" name="xlfile" id="xlfile" class ="upload" enctype="multipart/form-data"  value="" multiple/>  &nbsp; (<font color="red"> Note : Only excel Row count should be less than 900 records are eligible</font>)
										</td>
										<td>&nbsp; </td>
									</tr>
									
									<tr>
										<td align="right" colspan="1"><a href="./DownLoadTemplateAction?fileName=ABCARDTemplate.xlsx"><h4>Sample Upload Template </h4></h4></a> </td>
									</tr> 
									
									<tr>
									    <td>
										 <input type="submit" name="submit" id="submit" value="submit" />
										</td>
									</tr>
									
								</table>
							</form>
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<%@include file="footer.jsp"%>