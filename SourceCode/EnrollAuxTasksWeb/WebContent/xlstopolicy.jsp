<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="header.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script> 
<link href="<%=request.getContextPath()%>/js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/1024.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cycle_plugin.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">
<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui-timepicker-addon.css">
<script src="<%=request.getContextPath()%>/js/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}

	
    
    function ddlPolicychange(policyId)
	{
		if(policyId==0){
			document.getElementById("dataId").style.display = "none";
		}else{
			document.getElementById("dataId").style.display = "";
		}
	 }
    
    
    function validateForm()
    {
    	 var message = "VALIDATION MESSAGE";
    	 var policyddl = document.getElementById("ddlPolicy");
		 var policyddlId = policyddl.options[policyddl.selectedIndex].value;
		 if(policyddlId == 0)
		 {
			 CustomConfirmation('Please select valid Policy',message);
			 return false; 
		 }
		 var xlfile = document.getElementById("xlfile").value;
		 if(xlfile==""){
				CustomConfirmation("please browse excel file only",message);
				document.getElementById("xlfile").focus();
				return false;
			}
		 if(xlfile!=""){
			 if(xlfile.lastIndexOf(".xlsx")==-1 && xlfile.lastIndexOf(".xls")==-1){
				 CustomConfirmation("Only xls, xlsx  files can be uploaded",message);
				 return false;
			 }
		 } 
    }
    
    
</script>


<h3 align="center">Banking Policies</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp"%>
		<td align="center" width="100%">
			<table width="80%" border="0" cellpadding="1" cellspacing="1" class="tablebg">
				<tr>
					<td>
						<fieldset>
							<legend>Search Criteria</legend>
							<form name="policyForm" method="post" action="./InsertBankPolicyAction" enctype="multipart/form-data" onSubmit="return validateForm();">
								<table width="100%" border="20" cellpadding="0" cellspacing="0" class="tablebg">
								<input type="hidden" name="userId" id="userId" value="<%=session.getAttribute("userId")%>">
								<input type="hidden" name="XLpolicyForm" id="" value="XLSelfCardData" />
							
									<tr>
										<td align="right" colspan="2">Select Policy:
											<select name="ddlPolicy" id="ddlPolicy" style="width:300px;"  onchange="ddlPolicychange(this.value)">
											     <option selected="selected" value="0">- Select -</option>
											      <c:if test="${fn:length(policyDDLListObj) gt 0 }">
											     	<c:forEach items="${policyDDLListObj}" var="option" > 
	   													<option value="${option.policyId}">${option.policyHolderName}(${option.policyFrom} - ${option.policyTo})</option>
													</c:forEach>
												</c:if> 
											 </select>&nbsp;&nbsp;&nbsp; (<font color="red"> Note : Only self cards are eligible</font>)
										</td>
										
									</tr>
									
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									
									<tr id="dataId" style="display:none;">
									<td  width="100%"> <table>
									    <tr>
											<td align="left" width="33%">
											<input type="file" size="30" name="xlfile" id="xlfile"/></td>
										</tr>
										
										
										<tr>
										<td colspan="3">
										<input type="radio" name="format" value="vendor" checked>Vendor Format &nbsp;
  										<input type="radio" name="format" value="preprint">PrePrint format&nbsp;
  										<input type="radio" name="format" value="plainpaper">PlainPaper format&nbsp;
  										<input type="radio" name="format" value="noproposal">No proposal form format&nbsp;
  										</td>
									</tr>
										<tr>
									<td>Card print Type : </td>
										<td align="left">
										<input type="radio" name="printType" value="ECard" checked>ECard &nbsp;
  										<input type="radio" name="printType" value="Physical">&nbsp;Physical
  										
  										</td>
									</tr>
									</table>
									</td>
									</tr>
									
									
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<tr>
										<td align="center" colspan="2"><input type="submit" name="Submit" value="Generate Lot" class="button" /></td>
									</tr>
								</table>
							</form>
						</fieldset>

					</td>
				</tr>
			</table>
			</td>
	</tr>
</table>

<%@include file="footer.jsp"%>