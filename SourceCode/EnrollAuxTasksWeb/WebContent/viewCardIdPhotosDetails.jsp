<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="header.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script> 
<link href="<%=request.getContextPath()%>/js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/1024.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cycle_plugin.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">


<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui-timepicker-addon.css">
<script src="<%=request.getContextPath()%>/js/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}
</script>
<h3 align="center">DOWNLOAD POLICIES LIST</h3>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%" valign="top">
		   <c:if test="${fn:length(getPhotoPendingLotList) gt 0 }">
		   <table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
		  <!--  <tr align="center">
					<td colspan="9" ><font color="red"> ** The Data will be reset daily. so please make sure to download files before 8 pm.</font></td>
				</tr> -->
		   	   <tr>
				    <th>Lot Id</th>
				    <th>Module</th>
				  <!--   <th>XML</th> -->
				    <th>Lot Created</th>
                    <th>Cantact Email</th>
                    <th>UserName</th>
                    <th>Policy Id</th>
                    <th>Policy Holder Name</th>
                    <th>DownLoad</th>
				</tr>
		   <c:forEach items="${getPhotoPendingLotList}" var="getPhotoPendingLot" varStatus="rowCounter"> 
		   <tr>
				<td><c:out value="${getPhotoPendingLot.lotId}"/>   
				</td>
				<td>
					
					<c:if test="${getPhotoPendingLot.moduleId eq 1}">
						Corp
					</c:if>
					<c:if test="${getPhotoPendingLot.moduleId eq 2}">
						Bank Assurance
					</c:if>
					
					<c:if test="${getPhotoPendingLot.moduleId eq 4}">
						Individual
					</c:if>
					
				</td>
				<%-- <td>
					<c:out value="${getPhotoPendingLot.xmlString}"/>
				</td> --%>
		   	 	<td>
		   	 		<c:out value="${getPhotoPendingLot.lotCreated}"/>
		   	 	</td>
				<td>
					<c:out value="${getPhotoPendingLot.cantactEmail}"/>
				</td>
				<td>
					<c:out value="${getPhotoPendingLot.userName}"/>
				</td>
				<td>
					<c:out value="${getPhotoPendingLot.poicyId}"/>
				</td>
				<td>
					<c:out value="${getPhotoPendingLot.policyName}"/>
				</td>
				<!-- <td>&nbsp;#
					<input type="button" id="Download" name="Download" value="Download" />
				</td> -->
		   	 	<td>
                    <form name="brkupentry" method="post" action="./DownloadPhotoLotAction">
                     <c:if test="${getPhotoPendingLot.lotStatusFlag eq 3 }">
                     <input type="hidden" name="lotNumber" id="lotNumber" value="${getPhotoPendingLot.lotId}" />
                     <input type="hidden" name="moduleId" id="moduleId" value="${getPhotoPendingLot.moduleId}" />
                    <input type="submit" name="submit" value="DownLoad" />
                    </c:if>
                     <c:if test="${getPhotoPendingLot.lotStatusFlag eq 1 }">
                			#
                     </c:if>
                    </form>
                    </td> 
		   	</tr>
		   </c:forEach>
		   </table>
		   </c:if>
		   <c:if test="${fn:length(getPhotoPendingLotList) eq 0 }">
		   		<table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
			   	    <tr>
					<th>Index</th>
				    <th>Lot Id</th>
				    <th>Module</th>
				    <!-- <th>XML</th> -->
				    <th>Lot Created</th>
                    <th>Cantact Email</th>
                    <th>UserName</th>
                    <th>Policy Id</th>
                    <th>DownLoad</th>
					</tr>
			   	    <tr align="center">
			   			<td colspan="11" align="center">No data found</td>
			   		</tr>
		   		</table>
		   </c:if>
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>