<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="header.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script> 
<link href="<%=request.getContextPath()%>/js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/1024.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cycle_plugin.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">
<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui-timepicker-addon.css">
<script src="<%=request.getContextPath()%>/js/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}

	
    
    function ddlPolicychange(policyId)
	{
		if(policyId==0){
			document.getElementById("dataId").style.display = "none";
		}else{
			document.getElementById("dataId").style.display = "";
		}
	 }
    
    
    function validateForm()
    {
    	 var message = "VALIDATION MESSAGE";
   	 	 var letters = "^[a-zA-Z]{4}[a-zA-Z0-9-]{9}.";
    	 var policyddl = document.getElementById("ddlPolicy");
		 var policyddlId = policyddl.options[policyddl.selectedIndex].value;
		 if(policyddlId == 0)
		 {
			 CustomConfirmation('Please select valid Policy',message);
			 return false; 
		 }
		  var selfCardId1 = document.getElementById("selfCardId1").value;
		  if(selfCardId1 == ""){
	          CustomConfirmation('Please enter proper GHPL ID',message);  
	          document.getElementById("selfCardId1").focus();
	          return false;  
	      }
		  if(selfCardId1 !=null && selfCardId1 != "" && selfCardId1.length<14){
	          CustomConfirmation('Please enter proper GHPL ID',message);  
	          document.getElementById("selfCardId1").value="";
	          document.getElementById("selfCardId1").focus();
	          return false;  
	      }

	      if(selfCardId1 != null && selfCardId1 != "" && selfCardId1.length >13){
	          if(!selfCardId1.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId1").value="";
	              document.getElementById("selfCardId1").focus();
	              return false;  
	          }  
	      }
	      var selfCardId2 = document.getElementById("selfCardId2").value;
	      var selfCardId3 = document.getElementById("selfCardId3").value;
	      var selfCardId4 = document.getElementById("selfCardId4").value;
	      var selfCardId5 = document.getElementById("selfCardId5").value;
	      var selfCardId6 = document.getElementById("selfCardId6").value;
	      var selfCardId7 = document.getElementById("selfCardId7").value;
	      var selfCardId8 = document.getElementById("selfCardId8").value;
	      var selfCardId9 = document.getElementById("selfCardId9").value;
	      var selfCardId10 = document.getElementById("selfCardId10").value;
	      var selfCardId11 = document.getElementById("selfCardId11").value;
	      var selfCardId12 = document.getElementById("selfCardId12").value;
	      var selfCardId13 = document.getElementById("selfCardId13").value;
	      var selfCardId14 = document.getElementById("selfCardId14").value;
	      var selfCardId15 = document.getElementById("selfCardId15").value;
	      var selfCardId16 = document.getElementById("selfCardId16").value;
	      var selfCardId17 = document.getElementById("selfCardId17").value;
	      var selfCardId18 = document.getElementById("selfCardId18").value;
	      var selfCardId19 = document.getElementById("selfCardId19").value;
	      var selfCardId20 = document.getElementById("selfCardId20").value;
	      var selfCardId21 = document.getElementById("selfCardId21").value;
	      var selfCardId22 = document.getElementById("selfCardId22").value;
	      var selfCardId23 = document.getElementById("selfCardId23").value;
	      var selfCardId24 = document.getElementById("selfCardId24").value;
	      var selfCardId25 = document.getElementById("selfCardId25").value;
	      var selfCardId26 = document.getElementById("selfCardId26").value;
	      var selfCardId27 = document.getElementById("selfCardId27").value;
	      var selfCardId28 = document.getElementById("selfCardId28").value;
	      var selfCardId29 = document.getElementById("selfCardId29").value;
	      var selfCardId30 = document.getElementById("selfCardId30").value;
	      var selfCardId31 = document.getElementById("selfCardId31").value;
	      var selfCardId32 = document.getElementById("selfCardId32").value;
	      var selfCardId33 = document.getElementById("selfCardId33").value;
	      var selfCardId34 = document.getElementById("selfCardId34").value;
	      var selfCardId35 = document.getElementById("selfCardId35").value;
	      var selfCardId36 = document.getElementById("selfCardId36").value;
	      var selfCardId37 = document.getElementById("selfCardId37").value;
	      var selfCardId38 = document.getElementById("selfCardId38").value;
	      var selfCardId39 = document.getElementById("selfCardId39").value;
	      var selfCardId40 = document.getElementById("selfCardId40").value;
	      var selfCardId41 = document.getElementById("selfCardId41").value;
	      var selfCardId42 = document.getElementById("selfCardId42").value;
	      var selfCardId43 = document.getElementById("selfCardId43").value;
	      var selfCardId44 = document.getElementById("selfCardId44").value;
	      var selfCardId45 = document.getElementById("selfCardId45").value;
	      if(selfCardId2 != null && selfCardId2 != "" && selfCardId2.length <14){
	          if(!selfCardId2.match(letters)){
	        	  CustomConfirmation('Please enter proper GHPL ID 2',message);  
	              document.getElementById("selfCardId2").value="";
	              document.getElementById("selfCardId2").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId2 != null && selfCardId2 != "" && selfCardId2.length >13){
	          if(!selfCardId2.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID 2 must be alphabet characters only',message);  
	              document.getElementById("selfCardId2").value="";
	              document.getElementById("selfCardId2").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId3 != null && selfCardId3 != "" && selfCardId3.length <14){
	        	  CustomConfirmation('Please enter proper GHPL ID 3',message);  
	              document.getElementById("selfCardId3").value="";
	              document.getElementById("selfCardId3").focus();
	              return false;  
	      }
	      if(selfCardId3 != null && selfCardId3 != "" && selfCardId3.length >13){
	          if(!selfCardId3.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId3").value="";
	              document.getElementById("selfCardId3").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId4 != null && selfCardId4 != "" && selfCardId4.length <14){
	        	  CustomConfirmation('Please enter proper GHPL ID 4',message);    
	              document.getElementById("selfCardId4").value="";
	              document.getElementById("selfCardId4").focus();
	              return false;  
	      }
	      if(selfCardId4 != null && selfCardId4 != "" && selfCardId4.length >13){
	          if(!selfCardId4.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId4").value="";
	              document.getElementById("selfCardId4").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId5 != null && selfCardId5 != "" && selfCardId5.length >13){
	          if(!selfCardId5.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId5").value="";
	              document.getElementById("selfCardId5").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId6 != null && selfCardId6 != "" && selfCardId6.length >13){
	          if(!selfCardId6.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId6").value="";
	              document.getElementById("selfCardId6").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId7 != null && selfCardId7 != "" && selfCardId7.length >13){
	          if(!selfCardId7.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId7").value="";
	              document.getElementById("selfCardId7").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId8 != null && selfCardId8 != "" && selfCardId8.length >13){
	          if(!selfCardId8.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId8").value="";
	              document.getElementById("selfCardId8").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId9 != null && selfCardId9 != "" && selfCardId9.length >13){
	          if(!selfCardId9.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId9").value="";
	              document.getElementById("selfCardId9").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId10 != null && selfCardId10 != "" && selfCardId10.length >13){
	          if(!selfCardId10.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId10").value="";
	              document.getElementById("selfCardId10").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId11 != null && selfCardId11 != "" && selfCardId11.length >13){
	          if(!selfCardId11.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId11").value="";
	              document.getElementById("selfCardId11").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId12 != null && selfCardId12 != "" && selfCardId12.length >13){
	          if(!selfCardId12.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId12").value="";
	              document.getElementById("selfCardId12").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId13 != null && selfCardId13 != "" && selfCardId13.length >13){
	          if(!selfCardId13.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId13").value="";
	              document.getElementById("selfCardId13").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId14 != null && selfCardId14 != "" && selfCardId14.length >13){
	          if(!selfCardId14.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId14").value="";
	              document.getElementById("selfCardId14").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId15 != null && selfCardId15 != "" && selfCardId15.length >13){
	          if(!selfCardId15.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId15").value="";
	              document.getElementById("selfCardId15").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId16 != null && selfCardId16 != "" && selfCardId16.length >13){
	          if(!selfCardId16.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId16").value="";
	              document.getElementById("selfCardId16").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId17 != null && selfCardId17 != "" && selfCardId17.length >13){
	          if(!selfCardId17.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId17").value="";
	              document.getElementById("selfCardId17").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId18 != null && selfCardId18 != "" && selfCardId18.length >13){
	          if(!selfCardId18.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId18").value="";
	              document.getElementById("selfCardId18").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId19 != null && selfCardId19 != "" && selfCardId19.length >13){
	          if(!selfCardId19.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId19").value="";
	              document.getElementById("selfCardId19").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId20 != null && selfCardId20 != "" && selfCardId20.length >13){
	          if(!selfCardId20.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId20").value="";
	              document.getElementById("selfCardId20").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId21 != null && selfCardId21 != "" && selfCardId21.length >13){
	          if(!selfCardId21.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId21").value="";
	              document.getElementById("selfCardId21").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId22 != null && selfCardId22 != "" && selfCardId22.length >13){
	          if(!selfCardId22.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId22").value="";
	              document.getElementById("selfCardId22").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId23 != null && selfCardId23 != "" && selfCardId23.length >13){
	          if(!selfCardId23.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId23").value="";
	              document.getElementById("selfCardId23").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId24 != null && selfCardId24 != "" && selfCardId24.length >13){
	          if(!selfCardId24.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId24").value="";
	              document.getElementById("selfCardId24").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId25 != null && selfCardId25 != "" && selfCardId25.length >13){
	          if(!selfCardId25.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId25").value="";
	              document.getElementById("selfCardId25").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId26 != null && selfCardId26 != "" && selfCardId26.length >13){
	          if(!selfCardId26.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId26").value="";
	              document.getElementById("selfCardId26").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId27 != null && selfCardId27 != "" && selfCardId27.length >13){
	          if(!selfCardId27.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId27").value="";
	              document.getElementById("selfCardId27").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId28 != null && selfCardId28 != "" && selfCardId28.length >13){
	          if(!selfCardId28.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId28").value="";
	              document.getElementById("selfCardId28").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId29 != null && selfCardId29 != "" && selfCardId29.length >13){
	          if(!selfCardId29.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId29").value="";
	              document.getElementById("selfCardId29").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId30 != null && selfCardId30 != "" && selfCardId30.length >13){
	          if(!selfCardId30.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId30").value="";
	              document.getElementById("selfCardId30").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId31 != null && selfCardId31 != "" && selfCardId31.length >13){
	          if(!selfCardId31.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId31").value="";
	              document.getElementById("selfCardId31").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId32 != null && selfCardId32 != "" && selfCardId32.length >13){
	          if(!selfCardId32.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId32").value="";
	              document.getElementById("selfCardId32").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId33 != null && selfCardId33 != "" && selfCardId33.length >13){
	          if(!selfCardId33.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId33").value="";
	              document.getElementById("selfCardId33").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId34 != null && selfCardId34 != "" && selfCardId34.length >13){
	          if(!selfCardId34.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId34").value="";
	              document.getElementById("selfCardId34").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId35 != null && selfCardId35 != "" && selfCardId35.length >13){
	          if(!selfCardId35.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId35").value="";
	              document.getElementById("selfCardId35").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId36 != null && selfCardId36 != "" && selfCardId36.length >13){
	          if(!selfCardId36.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId36").value="";
	              document.getElementById("selfCardId36").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId37 != null && selfCardId37 != "" && selfCardId37.length >13){
	          if(!selfCardId37.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId37").value="";
	              document.getElementById("selfCardId37").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId38 != null && selfCardId38 != "" && selfCardId38.length >13){
	          if(!selfCardId38.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId38").value="";
	              document.getElementById("selfCardId38").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId39 != null && selfCardId39 != "" && selfCardId39.length >13){
	          if(!selfCardId39.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId39").value="";
	              document.getElementById("selfCardId39").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId40 != null && selfCardId40 != "" && selfCardId40.length >13){
	          if(!selfCardId40.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId40").value="";
	              document.getElementById("selfCardId40").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId41 != null && selfCardId41 != "" && selfCardId41.length >13){
	          if(!selfCardId41.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId41").value="";
	              document.getElementById("selfCardId41").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId42 != null && selfCardId42 != "" && selfCardId42.length >13){
	          if(!selfCardId42.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId42").value="";
	              document.getElementById("selfCardId42").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId43 != null && selfCardId43 != "" && selfCardId43.length >13){
	          if(!selfCardId43.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId43").value="";
	              document.getElementById("selfCardId43").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId44 != null && selfCardId44 != "" && selfCardId44.length >13){
	          if(!selfCardId44.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId44").value="";
	              document.getElementById("selfCardId44").focus();
	              return false;  
	          }  
	      }
	      if(selfCardId45 != null && selfCardId45 != "" && selfCardId45.length >13){
	          if(!selfCardId45.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("selfCardId45").value="";
	              document.getElementById("selfCardId45").focus();
	              return false;  
	          }  
	      }
	    
	      
    }
</script>


<h3 align="center">Banking Policies</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp"%>
		<td align="center" width="100%">
			<table width="80%" border="0" cellpadding="1" cellspacing="1" class="tablebg">
				<tr>
					<td>
						<fieldset>
							<legend>Search Criteria</legend>
							<form name="policyForm" method="post" action="./InsertBankPolicyAction" onSubmit="return validateForm();">
								<table width="100%" border="20" cellpadding="0" cellspacing="0" class="tablebg">
								<input type="hidden" name="userId" id="userId" value="<%=session.getAttribute("userId")%>">
								<input type="hidden" name="policyForm" id="" value="SelfCardData" />
								
									<tr>
										<td align="right" colspan="2">Select Policy:
											<select name="ddlPolicy" id="ddlPolicy" style="width:300px;"  onchange="ddlPolicychange(this.value)">
											     <option selected="selected" value="0">- Select -</option>
											      <c:if test="${fn:length(policyDDLListObj) gt 0 }">
											     	<c:forEach items="${policyDDLListObj}" var="option"> 
	   													<option value="${option.policyId}">${option.policyHolderName}(${option.policyFrom} - ${option.policyTo})</option>
													</c:forEach>
												</c:if> 
											 </select>&nbsp;&nbsp;&nbsp; (<font color="red"> Note : Only self cards are eligible</font>)
										</td>
										<td> <a href="./xlstopolicy.jsp" >Import XLS</a> </td>
									</tr>
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									
									<tr id="dataId" style="display:none;">
									<td  width="100%"> <table>
									    <tr>
											<td align="left" width="33%">
											Select CardId1:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId1" id="selfCardId1"/></td>
											<td align="left" width="33%">
											Select CardId2:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId2" id="selfCardId2"/></td>
											<td align="left" width="33%">
											Select CardId3:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId3" id="selfCardId3"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId4:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId4" id="selfCardId4"/></td>
											<td align="left" width="33%">
											Select CardId5:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId5" id="selfCardId5"/></td>
											<td align="left" width="33%">
											Select CardId6:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId6" id="selfCardId6"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId7:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId7" id="selfCardId7"/></td>
											<td align="left" width="33%">
											Select CardId8:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId8" id="selfCardId8"/></td>
											<td align="left" width="33%">
											Select CardId9:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId9" id="selfCardId9"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId10:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId10" id="selfCardId10"/></td>
											<td align="left" width="33%">
											Select CardId11:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId11" id="selfCardId11"/></td>
											<td align="left" width="33%">
											Select CardId12:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId12" id="selfCardId12"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId13:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId13" id="selfCardId13"/></td>
											<td align="left" width="33%">
											Select CardId14:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId14" id="selfCardId14"/></td>
											<td align="left" width="33%">
											Select CardId15:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId15" id="selfCardId15"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId16:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId16" id="selfCardId16"/></td>
											<td align="left" width="33%">
											Select CardId17:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId17" id="selfCardId17"/></td>
											<td align="left" width="33%">
											Select CardId18:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId18" id="selfCardId18"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId19:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId19" id="selfCardId19"/></td>
											<td align="left" width="33%">
											Select CardId20:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId20" id="selfCardId20"/></td>
											<td align="left" width="33%">
											Select CardId21:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId21" id="selfCardId21"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId22:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId22" id="selfCardId22"/></td>
											<td align="left" width="33%">
											Select CardId23:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId23" id="selfCardId23"/></td>
											<td align="left" width="33%">
											Select CardId24:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId24" id="selfCardId24"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId25:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId25" id="selfCardId25"/></td>
											<td align="left" width="33%">
											Select CardId26:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId26" id="selfCardId26"/></td>
											<td align="left" width="33%">
											Select CardId27:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId27" id="selfCardId27"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId28:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId28" id="selfCardId28"/></td>
											<td align="left" width="33%">
											Select CardId29:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId29" id="selfCardId29"/></td>
											<td align="left" width="33%">
											Select CardId30:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId30" id="selfCardId30"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId31:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId31" id="selfCardId31"/></td>
											<td align="left" width="33%">
											Select CardId32:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId32" id="selfCardId32"/></td>
											<td align="left" width="33%">
											Select CardId33:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId33" id="selfCardId33"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId34:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId34" id="selfCardId34"/></td>
											<td align="left" width="33%">
											Select CardId35:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId35" id="selfCardId35"/></td>
											<td align="left" width="33%">
											Select CardId36:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId36" id="selfCardId36"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId37:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId37" id="selfCardId37"/></td>
											<td align="left" width="33%">
											Select CardId38:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId38" id="selfCardId38"/></td>
											<td align="left" width="33%">
											Select CardId39:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId39" id="selfCardId39"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId40:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId40" id="selfCardId40"/></td>
											<td align="left" width="33%">
											Select CardId41:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId41" id="selfCardId41"/></td>
											<td align="left" width="33%">
											Select CardId42:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId42" id="selfCardId42"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId43:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId43" id="selfCardId43"/></td>
											<td align="left" width="33%">
											Select CardId44:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId44" id="selfCardId44"/></td>
											<td align="left" width="33%">
											Select CardId45:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId45" id="selfCardId45"/></td>
										</tr>
										<!-- <tr>
											<td align="left" width="33%">
											Select CardId46:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId46" id="selfCardId46"/></td>
											<td align="left" width="33%">
											Select CardId47:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId47" id="selfCardId47"/></td>
											<td align="left" width="33%">
											Select CardId48:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId48" id="selfCardId48"/></td>
										</tr>
										<tr>
											<td align="left" width="33%">
											Select CardId49:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId49" id="selfCardId49"/></td>
											<td align="left" width="33%">
											Select CardId50:&nbsp;&nbsp;&nbsp;&nbsp;
											<input type="text" size="30" name="selfCardId50" id="selfCardId50"/></td>
										</tr> -->
										<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<tr>
										<td colspan="3">
										<input type="radio" name="format" value="vendor" checked>Vendor Format &nbsp;
  										<input type="radio" name="format" value="preprint">PrePrint format&nbsp;
  										<input type="radio" name="format" value="plainpaper">PlainPaper format&nbsp;
  										<input type="radio" name="format" value="noproposal">No proposal form format&nbsp;
  										</td>
									</tr>
									<tr>
									<td>Card print Type : </td>
										<td align="left">
										<input type="radio" name="printType" value="ECard" checked>ECard &nbsp;
  										<input type="radio" name="printType" value="Physical">&nbsp;Phycial
  										
  										</td>
									</tr>
									</table>
									</td>
									</tr>
									
									
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<tr>
										<td align="center" colspan="2"><input type="submit" name="Submit" value="Generate Lot" class="button" /></td>
									</tr>
								</table>
							</form>
						</fieldset>

					</td>
				</tr>
			</table>
			</td>
	</tr>
</table>

<%@include file="footer.jsp"%>