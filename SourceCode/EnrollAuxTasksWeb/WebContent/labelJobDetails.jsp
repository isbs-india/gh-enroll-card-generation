<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="header.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script> 
<link href="<%=request.getContextPath()%>/js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/1024.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cycle_plugin.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">


<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui-timepicker-addon.css">
<script src="<%=request.getContextPath()%>/js/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}
	
	
	
	 function validateForm()
	    {
	    	 var message = "VALIDATION MESSAGE";
	    	 var moduleId = document.getElementById("moduleId");
	    	 var moduleIdId = moduleId.options[moduleId.selectedIndex].value;
	    	 if(moduleIdId == 0)
	    	 {
	    		 CustomConfirmation('Please select valid Module Id',message);
	    		 return false; 
	    	 }
	    	 
			
	    }
</script>
<h3 align="center">JOB CARD DETAILS</h3>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%" valign="top">
		<table width="80%" border="0" cellpadding="1" cellspacing="1" class="tablebg">
				<tr>
					<td>
						<fieldset>
							<legend>Address Print</legend>
							
								<form name="policyForm" method="post" action="./ViewPrintAddressLableAction"  onSubmit="return validateForm();">
								
								<table width="70%" border="20" cellpadding="0" cellspacing="0" class="tablebg">
								<input type="hidden" name="userId" id="userId" value="<%=session.getAttribute("userId")%>">
								<input type="hidden" name="XLpolicyForm" id="" value="XLSelfCardData" />
							
									<tr>
										<td align="left">Module Id <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 <select name="moduleId" id="moduleId" >
											<option id="0" value="0">Select</option>
											<option id="1" value="1">Corp</option> 
											<option id="2" value="2">Bank Assurance</option>
											<option id="4" value="4">Individual</option>
										</select> 
										
										</td><td>&nbsp; </td>
									</tr>
									
								
									
									
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<!--  <tr>
										<td align="right" colspan="2"><a href="./DownLoadTemplateAction?fileName=WINDLABELTemplate.xlsx"><h4>Sample Upload Template </h4></h4></a> </td>
									</tr>  -->
									<tr>
										<td align="center" colspan="2"><input type="submit" name="Submit" value="Generate Lot" class="button" /></td>
									</tr>
								</table>
							</form>
						</fieldset>

					</td>
				</tr>
			</table>
		
		<br/>
   <br/><br/>
		   <c:if test="${fn:length(lotBeanListObj) gt 0 }">
		   <table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
		   <tr align="center">
					<td colspan="9" ><font color="red"> ** The Data will be reset daily. so please make sure to download files before 8 pm.</font></td>
				</tr>
		   	   <tr>
					<th>Index</th>
				    <th>Lot No.</th>
				    <th>Module</th>
				   <!--  <th>Policy Holder Name</th> -->
				   <!--  <th>Endrosement No.</th> -->
                    <th>Status Desc.</th>
                    <th>Created on</th>
                    <th>Current User</th>
                    <th>DownLoad</th>
				</tr>
		   <c:forEach items="${lotBeanListObj}" var="labelJob" varStatus="rowCounter"> 
		   <tr>
				<td><c:out value="${labelJob.serialNo}"></c:out></td>
				<td><c:out value="${labelJob.lotNumber}"/>   
				</td>
				<td>
					<c:out value="${labelJob.moduleName}"/>
				</td>
				<%-- <td>
					<c:out value="${labelJob.policyHolderName}"/>
				</td> --%>
		   	 	<%-- <td>
		   	 		<c:out value="${labelJob.endorsementNo}"/>
		   	 	</td> --%>
				<td>
					<c:out value="${labelJob.statusDesc}"/>
				</td>
				<td>
					<c:out value="${labelJob.createdOn}"/>
				</td>
				<td>
					<c:out value="${labelJob.userName}"/>
				</td>
		   	  	<td>
                    <form name="brkupentry" method="post" action="./DownloadLabelAction">
                  
                     <c:if test="${labelJob.lotStatus ne 1 }">
                     <input type="hidden" name="lotNumber" id="lotNumber" value="${labelJob.lotNumber}" />
                     <input type="hidden" name="moduleId" id="moduleId" value="${labelJob.moduleId}" />
                     <input type="hidden" name="formatId" id="formatId" value="${labelJob.formatId}" />
                     
                     
                    <input type="submit" name="submit" value="DownLoad" />
                    </c:if>
                     <c:if test="${labelJob.lotStatus eq 1 }">
                     #
                     </c:if>
                    </form>
                    </td>
		   	</tr>
		   </c:forEach>
		   </table>
		   </c:if>
		   <c:if test="${fn:length(lotBeanListObj) eq 0 }">
		   		<table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
			   	    <tr>
						<th>Index</th>
					    <th>Lot No.</th>
					    <th>Module</th>
					  <!--   <th>Policy Holder Name</th> -->
					    <!-- <th>Endrosement No.</th> -->
	                    <th>Status Desc.</th>
	                    <th>Created on</th>
	                    <th>Current User</th>
	                    <th>DownLoad</th>
					</tr>
			   	    <tr align="center">
			   			<td colspan="11" align="center">No data found</td>
			   		</tr>
		   		</table>
		   </c:if>
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>