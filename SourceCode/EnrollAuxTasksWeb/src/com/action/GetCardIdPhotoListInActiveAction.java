package com.action;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.GetPhotoPendingLot;
import com.model.PolicyDataBean;
import com.persistence.common.PhotoUploadDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class GetCardIdPhotoListInActiveAction
 */
@WebServlet("/GetCardIdPhotoListInActiveAction")
public class GetCardIdPhotoListInActiveAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetCardIdPhotoListInActiveAction");  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetCardIdPhotoListInActiveAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session = request.getSession(false); 
			if(session != null && !session.isNew()) {
				RequestDispatcher rd =null;
					List<GetPhotoPendingLot> getPhotoPendingLotList = null;
					getPhotoPendingLotList = new ArrayList<GetPhotoPendingLot>();
					PolicyDataBean policyDataBean= null;
					policyDataBean= PhotoUploadDAOManager.getCardIdPhotoLot(1);
					if(policyDataBean!=null){
						if(policyDataBean.getStatusBean().getStatusFlag()==1 || policyDataBean.getStatusBean().getStatusFlag()==0){
							request.setAttribute("getPhotoPendingLotList",policyDataBean.getPhotoPendingLotList()); 
							rd = request.getRequestDispatcher("/viewCardIdPhotosDetails.jsp");
							rd.forward(request,response);
						}else if(policyDataBean.getStatusBean().getStatusFlag()==2){
							 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
							 rd = request.getRequestDispatcher("/error.jsp");
							 rd.forward(request,response);
						}
					}else{
						request.setAttribute("getPhotoPendingLotList",getPhotoPendingLotList); 
						rd = request.getRequestDispatcher("/viewCardIdPhotosDetails.jsp");
						rd.forward(request,response);
					}

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
