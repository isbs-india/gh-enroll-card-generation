package com.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.PolicyDataBean;
import com.persistence.common.PhotoUploadDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class GetUploadPoliciesListAction
 */
@WebServlet("/GetUploadPoliciesListAction")
public class GetUploadPoliciesListAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetUploadPoliciesListAction");  
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetUploadPoliciesListAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session = request.getSession(false); 
			if(session != null && !session.isNew()) {
				int moduleUserId= 0;
				//int userId=0;
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =loginBean.getModuleUserId(1);
					if(moduleUserId==0){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				PolicyDataBean policyDataBean = null;
				//List<InsertLotDetailsBean> lotBeanListObj = null;
				//lotBeanListObj=new ArrayList<InsertLotDetailsBean>();
				//int isCron=0;
		
				//List<GetPhotoPendingLot> getPhotoPendingLotList = null;
				policyDataBean = PhotoUploadDAOManager.getPhotoPendingLot(1);
				
				RequestDispatcher rd =null;
				if(policyDataBean!=null){
					if(policyDataBean.getStatusBean().getStatusFlag()==1 || policyDataBean.getStatusBean().getStatusFlag()==0){
						request.setAttribute("getPhotoPendingLotList",policyDataBean.getPhotoPendingLotList()); 
						rd = request.getRequestDispatcher("/viewUploadPhotosDetails.jsp");
						rd.forward(request,response);
					}else if(policyDataBean.getStatusBean().getStatusFlag()==2){
						 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
						 rd = request.getRequestDispatcher("/error.jsp");
						 rd.forward(request,response);
					}
				}else{
					 request.setAttribute("message", "No data found. Please try again");
					 rd=request.getRequestDispatcher("./status.jsp");
					 rd.forward(request,response);
				}
				

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
