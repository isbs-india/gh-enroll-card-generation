package com.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.util.Constants;

/**
 * Servlet implementation class DownLoadTemplateAction
 */
@WebServlet("/DownLoadTemplateAction")
public class DownLoadTemplateAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("DownLoadTemplateAction"); 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownLoadTemplateAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FileInputStream inputStream=null;
		try{
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				//int userId=0;
				String moduleUserId= "";
				String inputFilepath="";
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
				//	int userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(2);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				String fileName =null;
				if((request.getParameter("fileName")!=null )  && (!request.getParameter("fileName").equals(""))){
					fileName =request.getParameter("fileName");
					response.setContentType("application/xlsx");
					inputFilepath = getServletContext().getRealPath("Template") + File.separator +fileName;
					response.setHeader("Content-Disposition", "attachment; filename="+fileName);
					 inputStream = new FileInputStream(inputFilepath);
						ServletOutputStream output = response.getOutputStream();
						 byte[] bytesArray = new byte[4096];
					        int bytesRead = -1;
					        while ((bytesRead = inputStream.read(bytesArray)) != -1) {
					        	output.write(bytesArray, 0, bytesRead);
					        }
			        	  output.flush();
			        	  output.close();
			        	  
				}

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}finally{
			if(inputStream!=null){
				 inputStream=null;
			}
		}
	}

}
