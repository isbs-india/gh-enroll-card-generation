package com.action;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.PolicyDataBean;
import com.model.SinglePhotoBean;
import com.persistence.common.PhotoUploadDAOManager;

/**
 * Servlet implementation class UploadPhotoAction
 */
@WebServlet("/UploadPhotoAction")
public class UploadPhotoAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("UploadPhotoAction");
	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadPhotoAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("enter in dopost methos");
		System.out.println("enter in dopost methos");
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				int moduleId =0;
				if(request.getParameter("moduleId1")!=null){
					moduleId=Integer.parseInt(request.getParameter("moduleId1"));
					//session.setAttribute("moduleId", 0);
				}
				int moduleUserId= 0;
				//int userId=0;
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =loginBean.getModuleUserId(moduleId);
					if(moduleUserId==0){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				String cardid = null;
				
				if(request.getParameter("cardid")!=null){
					cardid = request.getParameter("cardid").trim().toUpperCase();
				}
				RequestDispatcher rd = null;
				List<SinglePhotoBean> empList= null;
				PolicyDataBean policyDataBean = null;
				logger.info("beofre collaing getEmployeeDetailsCardId  ");
				System.out.println("beofre collaing getEmployeeDetailsCardId  ");
				policyDataBean = PhotoUploadDAOManager.getEmployeeDetailsCardId(cardid, moduleId);
				if(policyDataBean!=null){
					System.out.println("policyDataBean.getStatusBean().getStatusFlag()    --->  "+policyDataBean.getStatusBean().getStatusFlag());
					logger.info("policyDataBean.getStatusBean().getStatusFlag()    --->  "+policyDataBean.getStatusBean().getStatusFlag());
					if(policyDataBean.getStatusBean().getStatusFlag()==1 || policyDataBean.getStatusBean().getStatusFlag()==0){
						request.setAttribute("empList", policyDataBean.getSinglephotoList());
						 rd = request.getRequestDispatcher("./uploadPhotos.jsp");
						rd.forward(request,response);
					}else if(policyDataBean.getStatusBean().getStatusFlag()==0){
						 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
						 rd = request.getRequestDispatcher("/error.jsp");
						 rd.forward(request,response);
					}
				}else{
					request.setAttribute("empList", empList);
					 rd = request.getRequestDispatcher("./uploadPhotos.jsp");
					rd.forward(request,response);
				}
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
		    		session.invalidate();
		    	}
				 response.sendRedirect("./login.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e);
				getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
				//sendMail.SendMailToUsers(String subject, String messageText,String displayfilenme,String TO,String cc,String bcc)
				SendMail sendMail = new SendMail();
				//SendMail_preAuth mailsend=new SendMail_preAuth();
				try {
					sendMail.SendMailToUsers("ENROLL_AUXILLARY_TASKS_EXCEPTION_UploadPhotoAction",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
			}
	}

}
