package com.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.model.LoginBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class InsertXLSXBankPolicyInActiveAction
 */
@WebServlet("/InsertXLSXBankPolicyInActiveAction")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)   // 50MB
public class InsertXLSXBankPolicyInActiveAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("InsertXLSXBankPolicyInActiveAction");   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertXLSXBankPolicyInActiveAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			HttpSession session = request.getSession(true); 
			RequestDispatcher rd =null;
			if(session != null && !session.isNew()) {
				List<String> list =new ArrayList<String>();
				String policyId="0";
				int userId=0;
				String fileFormat="";
				String policyForm = "";
				String userName="";
				
				String moduleUserId= "";
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(2);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				 int formatId=0;
				String param = null;
				if(request.getParameter("printType")!=null && request.getParameter("printType").equals("ECard")){
					param ="E";
					formatId=1;
				}
				if(request.getParameter("printType")!=null && request.getParameter("printType").equals("Physical")){
					param ="P";
					formatId=2;
				}
				if(request.getParameter("printType")!=null && request.getParameter("printType").equals("Vendor")){
					param ="V";
					formatId=3;
				}
				  
				 if(request.getParameter("XLpolicyForm")!=null && !request.getParameter("XLpolicyForm").equals("") && request.getParameter("XLpolicyForm").equals("XLSelfCardData")){
						response.setContentType("text/html; charset=UTF-8");
					 try {
						 /*if((request.getParameter("userId")!=null) && (!request.getParameter("userId").equals("")))
							{	
								userId=Integer.parseInt(request.getParameter("userId"));	
							}	*/
							if((request.getParameter("ddlPolicy")!=null) && (!request.getParameter("ddlPolicy").equals("")))
							{	
								policyId=request.getParameter("ddlPolicy");	
							}
							
							/*if((request.getParameter("format")!=null) && (!request.getParameter("format").equals("")))
							{	
								fileFormat=request.getParameter("format");	
							}*/
							String extension ="";
							int BUFFER_LENGTH = 4096;
							File uploadPath = (File)request.getServletContext().getAttribute("javax.servlet.context.tempdir");  
							 Part filePart = request.getPart("xlfile");
							 if (filePart != null && filePart.getSize() > 0) {
								 String fileName = getFileName(filePart);
								 int i = fileName.lastIndexOf('.');
								 if (i > 0) {
								     extension = fileName.substring(i+1);
								     extension.toLowerCase();
								     if(!extension.equals("xlsx")){
								     request.setAttribute("message", "Uploaded file format is not in xlsx");
										 rd=request.getRequestDispatcher("./status.jsp");
										 rd.forward(request,response);
								     
								     }else{
								 
								 String filePath = uploadPath + File.separator + fileName;
								        InputStream is = request.getPart(filePart.getName()).getInputStream();
								        FileOutputStream os = new FileOutputStream(filePath);
								        byte[] bytes = new byte[BUFFER_LENGTH];
								        int read = 0;
								        while ((read = is.read(bytes, 0, BUFFER_LENGTH)) != -1) {
								            os.write(bytes, 0, read);
								        }
								        os.flush();
								        is.close();
								        os.close();
								     
								      //  InputStream ExcelFileToRead = new FileInputStream(filePath);
								        InputStream excelFileToRead = new FileInputStream(filePath);
										XSSFWorkbook  wb = new XSSFWorkbook(excelFileToRead);
										XSSFSheet sheet = wb.getSheetAt(0);
										XSSFRow row; 
										XSSFCell cell;
										int rowCount=0;
										Iterator rows = sheet.rowIterator();
										while (rows.hasNext())
										{
											rowCount++;
											row=(XSSFRow) rows.next();
											Iterator cells = row.cellIterator();
											while (cells.hasNext())
											{
												cell=(XSSFCell) cells.next();
												if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
												{
													System.out.print(cell.getStringCellValue()+" ");
													if(rowCount!=1){
														list.add(cell.getStringCellValue());
													}
												}
											}
										}
							String FinalMessage="";
							logger.info("row count : "+rowCount);
							logger.info("list size  : "+list.size());
						   if(rowCount<900){
							   if(list.size()>0){
									String xml = sendFaxDataResonse(list, policyId, moduleUserId, formatId);
								
								logger.info("xml  : "+xml);
								logger.info("param  : "+param);
								int errorFlag =EnrollAuxDAOManager.insertSelfCardDetailsInActive(xml,param);
								logger.info("errorFlag   "+errorFlag);
								if(errorFlag == 1)
								{
								    FinalMessage="Data inserted successfully<br/><br/>";
								}
								else
								{
									FinalMessage="Data insertion failed<br/><br/>";	
								}
								String FinalMessage1="Ecard generation request status";
								request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
								request.setAttribute("message","<center>"+FinalMessage+"</center>");
								rd = request.getRequestDispatcher("./status.jsp");
								rd.forward(request,response);
							   }else{
								   FinalMessage="Uploaded excel not proper format <br/><br/>";	
								   String FinalMessage1="Uploaded excel not proper format";
									request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
									request.setAttribute("message","<center>"+FinalMessage+"</center>");
									rd = request.getRequestDispatcher("./status.jsp");
									rd.forward(request,response);
								   
							   }
						   }else{
							   FinalMessage="excel Row count should be less than 900 records <br/><br/>";	
							   String FinalMessage1="Ecard generation request status";
								request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
								request.setAttribute("message","<center>"+FinalMessage+"</center>");
								rd = request.getRequestDispatcher("./status.jsp");
								rd.forward(request,response);
						   }
							 }
								 }// file exten end
								 }// file part end
						} catch (Exception e) {
						    e.printStackTrace();
							request.setAttribute("message","<center>"+e.getMessage()+"</center>");
							rd = request.getRequestDispatcher("./status.jsp");
							rd.forward(request,response);
						} 
				}
				
				

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	
	}
	
	
	public String sendFaxDataResonse(List<String> selfCardList,String policyId,String userId,int fileFormat) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		// root elements
		Document doc = null;
		Element breakupElement = null;
		StringWriter resultAsString = new StringWriter();
        try{
        	docFactory = DocumentBuilderFactory.newInstance();
        	docBuilder = docFactory.newDocumentBuilder();
        	doc = docBuilder.newDocument();
        	breakupElement = doc.createElement("NODE");
        	doc.appendChild(breakupElement);
        	Element jobElement = doc.createElement("JOBS");
        	breakupElement.appendChild(jobElement);
        	Element PolicyIdElement = doc.createElement("POLICYID");
        	PolicyIdElement.appendChild(doc.createTextNode(policyId));
        	jobElement.appendChild(PolicyIdElement);
        	Element userIdElement = doc.createElement("USERID");
        	userIdElement.appendChild(doc.createTextNode(userId));
        	jobElement.appendChild(userIdElement);
        	Element fileFormatElement = doc.createElement("FORMATID");
        	fileFormatElement.appendChild(doc.createTextNode(fileFormat+""));
        	jobElement.appendChild(fileFormatElement);
        	if(selfCardList!=null && selfCardList.size()>0){
        		Element cardIdsElement = doc.createElement("GHCARDIDS");
        		jobElement.appendChild(cardIdsElement);
			    for(int i=0;i<selfCardList.size();i++){
		        	Element selfcardIdElement = doc.createElement("CARDID");
		        	selfcardIdElement.appendChild(doc.createTextNode(String.valueOf(selfCardList.get(i))));
		        	cardIdsElement.appendChild(selfcardIdElement);
				}
        	}
        	
        	/*Element CARD_FORMATElement = doc.createElement("CARD_FORMAT");
        	CARD_FORMATElement.appendChild(doc.createTextNode(param));
        	jobElement.appendChild(CARD_FORMATElement);*/
        	
        	TransformerFactory transformerFactory = TransformerFactory.newInstance();
    		Transformer transformer = transformerFactory.newTransformer();
    		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    		DOMSource source = new DOMSource(doc);
    		StreamResult result = new StreamResult(resultAsString);
    		// StreamResult streamResult = new StreamResult(new File(FaxData.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/ghplfax/faxresponse.xml")); 
    		transformer.transform(source, result);
            }catch(Exception e){
            	 logger.error(e);
            }finally{
            	 docFactory = null;
        		 docBuilder = null;
        		 doc = null;
        		 breakupElement = null;
            }
            return resultAsString.toString();
	}
	
	
	private String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
          if (cd.trim().startsWith("filename")) {
            return cd.substring(cd.indexOf('=') + 1).trim()
                    .replace("\"", "");
          }
        }
        return null;
      }

}
