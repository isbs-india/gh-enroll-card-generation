package com.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.PolicyDataBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class ViewPrintAddressLableAction
 */
@WebServlet("/ViewPrintAddressLableAction")
public class ViewPrintAddressLableAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("ViewPrintAddressLableAction");        
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ViewPrintAddressLableAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				//int userId=0;
				String moduleUserId= "";
				int moduleId=0;
				if((request.getParameter("moduleId")!=null) && (!request.getParameter("moduleId").equals("")))
				{	
					moduleId=Integer.parseInt(request.getParameter("moduleId"));	
				}
				
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(moduleId);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				
				
				PolicyDataBean policyDataBean=null;
				//List<InsertLotDetailsBean> lotBeanListObj = null;
				//lotBeanListObj=new ArrayList<InsertLotDetailsBean>();
				int isCron=0;
				policyDataBean=EnrollAuxDAOManager.getLabelLotDetails(isCron,moduleId);
				RequestDispatcher rd =null;
				if(policyDataBean.getStatusBean().getStatusFlag()==1 || policyDataBean.getStatusBean().getStatusFlag()==0){
					request.setAttribute("lotBeanListObj",policyDataBean.getLotListObj()); 
					rd = request.getRequestDispatcher("/labelJobDetails.jsp");
					rd.forward(request,response);
				}else if(policyDataBean.getStatusBean().getStatusFlag()==2){
					 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
					 rd = request.getRequestDispatcher("/error.jsp");
					 rd.forward(request,response);
				}

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
