package com.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.model.LoginBean;
import com.model.PolicyDataBean;
import com.model.PolicyListDataBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;
import com.util.Util;

/**
 * Servlet implementation class InsertPolicyLotDetailsAction
 */
@WebServlet("/InsertPolicyLotDetailsAction")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2KB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)   // 50MB
public class InsertPolicyLotDetailsAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("InsertPolicyLotDetailsAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertPolicyLotDetailsAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			HttpSession session = request.getSession(true); 
			RequestDispatcher rd =null;
			if(session != null && !session.isNew()) {
				List<String> list =new ArrayList<String>();
				String policyId="0";
				String endorsmentCode="0";
				
				//String userId="0";
				String fileFormat="";
				String policyForm = "";
				
				int userId=0;
				String moduleUserId= "";
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(1);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						request.setAttribute("userName", loginBean.getLoginUserName());
						request.setAttribute("moduleId", 1);
						request.setAttribute("moduleName", Util.getModuleName(1));
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}	
				
				// if(request.getParameter("XLpolicyForm")!=null && !request.getParameter("XLpolicyForm").equals("") && request.getParameter("XLpolicyForm").equals("XLPolicyData")){
						response.setContentType("text/html; charset=UTF-8");
					 try {
							//  get the user entered data to servlet using request scope
							int BUFFER_LENGTH = 4096;
							File uploadPath = (File)request.getServletContext().getAttribute("javax.servlet.context.tempdir"); 
							 
							//  get the file data to servlet using request scope
							 Part filePart = request.getPart("xlfile");
							 logger.info("get the file : "+filePart);
							 String extension ="";
							 if (filePart != null && filePart.getSize() > 0) {
								 String fileName = getFileName(filePart);
								 int i = fileName.lastIndexOf('.');
								 if (i > 0) {
								     extension = fileName.substring(i+1);
								     extension.toLowerCase();
								     if(!extension.equals("xlsx")){
								     request.setAttribute("message", "Please check Uploaded file format is not in xlsx");
										 rd=request.getRequestDispatcher("./status.jsp");
										 rd.forward(request,response);
								     }else{
								 
								 String filePath = uploadPath + File.separator + fileName;
								        InputStream is = request.getPart(filePart.getName()).getInputStream();
								        FileOutputStream os = new FileOutputStream(filePath);
								        byte[] bytes = new byte[BUFFER_LENGTH];
								        int read = 0;
								        while ((read = is.read(bytes, 0, BUFFER_LENGTH)) != -1) {
								            os.write(bytes, 0, read);
								        }
								        os.flush();
								        is.close();
								        os.close();
								     
								      // reading data from xls file
								        logger.info("reading data from xls file ");
								        InputStream excelFileToRead = new FileInputStream(filePath);
										XSSFWorkbook  wb = new XSSFWorkbook(excelFileToRead);
										XSSFSheet sheet = wb.getSheetAt(0);
										XSSFRow row; 
										XSSFCell cell;
										int rowCount=0;
										Iterator rows = sheet.rowIterator();
										while (rows.hasNext())
										{
											rowCount++;
											row=(XSSFRow) rows.next();
											Iterator cells = row.cellIterator();
											while (cells.hasNext())
											{
												cell=(XSSFCell) cells.next();
												
												//System.out.print(cell.toString() +"  ");
												if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
												{
													System.out.print(cell.getStringCellValue()+" ");													
													if(rowCount!=1){														
														list.add(cell.getStringCellValue()+"");														
													}
												}else if (cell.getCellType() == XSSFCell.CELL_TYPE_NUMERIC){
													System.out.print(cell.getNumericCellValue()+" ");
													if(rowCount!=1){
														list.add((long)cell.getNumericCellValue()+"");
													}
												}
											}
										}
										Iterator<String> iterator = list.iterator();
										while (iterator.hasNext()) {
											System.out.println(iterator.next());
										}
										
										//int x=Util.getSpecialCharacterCount();
										
							String FinalMessage="";
							logger.info("rowCount   "+rowCount);
							logger.info("list size  : "+list.size());
						   if(rowCount<900){
							   if(list.size()>0){
								String xml = sendFaxDataResonse(list, moduleUserId);
								logger.info("Generated xml string is : "+xml);
								//inserting xml,policyId,endorsement,moduleUserId,cardType to GHPL database 
								logger.info("inserting xml,policyId,endorsement,moduleUserId,cardType to GHPL database ");
								logger.info(" endorsmentCode   "+endorsmentCode);
								logger.info("policyId  "+policyId);
								logger.info("module userId "+moduleUserId);
								PolicyListDataBean policyListDataBean = null;
								
								
								policyListDataBean =EnrollAuxDAOManager.insertPolicyNumberDetails(xml ,moduleUserId, 4);
								
								/* policyDataBean =EnrollAuxDAOManager.insertGHPLSelfCardDetails(xml,endorsmentCode,policyId, moduleUserId);*/
								
								//data inserting GHPL Database successfully the error flag is 1 
								/*if(errorFlag == 1)
								{
								    FinalMessage="Data inserted successfully<br/><br/>";
								}
								else
								{
									FinalMessage="Data insertion failed<br/><br/>";	
								}*/
								if(policyListDataBean!=null){
									if(policyListDataBean.getStatusBean().getStatusFlag()==1 || policyListDataBean.getStatusBean().getStatusFlag()==0){
										logger.info("if the the error flag is 1 data inserting GHPL Database successfully ");
										logger.info("errorFlag  is : "+policyListDataBean.getStatusBean().getStatusMessage());
										String FinalMessage1="Ecard generation request status";
										request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
										request.setAttribute("message","<center>"+policyListDataBean.getStatusBean().getStatusMessage()+"</center>");
										rd = request.getRequestDispatcher("./status.jsp");
										rd.forward(request,response);
									}if(policyListDataBean.getStatusBean().getStatusFlag()==2){
										request.setAttribute("message", policyListDataBean.getStatusBean().getStatusMessage()); 
										 rd = request.getRequestDispatcher("/error.jsp");
										 rd.forward(request,response);
									}
								}
								
							   }else{
								   FinalMessage="Uploaded excel not proper format <br/><br/>";	
								   String FinalMessage1="Uploaded excel not proper format";
									request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
									request.setAttribute("message","<center>"+FinalMessage+"</center>");
									rd = request.getRequestDispatcher("./status.jsp");
									rd.forward(request,response);
								   
							   }
						   }else{
							   logger.info("excel Row count should be less than 900 records  : "+rowCount);
							    FinalMessage="excel Row count should be less than 900 records <br/><br/>";	
							    String FinalMessage1="Ecard generation request status";
								request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
								request.setAttribute("message","<center>"+FinalMessage+"</center>");
								rd = request.getRequestDispatcher("./status.jsp");
								rd.forward(request,response);
						   }
							 }}
							 }else{
								 request.setAttribute("message","<center>please updload file of correct format</center>");
									rd = request.getRequestDispatcher("./status.jsp");
									rd.forward(request,response);
							 }
						} catch (Exception e) {
						    e.printStackTrace();
							request.setAttribute("message","<center>"+ e.getMessage()+"</center>");
							rd = request.getRequestDispatcher("./status.jsp");
							rd.forward(request,response);
						} 
				//}
				
				
			 

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}
	//xml string generation
	public String sendFaxDataResonse(List<String> policyNumberList,String userId) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		// root elements
		Document doc = null;
		Element breakupElement = null;
		StringWriter resultAsString = new StringWriter();
        try{
        	docFactory = DocumentBuilderFactory.newInstance();
        	docBuilder = docFactory.newDocumentBuilder();
        	doc = docBuilder.newDocument();
        	breakupElement = doc.createElement("NODE");
        	doc.appendChild(breakupElement);
        	
        	if(policyNumberList!=null && policyNumberList.size()>0){
			    for(int i=0;i<policyNumberList.size();i++){
			    	Element jobElement = doc.createElement("JOBS");
		        	breakupElement.appendChild(jobElement);
		        	Element selfcardIdElement = doc.createElement("POLICYNO");
		        	selfcardIdElement.appendChild(doc.createTextNode(String.valueOf(policyNumberList.get(i))));
		        	jobElement.appendChild(selfcardIdElement);
		        	Element userIdElement = doc.createElement("USERID");
		        	userIdElement.appendChild(doc.createTextNode(userId));
		        	jobElement.appendChild(userIdElement);
		        	
				}
        	}
        	        	
        	TransformerFactory transformerFactory = TransformerFactory.newInstance();
    		Transformer transformer = transformerFactory.newTransformer();
    		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    		DOMSource source = new DOMSource(doc);
    		StreamResult result = new StreamResult(resultAsString);
    		// StreamResult streamResult = new StreamResult(new File(FaxData.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/ghplfax/faxresponse.xml")); 
    		transformer.transform(source, result);
            }catch(Exception e){
            	 logger.error(e);
            }finally{
            	 docFactory = null;
        		 docBuilder = null;
        		 doc = null;
        		 breakupElement = null;
            }
            return resultAsString.toString();
	}
//  to find the user uploaded file name 
	private String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
          if (cd.trim().startsWith("filename")) {
            return cd.substring(cd.indexOf('=') + 1).trim()
                    .replace("\"", "");
          }
        }
        return null;
      }

}
