package com.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.PolicyListDataBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class PolicyReportDownloadAction
 */
@WebServlet("/PolicyReportDownloadAction")
public class PolicyReportDownloadAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PolicyReportDownloadAction");  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PolicyReportDownloadAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				
				//int userId=0;
				String moduleUserId= "";
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(1);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				PolicyListDataBean policyListDataBeanBean =null;
				//List<InsertLotDetailsBean> lotBeanListObj = null;
				//lotBeanListObj=new ArrayList<InsertLotDetailsBean>();
				int isCron=0;
				logger.info("isCron  "+isCron);
				policyListDataBeanBean=EnrollAuxDAOManager.getPolicyLotDetails(isCron, 4);
				RequestDispatcher rd =null;
				
				if(policyListDataBeanBean!=null){
					if(policyListDataBeanBean.getStatusBean().getStatusFlag()==1 || policyListDataBeanBean.getStatusBean().getStatusFlag()==0){
						request.setAttribute("lotBeanListObj",policyListDataBeanBean.getLotListObj()); 
						rd = request.getRequestDispatcher("/policyJobDetails.jsp");
						rd.forward(request,response);
					}else{
						request.setAttribute("message", policyListDataBeanBean.getStatusBean().getStatusMessage()); 
						 rd = request.getRequestDispatcher("/error.jsp");
						 rd.forward(request,response);
					}
				}

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
