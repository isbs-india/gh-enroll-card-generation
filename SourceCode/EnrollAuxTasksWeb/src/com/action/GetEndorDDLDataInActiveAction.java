package com.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.EndorsementBean;
import com.model.LoginBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class GetEndorDDLDataInActiveAction
 */
@WebServlet("/GetEndorDDLDataInActiveAction")
public class GetEndorDDLDataInActiveAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GetEndorDDLDataInActiveAction");   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetEndorDDLDataInActiveAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int policyId=0;
		String endorsementData="";
		List<EndorsementBean> endorBeanListObj = null;

		try{
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {

				//int userId=0;
				String moduleUserId= "";
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(1);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				if((request.getParameter("key")!=null) && (!request.getParameter("key").equals("")))
				{
					policyId=Integer.parseInt(request.getParameter("key"));
				


				
				endorBeanListObj=new ArrayList<EndorsementBean>();
				endorBeanListObj=EnrollAuxDAOManager.getEndorDDLDetailsInActive(policyId, 1);

				
				endorsementData="<option value='0'>-Select-</option>";
				
				for(EndorsementBean endorObj:endorBeanListObj)
				{
					endorsementData+="<option value='"+endorObj.getEndorsementId()+"'>"+endorObj.getEndorsementNo().trim()+"("+endorObj.getEndorEffectiveDate()+")</option>";
				}


				PrintWriter pw = response. getWriter ();
				pw.print (endorsementData);
				pw. close ();
				
				}

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
