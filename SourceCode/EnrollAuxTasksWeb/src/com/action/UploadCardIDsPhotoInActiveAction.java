package com.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.model.LoginBean;
import com.model.UploadPhotoDetails;
import com.persistence.common.PhotoUploadDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class UploadCardIDsPhotoInActiveAction
 */
@WebServlet("/UploadCardIDsPhotoInActiveAction")
@MultipartConfig(fileSizeThreshold=1024*1024*20, // 2MB
maxFileSize=1024*1024*100,      // 10MB
maxRequestSize=1024*1024*500)   // 50MB
public class UploadCardIDsPhotoInActiveAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("UploadCardIDsPhotoIndividualAction");  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadCardIDsPhotoInActiveAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		logger.info("*******************start the controll of UploadCardIDsPhotoAction********************");
		UploadPhotoDetails uploadPhotoDetails = null;
		try{
			HttpSession session = request.getSession(true); 
			RequestDispatcher rd =null;
			uploadPhotoDetails  = new UploadPhotoDetails();
			if(session != null && !session.isNew()) {
				int moduleUserId= 0;
				int moduleId= 0;
				int userId=0;
				//int moduleId=4;
				System.out.println("moduleId : "+request.getParameter("moduleId"));
				if(request.getParameter("moduleId")!=null){
					moduleId=Integer.parseInt(request.getParameter("moduleId"));
					uploadPhotoDetails.setModuleId(moduleId);
				}
				
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					userId =loginBean.getUid();
					uploadPhotoDetails.setUserId(userId);
					moduleUserId =loginBean.getModuleUserId(moduleId);
					
					uploadPhotoDetails.setModuleId(moduleId);
					if(moduleUserId==0){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				
				List<String> list =new ArrayList<String>();
				String userName="";
				String  policy="";
				String policyName="";
				int policyId =0;
				if((request.getParameter("policy"))!=null && !(request.getParameter("policy").equals("0"))){
					policy = request.getParameter("policy");
					policyId=Integer.parseInt(request.getParameter("policy").split("~")[0]);
					policyName = policy.split("~")[1];
					uploadPhotoDetails.setPolicyId(policyId);
					uploadPhotoDetails.setPolicyName(policyName);
				}
				String extension ="";
				int BUFFER_LENGTH = 4096;
				File uploadPath = (File)request.getServletContext().getAttribute("javax.servlet.context.tempdir");  
				 Part filePart = request.getPart("xlfile");
				 if (filePart != null && filePart.getSize() > 0) {
					 String fileName = getFileName(filePart);
					 int i = fileName.lastIndexOf('.');
					 if (i > 0) {
					     extension = fileName.substring(i+1);
					     extension.toLowerCase();
					     if(!extension.equals("xlsx")){
					     request.setAttribute("message", "Uploaded file format is not in xlsx");
							 rd=request.getRequestDispatcher("./status.jsp");
							 rd.forward(request,response);
					     }else{
					 
					 String filePath = uploadPath + File.separator + fileName;
					        InputStream is = request.getPart(filePart.getName()).getInputStream();
					        FileOutputStream os = new FileOutputStream(filePath);
					        byte[] bytes = new byte[BUFFER_LENGTH];
					        int read = 0;
					        while ((read = is.read(bytes, 0, BUFFER_LENGTH)) != -1) {
					            os.write(bytes, 0, read);
					        }
					        os.flush();
					        is.close();
					        os.close();
					        InputStream excelFileToRead = new FileInputStream(filePath);
							XSSFWorkbook  wb = new XSSFWorkbook(excelFileToRead);
							XSSFSheet sheet = wb.getSheetAt(0);
							XSSFRow row; 
							XSSFCell cell;
							int rowCount=0;
							Iterator rows = sheet.rowIterator();
							String str ="";
							while (rows.hasNext())
							{
								rowCount++;
								row=(XSSFRow) rows.next();
								Iterator cells = row.cellIterator();
								while (cells.hasNext())
								{
									cell=(XSSFCell) cells.next();
									if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
									{
										System.out.print(cell.getStringCellValue()+" ");
										if(rowCount!=1){
											list.add(cell.getStringCellValue());
											if(cell.getStringCellValue()!=""){
												str +=cell.getStringCellValue()+",";
											}
										}
									}
								}
							}
				String FinalMessage="";
				logger.info("rowCount    "+rowCount);
				logger.info("cards count in xlsx file :    "+list.size());
			   if(rowCount<900){
				   if(list.size()>0){
						String xml = sendFaxDataResonse(list);
					//System.out.println("xml  :"+xml);
						
					UploadPhotoDetails uploadPhotoDetails_local= null;
					logger.info("before calling method of Manager :insertPhotoCardDetails");
					if(moduleId!=0){
					uploadPhotoDetails_local= PhotoUploadDAOManager.insertPhotoCardDetails(uploadPhotoDetails,str,moduleId);
					}
					logger.info("after calling method of Manager :insertPhotoCardDetails");
					if(uploadPhotoDetails_local!=null){
							if(uploadPhotoDetails_local.getStatusBean().getStatusFlag()==1 || uploadPhotoDetails_local.getStatusBean().getStatusFlag()==0){
								request.setAttribute("message", uploadPhotoDetails_local.getStatusBean().getStatusMessage());
								rd=request.getRequestDispatcher("./status.jsp");
								rd.forward(request,response);
							}else if(uploadPhotoDetails_local.getStatusBean().getStatusFlag()==2){
								 request.setAttribute("message", uploadPhotoDetails_local.getStatusBean().getStatusMessage()); 
								 rd = request.getRequestDispatcher("/error.jsp");
								 rd.forward(request,response);
							}
 						}else{
 							request.setAttribute("message", "Database Insertion failed. Please try again");
 							 rd=request.getRequestDispatcher("./status.jsp");
 							 rd.forward(request,response);
 						}
 					
				   }else{
					   FinalMessage="Uploaded excel not proper format <br/><br/>";	
					   String FinalMessage1="Uploaded excel not proper format";
						request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
						request.setAttribute("message","<center>"+FinalMessage+"</center>");
						rd = request.getRequestDispatcher("./status.jsp");
						rd.forward(request,response);
					   
				   }
			   }else{
				   FinalMessage="excel Row count should be less than 900 records <br/><br/>";	
				   String FinalMessage1="Ecard generation request status";
					request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
					request.setAttribute("message","<center>"+FinalMessage+"</center>");
					rd = request.getRequestDispatcher("./status.jsp");
					rd.forward(request,response);
			   }
					      
				 }}
					 }
			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
			logger.info("*******************end the controll of UploadCardIDsPhotoAction********************");
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}
	
	public String sendFaxDataResonse(List<String> selfCardList) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		// root elements
		Document doc = null;
		
		StringWriter resultAsString = new StringWriter();
        try{
        	docFactory = DocumentBuilderFactory.newInstance();
        	docBuilder = docFactory.newDocumentBuilder();
        	doc = docBuilder.newDocument();
        	if(selfCardList!=null && selfCardList.size()>0){
        		Element cardIdsElement = doc.createElement("GHCARDIDS");
        		doc.appendChild(cardIdsElement);
			    for(int i=0;i<selfCardList.size();i++){
		        	Element selfcardIdElement = doc.createElement("CARDID");
		        	selfcardIdElement.appendChild(doc.createTextNode(String.valueOf(selfCardList.get(i))));
		        	cardIdsElement.appendChild(selfcardIdElement);
				}
        	}
        	
        	TransformerFactory transformerFactory = TransformerFactory.newInstance();
    		Transformer transformer = transformerFactory.newTransformer();
    		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    		DOMSource source = new DOMSource(doc);
    		StreamResult result = new StreamResult(resultAsString);
    		// StreamResult streamResult = new StreamResult(new File(FaxData.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/ghplfax/faxresponse.xml")); 
    		transformer.transform(source, result);
            }catch(Exception e){
            	 logger.error(e);
            }finally{
            	 docFactory = null;
        		 docBuilder = null;
        		 doc = null;
        		 
            }
            return resultAsString.toString();
	}
	
	private String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
          if (cd.trim().startsWith("filename")) {
            return cd.substring(cd.indexOf('=') + 1).trim()
                    .replace("\"", "");
          }
        }
        return null;
      }

}
