package com.action;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.model.LoginBean;

/**
 * Servlet implementation class CheckUploadPhotoAction
 */
@WebServlet("/CheckUploadPhotoAction")
@MultipartConfig(fileSizeThreshold=1024*1024*20, // 2MB
maxFileSize=1024*1024*100,      // 10MB
maxRequestSize=1024*1024*500)   // 50MB
public class CheckUploadPhotoAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("CheckUploadPhotoAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckUploadPhotoAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = null;
		try{
			
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				if (!ServletFileUpload.isMultipartContent(request)) {
					return;
				}
				//int moduleId=0;
				int moduleUserId= 0;
				//int userId=0;
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//int userId =loginBean.getUid();
					moduleUserId =loginBean.getModuleUserId(1);
					if(moduleUserId==0){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
	            String uploadPath = getServletContext().getRealPath("CHECKSINGLEPHOTOUPLOAD");
	            final Part filePart1 = request.getPart("file"); 
	            String fileName = null;
	            if(filePart1 !=null){
	            	fileName = extractFileName(filePart1);
	            	filePart1.write(uploadPath + File.separator + fileName);
	            }
	            File f = new File(uploadPath+ File.separator +fileName);
	            Image image= null;
	            try{
	            	 image = ImageIO.read(f);
	            }catch(Exception e){
	            	logger.info(e.getMessage());
	            	request.setAttribute("message", "Uploaded image is not a proper JPG Image file of Name : "+fileName);
			 		rd=request.getRequestDispatcher("./status.jsp");
			 		if(f!=null){
			 			f.delete();
			 		}
			 		rd.forward(request,response);
			 		return;
	            }
				
				boolean b=false;
				if(image != null){
				b= isJPEG(f);
				if(b){
					System.out.println("Uploaded image is in JPG");
					request.setAttribute("message", "Uploaded image is in JPG");
			 		rd=request.getRequestDispatcher("./status.jsp");
			 		f.delete();
			 		rd.forward(request,response);
			 		return;
				}else{
					System.out.println("Uploaded image is not in JPG");
					request.setAttribute("message", "Uploaded image is not in JPG");
					f.delete();
			 		rd=request.getRequestDispatcher("./status.jsp");
			 		rd.forward(request,response);
			 		return;
				}
				}
			}
	}
		catch(Exception e){
	e.printStackTrace();	
	logger.error(e.getMessage());
	getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
	}
}
	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}
	private static Boolean isJPEG(File filename) throws Exception {
	    DataInputStream ins = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)));
	    try {
	        if (ins.readInt() == 0xffd8ffe0) {
	            return true;
	        } else {
	            return false;

	        }
	    } finally {
	        ins.close();
	    }
	}
}
