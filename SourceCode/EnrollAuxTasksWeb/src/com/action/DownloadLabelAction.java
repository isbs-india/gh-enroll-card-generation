package com.action;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.util.MyProperties;

/**
 * Servlet implementation class DownloadLabelAction
 */
@WebServlet("/DownloadLabelAction")
public class DownloadLabelAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadLabelAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FileInputStream inputStream=null;
		try {
			MyProperties myResources = new MyProperties();
			String lotNumber = request.getParameter("lotNumber");
			String moduleId = request.getParameter("moduleId");
			//String formatId = request.getParameter("formatId");
			String downloadslocal = myResources.getMyProperties("downloadsLabel");
			String inputFilepath="";
			//if(!moduleId.equals("2")){
				
				response.setContentType("application/zip");
			if(moduleId.equals("1")){
				inputFilepath= downloadslocal+lotNumber+"_CORP.zip";
				response.setHeader("Content-Disposition", "attachment; filename="+lotNumber+"_CORP.zip");
			}else if(moduleId.equals("2")){
				inputFilepath= downloadslocal+lotNumber+"_AB.zip";
				response.setHeader("Content-Disposition", "attachment; filename="+lotNumber+"_AB.zip");
			}else if(moduleId.equals("4")){
				inputFilepath= downloadslocal+lotNumber+"_WIND.zip";
				response.setHeader("Content-Disposition", "attachment; filename="+lotNumber+"_WIND.zip");
			}
				
			 inputStream = new FileInputStream(inputFilepath);
				ServletOutputStream output = response.getOutputStream();
				 byte[] bytesArray = new byte[4096];
			        int bytesRead = -1;
			        while ((bytesRead = inputStream.read(bytesArray)) != -1) {
			        	output.write(bytesArray, 0, bytesRead);
			        }
	        	  output.flush();
	        	  output.close();
        	
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
        	if(inputStream!=null){
        		inputStream=null;
        	}
        }
	}

}
