package com.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.PoliciesBean;
import com.model.PolicyDataBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class InActivePolicySearch
 */
@WebServlet("/InActivePolicySearch")
public class InActivePolicySearch extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("PolicySearch");  
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InActivePolicySearch() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try{
			HttpSession session = request.getSession(true); 
			RequestDispatcher rd = null;
			if(session != null && !session.isNew()) {
				int moduleUserId= 0;
				//int userId=0;
				int moduleId=0;
						
				if(session.getAttribute("user")!=null){
					 if(request.getParameter("key")!=null && !request.getParameter("key").equals("")){
						 moduleId = Integer.parseInt(request.getParameter("key"));
					 }
					
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =loginBean.getModuleUserId(moduleId);
					if(moduleUserId==0){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
              
            	 
            	if(moduleId ==1){
            	   response.setContentType("text/xml;charset=UTF-8");
					String stdkey=request.getParameter("key").trim();
					String policy="";
					policy="<option value='0'>-Select-</option>";
					//Retrieving Cities List
					List<PoliciesBean> policiesBeanListObj=null;
					PolicyDataBean policyDataBean= null;
					policiesBeanListObj=new ArrayList<PoliciesBean>();
				
						policyDataBean=EnrollAuxDAOManager.getPoliciesDDLDetailsInActive(1);
				
					policiesBeanListObj = policyDataBean.getPolBeanListObj();
					if(policyDataBean!=null){
						if(policiesBeanListObj!=null && policiesBeanListObj.size()>0){
							for(PoliciesBean policiesObj:policiesBeanListObj)
							{
								policy+="<option value='"+policiesObj.getPolicyId()+"~"+policiesObj.getPolicyHolderName().trim()+"'>"+policiesObj.getPolicyHolderName().trim()+"("+policiesObj.getPolicyFrom()+" - "+policiesObj.getPolicyTo()+")"+"</option>";
							}
							PrintWriter pw = response. getWriter ();
							pw.print (policy);
							pw. close ();
						}else{
							request.setAttribute("message", "data not found ");
					 		getServletContext().getRequestDispatcher("/status.jsp").forward(request, response);
			        		return;
						}
					}else{
						request.setAttribute("message", "data not found ");
				 		getServletContext().getRequestDispatcher("/status.jsp").forward(request, response);
		        		return;
					}
	             }else if(moduleId ==2){
                 	   response.setContentType("text/xml;charset=UTF-8");
     					String stdkey=request.getParameter("key").trim();
     					String policy="";
     					PolicyDataBean policyDataBean=null;
     					policy="<option value='0'>-Select-</option>";
     					//Retrieving Cities List
     					List<PoliciesBean> policiesBeanListObj=null;
     					policiesBeanListObj=new ArrayList<PoliciesBean>();
     					policyDataBean = EnrollAuxDAOManager.getPoliciesDDLDetailsInActive(2);
     					if(policyDataBean!=null){
	     					policiesBeanListObj = policyDataBean.getPolBeanListObj();
	     					if(policiesBeanListObj!=null && policiesBeanListObj.size()>0){
		     					for(PoliciesBean policiesObj:policiesBeanListObj)
		     					{
		     						policy+="<option value='"+policiesObj.getPolicyId()+"~"+policiesObj.getPolicyHolderName().trim()+"'>"+policiesObj.getPolicyHolderName().trim()+"( "+policiesObj.getPolicyFrom()+" - "+policiesObj.getPolicyTo()+")"+"</option>";
		     					}
	     					}else{
	     						 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
	   							 rd = request.getRequestDispatcher("/error.jsp");
	   							 rd.forward(request,response);
	   							 return;
	     					}
     					}else{
    						 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
  							 rd = request.getRequestDispatcher("/error.jsp");
  							 rd.forward(request,response);
  							 return;
    					}
     					PrintWriter pw = response. getWriter();
     					pw.print (policy);
     					pw. close ();
     	             }else if(moduleId ==4){
                   	   response.setContentType("text/xml;charset=UTF-8");
       					String stdkey=request.getParameter("key").trim();
       					String policy="";
       					PolicyDataBean policyDataBean=null;
       					policy="<option value='0'>-Select-</option>";
       					//Retrieving Cities List
       					List<PoliciesBean> policiesBeanListObj=null;
       					policiesBeanListObj=new ArrayList<PoliciesBean>();
       					policyDataBean = EnrollAuxDAOManager.getPoliciesDDLDetails(4);
       					if(policyDataBean!=null){
  	     					policiesBeanListObj = policyDataBean.getPolBeanListObj();
  	     					if(policiesBeanListObj!=null && policiesBeanListObj.size()>0){
  		     					for(PoliciesBean policiesObj:policiesBeanListObj)
  		     					{
  		     						policy+="<option value='"+policiesObj.getPolicyId()+"~"+policiesObj.getPolicyHolderName().trim()+"'>"+policiesObj.getPolicyHolderName().trim()+"( "+policiesObj.getPolicyFrom()+" - "+policiesObj.getPolicyTo()+")"+"</option>";
  		     					}
  	     					}else{
  	     						 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
  	   							 rd = request.getRequestDispatcher("/error.jsp");
  	   							 rd.forward(request,response);
  	   							 return;
  	     					}
       					}else{
      						 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
    							 rd = request.getRequestDispatcher("/error.jsp");
    							 rd.forward(request,response);
    							 return;
      					}
       					PrintWriter pw = response. getWriter();
       					pw.print (policy);
       					pw. close ();
       	             }else{
     	            	request.setAttribute("message", "data not found ");
				 		getServletContext().getRequestDispatcher("/status.jsp").forward(request, response);
		        		return;
     	             }
			

			} else {
				if(session != null){
					session.invalidate();
				}
				getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
        		return;
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("IADMIN",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
