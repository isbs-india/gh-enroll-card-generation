package com.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.exception.ServiceLocatorException;
 
public class ServiceLocator {
	private InitialContext ic;
    private Map cache;  
    private static ServiceLocator me;
    
    static {
    	try {
        	me = new ServiceLocator();
        } catch(ServiceLocatorException se) {         
        	se.printStackTrace(System.err);
        }
    }
    
    private ServiceLocator() throws ServiceLocatorException {
    	try {
        	ic = new InitialContext();
        	cache = Collections.synchronizedMap(new HashMap());
        	
        } catch (NamingException ne) {
        	throw new ServiceLocatorException(ne);
        } catch (Exception e) {
        	throw new ServiceLocatorException(e);
        }
    }

    static public ServiceLocator getInstance() {
    	return me;
    } 
    
    /**
     * This method obtains the datasource itself for a caller
     * @return the DataSource corresponding to the name parameter
     */
    public DataSource getDataSource(String dataSourceName) throws ServiceLocatorException {
    	DataSource dataSource = null;
    	try {
    		System.out.println("Service locator dataSourceName  : "+dataSourceName);
    		if (cache.containsKey(dataSourceName)) {
    			System.out.println("if catche  : "+dataSourceName);
    			dataSource = (DataSource) cache.get(dataSourceName);
    		} else {
    			System.out.println("else catche  : "+dataSourceName);
    			dataSource = (DataSource)ic.lookup(dataSourceName);
    			cache.put(dataSourceName, dataSource );
    		}
    	} catch (NamingException ne) {
    		throw new ServiceLocatorException(ne);
    	} catch (Exception e) {
    		throw new ServiceLocatorException(e);
    	}
    	return dataSource;
    }
}
