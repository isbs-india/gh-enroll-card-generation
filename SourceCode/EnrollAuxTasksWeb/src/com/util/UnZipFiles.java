package com.util;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


public class UnZipFiles {
	
	@SuppressWarnings({ "unused", "resource" })
	private static Boolean isJPEG(File filename) throws Exception {
	    DataInputStream ins = new DataInputStream(new BufferedInputStream(new FileInputStream(filename)));
	        if (ins.readInt() == 0xffd8ffe0) {
	            return true;
	        } else {
	            return false;
	        }
	}
	
	public int unzipImage(String zipFile, String extractFolder) {
         
		 try {
	          //  CreateDir();
	            int BUFFER = 4096;
	            File file = new File(zipFile);

	            ZipFile  zip = new ZipFile(zipFile);
	           
	            String newPath = extractFolder;
	           
	            new File(newPath).mkdir();
	            @SuppressWarnings("rawtypes")
				Enumeration zipFileEntries = zip.entries();
	            long compressedSize_final = 0;
	           
	            // Process each entry
	            while (zipFileEntries.hasMoreElements()) {
	                // grab a zip file entry
	                ZipEntry entry = (ZipEntry) zipFileEntries.nextElement();
	                long compressedSize = entry.getCompressedSize();
	                //System.out.println("compressedSize  >>"+compressedSize);
	                compressedSize_final = compressedSize_final + compressedSize;
	                String currentEntry = entry.getName();
	                currentEntry = currentEntry.replace('\\', '/');
	                File destFile = new File(newPath, currentEntry);
	                
	                // destFile = new File(newPath, destFile.getName());
	                File destinationParent = destFile.getParentFile();

	                // create the parent directory structure if needed
	                destinationParent.mkdirs();

	                if (!entry.isDirectory()) {
	                    BufferedInputStream is = new BufferedInputStream(
	                            zip.getInputStream(entry));
	                    int currentByte;
	                    // establish buffer for writing file
	                    byte data[] = new byte[BUFFER];
	                    // write the current file to disk
	                    FileOutputStream fos = new FileOutputStream(destFile);
	                    BufferedOutputStream dest = new BufferedOutputStream(fos,
	                            BUFFER);
	                    // read and write until last byte is encountered
	                    while ((currentByte = is.read(data, 0, BUFFER)) != -1) {
	                        dest.write(data, 0, currentByte);
	                    }
	                    dest.flush();
	                    dest.close();
	                    is.close();
	                }
	            }
	          //  System.out.println("compressedSize_final    >>"+compressedSize_final);
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
			return 0;
       
    }

}
