package com.util;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;

public class Constants {
	public static final String JNDI_KEY_CORP_REPORTS="java:comp/env/jdbc/iauthdsCORPREPORTS";
	public static final String JNDI_KEY_AB_REPORTS="java:comp/env/jdbc/iauthdsABREPORTS";
	public static final String JNDI_KEY_WIND_REPORTS="java:comp/env/jdbc/iauthdsWINDREPORTS";
	
	public static final String JNDI_KEY_CORP="java:comp/env/jdbc/iauthdsCORP";
	public static final String JNDI_KEY_AB="java:comp/env/jdbc/iauthdsAB";
	public static final String JNDI_KEY_WIND="java:comp/env/jdbc/iauthdsWIND";
	
	public static final String JNDI_KEY_CORP_LIVE="java:comp/env/jdbc/iauthdsCORPLIVE";
	public static final String JNDI_KEY_AB_LIVE="java:comp/env/jdbc/iauthdsABLIVE";
	public static final String JNDI_KEY_WIND_LIVE="java:comp/env/jdbc/iauthdsWINDLIVE";
	
	public static final String JNDI_KEY_Mysql="java:comp/env/jdbc/iauthdsMysql";
	public static final String COMMAN_DB="java:comp/env/jdbc/COMMAN_DB";
	public static final String JNDI_KEY_XL_PREAUTH="java:comp/env/jdbc/XL_PREAUTH";
	public static final int XL_PREAUTH_Module_ID=10;
		
	public static final String RET="RET";
	
	public static final String MODULE_1 = "GHPL";
	public static final String MODULE_2 = "AB";
	public static final String MODULE_3 = "VENUS";
	public static final String MODULE_4 = "UNITED";
	public static final String MODULE_5 = "NEW INDIA";
	public static final String MODULE_6 = "ORIENTAL";
	public static final String MODULE_7 = "NATIONAL";
	public static final String MODULE_8 = "NIC NAV";
	
	//public static final String downloadslocal="D:\\Enrolement\\";
	public static final String downloadslocal="/root/MyCrons/AutoEcardsZipped/";
	
	//public static final String alertsEmailId="alerts@isharemail.in";
	public static final String alertsEmailId="anand.kumar@isharemail.in";
	
	public static final String HEADER_IMAGE="/resources/images/ClaimLettersHeader.jpg";
	public static final Rectangle pagesizeA4 =PageSize.A4;
	public static final Rectangle pagelandscape=PageSize.A4_LANDSCAPE;
	public static final String staticfilename="d:/itex/chequeleter.pdf";
	public static final String CLAIM_CLOSURE_LETTER = "/PDFDOC/Claims_Closure_letter_";
	public static final String CLAIM_CHEQUE_LETTER = "/PDFDOC/Claims_Cheque_letter_";
	public static final String LETTER_EXT = ".pdf";
	public static final String NewLine = "\n";
	public static final Font fontUnderlineBoldParaText1 = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	public static final Font fontDateText= new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontGeneralText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontPintText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontTableCellText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontBoldParaTextRed = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.RED);
	public static final Font fontNoteText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontItalicParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
	public static final Font fontItalicBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC, BaseColor.BLACK);
	public static final Font fontItalicUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontItalicBoldUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	
}
