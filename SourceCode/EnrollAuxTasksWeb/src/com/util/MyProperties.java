package com.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.action.ContextListener;


public class MyProperties {
    Logger logger =Logger.getLogger("MyProperties");
    public String getMyProperties(String PropName)
    {
        Properties prop = new Properties();
        try {
               //logger.info("to Get "+PropName+" Properties");
               // prop.load(getClass().getClassLoader().getResourceAsStream("config.properties")); 
                FileInputStream fin = new FileInputStream(ContextListener.customerDataFile);
                // System.out.println("fin   "+fin);
                 prop.load(fin);
                return prop.getProperty(PropName);
 
    	} catch (IOException ex) {
    		logger.error("Error DBConn - getting "+PropName+" Properties Set IO Exception"); 
    		logger.error("IN " + ex.getMessage());
                return "";
        }
    }     
    
  
}
