package com.model;

import java.io.Serializable;
import java.util.List;

import com.model.InsertLotDetailsBean;

public class LotDataBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<InsertLotDetailsBean> lotBeanListObj;
	private StatusBean statusBean;
	public List<InsertLotDetailsBean> getLotBeanListObj() {
		return lotBeanListObj;
	}
	public void setLotBeanListObj(List<InsertLotDetailsBean> lotBeanListObj) {
		this.lotBeanListObj = lotBeanListObj;
	}
	public StatusBean getStatusBean() {
		return statusBean;
	}
	public void setStatusBean(StatusBean statusBean) {
		this.statusBean = statusBean;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
