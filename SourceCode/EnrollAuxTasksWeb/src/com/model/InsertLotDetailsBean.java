package com.model;

import java.io.Serializable;

public class InsertLotDetailsBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private int serialNo;
	private String lotNumber;
	private int policyId;
	private int endorsementId;
	private String endorsementNo;
	private int moduleId;
	private int userId;
	private String policyHolderName;
	private String statusDesc;
	private String userName;
	private String moduleName;
	private String createdOn;
	private int lotStatus;
	private String formatId;
	
	
	public String getFormatId() {
		return formatId;
	}
	public void setFormatId(String formatId) {
		this.formatId = formatId;
	}
	public String getEndorsementNo() {
		return endorsementNo;
	}
	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public int getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus(int lotStatus) {
		this.lotStatus = lotStatus;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getStatusDesc() {
		return statusDesc;
	}
	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPolicyHolderName() {
		return policyHolderName;
	}
	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public int getPolicyId() {
		return policyId;
	}
	public void setPolicyId(int policyId) {
		this.policyId = policyId;
	}
	public int getEndorsementId() {
		return endorsementId;
	}
	public void setEndorsementId(int endorsementId) {
		this.endorsementId = endorsementId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}


}
