package com.model;

import java.io.Serializable;

public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private int uid;
	private int roleId;
	private String loginId;
	private String loginUserName;
	private String homePage;
	private String assignedMedicoName;
	private int abUid;
	private int venusUid;
	private int retUid;
	private StatusBean statusBean;
	
	public int getVenusUid() {
		return venusUid;
	}
	public void setVenusUid(int venusUid) {
		this.venusUid = venusUid;
	}
	public StatusBean getStatusBean() {
		return statusBean;
	}
	public void setStatusBean(StatusBean statusBean) {
		this.statusBean = statusBean;
	}
	public int getAbUid() {
		return abUid;
	}
	public void setAbUid(int abUid) {
		this.abUid = abUid;
	}
	public int getVeniusUid() {
		return venusUid;
	}
	public void setVeniusUid(int venusUid) {
		this.venusUid = venusUid;
	}
	public int getRetUid() {
		return retUid;
	}
	public void setRetUid(int retUid) {
		this.retUid = retUid;
	}
	public int getUid() {
		return uid;
	}
	public void setUid(int uid) {
		this.uid = uid;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getLoginUserName() {
		return loginUserName;
	}
	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}
	public String getHomePage() {
		return homePage;
	}
	public void setHomePage(String homePage) {
		this.homePage = homePage;
	}
	public String getAssignedMedicoName() {
		return assignedMedicoName;
	}
	public void setAssignedMedicoName(String assignedMedicoName) {
		this.assignedMedicoName = assignedMedicoName;
	}
	
	public int getModuleUserId(int Module){
		int userId=0;
		switch (Module) {
        /*case 1:userId =this.uid;break;
        case 2:userId =this.abUid;break;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:userId =this.retUid;break;*/
		
		case 1:userId =this.uid;break;
        case 2:userId =this.uid;break;
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:userId =this.uid;break;
    }
    return userId;
	}
	

}
