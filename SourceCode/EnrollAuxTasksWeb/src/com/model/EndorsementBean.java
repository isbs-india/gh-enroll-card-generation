package com.model;

import java.io.Serializable;

public class EndorsementBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int endorsementId;
	private String endorsementNo;
	private String endorEffectiveDate;
	
	
	public int getEndorsementId() {
		return endorsementId;
	}
	public void setEndorsementId(int endorsementId) {
		this.endorsementId = endorsementId;
	}
	public String getEndorsementNo() {
		return endorsementNo;
	}
	public void setEndorsementNo(String endorsementNo) {
		this.endorsementNo = endorsementNo;
	}
	public String getEndorEffectiveDate() {
		return endorEffectiveDate;
	}
	public void setEndorEffectiveDate(String endorEffectiveDate) {
		this.endorEffectiveDate = endorEffectiveDate;
	}
	
	
	


}
