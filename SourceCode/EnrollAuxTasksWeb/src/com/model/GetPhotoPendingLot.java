package com.model;

import java.io.Serializable;

public class GetPhotoPendingLot implements Serializable {

	private static final long serialVersionUID = 1L;
	private int lotId;
	private int moduleId;
	private String fileName;
	private String lotCreated;
	private String cantactEmail;
	private String userName;
	private String poicyId;
	private int lotStatusFlag;
	private String policyName;
	private String xmlString;
	private String moduleName;
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getXmlString() {
		return xmlString;
	}
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	public int getLotStatusFlag() {
		return lotStatusFlag;
	}
	public void setLotStatusFlag(int lotStatusFlag) {
		this.lotStatusFlag = lotStatusFlag;
	}
	/*LOTID, 
	MODULEID, 
	FILENAME, 
	TO_CHAR(LOTCREATED,'DD-MON-RRRR') LOTCREATED,
    A.CONTACTEMAIL,
    U.USERNAME,
    L.POLICYID*/
	public int getLotId() {
		return lotId;
	}
	public void setLotId(int lotId) {
		this.lotId = lotId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getLotCreated() {
		return lotCreated;
	}
	public void setLotCreated(String lotCreated) {
		this.lotCreated = lotCreated;
	}
	public String getCantactEmail() {
		return cantactEmail;
	}
	public void setCantactEmail(String cantactEmail) {
		this.cantactEmail = cantactEmail;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPoicyId() {
		return poicyId;
	}
	public void setPoicyId(String poicyId) {
		this.poicyId = poicyId;
	}
	
	

}
