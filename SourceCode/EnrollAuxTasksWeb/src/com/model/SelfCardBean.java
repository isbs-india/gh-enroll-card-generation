package com.model;

import java.io.Serializable;

public class SelfCardBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private String empName;
	private String age;
	private String code;
	private String policyFrom;
	private String policyTo;
	private String inceptionDate;
	private String bano;
	private String nooflives;
	private String policyId;
	private String fileFormat;
	private String selfCardId;
	
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getPolicyFrom() {
		return policyFrom;
	}
	public void setPolicyFrom(String policyFrom) {
		this.policyFrom = policyFrom;
	}
	public String getPolicyTo() {
		return policyTo;
	}
	public void setPolicyTo(String policyTo) {
		this.policyTo = policyTo;
	}
	public String getInceptionDate() {
		return inceptionDate;
	}
	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}
	public String getBano() {
		return bano;
	}
	public void setBano(String bano) {
		this.bano = bano;
	}
	public String getNooflives() {
		return nooflives;
	}
	public void setNooflives(String nooflives) {
		this.nooflives = nooflives;
	}
	public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public String getFileFormat() {
		return fileFormat;
	}
	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	public String getSelfCardId() {
		return selfCardId;
	}
	public void setSelfCardId(String selfCardId) {
		this.selfCardId = selfCardId;
	}
}
