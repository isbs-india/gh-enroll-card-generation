package com.persistence.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import oracle.jdbc.OracleTypes;

import com.exception.DAOException;
import com.model.GetPhotoPendingLot;
import com.model.PolicyDataBean;
import com.model.SinglePhotoBean;
import com.model.StatusBean;
import com.model.UploadPhotoDetails;
import com.util.ProcedureConstants;
import com.util.Util;

public class PhotoUploadDAOImpl extends DAOBase implements PhotoUploadDAO {
	Logger logger =(Logger) Logger.getInstance("PhotoUploadDAOImpl");
	public UploadPhotoDetails insertUploadBankPolicyPhotos(UploadPhotoDetails uploadPhotoDetails,int moduleId) throws DAOException 
	{
		UploadPhotoDetails uploadPhotoDetailsObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		
		int indexpos=0;
		//int errorFlag=0;
		
		String statusMessage="";
		StatusBean statusBean= null;
		
		try
		{
			con =getLiveMyConnection(1);
			con.setAutoCommit(false);
				uploadPhotoDetailsObj = new UploadPhotoDetails();
				cstmt = con.prepareCall(ProcedureConstants.PROC_PHOTO_LOT_INSERT);
				cstmt.setInt(++indexpos,uploadPhotoDetails.getModuleId());
				cstmt.setInt(++indexpos,uploadPhotoDetails.getUserId());
				cstmt.setString(++indexpos,uploadPhotoDetails.getFileName());
				cstmt.setInt(++indexpos,uploadPhotoDetails.getPolicyId());
				cstmt.setString(++indexpos,uploadPhotoDetails.getPolicyName());
				cstmt.registerOutParameter(++indexpos, OracleTypes.BIGINT);
				cstmt.registerOutParameter(++indexpos, OracleTypes.LONGVARCHAR);
				cstmt.execute();
				con.commit();
				//uploadPhotoDetailsObj.setStatus(Integer.parseInt(cstmt.getString(indexpos-1)));
				//uploadPhotoDetailsObj.setStatusMsg(cstmt.getString(indexpos));
			
			if(cstmt.getInt(indexpos-1) ==1){
				statusBean = new StatusBean();
				statusBean.setStatusFlag(1);
				statusBean.setStatusMessage(cstmt.getString(indexpos));
				uploadPhotoDetailsObj.setStatusBean(statusBean);
			}else if(cstmt.getInt(indexpos-1) ==0){
				statusBean = new StatusBean();
				statusBean.setStatusFlag(0);
				statusBean.setStatusMessage(cstmt.getString(indexpos));
				uploadPhotoDetailsObj.setStatusBean(statusBean);
			}
		}
		catch(Exception ex)
		{
			logger.error(ex);
			uploadPhotoDetailsObj = new UploadPhotoDetails();
			if(con==null){
				statusMessage ="Database connection null <br/>";
			}
			if(cstmt==null){
				statusMessage +="procedure related exception <br/>";
			}
			statusMessage +="Error in insertUploadBankPolicyPhotos:  "+ex;
			statusBean = new StatusBean();
			statusBean.setStatusFlag(2);
			statusBean.setStatusMessage(statusMessage);
			uploadPhotoDetailsObj.setStatusBean(statusBean);
			ex.printStackTrace();
			//throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN insertUploadBankPolicyPhotos:"+ex);
				//throw new DAOException();
			}
		}
		return uploadPhotoDetailsObj;
	}
	
	
	public UploadPhotoDetails insertPhotoCardDetails(UploadPhotoDetails uploadPhotoDetails,String xml,int moduleId) throws DAOException 
	{
		logger.info("enter insertPhotoCardDetails");
		UploadPhotoDetails uploadPhotoDetailsObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		StatusBean statusBean= null;
		String statusMessage="";
		try
		{
			con =getLiveMyConnection(1);
			con.setAutoCommit(false);
				uploadPhotoDetailsObj = new UploadPhotoDetails();
				cstmt = con.prepareCall(ProcedureConstants.PROC_PHOTODOWNLOAD_LOT_INSERT);
				cstmt.setInt(++indexpos,uploadPhotoDetails.getModuleId());
				cstmt.setInt(++indexpos,uploadPhotoDetails.getUserId());
				cstmt.setString(++indexpos,xml);
				cstmt.setInt(++indexpos,uploadPhotoDetails.getPolicyId());
				cstmt.setString(++indexpos,uploadPhotoDetails.getPolicyName());
				
				logger.info("getModuleId "+uploadPhotoDetails.getModuleId());
				logger.info("getUserId "+uploadPhotoDetails.getUserId());
				logger.info("getXmlString "+xml);
				logger.info("getPolicyId "+uploadPhotoDetails.getPolicyId());
				logger.info("getPolicyName "+uploadPhotoDetails.getPolicyName());
				
				cstmt.registerOutParameter(++indexpos, OracleTypes.BIGINT);
				cstmt.registerOutParameter(++indexpos, OracleTypes.LONGVARCHAR);
				cstmt.execute();
				con.commit();
				//uploadPhotoDetailsObj.setStatus(Integer.parseInt(cstmt.getString(indexpos-1)));
				//uploadPhotoDetailsObj.setStatusMsg(cstmt.getString(indexpos));
				
				if(cstmt.getInt(indexpos-1) ==1){
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage(cstmt.getString(indexpos));
					uploadPhotoDetailsObj.setStatusBean(statusBean);
				}else if(cstmt.getInt(indexpos-1) ==0){
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage(cstmt.getString(indexpos));
					uploadPhotoDetailsObj.setStatusBean(statusBean);
				}
		}
		catch(Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			uploadPhotoDetailsObj = new UploadPhotoDetails();
			if(con==null){
				statusMessage ="Database connection null <br/>";
			}
			if(cstmt==null){
				statusMessage +="procedure related exception <br/>";
			}
			statusMessage +="Error in insertSelfCardDetails:  "+ex;
			statusBean = new StatusBean();
			statusBean.setStatusFlag(2);
			statusBean.setStatusMessage(statusMessage);
			uploadPhotoDetailsObj.setStatusBean(statusBean);
			//throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN insertSelfCardDetails:"+ex);
				throw new DAOException();
			}
		}
		return uploadPhotoDetailsObj;
	}
	
	public PolicyDataBean getPhotoPendingLot(int moduleId)throws DAOException 
	{
		Connection con = null;
		CallableStatement cstmt = null;
		GetPhotoPendingLot getPhotoPendingLot=null;
		ResultSet rs=null;
		int indexpos=0;
		List<GetPhotoPendingLot> photoPendingLotList = new ArrayList<GetPhotoPendingLot>();
		String statusMessage="";
		StatusBean statusBean= null;
		PolicyDataBean policyDataBean = new PolicyDataBean();
		try {
			con =getLiveMyConnection(1);
			con.setAutoCommit(false);
			cstmt = con.prepareCall(ProcedureConstants.PROC_GET_PHOTO_PENDING_LOT_WEB);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			while (rs.next()) {
				getPhotoPendingLot=new GetPhotoPendingLot();
				getPhotoPendingLot.setCantactEmail(rs.getString("CONTACTEMAIL"));         
				getPhotoPendingLot.setFileName(rs.getString("FILENAME"));
				getPhotoPendingLot.setLotCreated(rs.getString("LOTCREATED"));    
				getPhotoPendingLot.setLotId(Integer.parseInt(rs.getString("LOTID")));
				getPhotoPendingLot.setModuleId(Integer.parseInt(rs.getString("MODULEID")));    
				getPhotoPendingLot.setPoicyId(rs.getString("POLICYID"));
				getPhotoPendingLot.setUserName(rs.getString("USERNAME")); 
				getPhotoPendingLot.setLotStatusFlag(rs.getInt("LOTSTATUSFLAG"));;
				getPhotoPendingLot.setPolicyName(rs.getString("POLICY_DETAILS"));
				
				
				String moduleName="";
		    	 if(rs.getInt("MODULEID")==1){
		    		 moduleName="Corp";
		    	 }else if(rs.getInt("MODULEID")==2){
		    		 moduleName="Bank Assurance";
		    	 }else if(rs.getInt("MODULEID")==4){
		    		 moduleName="Individual";
		    	 }
		    	 getPhotoPendingLot.setModuleName(moduleName);  
				
		    	 photoPendingLotList.add(getPhotoPendingLot);
			}
			
			if(photoPendingLotList.size()>0){
				policyDataBean.setPhotoPendingLotList(photoPendingLotList);
				statusBean = new StatusBean();
				statusBean.setStatusFlag(1);
				statusBean.setStatusMessage("records got it");
				policyDataBean.setStatusBean(statusBean);
			}else {
				//policyDataBean.setPhotoPendingLotList(photoPendingLotList);
				statusBean = new StatusBean();
				statusBean.setStatusFlag(0);
				statusBean.setStatusMessage("No data found");
				policyDataBean.setStatusBean(statusBean);
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			if(con==null){
				statusMessage ="Database connection null <br/>";
			}
			if(cstmt==null){
				statusMessage +="procedure related exception <br/>";
			}
			statusMessage +="Error in getPhotoPendingLot:  "+ex;
			statusBean = new StatusBean();
			statusBean.setStatusFlag(2);
			statusBean.setStatusMessage(statusMessage);
			policyDataBean.setStatusBean(statusBean);
			//throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return policyDataBean;
	}
	
	
	public PolicyDataBean getCardIdPhotoLot(int moduleId)throws DAOException 
	{
		Connection con = null;
		CallableStatement cstmt = null;
		GetPhotoPendingLot getPhotoPendingLot=null;
		ResultSet rs=null;
		int indexpos=0;
		PolicyDataBean policyDataBean = new PolicyDataBean();
		List<GetPhotoPendingLot> photoPendingLotList = new ArrayList<GetPhotoPendingLot>();
		String statusMessage="";
		StatusBean statusBean= null;
		try {
			con =getLiveMyConnection(1);
			con.setAutoCommit(false);
			cstmt = con.prepareCall(ProcedureConstants.PROC_GET_PHOTO_DLOAD_LOT_WEB);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			while (rs.next()) {
				getPhotoPendingLot=new GetPhotoPendingLot();
				getPhotoPendingLot.setCantactEmail(rs.getString("CONTACTEMAIL"));         
				getPhotoPendingLot.setXmlString(rs.getString("CARDS_XML"));
				getPhotoPendingLot.setLotCreated(rs.getString("LOTCREATED"));    
				getPhotoPendingLot.setLotId(Integer.parseInt(rs.getString("LOTID")));
				getPhotoPendingLot.setModuleId(Integer.parseInt(rs.getString("MODULEID")));    
				getPhotoPendingLot.setPoicyId(rs.getString("POLICYID"));
				getPhotoPendingLot.setUserName(rs.getString("USERNAME")); 
				getPhotoPendingLot.setLotStatusFlag(rs.getInt("LOTSTATUSFLAG"));;
				getPhotoPendingLot.setPolicyName(rs.getString("POLICY_DETAILS"));
				photoPendingLotList.add(getPhotoPendingLot);
			}
			
			if(photoPendingLotList.size()>0){
				policyDataBean.setPhotoPendingLotList(photoPendingLotList);
				statusBean = new StatusBean();
				statusBean.setStatusFlag(1);
				statusBean.setStatusMessage("records got it");
				policyDataBean.setStatusBean(statusBean);
			}else {
				//policyDataBean.setPhotoPendingLotList(photoPendingLotList);
				statusBean = new StatusBean();
				statusBean.setStatusFlag(0);
				statusBean.setStatusMessage("No data found");
				policyDataBean.setStatusBean(statusBean);
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			ex.printStackTrace();
			if(con==null){
				statusMessage ="Database connection null <br/>";
			}
			if(cstmt==null){
				statusMessage +="procedure related exception <br/>";
			}
			statusMessage +="Error in getCardIdPhotoLot:  "+ex;
			statusBean = new StatusBean();
			statusBean.setStatusFlag(2);
			statusBean.setStatusMessage(statusMessage);
			policyDataBean.setStatusBean(statusBean);
			//throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				//throw new DAOException();
			}
		}
		return policyDataBean;
	}
	
	//santhosh added
		public PolicyDataBean getEmployeeDetailsCardId(String cardId,int moduleId) throws DAOException{
			//System.out.println("enter collaing getEmployeeDetailsCardId  "+cardId+"             moduleId  "+ moduleId);
			logger.info("enter collaing getEmployeeDetailsCardId  cardId    "+cardId+"             moduleId  "+ moduleId);
			Connection con = null;
			CallableStatement cstmt = null;
			SinglePhotoBean singlePhotoBean=null;
			ResultSet rs=null;
			int indexpos=0;
			int serialNo=0;
			List<SinglePhotoBean> singlephotoList = new ArrayList<SinglePhotoBean>();
			PolicyDataBean policyDataBean = new PolicyDataBean();
			String statusMessage="";
			StatusBean statusBean= null;
			System.out.println("before try bloak");
			try {
				//System.out.println("after try bloak");
				con =getLiveMyConnection(moduleId);
				//System.out.println(" con   "+con);
				logger.info("con   "+con);
				//con.setAutoCommit(false);
					cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_IAUTH_COMB_ONLY_CARDID_DTLS);
					cstmt.setString(++indexpos, cardId);
					cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
					cstmt.execute();
					rs = (ResultSet) cstmt.getObject(indexpos);
				//System.out.println(" rs   "+rs);
				while (rs.next()) {
					System.out.println("while loop : moduleId   "+moduleId);
					String preAuthUrl= null;
					singlePhotoBean=new SinglePhotoBean();
					singlePhotoBean.setModuleId(moduleId);
					singlePhotoBean.setSerialNo(++serialNo);
					singlePhotoBean.setCardid(rs.getString("CARDID"));
					singlePhotoBean.setCardHolderName(rs.getString("CARDHOLDERNAME"));
					singlePhotoBean.setEmpid(rs.getString("EMPID"));
					if(rs.getString("SUMINSURED")!=null){
						singlePhotoBean.setSumInsured(rs.getString("SUMINSURED"));
					}else{
						singlePhotoBean.setSumInsured("0");
					}
					singlePhotoBean.setPolicyNo(rs.getString("POLICYNO"));
					singlePhotoBean.setPolicyId(rs.getString("POLICYID"));
					singlePhotoBean.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
					singlePhotoBean.setRelation(rs.getString("RELATION"));
					singlePhotoBean.setAge(rs.getInt("AGE"));
					singlePhotoBean.setDob(rs.getString("DOB"));
					singlePhotoBean.setPolicyFrom(rs.getString("POLICYFROM"));
					singlePhotoBean.setPolicyTo(rs.getString("POLICYTO"));
					singlePhotoBean.setDoj(rs.getString("DOJ"));
					singlePhotoBean.setDol(rs.getString("DOL"));
					singlePhotoBean.setEnrolmentExcep(rs.getString("ENROLMENT_EXCEPTION"));
					singlePhotoBean.setBlocking(rs.getString("BLOCKFLAG"));
					singlePhotoBean.setFamilyId(rs.getInt("FAMILYID"));
					singlePhotoBean.setInsuredId(rs.getString("INSUREDID"));
					singlePhotoBean.setCurrentDate(rs.getString("CURRENTDATE"));
					singlePhotoBean.setRenewalStatus(rs.getString("RENEWALSTATUS"));
					singlePhotoBean.setBano(rs.getString("BANO"));
					singlePhotoBean.setUwCode(rs.getString("UWCODE"));
					singlePhotoBean.setUwId(rs.getString("UWID"));
					singlePhotoBean.setGender(rs.getString("gender"));
					singlePhotoBean.setRid(rs.getString("RID"));
					if(rs.getString("DOL")!=null && rs.getString("CURRENTDATE")!=null){
						preAuthUrl = Util.compareTwoDates(rs.getString("DOL"),rs.getString("CURRENTDATE"));
					}
					if(singlePhotoBean.getBlocking()!=null){
						if(singlePhotoBean.getBlocking().equals("1")){
							preAuthUrl="Blocking";
						}
					}
					if(singlePhotoBean.getEnrolmentExcep()!=null){
						if(!singlePhotoBean.getEnrolmentExcep().equals("1")){
							preAuthUrl="Enrolement Exception";
						}
					}
					if(preAuthUrl==null){
						preAuthUrl="preauth";
						singlePhotoBean.setPreAuthUrl(preAuthUrl);
					}else{
						singlePhotoBean.setPreAuthUrl(preAuthUrl);
					}
					singlePhotoBean.setInceptionDate(rs.getString("INCEPTION_DATE"));
					singlePhotoBean.setSelfName(rs.getString("SELFNAME"));
					singlePhotoBean.setIssusingofficeid(rs.getString("ISSUINGOFFICEID"));
					singlePhotoBean.setSelfcardid(rs.getString("selfcardid"));
					singlephotoList.add(singlePhotoBean);
				}
				System.out.println("singlephotoList     "+singlephotoList);
				if(singlephotoList.size()>0){
					policyDataBean.setSinglephotoList(singlephotoList);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage("records got it");
					policyDataBean.setStatusBean(statusBean);
				}else {
					//policyDataBean.setSinglephotoList(singlephotoList);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage("No data found");
					policyDataBean.setStatusBean(statusBean);
				}
				
			}
			catch(Exception ex)
			{
				System.out.println("ex   :"+ex);
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in getCardIdPhotoLot:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				ex.printStackTrace();
				singlephotoList =null;
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertSelfCardDetails:"+ex);
					//throw new DAOException();
				}
			}
			return policyDataBean;
		}
		@Override
		public PolicyDataBean getEmployeeDetailsEmpId(String policyId,String empId, int moduleId) throws DAOException {
			logger.info("enter name of method getEmployeeDetailsEmpId");
			Connection con = null;
			CallableStatement cstmt = null;
			SinglePhotoBean singlePhotoBean=null;
			ResultSet rs=null;
			int indexpos=0;
			int serialNo=0;
			List<SinglePhotoBean> employeeList = new ArrayList<SinglePhotoBean>();
			PolicyDataBean policyDataBean = new PolicyDataBean();
			String statusMessage="";
			StatusBean statusBean= null;
			try {
				con =getLiveMyConnection(moduleId);
				con.setAutoCommit(false);
				//if(moduleId==1 || moduleId==2 || moduleId==4){
					cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_IAUTH_COMB_POLID_EMPID_DTLS);
					cstmt.setString(++indexpos, policyId);
					cstmt.setString(++indexpos, empId);
					cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
					logger.info("execute before procedure PROC_FOR_GET_IAUTH_COMB_POLID_EMPID_DTLS");
					cstmt.execute();
					logger.info("execute after procedure PROC_FOR_GET_IAUTH_COMB_POLID_EMPID_DTLS");
					rs = (ResultSet) cstmt.getObject(indexpos);
					
				//}
				while (rs.next()) {
					String preAuthUrl= null;
					singlePhotoBean=new SinglePhotoBean();
					singlePhotoBean.setModuleId(moduleId);
					singlePhotoBean.setSerialNo(++serialNo);
					singlePhotoBean.setCardid(rs.getString("CARDID"));
					singlePhotoBean.setCardHolderName(rs.getString("CARDHOLDERNAME"));
					singlePhotoBean.setEmpid(rs.getString("EMPID"));
					if(rs.getString("SUMINSURED")!=null){
						singlePhotoBean.setSumInsured(rs.getString("SUMINSURED"));
					}else{
						singlePhotoBean.setSumInsured("0");
					}
					singlePhotoBean.setPolicyNo(rs.getString("POLICYNO"));
					singlePhotoBean.setPolicyId(rs.getString("POLICYID"));
					singlePhotoBean.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
					singlePhotoBean.setRelation(rs.getString("RELATION"));
					singlePhotoBean.setAge(rs.getInt("AGE"));
					singlePhotoBean.setDob(rs.getString("DOB"));
					singlePhotoBean.setPolicyFrom(rs.getString("POLICYFROM"));
					singlePhotoBean.setPolicyTo(rs.getString("POLICYTO"));
					singlePhotoBean.setDoj(rs.getString("DOJ"));
					singlePhotoBean.setDol(rs.getString("DOL"));
					singlePhotoBean.setEnrolmentExcep(rs.getString("ENROLMENT_EXCEPTION"));
					singlePhotoBean.setBlocking(rs.getString("BLOCKFLAG"));
					singlePhotoBean.setFamilyId(rs.getInt("FAMILYID"));
					singlePhotoBean.setInsuredId(rs.getString("INSUREDID"));
					singlePhotoBean.setCurrentDate(rs.getString("CURRENTDATE"));
					singlePhotoBean.setRenewalStatus(rs.getString("RENEWALSTATUS"));
					singlePhotoBean.setBano(rs.getString("BANO"));
					singlePhotoBean.setUwCode(rs.getString("UWCODE"));
					singlePhotoBean.setUwId(rs.getString("UWID"));
					singlePhotoBean.setGender(rs.getString("gender"));
					singlePhotoBean.setRid(rs.getString("RID"));
					if(rs.getString("DOL")!=null && rs.getString("CURRENTDATE")!=null){
						preAuthUrl = Util.compareTwoDates(rs.getString("DOL"),rs.getString("CURRENTDATE"));
					}
					if(singlePhotoBean.getBlocking()!=null){
						if(singlePhotoBean.getBlocking().equals("1")){
							preAuthUrl="Blocking";
						}
					}
					if(singlePhotoBean.getEnrolmentExcep()!=null){
						if(!singlePhotoBean.getEnrolmentExcep().equals("1")){
							preAuthUrl="Enrolement Exception";
						}
					}
					if(preAuthUrl==null){
						preAuthUrl="preauth";
						singlePhotoBean.setPreAuthUrl(preAuthUrl);
					}else{
						singlePhotoBean.setPreAuthUrl(preAuthUrl);
					}
					singlePhotoBean.setInceptionDate(rs.getString("INCEPTION_DATE"));
					singlePhotoBean.setSelfName(rs.getString("SELFNAME"));
					singlePhotoBean.setIssusingofficeid(rs.getString("ISSUINGOFFICEID"));
					singlePhotoBean.setSelfcardid(rs.getString("selfcardid"));
					employeeList.add(singlePhotoBean);
				}
				
				if(employeeList.size()>0){
					policyDataBean.setSinglephotoList(employeeList);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage("records got it");
					policyDataBean.setStatusBean(statusBean);
				}else {
					//policyDataBean.setSinglephotoList(singlephotoList);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage("No data found");
					policyDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in getCardIdPhotoLot:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				ex.printStackTrace();
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertSelfCardDetails:"+ex);
					throw new DAOException();
				}
			}
			return policyDataBean;
		}
		@Override
		public PolicyDataBean getEmployeeDetailsBANo(String policyId, String bano,int moduleId) throws DAOException {
			Connection con = null;
			CallableStatement cstmt = null;
			SinglePhotoBean singlePhotoBean=null;
			ResultSet rs=null;
			int indexpos=0;
			int serialNo=0;
			List<SinglePhotoBean> employeeList = new ArrayList<SinglePhotoBean>();
			PolicyDataBean policyDataBean = new PolicyDataBean();
			String statusMessage="";
			StatusBean statusBean= null;
			try {
				con =getMyConnection(moduleId);
				con.setAutoCommit(false);
				if(moduleId==1 || moduleId==2 || moduleId==4){
					cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_IAUTH_COMB_POLID_BANO_DTLS);
					cstmt.setString(++indexpos, policyId);
					cstmt.setString(++indexpos, bano);
					cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
					cstmt.execute();
					rs = (ResultSet) cstmt.getObject(indexpos);
				}
				while (rs.next()) {
					String preAuthUrl= null;
					singlePhotoBean=new SinglePhotoBean();
					singlePhotoBean.setModuleId(moduleId);
					singlePhotoBean.setSerialNo(++serialNo);
					singlePhotoBean.setCardid(rs.getString("CARDID"));
					singlePhotoBean.setCardHolderName(rs.getString("CARDHOLDERNAME"));
					singlePhotoBean.setEmpid(rs.getString("EMPID"));
					if(rs.getString("SUMINSURED")!=null){
						singlePhotoBean.setSumInsured(rs.getString("SUMINSURED"));
					}else{
						singlePhotoBean.setSumInsured("0");
					}
					singlePhotoBean.setPolicyNo(rs.getString("POLICYNO"));
					singlePhotoBean.setPolicyId(rs.getString("POLICYID"));
					singlePhotoBean.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
					singlePhotoBean.setRelation(rs.getString("RELATION"));
					singlePhotoBean.setAge(rs.getInt("AGE"));
					singlePhotoBean.setDob(rs.getString("DOB"));
					singlePhotoBean.setPolicyFrom(rs.getString("POLICYFROM"));
					singlePhotoBean.setPolicyTo(rs.getString("POLICYTO"));
					singlePhotoBean.setDoj(rs.getString("DOJ"));
					singlePhotoBean.setDol(rs.getString("DOL"));
					singlePhotoBean.setEnrolmentExcep(rs.getString("ENROLMENT_EXCEPTION"));
					singlePhotoBean.setBlocking(rs.getString("BLOCKFLAG"));
					singlePhotoBean.setFamilyId(rs.getInt("FAMILYID"));
					singlePhotoBean.setInsuredId(rs.getString("INSUREDID"));
					singlePhotoBean.setCurrentDate(rs.getString("CURRENTDATE"));
					singlePhotoBean.setRenewalStatus(rs.getString("RENEWALSTATUS"));
					singlePhotoBean.setBano(rs.getString("BANO"));
					singlePhotoBean.setUwCode(rs.getString("UWCODE"));
					singlePhotoBean.setUwId(rs.getString("UWID"));
					singlePhotoBean.setGender(rs.getString("gender"));
					singlePhotoBean.setRid(rs.getString("RID"));
					if(rs.getString("DOL")!=null && rs.getString("CURRENTDATE")!=null){
						preAuthUrl = Util.compareTwoDates(rs.getString("DOL"),rs.getString("CURRENTDATE"));
					}
					if(singlePhotoBean.getBlocking()!=null){
						if(singlePhotoBean.getBlocking().equals("1")){
							preAuthUrl="Blocking";
						}
					}
					if(singlePhotoBean.getEnrolmentExcep()!=null){
						if(!singlePhotoBean.getEnrolmentExcep().equals("1")){
							preAuthUrl="Enrolement Exception";
						}
					}
					if(preAuthUrl==null){
						preAuthUrl="preauth";
						singlePhotoBean.setPreAuthUrl(preAuthUrl);
					}else{
						singlePhotoBean.setPreAuthUrl(preAuthUrl);
					}
					singlePhotoBean.setInceptionDate(rs.getString("INCEPTION_DATE"));
					singlePhotoBean.setSelfName(rs.getString("SELFNAME"));
					singlePhotoBean.setIssusingofficeid(rs.getString("ISSUINGOFFICEID"));
					singlePhotoBean.setSelfcardid(rs.getString("selfcardid"));
					singlePhotoBean.setModuleId(moduleId);
					employeeList.add(singlePhotoBean);
				}
				
				if(employeeList.size()>0){
					policyDataBean.setSinglephotoList(employeeList);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage("records got it");
					policyDataBean.setStatusBean(statusBean);
				}else {
					//policyDataBean.setSinglephotoList(singlephotoList);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage("No data found");
					policyDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in getCardIdPhotoLot:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				ex.printStackTrace();
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertSelfCardDetails:"+ex);
					//throw new DAOException();
				}
			}
			return policyDataBean;
		}
		@Override
		public UploadPhotoDetails insertUploadPolicyPhoto(UploadPhotoDetails uploadPhotoDetails, int moduleId)throws DAOException 
		{
			logger.info("enter insertUploadPolicyPhoto");
			Connection con = null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			int indexpos=0;
			
			UploadPhotoDetails uploadPhotoDetails1 = null;
			String statusMessage="";
			StatusBean statusBean= null;
			try {
				con =getLiveMyConnection(moduleId);
				con.setAutoCommit(false);
				if(moduleId==1 || moduleId==2 || moduleId==4){
					uploadPhotoDetails1 = new UploadPhotoDetails();
					cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_PROC_UPDATE_PHOTO_STATUS);
					
					cstmt.setString(++indexpos, uploadPhotoDetails.getCardId());
					cstmt.setInt(++indexpos, uploadPhotoDetails.getPolicyId());
					cstmt.setInt(++indexpos, uploadPhotoDetails.getUserId());
					cstmt.setString(++indexpos, uploadPhotoDetails.getInsuredId());
					cstmt.setInt(++indexpos, uploadPhotoDetails.getStatus());
					cstmt.registerOutParameter(++indexpos, OracleTypes.NUMBER);
					cstmt.registerOutParameter(++indexpos, OracleTypes.VARCHAR);
					cstmt.execute();
					con.commit();
					uploadPhotoDetails1.setStatus(cstmt.getInt(indexpos-1));
					uploadPhotoDetails1.setStatusMsg(cstmt.getString(indexpos));
					logger.info("status "+cstmt.getInt(indexpos-1));
					logger.info("status message : "+cstmt.getString(indexpos));
					
					
					if(cstmt.getInt(indexpos-1) ==1){
						statusBean = new StatusBean();
						statusBean.setStatusFlag(1);
						statusBean.setStatusMessage(cstmt.getString(indexpos));
						uploadPhotoDetails1.setStatusBean(statusBean);
					}else if(cstmt.getInt(indexpos-1) ==0){
						statusBean = new StatusBean();
						statusBean.setStatusFlag(0);
						statusBean.setStatusMessage(cstmt.getString(indexpos));
						uploadPhotoDetails1.setStatusBean(statusBean);
					}
				}
			}
			catch(Exception ex)
			{
				uploadPhotoDetails1 = new UploadPhotoDetails();
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in getCardIdPhotoLot:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				uploadPhotoDetails1.setStatusBean(statusBean);
				ex.printStackTrace();
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertSelfCardDetails:"+ex);
					//throw new DAOException();
				}
			}
			return uploadPhotoDetails1;
		}
		//santhosh added end
}
