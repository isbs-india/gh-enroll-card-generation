package com.persistence.common;

import java.util.List;

import com.exception.DAOException;
import com.model.EndorsementBean;
import com.model.InsertLotDetailsBean;
import com.model.PolicyDataBean;
import com.model.PolicyListDataBean;


public interface EnrollAuxDAO {
	
	public PolicyDataBean getPoliciesDDLDetails(int moduleId)  throws DAOException;
	public List<EndorsementBean> getEndorDDLDetails(int policyId, int moduleId)  throws DAOException;
	public int putSearchDetails(InsertLotDetailsBean insertlotBeanObj)  throws DAOException;
	public PolicyDataBean getLotDetails(int isCron, int moduleId) throws DAOException; 
	public int insertSelfCardDetails(String xml,String param) throws DAOException;
	public int insertWINDSelfCardDetails(String xml,String param) throws DAOException;
	//inserting xml,param,endorsement,policyId,lotUserId to GHPL database 
	public PolicyDataBean insertGHPLSelfCardDetails(String xml, String param,String endorsement,String policyId,String lotUserId) throws DAOException;
	public PolicyDataBean insertGHPLSelfCardDetailsInActive(String xml, String param,String endorsement,String policyId,String lotUserId) throws DAOException;
	public PolicyDataBean getPoliciesDetails(int moduleId) throws DAOException;
	public PolicyDataBean getLabelLotDetails(int isCron,int moduleId) throws DAOException;
	public int insertWINDLabelDetails(String xml,int moduleId) throws DAOException;
	
	
	public PolicyDataBean getPoliciesDDLDetailsInActive(int moduleId)  throws DAOException;
	public List<EndorsementBean> getEndorDDLDetailsInActive(int policyId, int moduleId)  throws DAOException;
	public int insertSelfCardDetailsInActive(String xml,String param) throws DAOException;
	public PolicyDataBean getLotDetailsInActive(int isCron,int moduleId) throws DAOException;

	public  PolicyListDataBean insertPolicyNumberDetails(String xml,String moduleUserId,int moduleId) throws DAOException;
	public  PolicyListDataBean getPolicyLotDetails(int isCron,int moduleId) throws DAOException;

}
