package com.persistence.common;

import com.exception.DAOException;
import com.model.PolicyDataBean;
import com.model.UploadPhotoDetails;

public interface PhotoUploadDAO {
	public UploadPhotoDetails insertUploadBankPolicyPhotos(UploadPhotoDetails uploadPhotoDetails,int moduleId) throws DAOException;
	public UploadPhotoDetails insertPhotoCardDetails(UploadPhotoDetails uploadPhotoDetails,String xml,int moduleId)throws DAOException;
	public PolicyDataBean getPhotoPendingLot(int moduleId)throws DAOException;
	public PolicyDataBean getCardIdPhotoLot(int moduleId)throws DAOException;
	
	public PolicyDataBean getEmployeeDetailsCardId(String insuredorempcode,int moduleId) throws DAOException;
	public PolicyDataBean getEmployeeDetailsEmpId(String policyId,String empId,int moduleId) throws DAOException;
	public PolicyDataBean getEmployeeDetailsBANo(String policyId,String bano,int moduleId) throws DAOException;
	public UploadPhotoDetails insertUploadPolicyPhoto(UploadPhotoDetails uploadPhotoDetails,int moduleId) throws DAOException;
}
