package com.persistence.login;

import com.exception.DAOException;
import com.model.LoginBean;

public interface LoginDAO {
	public  LoginBean getUserDetails(String uname,String pwd,int serverip,String hostName) throws DAOException;
}
