package com.persistence.login;

import com.exception.DAOException;
import com.model.LoginBean;
import com.persistence.common.*;

public class LoginDAOManager {
	
	
	
	public static LoginBean getUserDetails(String userName,String pwd, int serverIp,String hostName)throws DAOException {
		try {
			return DAOFactory.getInstance().getLoginDAO().getUserDetails(userName,pwd,serverIp,hostName);
		} catch (DAOException e) {

		}
		return null;
	}

}
