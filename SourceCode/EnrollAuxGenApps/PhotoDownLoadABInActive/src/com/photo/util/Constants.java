package com.photo.util;


public class Constants {
	public static final String JNDI_KEY="java:comp/env/jdbc/testDB";
	
	public static final String alertsEmailId="alerts@isharemail.in";
	
	public static final String LOG4JConfig="/root/MyCrons/keys/PhotoDownLoadlog4jAB.properties";
	public static final String DBConfig="/root/MyCrons/keys/config.properties";
	public static final String PDFConfig="/root/MyCrons/keys/pdf.properties";
	
	// local sytem properties files 
		/*public static final String LOG4JConfig="D:/appconfig/PhotoDownLoadlog4jAB.properties";
		public static final String DBConfig="D:/appconfig/config.properties";
		public static final String PDFConfig="D:/appconfig/pdf.properties";*/
	
}
