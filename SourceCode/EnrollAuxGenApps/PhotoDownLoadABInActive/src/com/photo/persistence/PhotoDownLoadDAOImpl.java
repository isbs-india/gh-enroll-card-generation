package com.photo.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.photo.exception.DAOException;
import com.photo.model.CardIDPhotoBean;
import com.photo.util.DBConn;
import com.photo.util.ProcedureConstants;

public class PhotoDownLoadDAOImpl extends DBConn implements PhotoDownLoadDAO {
	Logger logger =(Logger) Logger.getInstance("UploadPhotosDAOImpl");

	@Override
	public List<CardIDPhotoBean> getPhotoCardLotList(int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		CardIDPhotoBean cardIDPhotoBean=null;
		ResultSet rs=null;
		int indexpos=0;
		List<CardIDPhotoBean> getPhotoCardLotList = new ArrayList<CardIDPhotoBean>();
		try {
			con = getMyConnection(1);
			con.setAutoCommit(false);
				cstmt = con.prepareCall(ProcedureConstants.PROC_GET_PHOTO_DLOAD_LOT_CRON);
				cstmt.setInt(++indexpos,moduleId);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(indexpos);
			
			while (rs.next()) {
				cardIDPhotoBean=new CardIDPhotoBean();
				cardIDPhotoBean.setLotId(rs.getInt("LOTID"));
				cardIDPhotoBean.setModuleId(rs.getInt("MODULEID"));    
				cardIDPhotoBean.setXmlString(rs.getString("CARDS_XML"));
				cardIDPhotoBean.setCantactEmail(rs.getString("CONTACTEMAIL"));         
				cardIDPhotoBean.setLotCreated(rs.getString("LOTCREATED"));    
				cardIDPhotoBean.setPoicyId(rs.getInt("POLICYID"));
				cardIDPhotoBean.setUserName(rs.getString("USERNAME")); 
				getPhotoCardLotList.add(cardIDPhotoBean);
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			System.out.println("exception     >>"+ex);
     		
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return getPhotoCardLotList;
	}

	public int updatePhotoCardLotStatus(int lotNumber, int lotStatus, int moduleId)throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		int outputStatus=0;
		try
		{
			con =getMyConnection(1);
			con.setAutoCommit(false);
			cstmt = con.prepareCall(ProcedureConstants.PHOTO_DOWNLOAD_LOT_UPDATE);
			cstmt.setInt(++indexpos,lotNumber);
			cstmt.setInt(++indexpos,lotStatus);
			cstmt.setInt(++indexpos,moduleId);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			con.commit();
			outputStatus = cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN putLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return outputStatus;
	}
}
