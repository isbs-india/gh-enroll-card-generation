package com.photo.persistence;

import java.util.List;

import com.photo.exception.DAOException;
import com.photo.model.CardIDPhotoBean;

public interface PhotoDownLoadDAO {
	public List<CardIDPhotoBean> getPhotoCardLotList(int moduleId) throws DAOException;
	public int updatePhotoCardLotStatus(int lotNumber, int status,int moduleId)throws DAOException;
}
