package com.photo.action;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.photo.model.CardIDPhotoBean;
import com.photo.persistence.PhotoDownLoadDAOManager;
import com.photo.util.MyProperties;

public class PhotoDownLoadAction {
	static Logger logger = Logger.getLogger("PhotoDownLoadAction");
	/**
	 * 
	 * @param args
	 * @throws IOException
	 * this method cron will execution stated
	 * at the time of execution start one lock file generated after that featching the data photo cards list from database
	 * lot number and cards list to xml string 
	 * contact email 
	 * xml String is not empty split all cards to arry object that object will
	 * connect ftp server 
	 * arry object in loop valid  jpg file or not create .xlsx file s.no cardid jpgfile ftp status of headers in this file
	 * at time writing .xlsx file all are jpg files in this lot generate lot number .zip file created
	 * the cards add to list object after that update .xlsx file already exist in ftp server or not
	 * after that database update in crop module if status success send mail to contact email 
	 * this process will be repeated end of the list object of photo cards
	 * after that delete the lock file
	 */
	public static void main(String[] args) throws IOException {
		 MyProperties myResources = new MyProperties();
		   logger = myResources.getLogger();
		 String LCKPATH = myResources.getMyProperties("photoValidDownloadPhotosLckAB");
			File ff = new File(LCKPATH);
			if (ff.exists()) {
				System.out.println("#######   Please delete Leak file  ######################");
				logger.error("#############  Please delete Leak file  ######################");
				return;
			} else {
				// step 1
				System.out.println("#######   enter else block  ######################");
				 logger.info("#############  start PHOTO UPLOAD  ######################");
				File file1 = new File(LCKPATH);
				System.out.println("file    "+file1);
				file1.createNewFile();
				FileWriter fw = new FileWriter(file1.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				Date date = new Date();
				bw.write("" + date);
				bw.close();
		
		System.out.println("#######   PhotoDownLoadAction start  ######################");
		logger.error("#############  PhotoDownLoadAction start  ######################");
			try{
					
					List<CardIDPhotoBean> getPhotoCardLotList = new ArrayList<CardIDPhotoBean>();
					getPhotoCardLotList = PhotoDownLoadDAOManager.getPhotoCardLotList(2);
					logger.info("getPhotoCardLotList  "+getPhotoCardLotList.size());
					System.out.println("getPhotoCardLotList  "+getPhotoCardLotList.size());
					if(getPhotoCardLotList!=null && getPhotoCardLotList.size()>0){
						int lotNumber = 0;
						String xmlString ="";
						String contactEmail="";
						FTPClient client = new FTPClient();
						
						String photodestinationftp_host=null;
						String photodestinationftp_username = null;
						String photodestinationftp_password = null;
						String PHOTOUPLOADS_TEMP= null;
						String FTPHOST=null;
						String FTPUName=null;
						String FTPPwd=null;
						String ftpupload_ftppath=null;
						boolean result=false;
						String dbFileName = null;
						String ftpFileName = null;
						MyProperties myResources1 = new MyProperties();
						
						photodestinationftp_host = myResources1.getMyProperties("photodestinationftp_host");
						photodestinationftp_username = myResources1.getMyProperties("photodestinationftp_username");
						photodestinationftp_password = myResources1.getMyProperties("photodestinationftp_password");
						String photodownload = myResources.getMyProperties("photodownload_file_path");
						String photodownloadzip = myResources.getMyProperties("photodownload_file_path_zip");
						
						PHOTOUPLOADS_TEMP = myResources.getMyProperties("photodestination_PHOTOUPLOADS");
						
		    			
		    			int policyId = 0;
		    			int lotId = 0;
			            /*FTPFile[] foldersList = client.listFiles(ftpupload_ftppath);  
			            System.out.println("FTP folder length    : "+foldersList.length);*/
		    			 Map<String, Object[]> data =null;
						for(int i=0;i<getPhotoCardLotList.size();i++){
							int status =0;
							lotNumber = getPhotoCardLotList.get(i).getLotId();
							xmlString = getPhotoCardLotList.get(i).getXmlString();
							contactEmail = getPhotoCardLotList.get(i).getCantactEmail();
							if(lotNumber!=0){
						try{
							 String subDirectory = photodownload + lotNumber;
	                    	 File newsubDir = new File(subDirectory);
	                         boolean subcreated = newsubDir.mkdirs();
	                         if (subcreated) {
	                            // System.out.println("CREATED the sub directory: " + newsubDir);
	                         } else {
	                            // System.out.println("NOT create the sub directory: " + newsubDir);
	                         }
							if(xmlString!=null && !xmlString.equals("")){
							 List<String> cardList = new ArrayList<String>();
							 String xmlStringobj[] = xmlString.split(",");
							
							 int count=1;
							 data = new TreeMap<String, Object[]>();
							 data.put("1", new Object[] { "S.No", "CardId","Ftp Status per JPG file" });
							 int m = 1;
							 for(int j=0;j<xmlStringobj.length;j++){
								 if(xmlStringobj[j]!=""){
									try{
									    client.connect(photodestinationftp_host);
						                int replyCode = client.getReplyCode();
						                if (!FTPReply.isPositiveCompletion(replyCode)) {
						                	if (ff.exists()) {
												file1.delete();
											}
						                	logger.info("Operation failed. FTP Server reply code: " + replyCode);
						                    return;
						                }
							            result = client.login(photodestinationftp_username, photodestinationftp_password);
							            logger.info("FTP client  status    : "+result);
							            if (!result) {
							            	if (ff.exists()) {
												file1.delete();
											}
							            	 logger.info("Could not login to the  FTP server");
							            	 return;
							            }
								 } catch(Exception e){
					    				    if (ff.exists()) {
												file1.delete();
											}
					    				 	logger.error("ftp connection failed  Please check it:   ping "+photodestinationftp_host +" -t if not ping ip address contact ITADMIN" );
										 	return;
					 			     }   
							            client.setFileType(FTP.BINARY_FILE_TYPE);
							            client.enterLocalPassiveMode();
							            
									 //System.out.println(xmlStringobj[j]);
									 
									 String localDirectory = newsubDir+File.separator+ xmlStringobj[j]+".jpg";
									String remoteFile2 = PHOTOUPLOADS_TEMP+xmlStringobj[j]+".jpg";
									/*FTPFile[] list2 = client.listFiles(PHOTOUPLOADS_TEMP);
									System.out.println(list2.length);
									
									  for (int l = 0; l < list2.length; l++) {
			            		        	 String subcurrentFileName = list2[l].getName();
			            		             remoteFile2 = list2[l].getName();
			            		             if(list2[l].getName().equals(".") || list2[l].getName().equals("..")){
			            		            	 continue;
			            		             }
			            		             if(list2[l].isDirectory()){
			            		            	 String subDirectory = localDirectory + subcurrentFileName;
			            		             }
									  }*/
									// FTPFile[] files1 = client.listFiles(PHOTOUPLOADS_TEMP);
            		                // List list = printFileDetails(files1);
            		               // boolean  filecheckstatus =  list.contains(xmlStringobj[j]+".jpg");
									
									boolean filecheckstatus = checkFileExists(remoteFile2 ,client);
									client.disconnect();
									if(filecheckstatus){
										data.put(++count+"",new Object[] { m++ ,xmlStringobj[j],"Exist in ftp" });
										client.connect(photodestinationftp_host);
						                int replCode = client.getReplyCode();
						                if (!FTPReply.isPositiveCompletion(replCode)) {
						                	if (ff.exists()) {
												file1.delete();
											}
						                	logger.info("Operation failed. FTP Server reply code: " + replCode);
						                    return;
						                }
							            result = client.login(photodestinationftp_username, photodestinationftp_password);
							            logger.info("FTP client  status    : "+result  +"cleint code :       "+client.getReplyCode()+"       client reply message :"+client.getReplyString());
							            if (!result) {
							            	if (ff.exists()) {
												file1.delete();
											}
							            	 logger.info("Could not login to the  FTP server");
							            	 return;
							            }
							            client.setFileType(FTP.BINARY_FILE_TYPE);
							            client.enterLocalPassiveMode();
			                    	 File downloadFile2 = new File(localDirectory);
	            		             BufferedOutputStream outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadFile2));
	            		             client.retrieveFile(remoteFile2, outputStream2);
	            		             logger.info("ftp client retrive file stream  reply code :"+client.getReplyCode() +"reply String "+client.getReplyString());
	            		             outputStream2.close();
	            		             client.disconnect();
									}else{
										data.put(++count+"",new Object[] { m++ ,xmlStringobj[j],"Not Exist in ftp" });
									}
									//client.disconnect();
								 }
							 }
					}
							 //write zip file 
							 String destZipFile= photodownloadzip+lotNumber+".zip";
			        		 File zipfile = new File(destZipFile);
			        		 if(zipfile.exists()){
			        			 zipfile.delete();
			        			 destZipFile= subDirectory+".zip";
			        			 zipfile = new File(destZipFile);
			        		 }
							
							if (newsubDir.exists() && newsubDir.isDirectory()) {
								createExcelfie(data,lotNumber);
								  //System.out.println("destZipFile  "+destZipFile);
								  //System.out.println("srcFolder  "+srcFolder);
								  String command ="zip -rj " + destZipFile +  " " + newsubDir;
								  //System.out.println("command : "+command);
								  logger.info(command);
									  try {
									      Process proc = Runtime.getRuntime().exec(command);
									      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
									      String line = null;
								            while ((line = in.readLine()) != null) {
								                //System.out.println(line);
								            }
								             status =3;
									      //System.out.println("status : "+status);
									     // logger.info("status :"+status);
									  }catch (Exception e) {
										   status=4;
										   logger.error("status :"+status);
										   if (ff.exists()) {
												file1.delete();
											}
									      e.printStackTrace();
									      logger.error(e);
									   }
							}
							
							}catch(Exception e){
								 status=4;
								e.printStackTrace();
								if (ff.exists()) {
									file1.delete();
								}
								logger.error(e.getMessage());
							}
								
						logger.info("before status update :  lotNumber :"+lotNumber +"          status   :"+status);
							int statusupdate = PhotoDownLoadDAOManager.updatePhotoCardLotStatus(lotNumber,status,2);
							logger.info("after status update   statusupdate :"+statusupdate);
							logger.info("contactEmail     "+contactEmail);
							if(statusupdate==1 && !contactEmail.equals("")){
								SendMail_preAuth mailsend=new SendMail_preAuth();
								//mailsend.SendMail_intimationreports("Mahindra SATYAM Intimation Report" , "Dear Sir / Madam, <br><br>Please find attached intimation report for Mahindra SATYAM.<br><br> Number of intimations received on "+dateFormat.format(cal.getTime())+"  are "+totalintimationcount+" <br><br> Regards, <br> GHPL TEAM","Mahindra_Satyam_Report.xls","ghplintimations_list@ghpltpa.com","","Mahindra_Satyam_Report.xls");
								int mailstatus = mailsend.SendMail_intimationreports("PHOTO DOWNLOAD STATUS" , "Dear Sir / Madam, <br><br>Please find attached photo download status report.<br><br> Number of photo available in ftp server   <br><br> Regards, <br> GHPL TEAM",lotNumber,lotNumber+".xlsx",contactEmail,"","");
								logger.info("mailstatus  : "+mailstatus);
								System.out.println("mailstatus  : "+mailstatus);
							}
							
							}//lotNumber zero checking
							
						}//end for loop
						if (ff.exists()) {
							file1.delete();
						}
					}else{
						if (ff.exists()) {
							file1.delete();
						}
					}
			}catch(Exception e){
				if (ff.exists()) {
					file1.delete();
				}
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			}//end else
			
			System.out.println("#######   PhotoDownLoadAction end  ######################");
			logger.error("#############  PhotoDownLoadAction end  ######################");
			
	}
	
	
	private static List printFileDetails(FTPFile[] files) {
	      List<String> list = new ArrayList<String>();
	      for (FTPFile file : files) {
	          String details = file.getName();
	         // System.out.println(details);
	          list.add(details);
	      }
	      return list;
	  }
	
	static boolean checkFileExists(String filePath ,FTPClient ftpClient) throws Exception {
	    InputStream inputStream = ftpClient.retrieveFileStream(filePath);
	    int returnCode = ftpClient.getReplyCode();
	    logger.info("ftpClient file checking reply code "+ftpClient.getReplyCode()  +    "      message :  "+ftpClient.getReplyString());
	    if (inputStream == null || returnCode == 550) {
	        return false;
	    }
	    return true;
	}
	
	
	public static int createExcelfie(Map<String, Object[]> data, int lotNumber){
		 int statuscode =0;
		 MyProperties myResources = new MyProperties();
		 String photodownload = myResources.getMyProperties("photodownload_file_path");
		 String subDirectory = photodownload + lotNumber;
    	 File newsubDir = new File(subDirectory);
         boolean subcreated = newsubDir.mkdirs();
         if (subcreated) {
             System.out.println("CREATED the sub directory: in xl file " + newsubDir);
         } else {
             System.out.println("NOT create the sub directory: in xm file " + newsubDir);
         }
		// make sure it's a directory
		XSSFWorkbook workbook = new XSSFWorkbook();
		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("DOWNLOAD PHOTOS PER LOT");
		sheet.setColumnWidth(0, 1500);
		sheet.setColumnWidth(1, 5000);
		sheet.setColumnWidth(2, 2600);
		sheet.setVerticallyCenter(true);
		sheet.setTabColor(4);
		sheet.setDisplayRowColHeadings(true);
		// This data needs to be written (Object[])
		Set<String> keyset = data.keySet();
		int rownum = 0;
		int k = 1;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String) {
					cell.setCellValue((String) obj);
				} else if (obj instanceof Integer) {
					cell.setCellValue(k++);
				}
			}
		}
		// Write the workbook in file system
		FileOutputStream out;
		try {
			out = new FileOutputStream( new File(subDirectory+ File.separator+lotNumber + ".xlsx"));
			workbook.write(out);
			out.close();
			statuscode=1;
			//System.out.println("Excel file  : "+EXCELDOCS+ lotId + ".xlsx");
		} catch (Exception e) {
			statuscode=2;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return statuscode;
		
	}
	
	
	
	/*public static ArrayList CreateList(NodeList ArrayNodeList) throws Exception
	{
	    ArrayList List = new ArrayList();

	    for(int x = 0; x < ArrayNodeList.getLength();x++)
	    {
	        if(!(ArrayNodeList.item(x) instanceof Text)) {
	            org.w3c.dom.Node curNode = ArrayNodeList.item(x);

	            NodeList att = curNode.getChildNodes();
	            String Location = ArrayNodeList.item(x).getAttributes().item(0).getNodeValue();
	            Object newOne = CreateObject(att, Location);
	            List.add(newOne);

	        }
	    }
	    return List;
	}*/



	
	
	
	public static List<String> createCardIdList(String  xmlString){
		 List<String> cardList = new ArrayList<String>();
		
		 // create a new DocumentBuilderFactory
	      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	      try {
	         // use the factory to create a documentbuilder
	         DocumentBuilder builder = factory.newDocumentBuilder();
	         // create a new document from input stream and an empty systemId
	        // Document doc = builder.parse(xmlString);
	         Document doc = builder.parse(new InputSource(new StringReader(xmlString)));

	         // get the first element
	         Element element = doc.getDocumentElement();

	         // get all child nodes
	         NodeList nodes = element.getChildNodes();

	         // print the text content of each child
	         for (int i = 0; i < nodes.getLength(); i++) {
	            System.out.println("" + nodes.item(i).getTextContent());
	            cardList.add(nodes.item(i).getTextContent());
	         }
	      } catch (Exception ex) {
	         ex.printStackTrace();
	      }
		
		return cardList;
		
	}
	
	/*private static String getTagValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
        return node.getNodeValue();
    }*/

}
