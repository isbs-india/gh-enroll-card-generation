package com.photo.util;

public class ProcedureConstants { 
	public static final String PROC_GET_PHOTO_DLOAD_LOT_CRON ="{call PKG_ECARDS_IN_PDF.PROC_GET_PHOTO_DLOAD_LOT_CRON(?,?)}";
	//public static final String PROC_UPDATE_IPID_FROM_CARDID ="{call PKG_ECARDS_IN_PDF.PROC_UPDATE_IPID_FROM_CARDID(?,?,?)}";
	//public static final String PHOTO_UPLOAD_LOT_UPDATE ="{call PKG_ECARDS_IN_PDF.PHOTO_UPLOAD_LOT_UPDATE(?,?,?)}";
	public static final String PHOTO_DOWNLOAD_LOT_UPDATE ="{call PKG_ECARDS_IN_PDF.PHOTO_DOWNLOAD_LOT_UPDATE(?,?,?,?)}";
}

