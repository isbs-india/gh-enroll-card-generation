package com.photo.action;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;

import org.apache.log4j.Logger;






import com.photo.util.MyProperties;

import java.util.*;
import java.util.regex.*;

public class SendMail_preAuth
{
	private static Logger logger=Logger.getLogger("SendMail_preAuth AllReportsData");
	MyProperties myResources;
    String username;
    String password;
    String mail_transport_protocol;
    String mail_smtp_port;
    String mail_smtp_host;
    String mail_smtp_auth;
    String mail_messageText;
    String sec_username;
    String sec_password;
    String sec_mail_transport_protocol;
    String sec_mail_smtp_port;
    String sec_mail_smtp_host;
    String sec_mail_smtp_auth;
    String sec_mail_messageText;
    String bccMailID;
    String sec_bccMailID;
    String mail_from;
    String sec_mail_from;
    
    String fileName=null;
  String filePath=null;
  String FTPHOST=null;
  String FTPUName=null;
  String FTPPwd=null;
  
  String docpath=null;
  String docpath_http=null;
    
	
		public int SendMail_intimationreports(String subject, String messageText,int lotNumber,String displayfilenme,String TO,String cc,String bcc)
		        throws MessagingException
		  {
			int k=0;  
		    myResources = new MyProperties();
	       try
	        { 
	    	   String photodownload = myResources.getMyProperties("photodownload_file_path");
                username = myResources.getMyProperties("mail_username");
                password = myResources.getMyProperties("mail_password");
                mail_transport_protocol = myResources.getMyProperties("mail_transport_protocol");
                mail_smtp_port = myResources.getMyProperties("mail_smtp_port");
                mail_smtp_host = myResources.getMyProperties("mail_smtp_host");
                mail_smtp_auth = myResources.getMyProperties("mail_smtp_auth");
                mail_messageText = myResources.getMyProperties("mail_messageText");
                mail_from = myResources.getMyProperties("mail_from");		
                
                logger.info("username  "+username);
                logger.info("password  "+password);
                logger.info("mail_transport_protocol  "+mail_transport_protocol);
                logger.info("mail_smtp_port  "+mail_smtp_port);
                logger.info("mail_smtp_host  "+mail_smtp_host);
                logger.info("mail_smtp_auth  "+mail_smtp_auth);
                logger.info("mail_messageText  "+mail_messageText);
                logger.info("mail_from  "+mail_from);
               
	        
	            Properties props = new Properties();
	            props.put("mail.transport.protocol", mail_transport_protocol);
	            props.put("mail.smtp.port", mail_smtp_port);
	            props.put("mail.smtp.host", mail_smtp_host);
	            props.put("mail.smtp.auth", mail_smtp_auth);
	            props.put("mail.smtp.ssl.trust", "*");
	            props.put("mail.smtp.starttls.enable", "true");
	            Authenticator auth = new SMTPAuthenticator();
	            
	            Session session = Session.getInstance(props, auth);
	            MimeMessage message = new MimeMessage(session);
	            message.setContent(messageText, mail_messageText);
	            message.setSubject(subject);
	            
	            String toarray[] = TO.split(",");
	            
		           // Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
		            Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
		            
		            for(int i = 0; i < toarray.length; i++)
		            {
		            	 InternetAddress fromAddress = new InternetAddress(mail_from);
		                // System.out.println("fromAddress"+fromAddress);
		                 
		                 message.setFrom(fromAddress);
		                // System.out.println((new StringBuilder("tomail=")).append(toarray[i]).toString());
		                 //VALIDATING TO EMAILID
		         		//Validating HR MAIL ID
		         		Matcher g=p.matcher(toarray[i]);
		         		//Matcher m=p.matcher(args[0]);
		         		boolean b=g.matches();
		         		if(b==true)
		         		{

		         		//System.out.println("EMAILID:"+toarray[i]);
		                 
		                 //END

		                 InternetAddress toAddress = new InternetAddress(toarray[i]);
		                 message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);
		                 
		                 //CC*******************
		                 String toarraycc[] = cc.split(",");
		                 ArrayList ccemailidarr=new ArrayList();
		                 for(int l = 0; l < toarraycc.length; l++)
		 	             {
		         		Matcher v=p.matcher(toarraycc[l]);
		         		boolean h=v.matches();
		         		if(h==true)
		         		{
		         			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
		       				while (st.hasMoreTokens()) {
		       					ccemailidarr.add(st.nextToken());
		       				}//while
		         		}//if h
		                 
		         		}//for CC
		              int sizecc = ccemailidarr.size(); 
		             // System.out.println("SIZE CC"+sizecc);
		              InternetAddress ccAddress[] = new InternetAddress[sizecc];
			                 for(int m = 0; m < sizecc; m++){
			                     ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());
			                 }
			                 message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);
		                 
		                 //CC END
			                 
			                 //BCC*******************
			                 String toarraybcc[] = bcc.split(",");
			                 ArrayList bccemailidarr=new ArrayList();
			                 for(int l = 0; l < toarraybcc.length; l++)
			 	             {
			         		Matcher vh=p.matcher(toarraybcc[l]);
			         		boolean hh=vh.matches();
			         		if(hh==true)
			         		{
			         			StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
			       				while (st.hasMoreTokens()) {
			       					bccemailidarr.add(st.nextToken());
			       				}//while
			         		}//if h
			                 
			         		}//for BCC
			              int sizebcc = bccemailidarr.size(); 
			             // System.out.println("SIZE CC"+sizecc);
			              InternetAddress bccAddress[] = new InternetAddress[sizebcc];
				                 for(int bm = 0; bm < sizebcc; bm++){
				                	 bccAddress[bm] = new InternetAddress(bccemailidarr.get(bm).toString());
				                 }
				                 message.setRecipients(javax.mail.Message.RecipientType.BCC, bccAddress);
			                 //BCC END  
		                BodyPart messageBodyPart = new MimeBodyPart();
		                messageBodyPart.setContent(messageText, mail_messageText);
		                Multipart multipart = new MimeMultipart();
		                multipart.addBodyPart(messageBodyPart);
		                messageBodyPart = new MimeBodyPart();
		               //DataSource source = new FileDataSource(attachementPath);
		               
		               String filepath=photodownload+lotNumber+File.separator+displayfilenme;
		               //String filepath=SendMail_preAuth.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/transaction/"+displayfilenme;
			            //url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/PREAUTH/"+attachementname);
		               System.out.println("attached file path : "+filepath);
		             
		               FileDataSource source = new FileDataSource(filepath);
		                messageBodyPart.setDataHandler(new DataHandler(source));
		                messageBodyPart.setFileName(displayfilenme);
		                multipart.addBodyPart(messageBodyPart);
		                message.setContent(multipart);
		                
		                //END DOCS ATTACHMENT
		             
		             // System.out.println("BEFORE SENDING MSG");
		              
		                  Transport.send(message);
		             
		                   // System.out.println("k"+k);
		                    
		                    k=1;
		         		}//if b is true
		            }//for  LOOP FOR TO EMAILID

		        }
	        catch(Exception ex)
	        {
	            logger.error((new StringBuilder("Error in sendmail trying with the Primary configuratiion ::")).append(ex).toString());
	            k=2;
	        }
	        return k;
	    }
	
	public int SendMail_NoAttachments(String subject, String messageText,String displayfilenme,String TO)
	        throws MessagingException
	    {
		int k=0;  
	    myResources = new MyProperties();
       try
        { 
    	   username = myResources.getMyProperties("mail_username");
           password = myResources.getMyProperties("mail_password");
           mail_transport_protocol = myResources.getMyProperties("mail_transport_protocol");
           mail_smtp_port = myResources.getMyProperties("mail_smtp_port");
           mail_smtp_host = myResources.getMyProperties("mail_smtp_host");
           mail_smtp_auth = myResources.getMyProperties("mail_smtp_auth");
           mail_messageText = myResources.getMyProperties("mail_messageText");
           mail_from = myResources.getMyProperties("mail_from");		        
        
            Properties props = new Properties();
            props.put("mail.transport.protocol", mail_transport_protocol);
            props.put("mail.smtp.port", mail_smtp_port);
            props.put("mail.smtp.host", mail_smtp_host);
            props.put("mail.smtp.auth", mail_smtp_auth);
            props.put("mail.smtp.ssl.trust", "*");
            props.put("mail.smtp.starttls.enable", "true");
            Authenticator auth = new SMTPAuthenticator();
            
            Session session = Session.getInstance(props, auth);
            MimeMessage message = new MimeMessage(session);
            message.setContent(messageText, mail_messageText);
            message.setSubject(subject);
	            
	            String toarray[] = TO.split(",");
	            
	           // Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	            Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	            
	            for(int i = 0; i < toarray.length; i++)
	            {
	            	InternetAddress fromAddress = new InternetAddress(mail_from);
	                message.setFrom(fromAddress);
	         		Matcher g=p.matcher(toarray[i]);
	         		boolean b=g.matches();
	         		if(b==true)
	         		{
	                 InternetAddress toAddress = new InternetAddress(toarray[i]);
	                 message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);
	                 
	                 //CC*******************
	                 /*String toarraycc[] = cc.split(",");
	                 ArrayList ccemailidarr=new ArrayList();
	                 for(int l = 0; l < toarraycc.length; l++)
	 	             {
	         		Matcher v=p.matcher(toarraycc[l]);
	         		boolean h=v.matches();
	         		if(h==true)
	         		{
	         			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
	       				while (st.hasMoreTokens()) {
	       					ccemailidarr.add(st.nextToken());
	       				}//while
	         		}//if h
	                 
	         		}//for CC
	              int sizecc = ccemailidarr.size(); 
	              InternetAddress ccAddress[] = new InternetAddress[sizecc];
		                 for(int m = 0; m < sizecc; m++){
		                     ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());
		                 }
		                 message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);
	                 //CC END
		                 
		                 //BCC*******************
		                 String toarraybcc[] = bcc.split(",");
		                 ArrayList bccemailidarr=new ArrayList();
		                 for(int l = 0; l < toarraybcc.length; l++)
		 	             {
		         		Matcher vh=p.matcher(toarraybcc[l]);
		         		boolean hh=vh.matches();
		         		if(hh==true)
		         		{
		         			StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
		       				while (st.hasMoreTokens()) {
		       					bccemailidarr.add(st.nextToken());
		       				}//while
		         		}//if h
		                 
		         		}//for BCC
		              int sizebcc = bccemailidarr.size(); 
		              InternetAddress bccAddress[] = new InternetAddress[sizebcc];
			                 for(int bm = 0; bm < sizebcc; bm++){
			                	 bccAddress[bm] = new InternetAddress(bccemailidarr.get(bm).toString());
			                 }
			                 message.setRecipients(javax.mail.Message.RecipientType.BCC, bccAddress);*/
		                 //BCC END  
	                BodyPart messageBodyPart = new MimeBodyPart();
	                messageBodyPart.setContent(messageText, mail_messageText);
	                Multipart multipart = new MimeMultipart();
	                multipart.addBodyPart(messageBodyPart);
	                message.setContent(multipart);
	                
	                //END DOCS ATTACHMENT
	                  Transport.send(message);
	                    k=1;
	         		}//if b is true
	            }//for  LOOP FOR TO EMAILID

	        }
	        catch(Exception ex)
	        {
	            logger.error((new StringBuilder("Error in sendmail trying with the Primary configuratiion ::")).append(ex).toString());
	            k=2;
	        }
	        return k;
	    }
	/**
	 * The Class SMTPAuthenticator.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}
	
	/**
	 * The Class Sec_SMTPAuthenticator.
	 */
	private class Sec_SMTPAuthenticator extends javax.mail.Authenticator {
		
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(sec_username, sec_password);
		}
	}
	    public static ResourceBundle getBundle_Mail() 
	    {
	      Locale currentLocale = new Locale("EN","IN");     
	      ResourceBundle myResources_collection = ResourceBundle.getBundle("resource.tpa_mail_IN",currentLocale);      
	      return myResources_collection;
	    }
}