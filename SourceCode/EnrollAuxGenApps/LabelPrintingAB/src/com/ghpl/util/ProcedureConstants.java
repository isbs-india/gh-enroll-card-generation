package com.ghpl.util;

public class ProcedureConstants { 
	public static final String PROC_FOR_GET_Label_LOT_DETAILS ="{call PKG_ECARDS_IN_PDF.GET_LOT_DETAILS_LABLE(?,?)}";
	//public static final String PROC_GET_LOT_LABLE_XML_TEMPLATE ="{call PKG_ECARDS_IN_PDF.GET_LOT_LABLE_XML_TEMPLATE(?,?)}";
	public static final String PROC_GET_LOT_LABLE_XML_TEMPLATE ="{call PKG_ECARDS_IN_PDF.CHECK_LOT_POLID_LABEL_ACTION(?,?)}";
	
	/*public static final String PROC_FOR_GET_LOT_DETAILS ="{call PKG_ECARDS_IN_PDF.GET_LOT_DETAILS(?,?)}";
	public static final String PROC_GET_LOT_CARDS_XML_TEMPLATE ="{call PKG_ECARDS_IN_PDF.GET_LOT_CARDS_XML_TEMPLATE(?,?)}";*/
	public static final String PROC_FOR_PROC_GET_LOT_INSCERT ="{call PKG_ECARDS_IN_PDF.GET_LOT_INSCERT(?,?)}";
	public static final String PROC_FOR_PUT_LABEL_LOT_DETAILS ="{call PKG_ECARDS_IN_PDF.PUT_LOT_DETAILS_LABLE(?,?,?)}";
	
	
}

