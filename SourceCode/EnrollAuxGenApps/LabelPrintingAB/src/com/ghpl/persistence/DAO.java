package com.ghpl.persistence;

import java.sql.Connection;

import com.ghpl.exception.DAOException;

public interface DAO {
	public Connection getConnection() throws DAOException;
}
