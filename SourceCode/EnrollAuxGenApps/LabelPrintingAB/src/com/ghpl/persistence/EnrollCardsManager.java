package com.ghpl.persistence;

import java.util.List;

import com.ghpl.exception.DAOException;
import com.ghpl.model.InsertLotDetailsBean;
import com.ghpl.model.LabelDataBean;

public class EnrollCardsManager {
	public static LabelDataBean getEnrollLabelDetails(int lotNumber,int moduleId) throws DAOException   {
		try {
			return DAOFactory.getInstance().getEnrollDAO().getEnrollLabelDetails(lotNumber,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static int putLabelLotDetails(int lotNumber, int LotStatus, int moduleId) {
		try {
			return DAOFactory.getInstance().getEnrollDAO().putLabelLotDetails(lotNumber,LotStatus,moduleId);
		} catch (DAOException e) {

		}
		return 0;
	}
	
	
	
	public static InsertLotDetailsBean getLabelLotDetails(int isCron, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getEnrollDAO().getLabelLotDetails(isCron,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	
}
