package com.photo.util;

import java.util.Locale;
import java.util.ResourceBundle;

public class Resource_Properties 
{
    
    /**
	 * Gets the main properties bundle
	 * 
	 * @return the bundle
	 */
    public ResourceBundle getBundle() 
    {
      Locale currentLocale = new Locale("EN","IN");     
      ResourceBundle myResources = ResourceBundle.getBundle("resource.tpa_en_IN",currentLocale);      
      return myResources;
    }
    
    /**
	 * Gets the properties bundle of mail configuration
	 * 
	 * @return the bundle_ mail
	 */
    public ResourceBundle getBundle_Mail() 
    {
      Locale currentLocale = new Locale("EN","IN");     
      ResourceBundle myResources_collection = ResourceBundle.getBundle("resource.tpa_mail_IN",currentLocale);      
      return myResources_collection;
    }
    
   
}
