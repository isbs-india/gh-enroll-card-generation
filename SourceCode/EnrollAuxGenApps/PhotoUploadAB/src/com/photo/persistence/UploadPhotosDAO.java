package com.photo.persistence;

import java.io.File;
import java.util.List;

import com.photo.exception.DAOException;
import com.photo.model.GetPhotoPendingLot;


public interface UploadPhotosDAO {
	public  List<GetPhotoPendingLot> getPhotoPendingLot(int moduleId,File ff,File file) throws DAOException;
	public int updatePhotoUploadLot(int moduleId,String xmlString,int policyId,File ff,File file) throws DAOException;
	public int getIPIDCountfromCardId (int moduleId,String xmlString,int policyId,File ff,File file) throws DAOException;
	public int updatePhotoUploadLotUpdate(int moduleId,int lotId,int status,File ff,File file) throws DAOException;
	
}
