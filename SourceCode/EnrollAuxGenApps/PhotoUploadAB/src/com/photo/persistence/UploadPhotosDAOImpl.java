package com.photo.persistence;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.photo.exception.DAOException;
import com.photo.model.GetPhotoPendingLot;
import com.photo.util.DBConn;
import com.photo.util.ProcedureConstants;

public class UploadPhotosDAOImpl extends DBConn implements UploadPhotosDAO{
	Logger logger =(Logger) Logger.getInstance("UploadPhotosDAOImpl");

	@Override
	public List<GetPhotoPendingLot> getPhotoPendingLot(int moduleId,File ff,File file) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		GetPhotoPendingLot getPhotoPendingLot=null;
		ResultSet rs=null;
		int indexpos=0;
		List<GetPhotoPendingLot> getPhotoPendingLotList=null;
		try {
			    con = getMyConnection(1);
			    getPhotoPendingLotList = new ArrayList<GetPhotoPendingLot>();
			    con.setAutoCommit(false);
				cstmt = con.prepareCall(ProcedureConstants.PROC_GET_PHOTO_PENDING_LOT);
				cstmt.setInt(++indexpos,moduleId );
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(indexpos);
			
			while (rs.next()) {
				getPhotoPendingLot=new GetPhotoPendingLot();
				/* SELECT LOTID, MODULEID, FILENAME, TO_CHAR(LOTCREATED,'DD-MON-RRRR') LOTCREATED,
				    A.CONTACTEMAIL,U.USERNAME,L.POLICYID  */
				getPhotoPendingLot.setCantactEmail(rs.getString("CONTACTEMAIL"));         
				getPhotoPendingLot.setFileName(rs.getString("FILENAME"));
				getPhotoPendingLot.setLotCreated(rs.getString("LOTCREATED"));    
				getPhotoPendingLot.setLotId(Integer.parseInt(rs.getString("LOTID")));
				getPhotoPendingLot.setModuleId(Integer.parseInt(rs.getString("MODULEID")));    
				getPhotoPendingLot.setPoicyId(Integer.parseInt(rs.getString("POLICYID")));
				getPhotoPendingLot.setUserName(rs.getString("USERNAME")); 
				getPhotoPendingLotList.add(getPhotoPendingLot);
			}
		}
		catch (Exception ex) {
			logger.error(ex);
			System.out.println("exception     >>"+ex);
			if(ff.exists()){
				file.delete();
			}
     		
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return getPhotoPendingLotList;
	}
	@Override
	public int updatePhotoUploadLot(int moduleId,String xmlString,int policyId,File ff,File file) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		int status = 0;
		try
		{
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_UPDATE_IPID_FROM_CARDID);
			cstmt.setString(++indexpos, xmlString);
			cstmt.setInt(++indexpos, policyId);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			status = cstmt.getInt(indexpos);
			System.out.println("status  >>"+status);
			con.commit();
		}
		catch(Exception ex)
		{
			logger.error(ex);
			System.out.println("error  >>"+ex);
			if(ff.exists()){
				file.delete();
			}
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getEndorDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return status;
	}
	
	public int getIPIDCountfromCardId(int moduleId,String xmlString,int policyId,File ff,File file) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		int status = 0;
		try
		{
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_GET_IPID_CNT_FROM_CARDID);
			logger.info("xmlString : "+xmlString);
			System.out.println("xmlString  "+xmlString);
			System.out.println("policy Id "+policyId);
			logger.info("policyId : "+policyId);;
			cstmt.setString(++indexpos, xmlString);
			cstmt.setInt(++indexpos, policyId);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			System.out.println("before execute");
			cstmt.execute();
			System.out.println("after execute");
			status = cstmt.getInt(indexpos);
			logger.info("status    >> "+status);
			//con.commit();
			if(status==0){
				if(ff.exists()){
					file.delete();
				}
			}
		}
		catch(Exception ex)
		{
			logger.error(ex);
			System.out.println("error  >>"+ex);
			if(ff.exists()){
				file.delete();
			}
			//throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				//throw new DAOException();
			}
		}
		return status;
	}
	@Override
	public int updatePhotoUploadLotUpdate(int moduleId, int lotId, int status,File ff,File file)
			throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		int status1=0;
		try
		{
			con =getMyConnection(1);
			cstmt = con.prepareCall(ProcedureConstants.PHOTO_UPLOAD_LOT_UPDATE);
			cstmt.setInt(++indexpos, lotId);
			cstmt.setInt(++indexpos, status);
			cstmt.setInt(++indexpos,moduleId );
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			status1 = cstmt.getInt(indexpos);
			//System.out.println("status1  >>"+status1);
			con.commit();
		}
		catch(Exception ex)
		{
			logger.error(ex);
			System.out.println("error  >>"+ex);
			if(ff.exists()){
				file.delete();
			}
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getEndorDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return status1;
	}
}