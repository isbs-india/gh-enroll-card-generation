package com.photo.persistence;

import java.io.File;
import java.util.List;

import com.photo.exception.DAOException;
import com.photo.model.GetPhotoPendingLot;
public class UploadPhotosManager {
	public static List<GetPhotoPendingLot> getPhotoPendingLot(int moduleId,File ff,File file) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().getPhotoPendingLot(moduleId,ff,file);

		}catch (DAOException e) {

		}
		return null;

	}
	
	public static int updatePhotoUploadLot(int moduleId,String xmlString,int policyId,File ff,File file) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().updatePhotoUploadLot(moduleId,xmlString,policyId,ff,file);

		}catch (DAOException e) {

		}
		return 0;
		
	}
	
	public static int getIPIDCountfromCardId (int moduleId,String xmlString,int policyId,File ff,File file) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().getIPIDCountfromCardId(moduleId,xmlString,policyId,ff,file);

		}catch (DAOException e) {

		}
		return 0;
		
	}
	
	public static int updatePhotoUploadLotUpdate(int moduleId,int lotId,int status,File ff,File file) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().updatePhotoUploadLotUpdate(moduleId,lotId,status, ff, file);

		}catch (DAOException e) {

		}
		return 0;
		
	}
	
	
		
}
