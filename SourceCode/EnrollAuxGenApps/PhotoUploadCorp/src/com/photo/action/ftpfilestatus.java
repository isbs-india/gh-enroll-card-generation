package com.photo.action;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import com.photo.util.MyProperties;

public class ftpfilestatus {
	static Logger logger = Logger.getLogger("PhotoUploadAction");
	public static void main(String[] args) throws SocketException, IOException {
		 MyProperties myResources1 = new MyProperties();
		   logger = myResources1.getLogger();
		   String photodestinationftp_host=null;
			String photodestinationftp_username = null;
			String photodestinationftp_password = null;
			String PHOTOUPLOADS_TEMP= null;
		   photodestinationftp_host = myResources1.getMyProperties("photodestinationftp_host");
			photodestinationftp_username = myResources1.getMyProperties("photodestinationftp_username");
			photodestinationftp_password = myResources1.getMyProperties("photodestinationftp_password");
			PHOTOUPLOADS_TEMP = myResources1.getMyProperties("photodestination_PHOTOUPLOADS");
			
		FTPClient desftpclient = new FTPClient();
        boolean filecheckstatus= false ;
        desftpclient.connect(photodestinationftp_host);
        int desreplyCodeftp = desftpclient.getReplyCode();
        logger.info("ftp client reply code: " + desreplyCodeftp);
        if (!FTPReply.isPositiveCompletion(desreplyCodeftp)) {
       	logger.info("Operation failed. FTP Server reply code: " + desreplyCodeftp);
        }
           //result = desftpclient.login(FTPUName, FTPPwd);
      boolean  result = desftpclient.login(photodestinationftp_username, photodestinationftp_password);
        logger.info("photodestinationftp_host  "+photodestinationftp_host +"   photodestinationftp_username   "+photodestinationftp_username  +"    photodestinationftp_password   "+photodestinationftp_password);;
           logger.info("FTP client  status    : "+result);
           //System.out.println("FTP client  status    : "+result);
           if (!result) {
           
           	 logger.info("Could not login to the  FTP server");
           	 return;
           }
           File desuploadDir = new File(PHOTOUPLOADS_TEMP);
           if (!desuploadDir.exists()){
				String str[] =PHOTOUPLOADS_TEMP.split("/");
				for(int k=0;k<str.length;k++){
					desftpclient.changeWorkingDirectory(str[k]);
					desftpclient.printWorkingDirectory();
				}
			}
           desftpclient.enterLocalPassiveMode();
           desftpclient.setFileType(FTPClient.BINARY_FILE_TYPE);
           desftpclient.setRemoteVerificationEnabled(false);
           
           
           String image = "";
            String extension_img = "";
       	 String ext_img = "";
       	File fileNamelocal = new File("C:\\Users\\prabhakar7\\Desktop\\PNB\\GHOIPB02335604.jpg");
            int num = fileNamelocal.getName().lastIndexOf('.');
				if (num > 0) {
					extension_img = fileNamelocal.getName().substring(num + 1);
					ext_img = extension_img.toLowerCase();
					if(extension_img == ext_img){
						image = fileNamelocal.getName();
					}else{
						image = fileNamelocal.getName().replaceAll(extension_img,ext_img);
					}
				}
				logger.info("checking the image in ftp folder or not");
	            //checking the image in ftp folder or not
            filecheckstatus =  checkFileExists(image, desftpclient);
        }
	
        
        static boolean checkFileExists(String filePath, FTPClient ftpClient) throws IOException {
            InputStream inputStream = ftpClient.retrieveFileStream(filePath);
           int returnCode = ftpClient.getReplyCode();
            if (inputStream == null || returnCode == 550) {
                return false;
            }
            return true;
        }

}
