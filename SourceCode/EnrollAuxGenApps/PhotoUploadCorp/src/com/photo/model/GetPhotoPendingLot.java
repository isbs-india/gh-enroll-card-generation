package com.photo.model;

public class GetPhotoPendingLot {
	private int lotId;
	private int moduleId;
	private String fileName;
	private String lotCreated;
	private String cantactEmail;
	private String userName;
	private int poicyId;
	

	/*LOTID, 
	MODULEID, 
	FILENAME, 
	TO_CHAR(LOTCREATED,'DD-MON-RRRR') LOTCREATED,
    A.CONTACTEMAIL,
    U.USERNAME,
    L.POLICYID*/
	public int getLotId() {
		return lotId;
	}
	public void setLotId(int lotId) {
		this.lotId = lotId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getLotCreated() {
		return lotCreated;
	}
	public void setLotCreated(String lotCreated) {
		this.lotCreated = lotCreated;
	}
	public String getCantactEmail() {
		return cantactEmail;
	}
	public void setCantactEmail(String cantactEmail) {
		this.cantactEmail = cantactEmail;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public int getPoicyId() {
		return poicyId;
	}
	public void setPoicyId(int poicyId) {
		this.poicyId = poicyId;
	}	
	

}
