package com.photo.util;

public class ProcedureConstants { 
	public static final String PROC_GET_PHOTO_PENDING_LOT ="{call PKG_ECARDS_IN_PDF.PROC_GET_PHOTO_PENDING_LOT(?,?)}";
	public static final String PROC_GET_IPID_CNT_FROM_CARDID ="{call PKG_ECARDS_IN_PDF.PROC_GET_IPID_CNT_FROM_CARDID(?,?,?)}";
	public static final String PROC_UPDATE_IPID_FROM_CARDID ="{call PKG_ECARDS_IN_PDF.PROC_UPDATE_IPID_FROM_CARDID(?,?,?)}";
	public static final String PHOTO_UPLOAD_LOT_UPDATE ="{call PKG_ECARDS_IN_PDF.PHOTO_UPLOAD_LOT_UPDATE(?,?,?,?)}";
	
	
}

