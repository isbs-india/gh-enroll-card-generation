package com.ghpl.persistence;

import java.util.List;

import com.ghpl.exception.DAOException;
import com.ghpl.model.CardDataBean;
import com.ghpl.model.InsertLotDetailsBean;

public class EnrollCardsManager {
	
	public static InsertLotDetailsBean getLotDetails(int isCron, int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getEnrollDAO().getLotDetails(isCron,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	public static CardDataBean getEnrollCardDetails(int lotNumber,int moduleId) throws DAOException   {
		try {
			return DAOFactory.getInstance().getEnrollDAO().getEnrollCardDetails(lotNumber,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static int putLotDetails(int lotNumber, int LotStatus, int moduleId) {
		try {
			return DAOFactory.getInstance().getEnrollDAO().putLotDetails(lotNumber,LotStatus,moduleId);
		} catch (DAOException e) {

		}
		return 0;
	}
	
	public static List<CardDataBean>  getEnrollCardDetailsold(int lotNumber,int moduleId) throws DAOException   {
		try {
			return DAOFactory.getInstance().getEnrollDAO().getEnrollCardDetailsold(lotNumber,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	
	
	public static List<com.ghpl.model.EnrollsBean> getLotInsurerCertificationDetails(int lotId,int moduleId){
		try {
			return DAOFactory.getInstance().getEnrollDAO().getLotInsurerCertificationDetails(lotId, moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
}
