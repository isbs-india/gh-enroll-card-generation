package com.ghpl.util;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;



public class Constants {
	public static final int COMMAN_DB_ID=102;
    public static final String RET="RET";
	public static final String JNDI_KEY="java:comp/env/jdbc/testDB";
	public static final String IAuth_REALPATH="Modules";
	public static final String IAUTH_EXT = ".inc";
	//public static final String IAUTH_INCPATH = "/MODULES";
	
	
	public static final String IAUTH_FOURTHHEAD_RET="IAUTH_FOURTHHEAD_RET";
	public static final String IAUTH_FOURTHHEAD_CORP="IAUTH_FOURTHHEAD_CORP";
	public static final String apiUrl = "https://mandrillapp.com/api/1.0/messages/send-template.json";
	// local sytem properties files 
	
		public static final String LOG4JConfig="/root/MyCrons/keys/WINDECardsCronlog4j.properties";
		public static final String DBConfig="/root/MyCrons/keys/config.properties";
		public static final String PDFConfig="/root/MyCrons/keys/pdf.properties";
		
		/*public static final String LOG4JConfig="D:/appconfig/ECardslog4j.properties";
		public static final String DBConfig="D:/appconfig/config.properties";
		public static final String PDFConfig="D:/appconfig/pdf.properties";*/
		
		
		
		
		//public static final String trackcardpath ="http://lion.isbsindia.in/bonesante/Ecard.svc/TrackCard";
		
		public static final String MAIL_INSURER_SUBJECT="AUTHORIZATION APPROVAL REQUEST FOR CLAIMID: ";
		
		//aws properties
		public static final String accesskey="AKIAIT7QH2KEZFPL3NHQ";
		public static final String secretKey ="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";
		public static final  String bucketName = "insurerdocs";
		
		
		public static final Font fontDateText= new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontGeneralText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontPintText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontTableCellText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
		public static final Font fontParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
		public static final Font fontBoldParaTextRed = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.RED);
		public static final Font fontNoteText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontItalicParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
		public static final Font fontItalicBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC, BaseColor.BLACK);
		public static final Font fontItalicUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC + Font.UNDERLINE, BaseColor.BLACK);
		public static final Font fontItalicBoldUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC + Font.UNDERLINE, BaseColor.BLACK);
		public static final Font fontUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);
		public static final Font fontUnderlineBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
		
		public static final Font fontParaTextverdana = FontFactory.getFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontBoldParaTextverdana = FontFactory.getFont("Verdana", 8, Font.BOLD, BaseColor.BLACK);
		public static final Font fontGeneralTextverdana = FontFactory.getFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontUnderlineParaTextverdana = FontFactory.getFont("Verdana", 8, Font.UNDERLINE, BaseColor.BLACK);
		public static final Font fontUnderlineBoldParaTextverdana = FontFactory.getFont("Verdana", 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
		public static final Font fontParaTextins = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontParaTextinsBold = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.BLACK);
		
		public static final Font fontBoldParaText12 = FontFactory.getFont("Verdana", 11, Font.BOLD, BaseColor.BLACK);
		public static final Font fontGeneralText12 = FontFactory.getFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK);
		public static final Font fontUnderlineParaText12 = FontFactory.getFont("Verdana", 11, Font.UNDERLINE, BaseColor.BLACK);
		public static final Font fontUnderlineBoldParaText12 = FontFactory.getFont("Verdana", 11, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
		
		
		
		
		
	  
}
