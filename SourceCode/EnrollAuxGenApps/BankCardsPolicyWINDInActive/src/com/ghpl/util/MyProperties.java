/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ghpl.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



/**
 *
 * @author Surya Kiran
 */
public class MyProperties {
    
    static Logger logger;
    public String getMyProperties(String PropName)
    {
        logger = this.getLogger();        
        Properties prop = new Properties();
        try {
        	FileInputStream fin = new FileInputStream(Constants.DBConfig);
             prop.load(fin);
             return prop.getProperty(PropName);
        	
    	} catch (IOException ex) {
    		logger.error("Error DBConn - getting "+PropName+" Properties Set IO Exception"); 
    		logger.error("IN " + ex.getMessage());
                return "";
        }
    }     
    
    
    public Logger getLogger() {
        Properties prop = new Properties();
        try {

        	FileInputStream fin = new FileInputStream(Constants.LOG4JConfig);
            prop.load(fin);
            PropertyConfigurator.configure(Constants.LOG4JConfig);
            logger = Logger.getLogger(MyProperties.class.getName());
       } catch (IOException O) {
           System.out.println("Log File Config Error " + O.getMessage());
       }
       return logger;

    }
 
}
