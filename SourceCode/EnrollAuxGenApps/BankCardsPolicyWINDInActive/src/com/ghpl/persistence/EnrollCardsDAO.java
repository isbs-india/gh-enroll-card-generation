package com.ghpl.persistence;

import java.util.List;








import com.ghpl.exception.DAOException;
import com.ghpl.model.Card;
import com.ghpl.model.CardDataBean;
import com.ghpl.model.EnrollsBean;
import com.ghpl.model.InsertLotDetailsBean;

public interface EnrollCardsDAO {
	public CardDataBean getEnrollCardDetails(int lotNumber,int moduleId) throws DAOException;
	public List<CardDataBean> getEnrollCardDetailsold(int lotNumber,int moduleId) throws DAOException;
	public InsertLotDetailsBean getLotDetails(int isCron, int moduleId) throws DAOException;
	public List<EnrollsBean> getLotInsurerCertificationDetails(int lotId, int moduleId) throws DAOException;
	public  int putLotDetails(int lotNumber, int LotStatus, int moduleId)throws DAOException;
}
