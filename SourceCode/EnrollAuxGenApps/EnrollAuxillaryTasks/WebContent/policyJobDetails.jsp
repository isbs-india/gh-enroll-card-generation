<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="header.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script> 
<link href="<%=request.getContextPath()%>/js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/1024.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cycle_plugin.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">


<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui-timepicker-addon.css">
<script src="<%=request.getContextPath()%>/js/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}
</script>
<h3 align="center">JOB POLICY REPORT DOWNLOAD DETAILS</h3>


<table width=100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp" %>
		<td align="center" width="100%" valign="top"><br/>
   <br/><br/>
		   <c:if test="${fn:length(lotBeanListObj) gt 0 }">
		   <table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
		   <tr align="center">
					<td colspan="9" ><font color="red"> ** The Data will be reset daily. so please make sure to download files before 8 pm.</font></td>
				</tr>
		   	   <tr>
					<th>Index</th>
				    <th>Lot No.</th>
				    <th>Module</th>
                    <th>Status Desc.</th>
                    <th>Created on</th>
                    <th>Current User</th>
                    <th>DownLoad</th>
				</tr>
		   <c:forEach items="${lotBeanListObj}" var="cardJob" varStatus="rowCounter"> 
		   <tr>
				<td><c:out value="${cardJob.serialNo}"></c:out></td>
				<td><c:out value="${cardJob.lotNumber}"/>   
				</td>
				<td>
					<c:out value="${cardJob.moduleName}"/>
				</td>
				<td>
					<c:out value="${cardJob.statusDesc}"/>
				</td>
				<td>
					<c:out value="${cardJob.createdOn}"/>
				</td>
				<td>
					<c:out value="${cardJob.userName}"/>
				</td>
		   	  	<td>
                    <form name="brkupentry" method="post" action="./DownloadPolicyReportAction">
                  
                     <c:if test="${cardJob.lotStatus != 1 && cardJob.lotStatus != 9}">
                     <input type="hidden" name="lotNumber" id="lotNumber" value="${cardJob.lotNumber}" />
                     <input type="hidden" name="moduleId" id="moduleId" value="${cardJob.moduleId}" />
                     <input type="hidden" name="formatId" id="formatId" value="${cardJob.formatId}" />
                                          
                    <input type="submit" name="submit" value="DownLoad" />
                    </c:if>
                      
                     <c:if test="${cardJob.lotStatus==1 || cardJob.lotStatus==9}">
                     #
                     </c:if>
                     
                     
                    </form>
                    </td>
		   	</tr>
		   </c:forEach>
		   </table>
		   </c:if>
		   <c:if test="${fn:length(lotBeanListObj) eq 0 }">
		   		<table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
			   	    <tr>
						<th>Index</th>
					    <th>Lot No.</th>
					    <th>Module</th>
	                    <th>Status Desc.</th>
	                    <th>Created on</th>
	                    <th>Current User</th>
	                    <th>DownLoad</th>
					</tr>
			   	    <tr align="center">
			   			<td colspan="11" align="center">No data found</td>
			   		</tr>
		   		</table>
		   </c:if>
		
		</td>
	</tr>
</table>

<%@include file="footer.jsp" %>