<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="header.jsp"%>

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script> 
<link href="<%=request.getContextPath()%>/js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/1024.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cycle_plugin.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">


<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui-timepicker-addon.css">
<script src="<%=request.getContextPath()%>/js/jquery-ui-timepicker-addon.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}

	//Function to generate ecard using jquery post data
	/* function InsertProduct() {
        
        var msg='{"TrackCard": [{"CARDID": "GHNA0600019984","AGE": "52","DOB": "15-08-1962","CURRENTPOLICYID": "4001","UWID": 14,"MODULEID": 1,"EMPID":"C1084"}]}';
      
        $.ajax({
        	
            url: "http://lion.isbsindia.in/bonesante/Ecard.svc/TrackCard",
            //url: "http://lion.isbsindia.in/bonesante/Ecard.svc/TrackCardByGHID",
            type: "POST",
            data: msg,
            //data: JSON.stringify({ cardId: 'GHOI0200000436' }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            processData: true,
            // responseType: 'arraybuffer',
            success: function (response) {
               // onRequestEnd();
                //alert("suc");
                alert(JSON.stringify(response));
                //alert(response.d.FILENAME);
                 var form = document.createElement("form");
                form.action = "./DownEcardTrackCardAction";
                form.method = "POST";
                form.target = "./DownEcardTrackCardAction" || "_self";
                if (response) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = 'key';
                    input.value = response.d.BASE64ARRAY;
                    form.appendChild(input);

                    var ifilename = document.createElement('input');
                    ifilename.type = 'hidden';
                    ifilename.name = 'filename';
                    ifilename.value = response.d.FILENAME;
                    form.appendChild(ifilename);
                }
                form.style.display = 'none';
                document.body.appendChild(form);
                form.submit(); 
            },
            error: function (response) {
              //  onRequestEnd();
                alert('Failed: ' + response.statusText);
            }
        });
    } */
    
    function ddlPolicychange(policyId)
	 {
		 //var selectedState = statesddl.options[statesddl.selectedIndex].value;
		 // var selCityId = statesddl.value.split("~");
		// var policyIdSelected = selCityId[0]; 
		//alert(policyId);
		var xmlhttp;
		 var strAjUrlData="GetEndorDDLDataAction?key="+policyId;
		 if (window.XMLHttpRequest)
		 {
			 // code for IE7+, Firefox, Chrome, Opera, Safari
		     xmlhttp=new XMLHttpRequest();
		 }
		 else
		 {
		  // code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		   {
			  //alert(xmlhttp.responseText);	
			  document.getElementById('ddlEndor').innerHTML=xmlhttp.responseText;
		   }
		   else
		   {
		   //alert(xmlhttp.status);
		   }
		  }
		   xmlhttp.open("POST",strAjUrlData,true);
		   xmlhttp.send();
		 
	 }
    
    
    function validateForm()
    {
    	 var message = "VALIDATION MESSAGE";
    	 var policyddl = document.getElementById("ddlPolicy");
		 var policyddlId = policyddl.options[policyddl.selectedIndex].value;
		 var ghplId = document.getElementById("ghplId").value;
		 
		 
		  
		  if(ghplId != null && ghplId != "" && ghplId.length < 14){
	          CustomConfirmation('Please enter proper GHPL ID',message);  
	          document.getElementById("ghplId").value="";
	          document.getElementById("ghplId").focus();
	          return false;  
	      }
	      if(ghplId != null && ghplId != "" && ghplId.length >13){
	    	  var letters = "^[a-zA-Z]{4}[a-zA-Z0-9-]{9}.";
	          if(!ghplId.match(letters)){
	              CustomConfirmation('First four letters of Good Health ID must be alphabet characters only',message);  
	              document.getElementById("ghplId").value="";
	              document.getElementById("ghplId").focus();
	              return false;  
	          }  
	      }
		 
		 if(policyddlId == 0 && ghplId=="")
		 {
			 CustomConfirmation('Please select valid Policy or enter Good Health ID',message);
			 return false; 
		 }
    }
</script>


<h3 align="center">Generate Cards</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp"%>
		<td align="center" width="100%">
			<table width="80%" border="0" cellpadding="1" cellspacing="1" class="tablebg">
				<tr>
					<td>
						<fieldset>
							<legend>Search Criteria</legend>
							<form name="policyForm" method="post" action="InsertLotDetailsAction" onSubmit="return validateForm();">
								<table width="50%" border="20" cellpadding="0" cellspacing="0" class="tablebg">
								<input type="hidden" name="userId" id="userId" value="<%=session.getAttribute("userId")%>">
									<tr>
										<td align="right" >Select Policy:</td><td>
											<select name="ddlPolicy" id="ddlPolicy" style="width:300px;"  onchange="ddlPolicychange(this.value)">
											     <option selected="selected" value="0">- Select -</option>
											      <c:if test="${fn:length(policyDDLListObj) gt 0 }">
											     	<c:forEach items="${policyDDLListObj}" var="option"> 
	   													<option value="${option.policyId}">${option.policyHolderName}(${option.policyFrom} - ${option.policyTo})</option>
													</c:forEach>
												</c:if> 
											 </select>
										</td>
									</tr>
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<tr>
										<td align="right">Select Endorsement:</td><td>
												<select name="ddlEndor" id="ddlEndor" style="width:300px;">
													 <option selected="selected" value="0">- Select -</option>
												</select> &nbsp;&nbsp; (or)
										</td>
									</tr>
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<!-- <tr>
										<td align="right">Card ID:</td><td>
											<input type="text" id="ghplId" name ="ghplId" style="width:300px;">
										</td>
									</tr> -->
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<tr>
										<td align="center" colspan="2"><input type="submit" name="Submit" value="Generate Lot" class="button" /></td>
									</tr>
								</table>
							</form>
						</fieldset>

					</td>
				</tr>
			</table>
			</td>
	</tr>
</table>

<%@include file="footer.jsp"%>