<%@ page import="com.itextpdf.text.log.SysoLogger" %>
<%@ page import="com.util.Util" %>
<%@ include file="header.jsp" %>
<%@ page import="com.util.MyProperties" %>
<meta http-equiv="Expires" content="Sat, 01 Dec 2012 00:00:00 GMT">
<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="./js/jquery-ui.min.js"></script>

<link href="./js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="./js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/cycle_plugin.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" href="./css/jquery-ui-timepicker-addon.css">
<script src="./js/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery-ui-timepicker-addon.js"></script>
<script src="js/jquery-ui-sliderAccess.js"></script>
<script src="js/bank.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<script src="js/jquery-ui.js"></script>

<script type="text/javascript">
var message = "VALIDATION MESSAGE";

function ddlModulechange(moduleddl)
{
	 var selPolicyId = moduleddl.value.split("~");
	 var moduleIdSelected = selPolicyId[0]; 
	 var xmlhttp;
	 var strAjUrlData="PolicySearch?key="+moduleIdSelected;
	 if (window.XMLHttpRequest)
	 {
		 // code for IE7+, Firefox, Chrome, Opera, Safari
	     xmlhttp=new XMLHttpRequest();
	 }
	 else
	 {
	  // code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	  xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	   {
		  document.getElementById('policy').innerHTML=xmlhttp.responseText;
	   }
	   else
	   {
	   }
	  }
	   xmlhttp.open("POST",strAjUrlData,true);
	   xmlhttp.send();
}
function myTrim(x) {
	return x.replace(/^\s+|\s+$/gm, '');
}
function CustomConfirmation(msg, title_msg){
	if ($('#ConfirmMessage').length == 0) {
	        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
	    } else {
	        $('#ConfirmMessage').html(msg);
	    }

	    $("#ConfirmMessage").dialog({
	        autoOpen: false,
			title: title_msg,
	        show: "blind",
	        hide: "explode",  
			resizable: false,      
	        height: 200,
	        width: 300,
	        modal: true,
			buttons: {
	            "Ok": function() 
	            {
	                $( this ).dialog( "close" );
	            }
			}
	    });
	  
	    $( "#ConfirmMessage" ).dialog("open");
	}

function validateForm(){
	
	 var moduleId = document.getElementById("moduleId");
	 var moduleIdId = moduleId.options[moduleId.selectedIndex].value;
	 
	 var policyobj = document.getElementById("policy");
	 var policyArray = policyobj.options[policyobj.selectedIndex].value;
	
	 var empId = document.getElementById("empId").value;
	 var bano = document.getElementById("bano").value;

	 var policyIdId = policyArray.split("~")[0];
	
	 if(moduleIdId == 0)
	 {
		 CustomConfirmation('Please select valid Module Id',message);
		 return false; 
	 }
	 if(policyIdId == 0)
	 {
		 CustomConfirmation('Please select valid policy',message);
		 return false; 
	 }
	 
	 if(moduleIdId == 1){
	 if(empId == "")
	 {
		 CustomConfirmation('Please select valid empId',message);
		 return false; 
	 }
	 }
	 
	 if(moduleIdId == 2)
	 {
		 if(empId == "" && bano == ""){
		 CustomConfirmation('Please select valid Emp Id (OR)  BA No',message);
		 return false; 
		 }
		 
		 if(empId != "" && bano != ""){
			 CustomConfirmation('Please select any one Emp Id (OR)  BA No',message);
			 return false; 
			 }
	 }
}

function validateForm1(){
	 var moduleId1 = document.getElementById("moduleId1");
	 var moduleIdId1 = moduleId1.options[moduleId1.selectedIndex].value;
	 var cardid=myTrim(document.getElementById('cardid').value);
	 
	 if(moduleIdId1 == 0)
	 {
		 CustomConfirmation('Please select valid Module Id',message);
		 return false; 
	 }
	 if(cardid == "")
	 {
		 CustomConfirmation('Please select valid cardid',message);
		 return false; 
	 }
		 if(cardid!="")
	  	  {
	    	 if((cardid.length != 0) && (cardid.length <14 ) || (cardid.length > 25)) 
	     	    {
	    		 CustomConfirmation("InValid CardID");
	    	           return false;
	    	    }
	  	  }
}
function validateUpload(srno){
	  var upload=myTrim(document.getElementById("upload"+srno).value);	
	  if(document.getElementById("upload"+srno).value == "")
	  {
		 CustomConfirmation("Please select file",message);
		 return false; 
	  }
	    var size = document.getElementById("upload"+srno).files[0].size;
		if (size>=1024){
			size = (size/1024).toFixed(2);
			if(size > 600)
			{
			 CustomConfirmation('Please select photo below 600 KB',message);
			 return false; 
			}
		}
		var ext = upload.substring(upload.lastIndexOf('.') + 1).toLowerCase();
		if((ext != 'jpg') && (ext != 'JPG') && (ext != 'jpeg') && (ext != 'JPEG'))
		{
			 CustomConfirmation('Please select photo jpg (or) jpeg ',message);
		     return false;
		}    
}
</script>
 <script>
function toggleOther(){
	var moduleId = document.getElementById("moduleId").value;
	if (moduleId == "2") {
		$("#dis").show();
		$("#dis1").show();
	} else if (moduleId == "1") {
		$("#dis").hide();
		$("#dis1").hide();
	}
}
</script>  
<h3 align="center">Upload Single Photo</h3>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<%
	MyProperties myResources = new MyProperties();
	
	%>
		<%@include file="leftmenu.jsp"%>
		<td align="center" width="100%" valign="top">
			<table width="60%" border="0" cellpadding="1" cellspacing="1" class="tablebg">
 				<tr>
					<td>
						<fieldset>
							<legend>Upload Single Photo</legend>
							<form name="uploadEmpForm" method="post" action="./UploadPhotoByEmpAction"  onSubmit="return validateForm();"> 
								<table width="100%" border="1" cellpadding="0" cellspacing="0" class="tablebg">
									
									<tr>
										<td align="left">Module Id <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 <select name="moduleId" id="moduleId" onchange="ddlModulechange(this);toggleOther();">
											<option id="0" value="0">Select</option>
											<option id="1" value="1">Corp</option> 
											<option id="2" value="2">Bank Assurance</option>
											<option id="4" value="4">Individual</option>
										</select> 
										
										</td>
									</tr>
									<tr>
										<td align="left">Select Policy <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 <select name="policy" id="policy" style="width: 200px;" >
												<option selected="selected" value="0">-Select -</option>
											</select>
										</td>
									</tr>
									<tr>
										<td align="left">Emp Id: <font color="red">*</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="text" id="empId" name="empId" value="">
										</td>
									</tr>
									 
									<tr id="dis1" style="display: none;">
										<td >
										(OR)
										</td>
									</tr>
									<tr id="dis" style="display: none;">
										<td align="left" >BA No <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
								        <input type="text" id="bano" name="bano" value="">
										</td>
									</tr>
									<tr>
									    <td>
										 <input type="submit" name="submit" id="submit" value="submit" />
										</td>
									</tr>
								</table>
							</form>
						</fieldset>
					</td>
				</tr> 
		    <tr>
				<td>
				(OR)
				</td>
			</tr>
				<tr>
					<td>
						<fieldset>
							<legend>Upload Single Photo</legend>
							<form name="uploadCardForm" method="post" action="./UploadPhotoAction" onSubmit="return validateForm1();"> 
								<table width="100%" border="1" cellpadding="0" cellspacing="0" class="tablebg">
									
									<tr>
										<td align="left">Module Id <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										 <select name="moduleId1" id="moduleId1">
											<option id="0" value="0">Select</option>
											<option id="1" value="1">Corp</option> 
											<option id="2" value="2">Bank Assurance</option>
											<option id="4" value="4">Individual</option>
										</select> 
										
										</td>
									</tr>
									
									<tr>
										<td align="left">Card Id <font color="red">*</font>:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<input type="text" id="cardid" name="cardid" value="">
										
										</td>
									</tr>
									<tr>
									    <td>
										 <input type="submit" name="submit" id="submit" value="submit" />
										</td>
									</tr>
									
								</table>
							</form>
						</fieldset>
					</td>
				</tr>
			</table>
			<table width="95%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="center" width="100%"><br/>
   <br/><br/>
		   <c:if test="${fn:length(empList) gt 0 }">
		   <table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
		   	    <tr>
		   	    	<th>S.No</th><th>Card ID<br/>(EmpID)</th><th>Insured Name<br/>(SumInsured)<br/>(Bano)</th><th>PolicyNo<br/>(Policy HolderName)</th><th>RelationShip<br/>(Gender)</th><th>Policy From<br/>(Policy To)</th><th>DOJ<br/>(DOL)</th>
		   	    	<th>Existing photo</th><th>Upload Photo</th>
		   	    </tr>
		   <c:forEach items="${empList}" var="emp"> 
		   <tr>
		   		 <td>${emp.serialNo}</td>
		   		 <td>${emp.cardid}<br/>(${emp.empid})</td>
		   		 <td>${emp.cardHolderName}(${emp.sumInsured})<br/>(${emp.bano})</td>
		   		 <td>${emp.policyNo}(${emp.policyHolderName})</td>
		   		 <td>${emp.relation}<br/>(${emp.gender})</td>
		   		 <td>${emp.policyFrom}<br/>(${emp.policyTo})</td>
		   		 <td>${emp.doj}<br/>(${emp.dol})</td>
		   		<c:set var="cardId" value="${emp.cardid}" scope="request"/>
		   		<%
		   		
				String FTPHOST=null;
 				
 				String ftpupload_ftppath=null;
 				
		   		FTPHOST = myResources.getMyProperties("photodestinationftp_host");
     			
     			ftpupload_ftppath = myResources.getMyProperties("IMAGE_PATH");
     			String httppath =null;
     			if(ftpupload_ftppath.contains("<CARDID>")){
     				httppath =ftpupload_ftppath.replace("<CARDID>", (String)request.getAttribute("cardId"));
     			}
		   		%>
		   		 <td>  &nbsp;
		   		 
		   		    <img src="<%=httppath%>?time=<%=System.currentTimeMillis()%>" width="90px" height="90px"> 
		   		  
		   		 </td> 
		   		 <td>
                <form name="uploadForm${emp.serialNo}" method="post" action="./UploadSinglePhotoAction" enctype="multipart/form-data" onSubmit="return validateUpload(${emp.serialNo});">
                <input type="hidden" id="serialNo" name="serialNo" value="${emp.serialNo}" />
                 <input type="hidden" id="moduleId${emp.serialNo}" name="moduleId${emp.serialNo}" value="${emp.moduleId}" />
                 <input type="hidden" id="cardid${emp.serialNo}" name="cardid${emp.serialNo}" value="${emp.cardid}" />
                 <input type="hidden" id="policyId${emp.serialNo}" name="policyId${emp.serialNo}" value="${emp.policyId}" />
                 <input type="hidden" id="insuredId${emp.serialNo}" name="insuredId${emp.serialNo}" value="${emp.insuredId}" />
		   		 <input type="file" name="upload${emp.serialNo}" id="upload${emp.serialNo}" class ="upload" enctype="multipart/form-data"  value="" /><br/><br/>
		   		 <input type="submit" name="uploadPhoto" id="uploadPhoto" class ="upload"  value="upload" /> 
		   		 </form>
		   		 </td>

		   	</tr>
		   </c:forEach>
		   </table>
		   </c:if>
		   <c:if test="${fn:length(empList) eq 0 }">
		   		<table border="0"  cellpadding="1" cellspacing="1" class="tablebg">
			   	    <tr>
		   	    	<th>S.No</th><th>Card ID</th><th>Insured Name<br/>(SumInsured)<br/>(Bano)</th><th>PolicyNo<br/>(Policy HolderName)</th><th>RelationShip</th><th>Policy From<br/>(Policy To)</th><th>DOJ<br/>(DOL)</th>
		   	    	<th>Existing photo</th><th>Upload Photo</th>
		   	       </tr>
			   	    <tr align="center">
			   			<td colspan="10" align="center">No data found</td>
			   		</tr>
		   		</table>
		   </c:if>
		
		</td>
	</tr>
</table>
		</td>
	</tr>
</table>
<%@include file="footer.jsp"%>