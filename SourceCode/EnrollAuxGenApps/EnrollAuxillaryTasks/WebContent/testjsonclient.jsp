<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@include file="header.jsp"%>

<script type="text/javascript" src="./js/jquery-1.11.2.min.js"></script>
 <script type="text/javascript" src="./js/jquery-ui.min.js"></script> 
<link href="./js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="./js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/cycle_plugin.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}

	//Function to generate ecard using jquery post data
	/*  function InsertProduct() {
        
        //var msg='{"TrackCard": [{"CARDID": "GHNA0600019984","AGE": "52","DOB": "15-08-1962","CURRENTPOLICYID": "4001","UWID": 14,"MODULEID": 1,"EMPID":"C1084"}]}';
        //var msg='{"ClaimRegister":{"A":3,"AILMENT":"AILMENT","ALCOHOL_INFLUENCE":0,"BED_NO":"3","BILL_DATE":"2015-05-12","BILL_NO":"43","CLAIM_FILE_NO":"12345","CLAIM_REGISTERED_DATE":"2015-05-15 11:23:34","CLAIM_STATUS":"SETTLED","CLAIM_STATUS_STATE":8,"CONSULTANT_CHARGES_CLAIM_AMOUNT":345.0,"DATE_OF_ADMISSION":"2015-05-14 11:23:34","DATE_OF_DISCHARGE":"2015-05-19 11:23:34","DATE_OF_INTIMATION":"2015-05-11 11:23:34","DIAGNOSIS_DESC":"FEVER","EDD":"2015-06-19","FILE_RECEIVED_DATE":"2015-05-12 11:23:34","FIR_MLC_NO":"0","G":3,"HOSPITAL_CITY":"HYDERABAD","HOSPITAL_NAME":"BAPUJI HOSPITAL","HOSPITAL_NO":423423,"HOSPITAL_PINCODE":23432432,"HOSPITAL_STATE":"TELANGANA","ICD_CODE":"A15","INSURER_MEMBER_ID":"GHPL1234567654","INVESTIGATION_CHARGES_CLAIM_AMOUNT":0.0,"IS_MATERNITY_CLAIM":1,"L":6,"LMP":"2014-06-19","MATERNITY_REMARKS":"OKK","MEDICINE_CHARGES_CLAIM_AMOUNT":345.0,"MISCELLANEOUS_CHARGES_CLAIM_AMOUNT":432.0,"MODE_OF_DELIVERY":1,"NURSING_CHARGES_CLAIM_AMOUNT":0.0,"P":2,"POLICYID":4632,"PROCEDURE":"MEDICAL","PROCEDURE_CODE":"A-435","PROCEDURE_DESC":"PROCEDURE DEC","SURGERY_CHARGES_CLAIM_AMOUNT":3.0,"TOTAL_CLAIM_AMOUNT":15000.0,"TPA_PAT_CARD_ID":"GHPL1234567654","TPA_PREAUTH_NO":"12345","COMMUNICATION_REMARKS":"Yes","IS_ROAD_ACCIDENT":"Yes","TYPE_OF_CLAIM":1,"TPA_CLAIM_NO":"12345"},"SERVICE_USERNAME":"iirmuseid","SERVICE_PWD":"iirm$123#"}';
        //IIRM OK
        //var msg='{"ClaimRegister":{"A":3,"AILMENT":"AILMENT","ALCOHOL_INFLUENCE":0,"BED_NO":"3","BILL_DATE":"2015-05-12","BILL_NO":"43","CLAIM_FILE_NO":"12345","CLAIM_REGISTERED_DATE":"2015-05-15 11:23:34","CLAIM_STATUS":"SETTLED","CLAIM_STATUS_STATE":8,"CONSULTANT_CHARGES_CLAIM_AMOUNT":345.0,"DATE_OF_ADMISSION":"2015-05-14 11:23:34","DATE_OF_DISCHARGE":"2015-05-19 11:23:34","DATE_OF_INTIMATION":"2015-05-11 11:23:34","DIAGNOSIS_DESC":"FEVER","EDD":"2015-06-19","FILE_RECEIVED_DATE":"2015-05-12 11:23:34","FIR_MLC_NO":"0","G":3,"HOSPITAL_CITY":"HYDERABAD","HOSPITAL_NAME":"BAPUJI HOSPITAL","HOSPITAL_NO":423423,"HOSPITAL_PINCODE":23432432,"HOSPITAL_STATE":"TELANGANA","ICD_CODE":"A15","INSURER_MEMBER_ID":"GHPL1234567654","INVESTIGATION_CHARGES_CLAIM_AMOUNT":0.0,"IS_MATERNITY_CLAIM":1,"L":6,"LMP":"2014-06-19","MATERNITY_REMARKS":"OKK","MEDICINE_CHARGES_CLAIM_AMOUNT":345.0,"MISCELLANEOUS_CHARGES_CLAIM_AMOUNT":432.0,"MODE_OF_DELIVERY":1,"NURSING_CHARGES_CLAIM_AMOUNT":0.0,"P":2,"POLICY_ID":342,"PROCEDURE":"MEDICAL","PROCEDURE_CODE":"A-435","PROCEDURE_DESC":"PROCEDURE DEC","SURGERY_CHARGES_CLAIM_AMOUNT":3.0,"TOTAL_CLAIM_AMOUNT":15000.0,"TPA_PAT_CARD_ID":"GHPL1234567654","TPA_PREAUTH_NO":"12345","TYPE_OF_CLAIM":1,"TPA_CLAIM_NO":"12345"},"UserName":"iirmuseid","Password":"iirm$123#"}';
        
       var msg='{"ClaimRegister":{"A":3,"AILMENT":"AILMENT","ALCOHOL_INFLUENCE":0,"BED_NO":"3","BILL_DATE":"2015-05-12","BILL_NO":"43","CLAIM_FILE_NO":"12345","CLAIM_REGISTERED_DATE":"2015-05-15 11:23:34","CLAIM_STATUS":"SETTLED","CLAIM_STATUS_STATE":8,"CONSULTANT_CHARGES_CLAIM_AMOUNT":345.0,"DATE_OF_ADMISSION":"2015-05-14 11:23:34","DATE_OF_DISCHARGE":"2015-05-19 11:23:34","DATE_OF_INTIMATION":"2015-05-11 11:23:34","DIAGNOSIS_DESC":"FEVER","EDD":"2015-06-19","FILE_RECEIVED_DATE":"2015-05-12 11:23:34","FIR_MLC_NO":"0","G":3,"HOSPITAL_CITY":"HYDERABAD","HOSPITAL_NAME":"BAPUJI HOSPITAL","HOSPITAL_NO":423423,"HOSPITAL_PINCODE":23432432,"HOSPITAL_STATE":"TELANGANA","ICD_CODE":"A15","INSURER_MEMBER_ID":"GHPL1234567654","INVESTIGATION_CHARGES_CLAIM_AMOUNT":0.0,"IS_MATERNITY_CLAIM":1,"L":6,"LMP":"2014-06-19","MATERNITY_REMARKS":"OKK","MEDICINE_CHARGES_CLAIM_AMOUNT":345.0,"MISCELLANEOUS_CHARGES_CLAIM_AMOUNT":432.0,"MODE_OF_DELIVERY":1,"NURSING_CHARGES_CLAIM_AMOUNT":0.0,"P":2,"POLICY_ID":4632,"PROCEDURE":"MEDICAL","PROCEDURE_CODE":"A-435","PROCEDURE_DESC":"PROCEDURE DEC","SURGERY_CHARGES_CLAIM_AMOUNT":3.0,"TOTAL_CLAIM_AMOUNT":15000.0,"TPA_PAT_CARD_ID":"GHPL1234567654","TPA_PREAUTH_NO":"12345","TYPE_OF_CLAIM":1,"TPA_CLAIM_NO":"12345"},"UserName":"iirmuseid","Password":"iirm$123#"}';      
      
        $.ajax({
        	
            url: "http://lion.isbsindia.in/vidalSVC/IIRMVidal.svc/ClaimRegister",
            //url: "http://lion.isbsindia.in/bonesante/Ecard.svc/TrackCardByGHID",
            type: "POST",
            data: msg,
            //data: JSON.stringify({ cardId: 'GHOI0200000436' }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            processData: true,
            // responseType: 'arraybuffer',
            success: function (response) {
               // onRequestEnd();
                //alert("suc");
                alert(JSON.stringify(response));
                //alert(response.d.FILENAME);
                 var form = document.createElement("form");
                form.action = "./DownEcardTrackCardAction";
                form.method = "POST";
                form.target = "./DownEcardTrackCardAction" || "_self";
                if (response) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = 'key';
                    input.value = response.d.BASE64ARRAY;
                    form.appendChild(input);

                    var ifilename = document.createElement('input');
                    ifilename.type = 'hidden';
                    ifilename.name = 'filename';
                    ifilename.value = response.d.FILENAME;
                    form.appendChild(ifilename);
                }
                form.style.display = 'none';
                document.body.appendChild(form);
                form.submit(); 
            },
            error: function (response) {
              //  onRequestEnd();
                alert('Failed: ' + response.statusText);
            }
        });
    }  */
    
    
    
  //Function to generate ecard using jquery post data
	 function InsertProduct() {
        
        var msg='{"TrackCard":[{"MODULEID":"1","CARDID":"GHNA0100302019","EMPID":"497","AGE":"26","UWID":"14","CURRENTPOLICYID":"4248"}]}';
      
        $.ajax({
        	
            url: "http://lion.isbsindia.in/bonesante/Ecard.svc/TrackCard",
            //url: "http://lion.isbsindia.in/bonesante/Ecard.svc/TrackCardByGHID",
            type: "POST",
            data: msg,
            //data: JSON.stringify({ cardId: 'GHOI0200000436' }),
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            processData: true,
            // responseType: 'arraybuffer',
            success: function (response) {
               // onRequestEnd();
                //alert("suc");
                alert(JSON.stringify(response));
                //alert(response.d.FILENAME);
                 var form = document.createElement("form");
                form.action = "./DownEcardTrackCardAction";
                form.method = "POST";
                form.target = "./DownEcardTrackCardAction" || "_self";
                if (response) {
                    var input = document.createElement('input');
                    input.type = 'hidden';
                    input.name = 'key';
                    input.value = response.d.BASE64ARRAY;
                    form.appendChild(input);

                    var ifilename = document.createElement('input');
                    ifilename.type = 'hidden';
                    ifilename.name = 'filename';
                    ifilename.value = response.d.FILENAME;
                    form.appendChild(ifilename);
                }
                form.style.display = 'none';
                document.body.appendChild(form);
                form.submit(); 
            },
            error: function (response) {
              //  onRequestEnd();
                alert('Failed: ' + response.statusText);
            }
        });
    }
    
    function ddlPolicychange(policyId)
	 {
		 //var selectedState = statesddl.options[statesddl.selectedIndex].value;
		 // var selCityId = statesddl.value.split("~");
		// var policyIdSelected = selCityId[0]; 
		//alert(policyId);
		var xmlhttp;
		 var strAjUrlData="GetEndorDDLDataAction?key="+policyId;
		 if (window.XMLHttpRequest)
		 {
			 // code for IE7+, Firefox, Chrome, Opera, Safari
		     xmlhttp=new XMLHttpRequest();
		 }
		 else
		 {
		  // code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		  xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200)
		   {
			  //alert(xmlhttp.responseText);	
			  document.getElementById('ddlEndor').innerHTML=xmlhttp.responseText;
		   }
		   else
		   {
		   //alert(xmlhttp.status);
		   }
		  }
		   xmlhttp.open("POST",strAjUrlData,true);
		   xmlhttp.send();
		 
	 }
    
    
    function validateForm()
    {
    	 var message = "VALIDATION MESSAGE";
    	 var policyddl = document.getElementById("ddlPolicy");
		 var policyddlId = policyddl.options[policyddl.selectedIndex].value;
		 
		 if(policyddlId == 0)
		 {
			 CustomConfirmation('Please select valid Policy',message);
			 return false; 
		 }
    	
    	
    }
    
	
</script>


<h3 align="center">Generate Cards</h3>


<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp"%>
		<td align="center" width="100%">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" class="tablebg">
				<tr>
					<td>
					
						<fieldset>
							<legend>Search Criteria</legend>
							<form name="policyForm" method="post" action="#" >

								<table width="100%" border="1" cellpadding="0" cellspacing="0" class="tablebg">
								
									
									
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									
									
									
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									
									
									<tr>
										<td align="center"><input type="button" name="Submit" value="Generate Lot" class="button" onClick="InsertProduct();" /></td>
									</tr>
								</table>
							</form>
						</fieldset>

					</td>
				</tr>
			</table>
			</td>
	</tr>
</table>

<%@include file="footer.jsp"%>