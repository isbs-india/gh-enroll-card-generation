<%@include file="header.jsp"%>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui.min.js"></script> 
<link href="<%=request.getContextPath()%>/js/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/css/1024.css" rel="stylesheet" type="text/css">
<link href="<%=request.getContextPath()%>/js/jquery-ui.theme.min.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/cycle_plugin.js"></script>

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css">

<script src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui.js"></script>
<script src="<%=request.getContextPath()%>/js/jquery-ui-sliderAccess.js"></script>

<script type="text/javascript">
	function preventBack() {
		window.history.forward();
	}
	setTimeout("preventBack()", 0);
	window.onunload = function() {
		null
	};

	function myTrim(x) {
		return x.replace(/^\s+|\s+$/gm, '');
	}
	
	function CustomConfirmation(msg, title_msg){
		if ($('#ConfirmMessage').length == 0) {
		        $(document.body).append('<div id="ConfirmMessage">'+msg+'</div>');
		    } else {
		        $('#ConfirmMessage').html(msg);
		    }

		    $("#ConfirmMessage").dialog({
		        autoOpen: false,
				title: title_msg,
		        show: "blind",
		        hide: "explode",  
				resizable: false,      
		        height: 200,
		        width: 300,
		        modal: true,
				buttons: {
		            "Ok": function() 
		            {
		                $( this ).dialog( "close" );
		            }
				}
		    });
		  
		    $( "#ConfirmMessage" ).dialog("open");
		}
    

    
    function validateForm()
    {
    	 var message = "VALIDATION MESSAGE";
    	
		 var xlfile = document.getElementById("xlfile").value;
		 if(xlfile==""){
				CustomConfirmation("please browse excel file only",message);
				document.getElementById("xlfile").focus();
				return false;
			}
		 if(xlfile!=""){
			 if(xlfile.lastIndexOf(".xlsx")==-1 && xlfile.lastIndexOf(".xls")==-1){
				 CustomConfirmation("Only xls, xlsx  files can be uploaded",message);
				 return false;
			 }
		 } 
    }
 
</script>
<h3 align="center">Policy Numbers upload</h3>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<%@include file="leftmenu.jsp"%>
		<td align="center" width="100%" valign="top">
			<table width="80%" border="0" cellpadding="1" cellspacing="1" class="tablebg">
				<tr>
					<td>
						<fieldset>
							<legend>Upload Policy Numbers</legend>
							
							<form name="policyForm" method="post" action="InsertPolicyLotDetailsAction" onSubmit="return validateForm();" enctype="multipart/form-data">
								<table width="70%" border="20" cellpadding="0" cellspacing="0" class="tablebg">
								<input type="hidden" name="userId" id="userId" value="<%=session.getAttribute("userId")%>">
								<input type="hidden" name="XLpolicyForm" id="" value="XLPolicyData" />
									
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<tr>
									<td  width="70%"> <table>
									    <tr>
											<td align="left" width="33%">
											<input type="file" size="30" name="xlfile" id="xlfile" /> &nbsp;&nbsp;&nbsp; (<font color="red"> Note : Only excel Row count should be less than 900 records are eligible</font>) </td>
										</tr>
										<tr>
										<td align="left">&nbsp;</td>
									</tr>
									</table>
									</td>
									</tr>
									 <tr>
										<td align="right" colspan="2"><a href="./DownLoadTemplateAction?fileName=RETAILPOLICYNUMBERTemplate.xlsx"><h4>Sample Upload Template </h4></h4></a> </td>
									</tr> 
									<tr>
										<td align="left">&nbsp;</td>
									</tr>
									<tr>
										<td align="center" colspan="2"><input type="submit" name="Submit" value="Generate Lot" class="button" /></td>
									</tr>
								</table>
							</form>
						</fieldset>

					</td>
				</tr>
			</table>
			</td>
	</tr>
</table>

<%@include file="footer.jsp"%>