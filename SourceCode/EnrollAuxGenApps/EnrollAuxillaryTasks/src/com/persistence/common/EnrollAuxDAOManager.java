package com.persistence.common;

import java.util.List;

import com.exception.DAOException;
import com.model.EndorsementBean;
import com.model.InsertLotDetailsBean;
import com.model.PolicyDataBean;
import com.model.PolicyListDataBean;

public class EnrollAuxDAOManager {

	public static PolicyDataBean getPoliciesDDLDetails(int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().getPoliciesDDLDetails(moduleId);

		}catch (DAOException e) {

		}
		return null;

	}
	
	public static PolicyDataBean getPoliciesDDLDetailsInActive(int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().getPoliciesDDLDetailsInActive(moduleId);

		}catch (DAOException e) {

		}
		return null;

	}

	public static List<EndorsementBean> getEndorDDLDetails(int policyId, int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().getEndorDDLDetails(policyId, moduleId);

		}catch (DAOException e) {

		}
		return null;

	}
	
	public static List<EndorsementBean> getEndorDDLDetailsInActive(int policyId, int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().getEndorDDLDetailsInActive(policyId, moduleId);

		}catch (DAOException e) {

		}
		return null;

	}

	
	public static int putSearchDetails(InsertLotDetailsBean insertlotBeanObj) throws DAOException {

		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().putSearchDetails(insertlotBeanObj);

		}catch (DAOException e) {

		}
		return 0;

	}
	public static PolicyDataBean getLotDetails(int isCron,int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().getLotDetails(isCron,moduleId);

		}catch (DAOException e) {

		}
		return null;

	}
	public static PolicyDataBean getLotDetailsInActive(int isCron,int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().getLotDetailsInActive(isCron,moduleId);

		}catch (DAOException e) {

		}
		return null;

	}
	public static int insertSelfCardDetails(String xml,String param) throws DAOException {
		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().insertSelfCardDetails(xml,param);

		}catch (DAOException e) {

		}
		return 0;

	}
	
	public static int insertSelfCardDetailsInActive(String xml,String param) throws DAOException {
		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().insertSelfCardDetailsInActive(xml,param);

		}catch (DAOException e) {

		}
		return 0;

	}
	public static int insertWINDSelfCardDetails(String xml,String param) throws DAOException {
		try {
			return DAOFactory.getInstance().getEnrollAuxDAO().insertWINDSelfCardDetails(xml,param);

		}catch (DAOException e) {

		}
		return 0;

	}
	
	//inserting xml,param,endorsement,policyId,lotUserId to GHPL database 
		public static PolicyDataBean insertGHPLSelfCardDetails(String xml, String param,String endorsement,String policyId,String lotUserId) throws DAOException {
			try {
				return DAOFactory.getInstance().getEnrollAuxDAO().insertGHPLSelfCardDetails(xml,param,endorsement,policyId,lotUserId);
			}catch (DAOException e) {
			}
			return null;

		}
		
		public static PolicyDataBean insertGHPLSelfCardDetailsInActive(String xml, String param,String endorsement,String policyId,String lotUserId) throws DAOException {
			try {
				return DAOFactory.getInstance().getEnrollAuxDAO().insertGHPLSelfCardDetailsInActive(xml,param,endorsement,policyId,lotUserId);
			}catch (DAOException e) {
			}
			return null;

		}
	//Label print start
		public static PolicyDataBean getPoliciesDetails(int moduleId) throws DAOException {
			try {
				return DAOFactory.getInstance().getEnrollAuxDAO().getPoliciesDetails(moduleId);

			}catch (DAOException e) {

			}
			return null;
		}
		public static PolicyDataBean getLabelLotDetails(int isCron,int moduleId) throws DAOException {
			try {
				return DAOFactory.getInstance().getEnrollAuxDAO().getLabelLotDetails(isCron,moduleId);

			}catch (DAOException e) {

			}
			return null;
		}
		
		public static int insertWINDLabelDetails(String xml,int moduleId) throws DAOException {
			try {
				return DAOFactory.getInstance().getEnrollAuxDAO().insertWINDLabelDetails(xml,moduleId);

			}catch (DAOException e) {

			}
			return 0;
		}
		//Label print end
	
		public static PolicyListDataBean insertPolicyNumberDetails(String xml ,String moduleUserId ,int moduleId) throws DAOException {
			try {
				return DAOFactory.getInstance().getEnrollAuxDAO().insertPolicyNumberDetails(xml,moduleUserId,moduleId);

			}catch (DAOException e) {

			}
			return null;
		}
		public static PolicyListDataBean getPolicyLotDetails(int isCron,int moduleId) throws DAOException {
			try {
				return DAOFactory.getInstance().getEnrollAuxDAO().getPolicyLotDetails(isCron,moduleId);

			}catch (DAOException e) {

			}
			return null;
		}
		
}
