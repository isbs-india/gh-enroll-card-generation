package com.persistence.common;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

//import javax.mail.MessagingException;
import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

//import com.action.SendMail;
import com.exception.DAOException;
import com.model.EndorsementBean;
import com.model.InsertLotDetailsBean;
import com.model.PoliciesBean;
import com.model.PolicyDataBean;
import com.model.PolicyListDataBean;
import com.model.StatusBean;
import com.util.ProcedureConstants;
//import com.util.Util;
//import com.util.Constants;

/**
 * @author prabhakar Gogula
 * 
 */
public class EnrollAuxDAOImpl extends DAOBase implements EnrollAuxDAO{
	Logger logger = Logger.getLogger("EnrollAuxDAOImpl");



	//For retrieving policy DDL data
	
	/* (non-Javadoc)
	 * @see com.persistence.common.EnrollAuxDAO#getPoliciesDDLDetails(int)
	 */
	public PolicyDataBean getPoliciesDDLDetails(int moduleId) throws DAOException
	{
		logger.info("enter getPoliciesDDLDetails");
		PoliciesBean polBeanObj = null;
		List<PoliciesBean> polBeanListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		String statusMessage="";
		StatusBean statusBean= null;
		PolicyDataBean policyDataBean = new PolicyDataBean();
		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			System.out.println("moduleId  : "+moduleId);
			con =getMyConnection(moduleId);
			System.out.println("con  : "+con);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_POLICIES_DDL_CORP);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			logger.info("before execute   PROC_FOR_GET_POLICIES_DDL_CORP");
			cstmt.execute();
			logger.info("after execute  PROC_FOR_GET_POLICIES_DDL_CORP");
			rs = (ResultSet) cstmt.getObject(indexpos);
			polBeanListObj = new ArrayList<PoliciesBean>();
			while(rs.next()) {
				polBeanObj = new PoliciesBean();
				polBeanObj.setPolicyId(rs.getInt("POLICYID"));
				polBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				polBeanObj.setPolicyFrom(rs.getString("POLICYFROM"));
				polBeanObj.setPolicyTo(rs.getString("POLICYTO"));
				polBeanListObj.add(polBeanObj);
			}//while
			
			if(polBeanListObj.size()>0){
				policyDataBean.setPolBeanListObj(polBeanListObj);
				statusBean = new StatusBean();
				statusBean.setStatusFlag(1);
				statusBean.setStatusMessage("records got it");
				policyDataBean.setStatusBean(statusBean);
			}else{
				statusMessage ="No data found";
				statusBean = new StatusBean();
				statusBean.setStatusFlag(0);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
			}
		}
		catch(Exception ex)
		{
			logger.error(ex);
			if(con==null){
				statusMessage ="Database connection null <br/>";
			}
			if(cstmt==null){
				statusMessage +="procedure related exception <br/>";
			}
			statusMessage +="Error in getPoliciesDDLDetails:  "+ex;
			statusBean = new StatusBean();
			statusBean.setStatusFlag(2);
			statusBean.setStatusMessage(statusMessage);
			policyDataBean.setStatusBean(statusBean);
			//throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getPoliciesDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return policyDataBean;
	}
	
	public PolicyDataBean getPoliciesDDLDetailsInActive(int moduleId) throws DAOException
	{
		logger.info("enter getPoliciesDDLDetails");
		PoliciesBean polBeanObj = null;
		List<PoliciesBean> polBeanListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		String statusMessage="";
		StatusBean statusBean= null;
		PolicyDataBean policyDataBean = new PolicyDataBean();
		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			System.out.println("moduleId  : "+moduleId);
			con =getMyConnection(moduleId);
			System.out.println("con  : "+con);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_POLICIES_DDL_CORP_EXP);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			logger.info("before execute   PROC_FOR_GET_POLICIES_DDL_CORP");
			cstmt.execute();
			logger.info("after execute  PROC_FOR_GET_POLICIES_DDL_CORP");
			rs = (ResultSet) cstmt.getObject(indexpos);
			polBeanListObj = new ArrayList<PoliciesBean>();
			while(rs.next()) {
				polBeanObj = new PoliciesBean();
				polBeanObj.setPolicyId(rs.getInt("POLICYID"));
				polBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				polBeanObj.setPolicyFrom(rs.getString("POLICYFROM"));
				polBeanObj.setPolicyTo(rs.getString("POLICYTO"));
				polBeanListObj.add(polBeanObj);
			}//while
			
			if(polBeanListObj.size()>0){
				policyDataBean.setPolBeanListObj(polBeanListObj);
				statusBean = new StatusBean();
				statusBean.setStatusFlag(1);
				statusBean.setStatusMessage("records got it");
				policyDataBean.setStatusBean(statusBean);
			}else{
				statusMessage ="No data found";
				statusBean = new StatusBean();
				statusBean.setStatusFlag(0);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
			}
		}
		catch(Exception ex)
		{
			logger.error(ex);
			if(con==null){
				statusMessage ="Database connection null <br/>";
			}
			if(cstmt==null){
				statusMessage +="procedure related exception <br/>";
			}
			statusMessage +="Error in getPoliciesDDLDetails:  "+ex;
			statusBean = new StatusBean();
			statusBean.setStatusFlag(2);
			statusBean.setStatusMessage(statusMessage);
			policyDataBean.setStatusBean(statusBean);
			//throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getPoliciesDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return policyDataBean;
	}


	//For retrieving endorsement DDL data based on I/P policyId
	public List<EndorsementBean> getEndorDDLDetails(int policyId, int moduleId) throws DAOException
	{
		EndorsementBean endorBeanObj = null;
		List<EndorsementBean> endorBeanListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;

		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			con =getMyConnection(moduleId);
			if(moduleId==1 || moduleId ==2){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_ENDORSEMENT_DDL_CORP);
			}
			
			cstmt.setInt(++indexpos,policyId);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			endorBeanListObj = new ArrayList<EndorsementBean>();
			while(rs.next()) {
				endorBeanObj = new EndorsementBean();
				endorBeanObj.setEndorsementId(rs.getInt("ENDORSEMENT_ID"));
				endorBeanObj.setEndorsementNo(rs.getString("ENDORSEMENTNO"));
				endorBeanObj.setEndorEffectiveDate(rs.getString("EFFECTIVEDATE"));
				endorBeanListObj.add(endorBeanObj);
			}//while


		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getEndorDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return endorBeanListObj;
	}
	
	
	public List<EndorsementBean> getEndorDDLDetailsInActive(int policyId, int moduleId) throws DAOException
	{
		EndorsementBean endorBeanObj = null;
		List<EndorsementBean> endorBeanListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;

		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			con =getMyConnection(moduleId);
			if(moduleId==1 || moduleId ==2){
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_ENDORSEMENT_DDL_CORP_EXP);
			}
			
			cstmt.setInt(++indexpos,policyId);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			endorBeanListObj = new ArrayList<EndorsementBean>();
			while(rs.next()) {
				endorBeanObj = new EndorsementBean();
				endorBeanObj.setEndorsementId(rs.getInt("ENDORSEMENT_ID"));
				endorBeanObj.setEndorsementNo(rs.getString("ENDORSEMENTNO"));
				endorBeanObj.setEndorEffectiveDate(rs.getString("EFFECTIVEDATE"));
				endorBeanListObj.add(endorBeanObj);
			}//while


		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getEndorDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return endorBeanListObj;
	}
	
	public PolicyDataBean getLotDetails(int isCron, int moduleId) throws DAOException
	{
		logger.info("ente getLotDetails method    isCron  "+isCron);
		InsertLotDetailsBean insertLotDetailsBeanObj = null;
		List<InsertLotDetailsBean> lotListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		PolicyDataBean policyDataBean= new PolicyDataBean();
		ResultSet rs=null;
		int indexpos=0;
		String statusMessage="";
		StatusBean statusBean= null;
		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_LOT_DETAILS);
			cstmt.setInt(++indexpos,isCron);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			lotListObj = new ArrayList<InsertLotDetailsBean>();
			
			int serialNo=0;
			while(rs.next()) {
				insertLotDetailsBeanObj = new InsertLotDetailsBean();
				insertLotDetailsBeanObj.setSerialNo(++serialNo);
				insertLotDetailsBeanObj.setLotNumber(rs.getString("LOTID"));
				//insertLotDetailsBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				
				String moduleName="";
				int modId=0;
		    	 if(moduleId==1){
		    		 insertLotDetailsBeanObj.setEndorsementNo(rs.getString("ENDORSEMENTNO"));
		    		 moduleName="Corp";
		    		 modId=1;
		    	 }else if(moduleId==2){
		    		 moduleName="Bank Assurance";
		    		 modId=2;
		    	 }else if(moduleId==4){
		    		 moduleName="Individual";
		    		 modId=4;
		    	 }
		    	 if(moduleId==2){
		    		 	/*insertLotDetailsBeanObj.setLotNumber(rs.getInt("LOTID"));
						insertLotDetailsBeanObj.setPolicyId(rs.getInt("POLICYID"));
						insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
						insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
						insertLotDetailsBeanObj.setCardCount(rs.getInt("CARDCOUNT"));*/
						insertLotDetailsBeanObj.setFormatId(rs.getString("FORMATID"));
						/*insertLotDetailsBeanObj.setContactEmailId(rs.getString("CONTACTEMAIL"));
				    	insertLotDetailsBeanObj.setEmpName(rs.getString("username"));
				    	insertLotDetailsBeanObj.setGenInsertStausFlag(rs.getInt("gen_ins_cer"));*/
		    	 }
		    	 insertLotDetailsBeanObj.setModuleName(moduleName);
		    	 insertLotDetailsBeanObj.setModuleId(modId);
		    	 insertLotDetailsBeanObj.setStatusDesc(rs.getString("STATUSDESC"));
		    	 logger.info("FORMATID   "+rs.getString("FORMATID")+"                STATUSDESC----    "+rs.getString("STATUSDESC") +"       LOTSTATUSFLAG    "+rs.getInt("LOTSTATUSFLAG"));
		    	 insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
		    	 insertLotDetailsBeanObj.setUserName(rs.getString("USERNAME"));
		    	 insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
		    	 lotListObj.add(insertLotDetailsBeanObj);
			}//while
			if(lotListObj.size()>0){
				policyDataBean.setLotListObj(lotListObj);
				statusBean = new StatusBean();
				statusBean.setStatusFlag(1);
				statusBean.setStatusMessage("records success fully got it");
				policyDataBean.setStatusBean(statusBean);
			}else{
				statusMessage ="No data found";
				statusBean = new StatusBean();
				statusBean.setStatusFlag(0);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
			}
		}
		catch(Exception ex)
		{
			logger.error(ex);
			if(con==null){
				statusMessage ="Database connection null <br/>";
			}
			if(cstmt==null){
				statusMessage +="procedure related exception <br/>";
			}
			statusMessage +="Error in getLotDetails:  "+ex;
			statusBean = new StatusBean();
			statusBean.setStatusFlag(2);
			statusBean.setStatusMessage(statusMessage);
			policyDataBean.setStatusBean(statusBean);
			//throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getEndorDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return policyDataBean;
	}


	//For inserting the search data into ECARDS_LOT table
	public int putSearchDetails(InsertLotDetailsBean insertlotBeanObj) throws DAOException
	{
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		int errorFlag=0;

		try
		{
			con =getMyConnection(insertlotBeanObj.getModuleId());
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_PUT_SEARCH_DATA_CORP);
			//cstmt.setString(++indexpos,insertlotBeanObj.getLotNumber());
			cstmt.setInt(++indexpos,insertlotBeanObj.getPolicyId());
			cstmt.setInt(++indexpos,insertlotBeanObj.getEndorsementId());
			cstmt.setInt(++indexpos,insertlotBeanObj.getModuleId());
			cstmt.setInt(++indexpos,insertlotBeanObj.getUserId());
			cstmt.execute();
		}
		catch(Exception ex)
		{
			errorFlag=1;
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getEndorDDLDetails:"+ex);
				throw new DAOException();
			}
		}

		return errorFlag;

	}
	public int insertSelfCardDetails(String xml,String param) throws DAOException
	{
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		int errorFlag=0;
		try
		{
			con =getMyConnection(2);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FORP_ROC_LOT_INSERT_AB);
			cstmt.setString(++indexpos,xml);
			cstmt.setString(++indexpos,param);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			errorFlag=  cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN insertSelfCardDetails:"+ex);
				throw new DAOException();
			}
		}
		return errorFlag;
	}
	
	
	public int insertSelfCardDetailsInActive(String xml,String param) throws DAOException
	{
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		int errorFlag=0;
		try
		{
			con =getMyConnection(2);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FORP_ROC_LOT_INSERT_EXP_AB);
			cstmt.setString(++indexpos,xml);
			cstmt.setString(++indexpos,param);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			errorFlag=  cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN insertSelfCardDetails:"+ex);
				throw new DAOException();
			}
		}
		return errorFlag;
	}
	
	public int insertWINDSelfCardDetails(String xml,String param) throws DAOException
	{
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;
		int errorFlag=0;
		try
		{
			con =getMyConnection(4);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FORP_ROC_LOT_INSERT_WIND);
			cstmt.setString(++indexpos,xml);
			cstmt.setString(++indexpos,param);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			errorFlag=  cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN insertSelfCardDetails:"+ex);
				throw new DAOException();
			}
		}
		return errorFlag;
	}
	
	/**
	 * based on under writer wise choosing policy user upload cardid's convert into xml  those details insert in database 
	 */
		@Override
		public PolicyDataBean insertGHPLSelfCardDetails(String xml, String param,String endorsement,String policyId,String lotUserId) throws DAOException
		{
			logger.info("enter insertGHPLSelfCardDetails");
			Connection con = null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			int indexpos=0;
			String statusMessage="";
			StatusBean statusBean= null;
			PolicyDataBean policyDataBean = new PolicyDataBean();
			try
			{
				con =getMyConnection(1);
				cstmt = con.prepareCall(ProcedureConstants.PROC_LOT_INSERT_XML_CORP);
				cstmt.setInt(++indexpos,Integer.parseInt(policyId));
				cstmt.setInt(++indexpos,Integer.parseInt(endorsement));
				cstmt.setInt(++indexpos,1);
				cstmt.setInt(++indexpos,Integer.parseInt(lotUserId));
				cstmt.setString(++indexpos,xml);
				cstmt.setString(++indexpos,param);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				//errorFlag=  cstmt.getInt(indexpos);
				logger.info("after execute ");
				if(cstmt.getInt(indexpos) == 1){
					statusMessage ="Data inserted successfully<br/><br/>";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage(statusMessage);
					policyDataBean.setStatusBean(statusBean);
				}else{
					statusMessage ="Data insertion failed<br/><br/>";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage(statusMessage);
					policyDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				ex.printStackTrace();
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in insertGHPLSelfCardDetails:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertSelfCardDetails:"+ex);
					throw new DAOException();
				}
			}
			return policyDataBean;
		}
		
		
		public PolicyDataBean insertGHPLSelfCardDetailsInActive(String xml, String param,String endorsement,String policyId,String lotUserId) throws DAOException 
		{
			logger.info("enter insertGHPLSelfCardDetails");
			Connection con = null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			int indexpos=0;
			String statusMessage="";
			StatusBean statusBean= null;
			PolicyDataBean policyDataBean = new PolicyDataBean();
			try
			{
				con =getMyConnection(1);
				cstmt = con.prepareCall(ProcedureConstants.PROC_LOT_INSERT_XML_CORP_EXP);
				cstmt.setInt(++indexpos,Integer.parseInt(policyId));
				cstmt.setInt(++indexpos,Integer.parseInt(endorsement));
				cstmt.setInt(++indexpos,1);
				cstmt.setInt(++indexpos,Integer.parseInt(lotUserId));
				cstmt.setString(++indexpos,xml);
				cstmt.setString(++indexpos,param);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				//errorFlag=  cstmt.getInt(indexpos);
				logger.info("after execute ");
				if(cstmt.getInt(indexpos) == 1){
					statusMessage ="Data inserted successfully<br/><br/>";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage(statusMessage);
					policyDataBean.setStatusBean(statusBean);
				}else{
					statusMessage ="Data insertion failed<br/><br/>";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage(statusMessage);
					policyDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				ex.printStackTrace();
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in insertGHPLSelfCardDetails:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertSelfCardDetails:"+ex);
					throw new DAOException();
				}
			}
			return policyDataBean;
		}
		
		
		// Label Print start
		public PolicyDataBean getPoliciesDetails(int moduleId) throws DAOException	
		{
			logger.info("enter getPoliciesDetails");
			PoliciesBean policyBeanObj = null;
			List<PoliciesBean> policyBeanListObj = null;
			Connection con = null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			int indexpos=0;
			String statusMessage="";
			StatusBean statusBean= null;
			PolicyDataBean policyDataBean = new PolicyDataBean();
			try
			{
				//Retrieving the policies from CORP GoddHealth Module
				System.out.println("moduleId  : "+moduleId);
				con =getMyConnection(moduleId);
				System.out.println("con  : "+con);
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_POLICIES_CORP);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				logger.info("before execute   PROC_FOR_GET_POLICIES_DDL_CORP");
				cstmt.execute();
				logger.info("after execute  PROC_FOR_GET_POLICIES_DDL_CORP");
				rs = (ResultSet) cstmt.getObject(indexpos);
				policyBeanListObj = new ArrayList<PoliciesBean>();
				while(rs.next()) {
					policyBeanObj = new PoliciesBean();
					policyBeanObj.setPolicyId(rs.getInt("POLICY_ID"));
					policyBeanObj.setPolicyHolderName(rs.getString("POLICY_NAME"));
					policyBeanListObj.add(policyBeanObj);
				}//while
				
				if(policyBeanListObj.size()>0){
					policyDataBean.setPolBeanListObj(policyBeanListObj);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage("records got it");
					policyDataBean.setStatusBean(statusBean);
				}else{
					statusMessage ="No data found";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage(statusMessage);
					policyDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in getPoliciesDDLDetails:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN getPoliciesDDLDetails:"+ex);
					throw new DAOException();
				}
			}
			return policyDataBean;
		}
		
		
		public PolicyDataBean getLabelLotDetails(int isCron, int moduleId) throws DAOException
		{
			logger.info("ente getLotDetails method    isCron  "+isCron);
			InsertLotDetailsBean insertLotDetailsBeanObj = null;
			List<InsertLotDetailsBean> lotListObj = null;
			Connection con = null;
			CallableStatement cstmt = null;
			PolicyDataBean policyDataBean= new PolicyDataBean();
			ResultSet rs=null;
			int indexpos=0;
			String statusMessage="";
			StatusBean statusBean= null;
			try
			{
				//Retrieving the policies from CORP GoddHealth Module
				con =getMyConnection(moduleId);
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_LOT_Label_DETAILS);
				cstmt.setInt(++indexpos,isCron);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(indexpos);
				lotListObj = new ArrayList<InsertLotDetailsBean>();
				
				int serialNo=0;
				while(rs.next()) {
					insertLotDetailsBeanObj = new InsertLotDetailsBean();
					insertLotDetailsBeanObj.setSerialNo(++serialNo);
					insertLotDetailsBeanObj.setLotNumber(rs.getString("LOTID"));
					//insertLotDetailsBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
					
					String moduleName="";
					int modId=0;
			    	 if(moduleId==1){
			    		// insertLotDetailsBeanObj.setEndorsementNo(rs.getString("ENDORSEMENTNO"));
			    		 moduleName="Corp";
			    		 modId=1;
			    	 }else if(moduleId==2){
			    		 moduleName="Bank Assurance";
			    		 modId=2;
			    	 }else if(moduleId==4){
			    		 moduleName="Individual";
			    		 modId=4;
			    	 }
			    	 if(moduleId==2){
							insertLotDetailsBeanObj.setFormatId(rs.getString("FORMATID"));
			    	 }
			    	 insertLotDetailsBeanObj.setModuleName(moduleName);
			    	 insertLotDetailsBeanObj.setModuleId(modId);
			    	 insertLotDetailsBeanObj.setStatusDesc(rs.getString("STATUSDESC"));
			    	 logger.info("FORMATID   "+rs.getString("FORMATID")+"                STATUSDESC----    "+rs.getString("STATUSDESC") +"       LOTSTATUSFLAG    "+rs.getInt("LOTSTATUSFLAG"));
			    	 insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
			    	 insertLotDetailsBeanObj.setUserName(rs.getString("USERNAME"));
			    	 insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
			    	 lotListObj.add(insertLotDetailsBeanObj);
				}//while
				if(lotListObj.size()>0){
					policyDataBean.setLotListObj(lotListObj);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage("records success fully got it");
					policyDataBean.setStatusBean(statusBean);
				}else{
					statusMessage ="No data found";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage(statusMessage);
					policyDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in getLotDetails:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN getEndorDDLDetails:"+ex);
					throw new DAOException();
				}
			}
			return policyDataBean;
		}
		
		public int insertWINDLabelDetails(String xml,int moduleId) throws DAOException 
		{
			Connection con = null;
			CallableStatement cstmt = null;
			ResultSet rs=null;
			int indexpos=0;
			int errorFlag=0;
			try
			{
				con =getMyConnection(moduleId);
				cstmt = con.prepareCall(ProcedureConstants.PROC_LABEL_LOT_INSERT);
				cstmt.setString(++indexpos,xml);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				errorFlag=  cstmt.getInt(indexpos);
			}
			catch(Exception ex)
			{
				logger.error(ex);
				ex.printStackTrace();
				throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertSelfCardDetails:"+ex);
					throw new DAOException();
				}
			}
			return errorFlag;
		}

		@Override
		public PolicyDataBean getLotDetailsInActive(int isCron, int moduleId)throws DAOException 
		{
			logger.info("ente getLotDetails method    isCron  "+isCron);
			InsertLotDetailsBean insertLotDetailsBeanObj = null;
			List<InsertLotDetailsBean> lotListObj = null;
			Connection con = null;
			CallableStatement cstmt = null;
			PolicyDataBean policyDataBean= new PolicyDataBean();
			ResultSet rs=null;
			int indexpos=0;
			String statusMessage="";
			StatusBean statusBean= null;
			try
			{
				//Retrieving the policies from CORP GoddHealth Module
				con =getMyConnection(moduleId);
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_LOT_DETAILS_EXP);
				cstmt.setInt(++indexpos,isCron);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(indexpos);
				lotListObj = new ArrayList<InsertLotDetailsBean>();
				
				int serialNo=0;
				while(rs.next()) {
					insertLotDetailsBeanObj = new InsertLotDetailsBean();
					insertLotDetailsBeanObj.setSerialNo(++serialNo);
					insertLotDetailsBeanObj.setLotNumber(rs.getString("LOTID"));
					//insertLotDetailsBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
					
					String moduleName="";
					int modId=0;
			    	 if(moduleId==1){
			    		 insertLotDetailsBeanObj.setEndorsementNo(rs.getString("ENDORSEMENTNO"));
			    		 moduleName="Corp";
			    		 modId=1;
			    	 }else if(moduleId==2){
			    		 moduleName="Bank Assurance";
			    		 modId=2;
			    	 }else if(moduleId==4){
			    		 moduleName="Individual";
			    		 modId=4;
			    	 }
			    	 if(moduleId==2){
			    		 	/*insertLotDetailsBeanObj.setLotNumber(rs.getInt("LOTID"));
							insertLotDetailsBeanObj.setPolicyId(rs.getInt("POLICYID"));
							insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
							insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
							insertLotDetailsBeanObj.setCardCount(rs.getInt("CARDCOUNT"));*/
							insertLotDetailsBeanObj.setFormatId(rs.getString("FORMATID"));
							/*insertLotDetailsBeanObj.setContactEmailId(rs.getString("CONTACTEMAIL"));
					    	insertLotDetailsBeanObj.setEmpName(rs.getString("username"));
					    	insertLotDetailsBeanObj.setGenInsertStausFlag(rs.getInt("gen_ins_cer"));*/
			    	 }
			    	 insertLotDetailsBeanObj.setModuleName(moduleName);
			    	 insertLotDetailsBeanObj.setModuleId(modId);
			    	 insertLotDetailsBeanObj.setStatusDesc(rs.getString("STATUSDESC"));
			    	 logger.info("FORMATID   "+rs.getString("FORMATID")+"                STATUSDESC----    "+rs.getString("STATUSDESC") +"       LOTSTATUSFLAG    "+rs.getInt("LOTSTATUSFLAG"));
			    	 insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
			    	 insertLotDetailsBeanObj.setUserName(rs.getString("USERNAME"));
			    	 insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
			    	 lotListObj.add(insertLotDetailsBeanObj);
				}//while
				if(lotListObj.size()>0){
					policyDataBean.setLotListObj(lotListObj);
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage("records success fully got it");
					policyDataBean.setStatusBean(statusBean);
				}else{
					statusMessage ="No data found";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage(statusMessage);
					policyDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in getLotDetails:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyDataBean.setStatusBean(statusBean);
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN getEndorDDLDetails:"+ex);
					throw new DAOException();
				}
			}
			return policyDataBean;
		}
		
		// Label Print start
		
		@Override
		public PolicyListDataBean insertPolicyNumberDetails(String xml,String moduleUserId,int moduleId) throws DAOException {
			
			Connection con = null;
			CallableStatement cstmt = null;
			PolicyListDataBean policyListDataBean= new PolicyListDataBean();
			ResultSet rs=null;
			int indexpos=0;
			int result=0;
			String statusMessage="";
			StatusBean statusBean= null;
			try
			{
				//Retrieving the policies from CORP GoddHealth Module
				con =getMyConnection(moduleId);
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_INSERT_POLICY_NUMBER_DETAILS);
				cstmt.setString(++indexpos,xml);
				cstmt.setString(++indexpos,moduleUserId);
				cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
				cstmt.execute();
				if(cstmt.getInt(indexpos) == 1){
					statusMessage ="Data inserted successfully<br/><br/>";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(1);
					statusBean.setStatusMessage(statusMessage);
					policyListDataBean.setStatusBean(statusBean);
				}else{
					statusMessage ="Data insertion failed<br/><br/>";
					statusBean = new StatusBean();
					statusBean.setStatusFlag(0);
					statusBean.setStatusMessage(statusMessage);
					policyListDataBean.setStatusBean(statusBean);
				}
			}
			catch(Exception ex)
			{
				logger.error(ex);
				if(con==null){
					statusMessage ="Database connection null <br/>";
				}
				if(cstmt==null){
					statusMessage +="procedure related exception <br/>";
				}
				statusMessage +="Error in insertPolicyNumberDetails:  "+ex;
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				policyListDataBean.setStatusBean(statusBean);
				//throw new DAOException();
			}
			finally
			{
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					if(rs!=null){
						rs.close();
					}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error("ERROR IN insertPolicyNumberDetails:"+ex);
					throw new DAOException();
				}
			}
			return policyListDataBean;
			}

			@Override
			public PolicyListDataBean getPolicyLotDetails(int isCron, int moduleId)	throws DAOException {
				logger.info("ente getPolicyLotDetails method    isCron  "+isCron);
				InsertLotDetailsBean insertLotDetailsBeanObj = null;
				List<InsertLotDetailsBean> lotListObj = null;
				Connection con = null;
				CallableStatement cstmt = null;
				PolicyListDataBean policyListDataBean= new PolicyListDataBean();
				ResultSet rs=null;
				int indexpos=0;
				String statusMessage="";
				StatusBean statusBean= null;
				try
				{
					//Retrieving the policies from CORP GoddHealth Module
					con =getMyConnection(moduleId);
					cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_POLICY_NUMBER_DETAILS);
					cstmt.setInt(++indexpos,isCron);
					cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
					cstmt.execute();
					rs = (ResultSet) cstmt.getObject(indexpos);
					lotListObj = new ArrayList<InsertLotDetailsBean>();
					
					int serialNo=0;
					while(rs.next()) {
						insertLotDetailsBeanObj = new InsertLotDetailsBean();
						insertLotDetailsBeanObj.setSerialNo(++serialNo);
						insertLotDetailsBeanObj.setLotNumber(rs.getString("LOTID"));
						//insertLotDetailsBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
						String moduleName="Individual";
				    	 insertLotDetailsBeanObj.setModuleName(moduleName);
				    	 insertLotDetailsBeanObj.setModuleId(4);
				    	 insertLotDetailsBeanObj.setStatusDesc(rs.getString("STATUSDESC"));
				    	 logger.info("                STATUSDESC----    "+rs.getString("STATUSDESC") +"       LOTSTATUSFLAG    "+rs.getInt("LOTSTATUSFLAG"));
				    	 insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
				    	 insertLotDetailsBeanObj.setUserName(rs.getString("USERNAME"));
				    	 insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
				    	 lotListObj.add(insertLotDetailsBeanObj);
					}//while
					if(lotListObj.size()>0){
						policyListDataBean.setLotListObj(lotListObj);
						statusBean = new StatusBean();
						statusBean.setStatusFlag(1);
						statusBean.setStatusMessage("records success fully got it");
						policyListDataBean.setStatusBean(statusBean);
					}else{
						statusMessage ="No data found";
						statusBean = new StatusBean();
						statusBean.setStatusFlag(0);
						statusBean.setStatusMessage(statusMessage);
						policyListDataBean.setStatusBean(statusBean);
					}
				}
				catch(Exception ex)
				{
					logger.error(ex);
					if(con==null){
						statusMessage ="Database connection null <br/>";
					}
					if(cstmt==null){
						statusMessage +="procedure related exception <br/>";
					}
					statusMessage +="Error in getLotDetails:  "+ex;
					statusBean = new StatusBean();
					statusBean.setStatusFlag(2);
					statusBean.setStatusMessage(statusMessage);
					policyListDataBean.setStatusBean(statusBean);
					//throw new DAOException();
				}
				finally
				{
					try {
						if(con!=null){
							con.close();
						}
						if(cstmt!=null){
							cstmt.close();
						}
						if(rs!=null){
							rs.close();
						}
					} catch (SQLException ex) {
						ex.printStackTrace();
						logger.error("ERROR IN getEndorDDLDetails:"+ex);
						throw new DAOException();
					}
				}
				return policyListDataBean;
			}
		
		
		

}
