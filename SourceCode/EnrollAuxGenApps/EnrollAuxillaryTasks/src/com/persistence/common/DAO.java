package com.persistence.common;

import java.sql.Connection;

import com.exception.DAOException;

public interface DAO {
	public Connection getConnection() throws DAOException;
}
