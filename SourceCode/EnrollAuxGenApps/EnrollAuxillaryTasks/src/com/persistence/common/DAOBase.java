package com.persistence.common;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.exception.DAOException;
import com.exception.ServiceLocatorException;
import com.util.Constants;
import com.util.ServiceLocator;

public class DAOBase {
	 
	public static Connection getMyConnection(int moduleId) throws DAOException {
		String jndiName = null;
		System.out.println("in DAOBASE : "+moduleId);
		switch (moduleId) {
		    case 1:jndiName = Constants.JNDI_KEY_CORP_REPORTS; break;
	        case 2:jndiName = Constants.JNDI_KEY_AB_REPORTS; break;
	        case 4:jndiName = Constants.JNDI_KEY_WIND_REPORTS; break;
	        
	       /* case 1:jndiName = Constants.JNDI_KEY_CORP; break;
	        case 2:jndiName = Constants.JNDI_KEY_AB; break;
	        case 4:jndiName = Constants.JNDI_KEY_WIND; break;*/
	      
	        case 9:jndiName = Constants.JNDI_KEY_Mysql;break;
	        case 10:jndiName = Constants.JNDI_KEY_XL_PREAUTH;break;
	        case 90:jndiName = Constants.COMMAN_DB;break;
    }
		try {
			DataSource source =null;
			System.out.println("jndiName    "+jndiName);
			source = ServiceLocator.getInstance().getDataSource(jndiName);
			System.out.println("source    "+source);
			if (source == null){
				return null;
			}
			System.out.println("source.getConnection()  ----->  "+source.getConnection());
			return source.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static Connection getLiveMyConnection(int moduleId) throws DAOException {
		String jndiName = null;
		System.out.println("in DAOBASE : "+moduleId);
		switch (moduleId) {
		 case 1:jndiName = Constants.JNDI_KEY_CORP_LIVE; break;
	        case 2:jndiName = Constants.JNDI_KEY_AB_LIVE; break;
	        case 4:jndiName = Constants.JNDI_KEY_WIND_LIVE; break;
	      
	        case 9:jndiName = Constants.JNDI_KEY_Mysql;break;
	        case 10:jndiName = Constants.JNDI_KEY_XL_PREAUTH;break;
	        case 90:jndiName = Constants.COMMAN_DB;break;
    }
		try {
			DataSource source =null;
			System.out.println("jndiName    "+jndiName);
			source = ServiceLocator.getInstance().getDataSource(jndiName);
			System.out.println("source    "+source);
			if (source == null){
				return null;
			}
			System.out.println("source.getConnection()  ----->  "+source.getConnection());
			return source.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ServiceLocatorException e) {
			e.printStackTrace();
		}
		return null;
	}
}
