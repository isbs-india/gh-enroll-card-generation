package com.persistence.common;

import com.exception.DAOException;
import com.model.PolicyDataBean;
import com.model.UploadPhotoDetails;

public class PhotoUploadDAOManager {
	
	//photo upload start
	public static UploadPhotoDetails insertUploadBankPolicyPhotos(UploadPhotoDetails uploadPhotoDetails,int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().insertUploadBankPolicyPhotos(uploadPhotoDetails,moduleId);

		}catch (DAOException e) {

		}
		return null;
	}
	public static PolicyDataBean getPhotoPendingLot(int moduleId)throws DAOException {
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().getPhotoPendingLot(moduleId);

		}catch (DAOException e) {

		}
		return null;
	}
	
	public static PolicyDataBean getCardIdPhotoLot(int moduleId)throws DAOException {
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().getCardIdPhotoLot(moduleId);

		}catch (DAOException e) {

		}
		return null;
	}
	
	
	public static UploadPhotoDetails insertPhotoCardDetails(UploadPhotoDetails uploadPhotoDetails,String xml,int moduleId) throws DAOException{
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().insertPhotoCardDetails(uploadPhotoDetails,xml,moduleId);

		}catch (DAOException e) {

		}
		return null;
	}
	
	//added sathosh
	public static PolicyDataBean getEmployeeDetailsCardId(String insuredorempcode,int moduleId) {
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().getEmployeeDetailsCardId(insuredorempcode,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static PolicyDataBean getEmployeeDetailsEmpId(String policyId,String empId,int moduleId) {
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().getEmployeeDetailsEmpId(policyId,empId,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	
	public static PolicyDataBean getEmployeeDetailsBANo(String policyId,String bano,int moduleId) {
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().getEmployeeDetailsBANo(policyId,bano,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
	public static UploadPhotoDetails insertUploadPolicyPhoto(UploadPhotoDetails uploadPhotoDetails,int moduleId) {
		try {
			return DAOFactory.getInstance().getPhotoUploadDAO().insertUploadPolicyPhoto(uploadPhotoDetails,moduleId);
		} catch (DAOException e) {

		}
		return null;
	}
}
