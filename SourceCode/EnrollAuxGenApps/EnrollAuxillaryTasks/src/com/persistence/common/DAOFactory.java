package com.persistence.common;

import com.persistence.login.LoginDAO;
import com.persistence.login.LoginDAOImpl;

public class DAOFactory {

	private static DAOFactory instance;

	static {
		instance = new DAOFactory();
	}

	private DAOFactory() {

	}

	public static DAOFactory getInstance() {
		return instance;
	}
	
	public EnrollAuxDAO getEnrollAuxDAO() {
		return new EnrollAuxDAOImpl();
	}
	
	public LoginDAO getLoginDAO() {
		return new LoginDAOImpl();
	}
	
	public PhotoUploadDAO getPhotoUploadDAO() {
		return new PhotoUploadDAOImpl();
	}
	
}
