package com.persistence.login;


import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.exception.DAOException;
import com.model.LoginBean;
import com.model.StatusBean;
import com.persistence.common.DAOBase;
import com.util.ProcedureConstants;

public class LoginDAOImpl extends DAOBase implements LoginDAO {

	Logger logger = Logger.getLogger("ENROLLAUX_LoginDAOImpl");


	public LoginBean getUserDetails(String uname, String pwd,int serverip, String hostName) throws DAOException {
		logger.info("enter getUserDetails  uname "+uname);
		logger.info("enter getUserDetails  pwd "+pwd);
		Connection con = null;
		CallableStatement cstmt = null;
		LoginBean user=null;
		ResultSet rs=null;
		int indexpos=0;
		StatusBean statusBean = null;
		int statusFlag = 0;
		String statusMessage = "";
		try {

			con =getMyConnection(1);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_CHK_LOGIN_ORACLE);
			cstmt.setString(++indexpos, uname);
			cstmt.setString(++indexpos, pwd);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos-1);
			if (rs.next()) {
				user=new LoginBean();
				user.setUid(rs.getInt("ID"));
				user.setRoleId(rs.getInt("ROLE"));
				user.setLoginUserName(rs.getString("USERNAME"));
				user.setHomePage(rs.getString("HOMEPAGE"));
				user.setAssignedMedicoName(rs.getString("ASSIGNED_MEDICO"));
				user.setLoginId(rs.getString("NAME"));
				user.setAbUid(rs.getInt("ABID"));
				user.setVeniusUid(rs.getInt("SAID"));
				user.setRetUid(rs.getInt("RETID"));
			}
			
			if(user!=null){
				statusFlag=1;
				statusMessage="Login credentials are success";
				statusBean = new StatusBean();
				statusBean.setStatusFlag(statusFlag);
				statusBean.setStatusMessage(statusMessage);
				user.setStatusBean(statusBean);
			}else if(user==null){
				statusFlag=0;
				statusMessage="Login credentials are not found";
				user= new LoginBean();
				statusBean = new StatusBean();
				statusBean.setStatusFlag(statusFlag);
				statusBean.setStatusMessage(statusMessage);
				user.setStatusBean(statusBean);
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			
				 if(con==null){
						statusMessage ="Database connection null <br/>";
					}
					if(cstmt==null){
						statusMessage +="procedure related exception <br/>";
					}
					if(rs==null){
						statusMessage += "resultset is null <br/>";
					}
				statusMessage +="Login credentials are not found  "+ex;
				user= new LoginBean();
				statusBean = new StatusBean();
				statusBean.setStatusFlag(2);
				statusBean.setStatusMessage(statusMessage);
				user.setStatusBean(statusBean);
			
			/*SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS:",ex.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}*/
			logger.error(ex);
			//throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error(ex);
				//throw new DAOException();
			}
		}		
		return user;
	}
}
