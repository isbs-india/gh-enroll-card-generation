package com.model;

import java.io.Serializable;

public class StatusBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private int statusFlag;
	private String statusMessage;
	
	public int getStatusFlag() {
		return statusFlag;
	}
	public void setStatusFlag(int statusFlag) {
		this.statusFlag = statusFlag;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

}
