package com.model;

import java.io.Serializable;

public class PoliciesBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int policyId;
	private String policyHolderName;
	private String policyFrom;
	private String policyTo;
	
	
	public int getPolicyId() {
		return policyId;
	}
	public void setPolicyId(int policyId) {
		this.policyId = policyId;
	}
	public String getPolicyHolderName() {
		return policyHolderName;
	}
	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}
	public String getPolicyFrom() {
		return policyFrom;
	}
	public void setPolicyFrom(String policyFrom) {
		this.policyFrom = policyFrom;
	}
	public String getPolicyTo() {
		return policyTo;
	}
	public void setPolicyTo(String policyTo) {
		this.policyTo = policyTo;
	}

}
