package com.model;

import java.util.List;

public class PolicyListDataBean {
	StatusBean statusBean;
	private List<InsertLotDetailsBean> lotListObj;
	
	public StatusBean getStatusBean() {
		return statusBean;
	}

	public void setStatusBean(StatusBean statusBean) {
		this.statusBean = statusBean;
	}

	public List<InsertLotDetailsBean> getLotListObj() {
		return lotListObj;
	}

	public void setLotListObj(List<InsertLotDetailsBean> lotListObj) {
		this.lotListObj = lotListObj;
	}

}
