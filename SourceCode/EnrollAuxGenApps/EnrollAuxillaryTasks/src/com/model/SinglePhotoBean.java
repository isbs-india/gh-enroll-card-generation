package com.model;

import java.io.Serializable;

public class SinglePhotoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String cardid;
	private int moduleId;
	private int serialNo;
	private String cardHolderName;
	
	private String empid;
	private String sumInsured;
	private String policyNo;
	private String policyId;
	
	private String policyHolderName;
	private String relation;
	private int age;
	private String dob;
	
	private String policyFrom;
	private String policyTo;
	private String doj;
	private String dol;
	
	private String enrolmentExcep;
	private String blocking;
	private int familyId;
	private String insuredId;
	
	private String currentDate;
	private String renewalStatus;
	private String bano;
	private String uwCode;
	
	private String uwId;
	private String gender;
	private String rid;
	private String preAuthUrl;
	
	private String inceptionDate;
	private String selfName;
	private String issusingofficeid;
	private String selfcardid;
	public String getCardid() {
		return cardid;
	}
	public void setCardid(String cardid) {
		this.cardid = cardid;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getCardHolderName() {
		return cardHolderName;
	}
	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getPolicyId() {
		return policyId;
	}
	public void setPolicyId(String policyId) {
		this.policyId = policyId;
	}
	public String getPolicyHolderName() {
		return policyHolderName;
	}
	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getPolicyFrom() {
		return policyFrom;
	}
	public void setPolicyFrom(String policyFrom) {
		this.policyFrom = policyFrom;
	}
	public String getPolicyTo() {
		return policyTo;
	}
	public void setPolicyTo(String policyTo) {
		this.policyTo = policyTo;
	}
	public String getDoj() {
		return doj;
	}
	public void setDoj(String doj) {
		this.doj = doj;
	}
	public String getDol() {
		return dol;
	}
	public void setDol(String dol) {
		this.dol = dol;
	}
	public String getEnrolmentExcep() {
		return enrolmentExcep;
	}
	public void setEnrolmentExcep(String enrolmentExcep) {
		this.enrolmentExcep = enrolmentExcep;
	}
	public String getBlocking() {
		return blocking;
	}
	public void setBlocking(String blocking) {
		this.blocking = blocking;
	}
	public int getFamilyId() {
		return familyId;
	}
	public void setFamilyId(int familyId) {
		this.familyId = familyId;
	}
	public String getInsuredId() {
		return insuredId;
	}
	public void setInsuredId(String insuredId) {
		this.insuredId = insuredId;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}
	public String getRenewalStatus() {
		return renewalStatus;
	}
	public void setRenewalStatus(String renewalStatus) {
		this.renewalStatus = renewalStatus;
	}
	public String getBano() {
		return bano;
	}
	public void setBano(String bano) {
		this.bano = bano;
	}
	public String getUwCode() {
		return uwCode;
	}
	public void setUwCode(String uwCode) {
		this.uwCode = uwCode;
	}
	public String getUwId() {
		return uwId;
	}
	public void setUwId(String uwId) {
		this.uwId = uwId;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getRid() {
		return rid;
	}
	public void setRid(String rid) {
		this.rid = rid;
	}
	public String getPreAuthUrl() {
		return preAuthUrl;
	}
	public void setPreAuthUrl(String preAuthUrl) {
		this.preAuthUrl = preAuthUrl;
	}
	public String getInceptionDate() {
		return inceptionDate;
	}
	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}
	public String getSelfName() {
		return selfName;
	}
	public void setSelfName(String selfName) {
		this.selfName = selfName;
	}
	public String getIssusingofficeid() {
		return issusingofficeid;
	}
	public void setIssusingofficeid(String issusingofficeid) {
		this.issusingofficeid = issusingofficeid;
	}
	public String getSelfcardid() {
		return selfcardid;
	}
	public void setSelfcardid(String selfcardid) {
		this.selfcardid = selfcardid;
	}
	
	
}