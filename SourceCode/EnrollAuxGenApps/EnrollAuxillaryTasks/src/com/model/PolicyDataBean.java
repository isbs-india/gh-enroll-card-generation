package com.model;

import java.io.Serializable;
import java.util.List;

public class PolicyDataBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<PoliciesBean> polBeanListObj;
	private StatusBean statusBean;
	private List<InsertLotDetailsBean> lotListObj;
	private List<GetPhotoPendingLot> photoPendingLotList;
	private List<SinglePhotoBean> singlephotoList;
	
	
	public List<SinglePhotoBean> getSinglephotoList() {
		return singlephotoList;
	}
	public void setSinglephotoList(List<SinglePhotoBean> singlephotoList) {
		this.singlephotoList = singlephotoList;
	}
	public List<GetPhotoPendingLot> getPhotoPendingLotList() {
		return photoPendingLotList;
	}
	public void setPhotoPendingLotList(List<GetPhotoPendingLot> photoPendingLotList) {
		this.photoPendingLotList = photoPendingLotList;
	}
	public List<InsertLotDetailsBean> getLotListObj() {
		return lotListObj;
	}
	public void setLotListObj(List<InsertLotDetailsBean> lotListObj) {
		this.lotListObj = lotListObj;
	}
	public List<PoliciesBean> getPolBeanListObj() {
		return polBeanListObj;
	}
	public void setPolBeanListObj(List<PoliciesBean> polBeanListObj) {
		this.polBeanListObj = polBeanListObj;
	}
	public StatusBean getStatusBean() {
		return statusBean;
	}
	public void setStatusBean(StatusBean statusBean) {
		this.statusBean = statusBean;
	}
}
