package com.model;

import java.io.Serializable;
import java.util.List;

public class CardListBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private List<String> list;

	public List<String> getList() {
		return list;
	}

	public void setList(List<String> list) {
		this.list = list;
	}

}
