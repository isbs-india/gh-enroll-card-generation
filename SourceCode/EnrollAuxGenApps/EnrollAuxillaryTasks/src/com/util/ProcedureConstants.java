package com.util;

public class ProcedureConstants {  

	public static final String PROC_FOR_GET_CHK_LOGIN_ORACLE ="{call PKG_USERLOGIN.CHK_LOGIN_TEMP(?,?,?,?)}";	
	
	//For retrieving policy DDL data
	public static final String PROC_FOR_GET_POLICIES_DDL_CORP ="{call PKG_ECARDS_IN_PDF.GET_POLID(?)}";
		
	public static final String PROC_FOR_GET_POLICIES_DDL_CORP_EXP ="{call PKG_ECARDS_IN_PDF.GET_POLID_EXP(?)}";
	
	
	//For retrieving endorsement DDL data based on I/P policyId
	public static final String PROC_FOR_GET_ENDORSEMENT_DDL_CORP ="{call PKG_ECARDS_IN_PDF.GET_ENDORSEMENTS(?,?)}";
	public static final String PROC_FOR_GET_ENDORSEMENT_DDL_CORP_EXP ="{call PKG_ECARDS_IN_PDF.GET_ENDORSEMENTS_EXP(?,?)}";
	
	//For inserting the search data into ECARDS_LOT table
	public static final String PROC_FOR_PUT_SEARCH_DATA_CORP ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT(?,?,?,?)}";	
	public static final String PROC_FORP_ROC_LOT_INSERT_AB ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT(?,?,?)}";
	public static final String PROC_FORP_ROC_LOT_INSERT_WIND ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT(?,?,?)}";
	
	public static final String PROC_FORP_ROC_LOT_INSERT_EXP_AB ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT_EXP(?,?,?)}";
	public static final String PROC_FOR_GET_LOT_DETAILS_EXP ="{call PKG_ECARDS_IN_PDF.GET_LOT_DETAILS_EXP(?,?)}";
	
	// getting lot details
	public static final String PROC_FOR_GET_LOT_DETAILS ="{call PKG_ECARDS_IN_PDF.GET_LOT_DETAILS(?,?)}";
	
	public static final String PROC_PHOTO_LOT_INSERT ="{call PKG_ECARDS_IN_PDF.PROC_PHOTO_LOT_INSERT(?,?,?,?,?,?,?)}";
	public static final String PROC_GET_PHOTO_PENDING_LOT_WEB ="{call PKG_ECARDS_IN_PDF.PROC_GET_PHOTO_PENDING_LOT_WEB(?)}";
	
	public static final String PROC_PHOTODOWNLOAD_LOT_INSERT ="{call PKG_ECARDS_IN_PDF.PROC_PHOTODOWNLOAD_LOT_INSERT(?,?,?,?,?,?,?)}";
	
	public static final String PROC_GET_PHOTO_DLOAD_LOT_WEB ="{call PKG_ECARDS_IN_PDF.PROC_GET_PHOTO_DLOAD_LOT_WEB(?)}";
	
	//santhosh added start
		public static final String PROC_FOR_GET_IAUTH_COMB_ONLY_CARDID_DTLS ="{call PKG_IAUTH.IAUTH_COMB_ONLY_CARDID_DTLS(?,?)}";
		
		public static final String PROC_FOR_GET_IAUTH_COMB_POLID_EMPID_DTLS ="{call PKG_IAUTH.IAUTH_COMB_POLID_EMPID_DTLS(?,?,?)}";
		
		public static final String PROC_FOR_GET_IAUTH_COMB_POLID_BANO_DTLS ="{call PKG_IAUTH.IAUTH_COMB_POLID_BANO_DTLS(?,?,?)}";
		public static final String PROC_FOR_PROC_UPDATE_PHOTO_STATUS ="{call PKG_ECARDS_IN_PDF.PROC_UPDATE_PHOTO_STATUS(?,?,?,?,?,?,?)}";
		public static final String PROC_FORP_ROC_LOT_INSERT_GHPL ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT_XML(?,?,?,?,?,?,?)}";
		
		
		public static final String PROC_LOT_INSERT_XML_CORP ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT_XML(?,?,?,?,?,?,?)}";
		
		public static final String PROC_LOT_INSERT_XML_CORP_EXP ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT_XML_EXP(?,?,?,?,?,?,?)}";
		
		//santhosh added end
		//Label code start
		public static final String PROC_FOR_GET_POLICIES_CORP ="{call PKG_ECARDS_IN_PDF.PROC_POL_LIST(?)}";
		public static final String PROC_LABEL_LOT_INSERT ="{call PKG_ECARDS_IN_PDF.PROC_LOT_INSERT_LABLE(?,?)}";
		public static final String PROC_FOR_GET_LOT_Label_DETAILS ="{call PKG_ECARDS_IN_PDF.GET_LOT_DETAILS_LABLE(?,?)}";
		//Label code end
			
		public static final String PROC_FOR_INSERT_POLICY_NUMBER_DETAILS ="{call PKG_ECARDS_IN_PDF.PROC_POLICY_DOC_LOT_INSERT(?,?,?)}";
		public static final String PROC_FOR_GET_POLICY_NUMBER_DETAILS ="{call PKG_ECARDS_IN_PDF.GET_POLICY_DOC_LOT_DETAILS(?,?)}";
	
}

