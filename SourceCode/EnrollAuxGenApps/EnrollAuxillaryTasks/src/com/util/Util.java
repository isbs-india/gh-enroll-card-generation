package com.util;

import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import javax.servlet.http.Part;

import org.apache.commons.net.ftp.FTPClient;


public class Util {
	private static DecimalFormat currencyWithDecimalFormatByComma = new DecimalFormat("#,##0.00");
	public static boolean isEmpty(String str){
		return (str == null || str.trim().length() == 0 || str.trim().equals("null"));
	}

	public static String getCurrenyFormatByCodeValueId(int codeValueId) {
		String currencyName = "";
		switch (codeValueId) {
		case 2801:
			currencyName = "US DOLLARS";
			break;
		case 2802:
			currencyName = "EURO";
			break;
		case 2803:
			currencyName = "Rs.";
			break;
		case 2804:
			currencyName = "BRITISH POUNDS";
			break;
		case 2805:
			currencyName = "JPY";
			break;
		default:
			currencyName = new Integer(codeValueId).toString();
		}
		return currencyName;
	}

	public static final double roundDouble(double d, int places) {
		return Math.round(d * Math.pow(10, (double) places)) / Math.pow(10,
				(double) places);
	}
	public static DecimalFormat getCurrencyWithDecimalFormatByComma() {
		return currencyWithDecimalFormatByComma;
	}

	public static String converDateFormat(String dateInput){
		String ddmmyy=null;
		String date=null;
		String year= null;
		String month= null;
		String hhmm= null;
		//String mm= null;
		String[] parts=null;
		String[] dateparts=null;
		String finaldate=null;

		if(dateInput!=null){
			parts = dateInput.split(" ");
			ddmmyy=parts[0];
			hhmm=parts[1];
			dateparts=ddmmyy.split("-");
			date=dateparts[0];
			month=dateparts[1];
			year=dateparts[2];

			finaldate=year+"-"+month+"-"+date+" "+hhmm;
		}

		return finaldate;

	}

	/*public static String convertddmmmyyyy(String value){
		String result = "";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		result=dateFormat.format(calendar.getTime());
		return result;
	}*/

	public static String convertddmmmyyyywithoutsysdate(String myDate)
	{
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date=null;
		String returnValue=null;
		try {                           
			if(myDate!=null && !myDate.equals("")){
				date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(myDate);
				returnValue = dateFormat.format(date);
			}
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return returnValue;
	}


	public static String convertddmmmyyyy(String myDate)
	{
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
		Date date=null;
		String returnValue="";
		try {                           
			if(myDate!=null && !myDate.equals("")){
				date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(myDate);
				returnValue = dateFormat.format(date);
			}else{
				Calendar calendar = Calendar.getInstance();
				SimpleDateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
				returnValue=dateFormat1.format(calendar.getTime());
			}
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public static String convertddmmmyyyyHHmm(String myDate)
	{
		DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
		Date date=null;
		String returnValue="";
		try {                            //20-06-2014 16:17
			date = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH).parse(myDate);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		returnValue = dateFormat.format(date);

		return returnValue;
	}

	public static String convertyyyymmddHHmm(String myDate)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd HH:mm");
		Date date=null;
		String returnValue="";
		try {                            //20-06-2014 16:17
			date = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH).parse(myDate);
		} catch (java.text.ParseException e) {
			e.printStackTrace();
		}
		returnValue = dateFormat.format(date);

		return returnValue;
	}

	public static String generateSessionKey(int length)
	{
		String alphabet = new String("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
		int n = alphabet.length();
		String result = new String();
		Random r = new Random();
		for(int i = 0; i < length; i++)
			result = (new StringBuilder(String.valueOf(result))).append(alphabet.charAt(r.nextInt(n))).toString();
		return result;
	}


	//Generating random string using System.nanotime & MD5
	public static String getRandomNanoTimeMd5String()
	{
		StringBuilder hexString=null;
		MessageDigest instance;
		try {
			instance = MessageDigest.getInstance("MD5");

			byte[] messageDigest = instance.digest(String.valueOf(System.nanoTime()).getBytes());
			hexString = new StringBuilder();
			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					// could use a for loop, but we're only dealing with a single
					// byte
					hexString.append('0');
				}
				hexString.append(hex);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		finally
		{
			//
		}
		return hexString.toString();
	}



	public static String fileExtenssion(String fileName){
		String extension = "";
		int i = fileName.lastIndexOf('.');
		int p = Math.max(fileName.lastIndexOf('/'), fileName.lastIndexOf('\\'));
		if (i > p) {
			extension = fileName.substring(i+1);
		}
		return extension;
	}
	public static String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}

	public static int getReAuthFlagStatusRet(int flag ,int currStatus) {
		int reAuthFlag = 0;
		if(currStatus==44 || currStatus==45 || currStatus==174 || currStatus==175){
			reAuthFlag=500;
		}else if(flag==0){
			reAuthFlag=200;
		}else if(flag>0 || flag==-1){
			reAuthFlag=300;
		}
		return reAuthFlag;
	}

	public static int getReAuthFlagStatus(int flag ,int currStatus) {
		int reAuthFlag = 0;
		if(currStatus==44 || currStatus==45 || currStatus==174 || currStatus==175){
			reAuthFlag=500;
		}else if(flag==0){
			reAuthFlag=200;
		}else if(flag>0 || flag==-1){
			reAuthFlag=300;
		}
		return reAuthFlag;
	}

	public static int getReAuthFlagStatus(int flag) {
		int reAuthFlag = 0;
		if(flag==0){
			reAuthFlag=0;
		}else if(flag>0){
			reAuthFlag=100;
		}
		return reAuthFlag;
	}
	
	public static String compareTwoDates(String dol,String currDate){
		String expired=null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
			Date dateDol = formatter.parse(dol);
			Date sysDate = formatter.parse(currDate);
				if(dateDol.before(sysDate)){
					expired="Expired";
	        	}
			} catch (Exception e) {
				e.printStackTrace();
			}
		return expired;
	}

	public static String getModuleName(int moduleId) {
		String moduleName = "";
		switch (moduleId) {
		case 1:
			moduleName = Constants.MODULE_1;
			break;
		case 2:
			moduleName = Constants.MODULE_2;
			break;
		case 3:
			moduleName = Constants.MODULE_3;
			break;
		case 4:
			moduleName = Constants.MODULE_4;
			break;
		case 5:
			moduleName = Constants.MODULE_5;
			break;
		case 6:
			moduleName = Constants.MODULE_6;
			break;
		case 7:
			moduleName = Constants.MODULE_7;
			break;
		case 8:
			moduleName = Constants.MODULE_8;
			break;
		}
		return moduleName;
	}

	

	public static Integer stringToInt(String s) throws NumberFormatException {
		return Integer.valueOf(Integer.parseInt(s));
	}
	
	public static boolean checkFileExists(String filePath ,FTPClient ftpClient) throws Exception {
	    InputStream inputStream = ftpClient.retrieveFileStream(filePath);
	    int returnCode = ftpClient.getReplyCode();
	    if (inputStream == null || returnCode == 550) {
	        return false;
	    }
	    return true;
	}
	
	
/*	public static boolean checkFileExists(String filePath) throws IOException {
		 FTPClient ftpClient = new FTPClient();
	     int returnCode =0;
	    InputStream inputStream = ftpClient.retrieveFileStream(filePath);
	    returnCode = ftpClient.getReplyCode();
	    if (inputStream == null || returnCode == 550) {
	        return false;
	    }
	    return true;
	}*/


}
