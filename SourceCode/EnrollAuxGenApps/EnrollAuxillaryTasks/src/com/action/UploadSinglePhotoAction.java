package com.action;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.UploadPhotoDetails;
import com.persistence.common.PhotoUploadDAOManager;
import com.util.MyProperties;

/**
 * Servlet implementation class UploadSinglePhotoAction
 */
@WebServlet("/UploadSinglePhotoAction")
@MultipartConfig(fileSizeThreshold=1024*1024*20, // 2MB
maxFileSize=1024*1024*100,      // 10MB
maxRequestSize=1024*1024*500)   // 50MB
public class UploadSinglePhotoAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("UploadSinglePhotoAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadSinglePhotoAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = null;
		logger.info("enter doPost");
		try{
			UploadPhotoDetails uploadPhotoDetails = null;
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				if (!ServletFileUpload.isMultipartContent(request)) {
					return;
				}
				int moduleId=0;
				String serialNo="0";
				int moduleUserId= 0;
				//int userId=0;
				if(session.getAttribute("user")!=null){
					 if(request.getParameter("serialNo")!=null){
			            	serialNo=request.getParameter("serialNo");
			         } 
					 if(request.getParameter("moduleId"+serialNo)!=null){
			            	moduleId=Integer.parseInt(request.getParameter("moduleId"+serialNo));
			         }
					
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =loginBean.getModuleUserId(moduleId);
					if(moduleUserId==0){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				String cardid="";
				String policyId="";
				String insuredId="";
				String FTPHOST=null;
 				String FTPUName=null;
 				String FTPPwd=null;
 				String ftpupload_ftppath=null;
 				boolean result=false;
 				
 				MyProperties myResources = new MyProperties();
 				FTPHOST = myResources.getMyProperties("photodestinationftp_host");
     			FTPUName = myResources.getMyProperties("photodestinationftp_username");
     			FTPPwd = myResources.getMyProperties("photodestinationftp_password");
     			ftpupload_ftppath = myResources.getMyProperties("photodestination_PHOTOUPLOADS");
 			    FTPClient client = new FTPClient();
 			   try{
	 			    client.connect(FTPHOST);
	                int replyCode = client.getReplyCode();
	                if (!FTPReply.isPositiveCompletion(replyCode)) {
	                	logger.info("Operation failed. FTP Server reply code: " + replyCode);
	                	request.setAttribute("message", "Single photo FTP Server host reply code: " + replyCode  +"       ftp status :   "+client.getStatus());
				 		rd=request.getRequestDispatcher("./status.jsp");
				 		rd.forward(request,response);
	                    //return;
	                }
		            result = client.login(FTPUName, FTPPwd);
		            if (!result) {
		            	 logger.info("Could not login to the  FTP server");
		            	 request.setAttribute("message", "Single photo FTP Server login details: " + result  +"       ftp status :   "+client.getStatus());
					 		rd=request.getRequestDispatcher("./status.jsp");
					 		rd.forward(request,response);
		            	// return;
		            }
 			   } catch(Exception e){
 			    	    request.setAttribute("message", "ftp connection failed  Please check it:   ping "+FTPHOST +" -t if not ping ip address contact ITADMIN" );
					 		rd=request.getRequestDispatcher("./status.jsp");
					 		return;
 			     }
	            client.setFileType(FTP.BINARY_FILE_TYPE);
	            client.enterLocalPassiveMode();
 			   
	           
	            if(request.getParameter("cardid"+serialNo)!=null){
	            	cardid=request.getParameter("cardid"+serialNo);
	            	//System.out.println("cardid  >>"+cardid);
	            }
	            if(request.getParameter("policyId"+serialNo)!=null){
	            	policyId=request.getParameter("policyId"+serialNo);
	            	//System.out.println("policyId  >>"+policyId);
	            }
	            if(request.getParameter("insuredId"+serialNo)!=null){
	            	insuredId=request.getParameter("insuredId"+serialNo);
	            	//System.out.println("insuredId  >>"+insuredId);
	            }
		
	            String uploadPath = getServletContext().getRealPath("SINGLEPHOTOUPLOAD");
	            final Part filePart1 = request.getPart("upload"+serialNo); 
	            String fileName = null;
	            if(filePart1 !=null){
	            	fileName = extractFileName(filePart1);
	            	filePart1.write(uploadPath + File.separator + fileName);
	            }
	            UploadPhotoDetails uploadPhotoDetails_local =null;
	            File f = new File(uploadPath+ File.separator +fileName);
				Image image = null;
	            try{
	            	 image = ImageIO.read(f);
	            }catch(Exception e){
	            	logger.info(e.getMessage());
	            	request.setAttribute("message", "Uploaded image is not a proper JPG Image file of Name : "+fileName);
			 		rd=request.getRequestDispatcher("./status.jsp");
			 		if(f!=null){
			 			f.delete();
			 		}
			 		rd.forward(request,response);
			 		return;
	            }
				String extension = "";
				if (image != null) {
                    logger.info("enter imamge not null condition");
					int k = f.getName().lastIndexOf('.');
					//logger.info("k value ---------     "+k);
					if (k > 0) {
						extension = f.getName().substring(k + 1).toLowerCase();
						if ((!extension.equals("jpg")) && (!extension.equals("jpeg"))) {
							logger.info("extension.equals    "+extension.equals("jpg"));
								request.setAttribute("message", "uploaded photo is not a valid jpeg (or) jpg");
								rd=request.getRequestDispatcher("./status.jsp");
								rd.forward(request,response);
						} else {
							File desuploadDir = new File(ftpupload_ftppath);
				            if (!desuploadDir.exists()){
								String str[] =ftpupload_ftppath.split("/");
								for(int i=0;k<str.length;i++){
									client.changeWorkingDirectory(str[i]);
									client.printWorkingDirectory();
								}
							}
				            client.enterLocalPassiveMode();
				            client.setFileType(FTPClient.BINARY_FILE_TYPE);
				            client.setRemoteVerificationEnabled(false);
				            boolean filestatus = checkFileExists(ftpupload_ftppath+cardid+".jpg", client);
				            logger.info("else part     "+extension.equals("jpg"));
				            if(filestatus){
				            	client.deleteFile(ftpupload_ftppath+cardid+".jpg");
				            	client.disconnect();
				            }
				            FTPClient clientfilestore = new FTPClient();
				            clientfilestore.connect(FTPHOST);
			                int replyCodestore = clientfilestore.getReplyCode();
			                if (!FTPReply.isPositiveCompletion(replyCodestore)) {
			                	logger.info("Operation failed. FTP Server reply code: " + replyCodestore);
			                	request.setAttribute("message", "Single photo FTP Server host reply code: " + replyCodestore  +"       ftp status :   "+clientfilestore.getStatus());
						 		rd=request.getRequestDispatcher("./status.jsp");
						 		rd.forward(request,response);
			                    return;
			                }
				            boolean resultstore = clientfilestore.login(FTPUName, FTPPwd);
				            if (!resultstore) {
				            	 logger.info("Could not login to the  FTP server");
				            	 request.setAttribute("message", "Single photo FTP Server login details: " + result  +"       ftp status :   "+clientfilestore.getStatus());
							 		rd=request.getRequestDispatcher("./status.jsp");
							 		rd.forward(request,response);
				            	 return;
				            }
				            clientfilestore.setFileType(FTP.BINARY_FILE_TYPE);
				            clientfilestore.enterLocalPassiveMode();
			    		    	boolean storefileStatuspart1 = false;
			    			 	BufferedInputStream bis = new BufferedInputStream(new FileInputStream(f));
			    			 	logger.info("path of jpg ------- >>>>"+ftpupload_ftppath+cardid);
			    			 	logger.info("FTPHOST    "+FTPHOST+"      /n =====     FTPUName :"+FTPUName+"  /n       =====     FTPPwd  "+FTPPwd +"   ======  /n   ftpupload_ftppath  :"+ftpupload_ftppath);
			    			 	storefileStatuspart1 = clientfilestore.storeFile(ftpupload_ftppath+cardid+".jpg", bis);
			    			 	int clientreplyCode = clientfilestore.getReplyCode();
			    			 	logger.info("image storing client status of FTP Server reply code: " + clientreplyCode  +"       status :   "+clientfilestore.getStatus());  
			                    if (!FTPReply.isPositiveCompletion(clientreplyCode)) {
			                    	logger.info("image storing client status of FTP Server reply code: " + clientreplyCode  +"       status :   "+clientfilestore.getStatus()+"                storefileStatuspart1   ===----->   "+storefileStatuspart1);
			                    	request.setAttribute("message", "Single photo save image in  FTP Server reply code: " + clientreplyCode  +"       ftp status :   "+clientfilestore.getStatus());
	    					 		rd=request.getRequestDispatcher("./status.jsp");
	    					 		rd.forward(request,response);
	    					 		return;
			                    }
	    			            bis.close();
	    			            f.delete();
	    			            if (storefileStatuspart1) {
		    			        	 uploadPhotoDetails = new UploadPhotoDetails();
		    			        	 uploadPhotoDetails.setUserId(moduleUserId);
		    			        	 uploadPhotoDetails.setCardId(cardid);
		    			        	 uploadPhotoDetails.setPolicyId(Integer.parseInt(policyId));
		    			        	 uploadPhotoDetails.setInsuredId(insuredId);
		    			        	 uploadPhotoDetails.setStatus(1);
		    			        	 
		    			        	 logger.info("before insert upload policy photo");
		    			        	  uploadPhotoDetails_local= PhotoUploadDAOManager.insertUploadPolicyPhoto(uploadPhotoDetails,moduleId);
		    			             logger.info("after insert insertUploadPolicyPhoto     "+uploadPhotoDetails_local.getStatus());
               			             if(uploadPhotoDetails_local!=null){
               			            	logger.info("uploadPhotoDetails_local  null condition :    "+uploadPhotoDetails_local.getStatus());
               			            	if(uploadPhotoDetails_local.getStatus()==1){
               					 		request.setAttribute("message", uploadPhotoDetails_local.getStatusMsg());
               					 		rd=request.getRequestDispatcher("./status.jsp");
               					 		rd.forward(request,response);
               			            	}
               						 }
	    			           }else{
	    			        	   logger.error("uploaded photo not inserted into ftp server pleae contact it admin");
	    							request.setAttribute("message", "uploaded photo not inserted into ftp server pleae contact it admin");
	    					 		rd=request.getRequestDispatcher("./status.jsp");
	    					 		rd.forward(request,response);
	    			        	   
	    			           }
						}
					}else{
						logger.error("uploaded photo not a valid jpeg");
						request.setAttribute("message", "uploaded photo not a valid jpeg");
				 		rd=request.getRequestDispatcher("./status.jsp");
				 		rd.forward(request,response);
					}
				} else {
					logger.error("uploaded photo not a valid jpeg");
					request.setAttribute("message", "uploaded photo not a valid jpeg");
			 		rd=request.getRequestDispatcher("./status.jsp");
			 		rd.forward(request,response);
				}
			}
	}
		catch(Exception e){
	e.printStackTrace();	
	logger.error(e.getMessage());
	getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
		}
}
	
	static boolean checkFileExists(String filePath, FTPClient ftpClient) throws IOException {
        InputStream inputStream = ftpClient.retrieveFileStream(filePath);
       int returnCode = ftpClient.getReplyCode();
        if (inputStream == null || returnCode == 550) {
            return false;
        }
        return true;
    }
	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}
}