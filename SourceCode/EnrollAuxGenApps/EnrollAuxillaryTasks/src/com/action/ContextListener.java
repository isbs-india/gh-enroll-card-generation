package com.action;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.PropertyConfigurator;

/**
 * Application Lifecycle Listener implementation class ContextListener
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {
	static File appHome;
	 public static  String databaseConfigFile;
	 public static File customerDataFile;
    /**
     * Default constructor. 
     */
    public ContextListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent event) {
    	 ServletContext context = event.getServletContext();
         String log4jConfigFile = context.getInitParameter("log4j-config-location");
         String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
         System.out.println("fullPath   "+fullPath);
         System.out.println("log4jConfigFile   "+log4jConfigFile);
         PropertyConfigurator.configure(log4jConfigFile);
         
         
          databaseConfigFile = context.getInitParameter("database-config-location");
         
        // appHome = new File(event.getServletContext().getInitParameter("database-config-location"));
         System.out.println("databaseConfigFile  "+databaseConfigFile);
          customerDataFile = new File(databaseConfigFile);
          System.out.println("customerDataFile  "+customerDataFile);
         //PropertyConfigurator.configure(databaseConfigFile);
    }
	
}
