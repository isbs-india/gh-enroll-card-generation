package com.action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.UploadPhotoDetails;
import com.persistence.common.PhotoUploadDAOManager;
import com.util.MyProperties;
import com.util.UnZipFiles;

/**
 * Servlet implementation class UploadBankPolicyPhotosIndividualAction
 */
@WebServlet("/UploadBankPolicyPhotosIndividualAction")
@MultipartConfig(fileSizeThreshold=1024*1024*20, // 2MB
maxFileSize=1024*1024*100,      // 10MB
maxRequestSize=1024*1024*500)   // 50MB
public class UploadBankPolicyPhotosIndividualAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("UploadBankPolicyPhotosIndividualAction");   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadBankPolicyPhotosIndividualAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = null;
		try{
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				int moduleUserId= 0;
				int userId=0;
				int moduleId=4;
				/*if(request.getParameter("moduleId")!=null){
					moduleId=Integer.parseInt(request.getParameter("moduleId"));
				}*/
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					userId =loginBean.getUid();
					moduleUserId =loginBean.getModuleUserId(4);
					if(moduleUserId==0){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				UploadPhotoDetails uploadPhotoDetails = null;
				//List<UploadPhotoDetails> uploadPhotoDetailsList = new ArrayList<UploadPhotoDetails>();
				if (!ServletFileUpload.isMultipartContent(request)) {
					return;
				}
				
				//String FtpFolder = "";
				String unzip = "";
				MyProperties myResources = new MyProperties();
				//FtpFolder = myResources.getMyProperties("FtpFolder");
				unzip = myResources.getMyProperties("photouploadfile");
				String  policy="";
				//String policyName="";
				//int policyId =0;
				
				/*if((request.getParameter("policy"))!=null && !(request.getParameter("policy").equals("0"))){
					policy = request.getParameter("policy");
					policyId=Integer.parseInt(request.getParameter("policy").split("~")[0]);
					policyName = policy.split("~")[1];
				}*/
				
				//String randamString="";
				char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
				StringBuilder sb = new StringBuilder();
				Random random = new Random();
				String randomString = sb.toString();
				for (int i = 0; i < 5; i++) {
    			    char c = chars[random.nextInt(chars.length)];
    			    sb.append(c);
    			}
				String fileformat="";
				randomString=sb.toString();
				 DateFormat dfm = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		            String uploadPath = getServletContext().getRealPath("PDFDOWNLOAD");  
		            final Part filePart1 = request.getPart("upload"); 
		            if(filePart1 !=null){
		            	
		 					String fileName = extractFileName(filePart1);
		 					String filePath = uploadPath + File.separator + fileName;
							filePart1.write(filePath);
		 					Date date = new Date();     
		 					 String fileFomat=null;
		 		   		    String dateFormat = dfm.format(date);
		          		//String extension = Util.fileExtenssion(fileName);
		 	                fileformat=dateFormat;
		 					if(fileName!=null && !fileName.equals("")){
		 						//File file = new File(filePath);
								UnZipFiles unZipFiles = new UnZipFiles();
								fileFomat = dateFormat+"_"+moduleId+"_"+randomString;
								String zipFile = unzip+File.separator+fileFomat;
								unZipFiles.unzipImage(filePath,zipFile);
		 				    		 File source = null;
		 				    		 File directory = new File(zipFile);
		 				    		    // get all the files from a directory
		 				    		    File[] fList = directory.listFiles();
		 				    		    if(fList.length>0){
			 				    		    for (File subDir : fList) {
			 					    			 source = new File(subDir.getPath());
			 					    			 if(source.isDirectory()){
			 					    		     FileUtils.copyDirectory(source, directory);
			 					    			 }
			 					    		}
			 				    		   if(source.isDirectory()){
			 				    		        deleteFile(source);
			 				    		   }
		 				    		    }
		 				    		        
		 				    		        String FTPHOST=null;
		 				    				String FTPUName=null;
		 				    				String FTPPwd=null;
		 				    				String ftpupload_ftppath=null;
		 				    				boolean result=false;
		 				    				//MyProperties myResources = new MyProperties();
		 				    				FTPHOST = myResources.getMyProperties("photosourceftp_host");
		 				        			FTPUName = myResources.getMyProperties("photosourceftp_username");
		 				        			FTPPwd = myResources.getMyProperties("photosourceftp_password");
		 				    		
		 				    			    FTPClient client = new FTPClient();
		 				    			       try{
		 				    		                client.connect(FTPHOST);
		 				    		                int replyCode = client.getReplyCode();
		 				    		                if (!FTPReply.isPositiveCompletion(replyCode)) {
		 				    		                	logger.info("Operation failed. FTP Server reply code: " + replyCode);
		 				    		                    return;
		 				    		                }
		 				    			            result = client.login(FTPUName, FTPPwd);
		 				    			            if (!result) {
		 				    			            	 logger.info("Could not login to the  FTP server");
		 				    			            	 return;
		 				    			            }
		 				    			       }catch(Exception e){
		 				    			    	    request.setAttribute("message", "ftp connection failed  Please check it:   ping "+FTPHOST +" -t if not ping ip address contact ITADMIN" );
		                   					 		rd=request.getRequestDispatcher("./status.jsp");
		                   					 		rd.forward(request,response);
		                   					 		return;
		 				    			       }
		 				    			            client.setFileType(FTP.BINARY_FILE_TYPE);
		 				    			            client.enterLocalPassiveMode();
		 				    						 String pathcurrent =null;
		 				    			            ftpupload_ftppath = myResources.getMyProperties("photosourcePHOTOUPLOADS");
		 											pathcurrent=ftpupload_ftppath+fileformat+"_"+moduleId+"_"+randomString;
		 											File uploadDir = new File(pathcurrent);
		 										    if (!uploadDir.exists()){
		 												String str[] =pathcurrent.split("/");
		 												for(int i=0;i<str.length;i++){
		 													client.makeDirectory(str[i]);
		 													client.changeWorkingDirectory(str[i]);
		 												}
		 											}
		 										    // get all the files from a directory
		 							    		    File[] fList1 = directory.listFiles();
		 							    		    boolean storefileStatuspart1 = false;
		 							    		    File source1 = null;
		 							    		    for (File subDir : fList1) {
		 							    		    	subDir.getAbsolutePath();
		 								    			source1 = new File(subDir.getAbsolutePath());
		 								    			 BufferedInputStream bis = new BufferedInputStream(new FileInputStream(source1));
		 								    			 System.out.println("Start uploading first file");
		 								    			 storefileStatuspart1 = client.storeFile(pathcurrent+"/"+source1.getName(), bis);
		 								    			 logger.info(client.getReplyCode() +"          reply  String    : "+client.getReplyString());
		 						    			            bis.close();
		 						    			            if (storefileStatuspart1) {
		 						    			                System.out.println("The first file is uploaded successfully.");
		 						    			            }
		 								    		}
		 				    		        
		                                         if(storefileStatuspart1){
		 			                			
		 			                			fileName = fileformat+"_"+moduleId+"_"+randomString;
		 			                			//System.out.println("fileName  >>"+fileName);
		 				                		uploadPhotoDetails = new UploadPhotoDetails();
		 				                		uploadPhotoDetails.setUserId(userId);
		 				                		uploadPhotoDetails.setModuleId(moduleId);
		 				                		uploadPhotoDetails.setFileName(fileName);
		 				                		//uploadPhotoDetails.setPolicyId(policyId);
		 				                		//uploadPhotoDetails.setPolicyName(policyName);
		 			                		}
		 				    		        
		                                         UploadPhotoDetails uploadPhotoDetails_local =null;
		                   			           uploadPhotoDetails_local= PhotoUploadDAOManager.insertUploadBankPolicyPhotos(uploadPhotoDetails,1);
		                   			          if(uploadPhotoDetails_local!=null){
		                   			        	  if(uploadPhotoDetails_local.getStatusBean().getStatusFlag()==1 || uploadPhotoDetails_local.getStatusBean().getStatusFlag()==0){
		                   					 		request.setAttribute("message", uploadPhotoDetails_local.getStatusBean().getStatusMessage());
		                   					 		rd=request.getRequestDispatcher("./status.jsp");
		                   					 		rd.forward(request,response);
		                   			        	  }else if(uploadPhotoDetails_local.getStatusBean().getStatusFlag()==2){
		                   			        		 request.setAttribute("message", uploadPhotoDetails_local.getStatusBean().getStatusMessage()); 
		                   							 rd = request.getRequestDispatcher("/error.jsp");
		                   							 rd.forward(request,response);
		                   			        	  }
		                   						}else{
		                   							request.setAttribute("message", "Queue update failed. Please try again");
		                   							 rd=request.getRequestDispatcher("./status.jsp");
		                   							rd.forward(request,response);
		                   						}
		                   					
		 			} else {
		 				if(session != null){
		 					session.invalidate();
		 				}
		 				response.sendRedirect("./index.jsp");
		 			}
		 		
		            }
		
			}
		}catch(Exception e){
			e.printStackTrace();
			request.setAttribute("message", "Queue update failed. Please try again \n"+e.getMessage());
			rd=request.getRequestDispatcher("./status.jsp");
			rd.forward(request,response);
			logger.error(e);
		}
	}
	
	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length()-1);
			}
		}
		return "";
	}
	
	public static void deleteFile(File element) {
	    if (element.isDirectory()) {
	        for (File sub : element.listFiles()) {
	            deleteFile(sub);
	        }
	    }
	    element.delete();
	}
	
	
	 public static boolean makeDirectories(FTPClient ftpClient, String dirPath)
	            throws IOException {
	        String[] pathElements = dirPath.split("/");
	        if (pathElements != null && pathElements.length > 0) {
	            for (String singleDir : pathElements) {
	                boolean existed = ftpClient.changeWorkingDirectory(singleDir);
	                if (!existed) {
	                    boolean created = ftpClient.makeDirectory(singleDir);
	                    if (created) {
	                        System.out.println("CREATED directory: " + singleDir);
	                        ftpClient.changeWorkingDirectory(singleDir);
	                    } else {
	                        System.out.println("COULD NOT create directory: " + singleDir);
	                        return false;
	                    }
	                }
	            }
	        }
	        return true;
	    }

}
