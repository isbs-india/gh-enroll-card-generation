package com.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.PolicyDataBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class GenerateWindBankCardJobXLSXAction
 */
@WebServlet("/GenerateWindBankCardJobXLSXAction")
public class GenerateWindBankCardJobXLSXAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("GenerateWindBankCardJobXLSXAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GenerateWindBankCardJobXLSXAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				
				//int userId=0;
				String moduleUserId= "";
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(2);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				PolicyDataBean policyDataBean=null;
				//List<PoliciesBean> polBeanListObj = null;
				//polBeanListObj=new ArrayList<PoliciesBean>();
				logger.info("enter in controller before calling getPoliciesDDLDetails ");
				policyDataBean=EnrollAuxDAOManager.getPoliciesDDLDetails(4);
				RequestDispatcher rd =null;
				
				
				if(policyDataBean!=null){
					if(policyDataBean.getStatusBean().getStatusFlag()==1 || policyDataBean.getStatusBean().getStatusFlag()==0 ){
						session.setAttribute("policyDDLListObj",policyDataBean.getPolBeanListObj()); 
						rd = request.getRequestDispatcher("/xlsxtowindpolicy.jsp");
						rd.forward(request,response);
					}else if(policyDataBean.getStatusBean().getStatusFlag()==2){
						request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
						 rd = request.getRequestDispatcher("/error.jsp");
						 rd.forward(request,response);
					}
				}else{
					session.setAttribute("policyDDLListObj",""); 
					rd = request.getRequestDispatcher("/xlsxtowindpolicy.jsp");
					rd.forward(request,response);
				}

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}

}
