package com.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.model.LoginBean;
import com.persistence.common.EnrollAuxDAOManager;
import com.util.Constants;

/**
 * Servlet implementation class InsertXLSXWINDBankPolicyAction
 */
@WebServlet("/InsertXLSXWINDBankPolicyAction")
@MultipartConfig(fileSizeThreshold=1024*1024*2, // 2MB
maxFileSize=1024*1024*10,      // 10MB
maxRequestSize=1024*1024*50)   // 50MB
public class InsertXLSXWINDBankPolicyAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("InsertXLSXWINDBankPolicyAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InsertXLSXWINDBankPolicyAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			HttpSession session = request.getSession(true); 
			RequestDispatcher rd =null;
			if(session != null && !session.isNew()) {
				List<String> list =new ArrayList<String>();
				String policyId="0";
				int userId=0;
				String fileFormat="";
				String policyForm = "";
				String userName="";
				
				String moduleUserId= "";
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(2);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				
				/*int moduleId=0;
				if(request.getParameter("moduleId")!=null){
					moduleId=Integer.parseInt(request.getParameter("moduleId"));
				}
				if((request.getParameter("policy"))!=null && !(request.getParameter("policy").equals("0"))){
					policy = request.getParameter("policy");
					policyId=request.getParameter("policy").split("~")[0]+"";
					policyName = policy.split("~")[1];
				}
				
				int moduleUserId= 0;
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					userId =loginBean.getUid();
					//role=loginBean.getRoleId();
					userName =loginBean.getLoginUserName();
					//System.out.println("userName   >>"+userName);
					moduleUserId =loginBean.getModuleUserId(moduleId);
					System.out.println("moduleUserId   >>"+moduleUserId);
					if(moduleUserId==0){
						request.setAttribute("userName", loginBean.getLoginUserName());
						request.setAttribute("moduleId", moduleId);
						request.setAttribute("moduleName", Util.getModuleName(moduleId));
	            		getServletContext().getRequestDispatcher("/loginerror.jsp").forward(request, response);
	            		return;
					}
				}else{
					request.setAttribute("userName", "");
					request.setAttribute("moduleId", moduleId);
					request.setAttribute("moduleName", Util.getModuleName(moduleId));
					getServletContext().getRequestDispatcher("/loginerror.jsp").forward(request, response);
            		return;
				}*/
				
				
				
				
				 int formatId=0;
				String param = null;
				if(request.getParameter("printType")!=null && request.getParameter("printType").equals("ECard")){
					param ="E";
					formatId=1;
				}
				if(request.getParameter("printType")!=null && request.getParameter("printType").equals("Physical")){
					param ="P";
					formatId=2;
				}
				if(request.getParameter("printType")!=null && request.getParameter("printType").equals("Vendor")){
					param ="V";
					formatId=3;
				}
				  
				/*if(request.getParameter("policyForm")!=null && !request.getParameter("policyForm").equals("") && request.getParameter("policyForm").equals("SelfCardData"))
				{
					
					policyForm =request.getParameter("policyForm");
					if((request.getParameter("userId")!=null) && (!request.getParameter("userId").equals("")))
					{	
						userId=Integer.parseInt(request.getParameter("userId"));	
					}	
					if((request.getParameter("ddlPolicy")!=null) && (!request.getParameter("ddlPolicy").equals("")))
					{	
						policyId=request.getParameter("ddlPolicy");	
					}
					
					if((request.getParameter("format")!=null) && (!request.getParameter("format").equals("")))
					{	
						fileFormat=request.getParameter("format");	
					}
					
					if(request.getParameter("selfCardId1")!=null && !request.getParameter("selfCardId1").trim().equals("")){
						String selfCardId1 = request.getParameter("selfCardId1");
						list.add(selfCardId1);
					}
					if(request.getParameter("selfCardId2")!=null && !request.getParameter("selfCardId2").trim().equals("")){
						String selfCardId2 = request.getParameter("selfCardId2");
						list.add(selfCardId2);
					}
					if(request.getParameter("selfCardId3")!=null && !request.getParameter("selfCardId3").trim().equals("")){
						String selfCardId3 = request.getParameter("selfCardId3");
						list.add(selfCardId3);
					}
					if(request.getParameter("selfCardId4")!=null && !request.getParameter("selfCardId4").trim().equals("")){
						String selfCardId4 = request.getParameter("selfCardId4");
						list.add(selfCardId4);
					}
					if(request.getParameter("selfCardId5")!=null && !request.getParameter("selfCardId5").trim().equals("")){
						String selfCardId5 = request.getParameter("selfCardId5");
						list.add(selfCardId5);
					}
					if(request.getParameter("selfCardId6")!=null && !request.getParameter("selfCardId6").trim().equals("")){
						String selfCardId6 = request.getParameter("selfCardId6");
						list.add(selfCardId6);
					}
					if(request.getParameter("selfCardId7")!=null && !request.getParameter("selfCardId7").trim().equals("")){
						String selfCardId7 = request.getParameter("selfCardId7");
						list.add(selfCardId7);
					}
					if(request.getParameter("selfCardId8")!=null && !request.getParameter("selfCardId8").trim().equals("")){
						String selfCardId8 = request.getParameter("selfCardId8");
						list.add(selfCardId8);
					}
					if(request.getParameter("selfCardId9")!=null && !request.getParameter("selfCardId9").trim().equals("")){
						String selfCardId9 = request.getParameter("selfCardId9");
						list.add(selfCardId9);
					}
					if(request.getParameter("selfCardId10")!=null && !request.getParameter("selfCardId10").trim().equals("")){
						String selfCardId10 = request.getParameter("selfCardId10");
						list.add(selfCardId10);
					}
					if(request.getParameter("selfCardId11")!=null && !request.getParameter("selfCardId11").trim().equals("")){
						String selfCardId11 = request.getParameter("selfCardId11");
						list.add(selfCardId11);
					}
					if(request.getParameter("selfCardId12")!=null && !request.getParameter("selfCardId12").trim().equals("")){
						String selfCardId12 = request.getParameter("selfCardId12");
						list.add(selfCardId12);
					}
					if(request.getParameter("selfCardId13")!=null && !request.getParameter("selfCardId13").trim().equals("")){
						String selfCardId13 = request.getParameter("selfCardId13");
						list.add(selfCardId13);
					}
					if(request.getParameter("selfCardId14")!=null && !request.getParameter("selfCardId14").trim().equals("")){
						String selfCardId14 = request.getParameter("selfCardId14");
						list.add(selfCardId14);
					}
					if(request.getParameter("selfCardId15")!=null && !request.getParameter("selfCardId15").trim().equals("")){
						String selfCardId15 = request.getParameter("selfCardId15");
						list.add(selfCardId15);
					}
					if(request.getParameter("selfCardId16")!=null && !request.getParameter("selfCardId16").trim().equals("")){
						String selfCardId16 = request.getParameter("selfCardId16");
						list.add(selfCardId16);
					}
					if(request.getParameter("selfCardId17")!=null && !request.getParameter("selfCardId17").trim().equals("")){
						String selfCardId17 = request.getParameter("selfCardId17");
						list.add(selfCardId17);
					}
					if(request.getParameter("selfCardId18")!=null && !request.getParameter("selfCardId18").trim().equals("")){
						String selfCardId18 = request.getParameter("selfCardId18");
						list.add(selfCardId18);
					}
					if(request.getParameter("selfCardId19")!=null && !request.getParameter("selfCardId19").trim().equals("")){
						String selfCardId19 = request.getParameter("selfCardId19");
						list.add(selfCardId19);
					}
					if(request.getParameter("selfCardId20")!=null && !request.getParameter("selfCardId20").trim().equals("")){
						String selfCardId20 = request.getParameter("selfCardId20");
						list.add(selfCardId20);
					}
					if(request.getParameter("selfCardId21")!=null && !request.getParameter("selfCardId21").trim().equals("")){
						String selfCardId21 = request.getParameter("selfCardId21");
						list.add(selfCardId21);
					}
					if(request.getParameter("selfCardId22")!=null && !request.getParameter("selfCardId22").trim().equals("")){
						String selfCardId22 = request.getParameter("selfCardId22");
						list.add(selfCardId22);
					}
					if(request.getParameter("selfCardId23")!=null && !request.getParameter("selfCardId23").trim().equals("")){
						String selfCardId23 = request.getParameter("selfCardId23");
						list.add(selfCardId23);
					}
					if(request.getParameter("selfCardId24")!=null && !request.getParameter("selfCardId24").trim().equals("")){
						String selfCardId24 = request.getParameter("selfCardId24");
						list.add(selfCardId24);
					}
					if(request.getParameter("selfCardId25")!=null && !request.getParameter("selfCardId25").trim().equals("")){
						String selfCardId25 = request.getParameter("selfCardId25");
						list.add(selfCardId25);
					}
					if(request.getParameter("selfCardId26")!=null && !request.getParameter("selfCardId26").trim().equals("")){
						String selfCardId26 = request.getParameter("selfCardId26");
						list.add(selfCardId26);
					}
					if(request.getParameter("selfCardId27")!=null && !request.getParameter("selfCardId27").trim().equals("")){
						String selfCardId27 = request.getParameter("selfCardId27");
						list.add(selfCardId27);
					}
					if(request.getParameter("selfCardId28")!=null && !request.getParameter("selfCardId28").trim().equals("")){
						String selfCardId28 = request.getParameter("selfCardId28");
						list.add(selfCardId28);
					}
					if(request.getParameter("selfCardId29")!=null && !request.getParameter("selfCardId29").trim().equals("")){
						String selfCardId29 = request.getParameter("selfCardId29");
						list.add(selfCardId29);
					}
					if(request.getParameter("selfCardId30")!=null && !request.getParameter("selfCardId30").trim().equals("")){
						String selfCardId30 = request.getParameter("selfCardId30");
						list.add(selfCardId30);
					}
					if(request.getParameter("selfCardId31")!=null && !request.getParameter("selfCardId31").trim().equals("")){
						String selfCardId31 = request.getParameter("selfCardId31");
						list.add(selfCardId31);
					}
					if(request.getParameter("selfCardId32")!=null && !request.getParameter("selfCardId32").trim().equals("")){
						String selfCardId32 = request.getParameter("selfCardId32");
						list.add(selfCardId32);
					}
					if(request.getParameter("selfCardId33")!=null && !request.getParameter("selfCardId33").trim().equals("")){
						String selfCardId33 = request.getParameter("selfCardId33");
						list.add(selfCardId33);
					}
					if(request.getParameter("selfCardId34")!=null && !request.getParameter("selfCardId34").trim().equals("")){
						String selfCardId34 = request.getParameter("selfCardId34");
						list.add(selfCardId34);
					}
					if(request.getParameter("selfCardId35")!=null && !request.getParameter("selfCardId35").trim().equals("")){
						String selfCardId35 = request.getParameter("selfCardId35");
						list.add(selfCardId35);
					}
					if(request.getParameter("selfCardId36")!=null && !request.getParameter("selfCardId36").trim().equals("")){
						String selfCardId36 = request.getParameter("selfCardId36");
						list.add(selfCardId36);
					}
					if(request.getParameter("selfCardId37")!=null && !request.getParameter("selfCardId37").trim().equals("")){
						String selfCardId37 = request.getParameter("selfCardId37");
						list.add(selfCardId37);
					}
					if(request.getParameter("selfCardId38")!=null && !request.getParameter("selfCardId38").trim().equals("")){
						String selfCardId38 = request.getParameter("selfCardId38");
						list.add(selfCardId38);
					}
					if(request.getParameter("selfCardId39")!=null && !request.getParameter("selfCardId39").trim().equals("")){
						String selfCardId39 = request.getParameter("selfCardId39");
						list.add(selfCardId39);
					}
					if(request.getParameter("selfCardId40")!=null && !request.getParameter("selfCardId40").trim().equals("")){
						String selfCardId40 = request.getParameter("selfCardId40");
						list.add(selfCardId40);
					}
					if(request.getParameter("selfCardId41")!=null && !request.getParameter("selfCardId41").trim().equals("")){
						String selfCardId41 = request.getParameter("selfCardId41");
						list.add(selfCardId41);
					}
					if(request.getParameter("selfCardId42")!=null && !request.getParameter("selfCardId42").trim().equals("")){
						String selfCardId42 = request.getParameter("selfCardId42");
						list.add(selfCardId42);
					}
					if(request.getParameter("selfCardId43")!=null && !request.getParameter("selfCardId43").trim().equals("")){
						String selfCardId43 = request.getParameter("selfCardId43");
						list.add(selfCardId43);
					}
					if(request.getParameter("selfCardId44")!=null && !request.getParameter("selfCardId44").trim().equals("")){
						String selfCardId44 = request.getParameter("selfCardId44");
						list.add(selfCardId44);
					}
					if(request.getParameter("selfCardId45")!=null && !request.getParameter("selfCardId45").trim().equals("")){
						String selfCardId45 = request.getParameter("selfCardId45");
						list.add(selfCardId45);
					}
					
					   if(list.size()>0){
						   int formatId=0;
						   if(fileFormat.equals("vendor")){
							   formatId=1;
							}else if(fileFormat.equals("preprint")){
								 formatId=2;
								//generateVendorFormat();
							}else if(fileFormat.equals("plainpaper")){
								 formatId=3;
								//generateVendorFormat();
							}else if(fileFormat.equals("noproposal")){
								 formatId=4;
							}
							String xml = sendFaxDataResonse(list, policyId, moduleUserId, formatId);
						System.out.println("xml  :"+xml);
						int errorFlag =EnrollAuxDAOManager.insertSelfCardDetails(xml,param);
						String FinalMessage="";
						if(errorFlag == 1)
						{
						    FinalMessage="Data inserted successfully<br/><br/>";
						}
						else
						{
							FinalMessage="Data insertion failed<br/><br/>";	
						}
						String FinalMessage1="Ecard generation request status";
						request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
						request.setAttribute("message","<center>"+FinalMessage+"</center>");
						rd = request.getRequestDispatcher("./status.jsp");
						rd.forward(request,response);
					   }
					
				}else*/ if(request.getParameter("XLpolicyForm")!=null && !request.getParameter("XLpolicyForm").equals("") && request.getParameter("XLpolicyForm").equals("XLSelfCardData")){
						response.setContentType("text/html; charset=UTF-8");
					 try {
						 /*if((request.getParameter("userId")!=null) && (!request.getParameter("userId").equals("")))
							{	
								userId=Integer.parseInt(request.getParameter("userId"));	
							}	*/
							/*if((request.getParameter("ddlPolicy")!=null) && (!request.getParameter("ddlPolicy").equals("")))
							{	
								policyId=request.getParameter("ddlPolicy");	
							}*/
							
							/*if((request.getParameter("format")!=null) && (!request.getParameter("format").equals("")))
							{	
								fileFormat=request.getParameter("format");	
							}*/
							String extension ="";
							int BUFFER_LENGTH = 4096;
							File uploadPath = (File)request.getServletContext().getAttribute("javax.servlet.context.tempdir");  
							 Part filePart = request.getPart("xlfile");
							 if (filePart != null && filePart.getSize() > 0) {
								 String fileName = getFileName(filePart);
								 int i = fileName.lastIndexOf('.');
								 if (i > 0) {
								     extension = fileName.substring(i+1);
								     extension.toLowerCase();
								     if(!extension.equals("xlsx")){
								     request.setAttribute("message", "Uploaded file format is not in xlsx");
										 rd=request.getRequestDispatcher("./status.jsp");
										 rd.forward(request,response);
								     
								     }else{
								 
								 String filePath = uploadPath + File.separator + fileName;
								        InputStream is = request.getPart(filePart.getName()).getInputStream();
								        FileOutputStream os = new FileOutputStream(filePath);
								        byte[] bytes = new byte[BUFFER_LENGTH];
								        int read = 0;
								        while ((read = is.read(bytes, 0, BUFFER_LENGTH)) != -1) {
								            os.write(bytes, 0, read);
								        }
								        os.flush();
								        is.close();
								        os.close();
								     
								      //  InputStream ExcelFileToRead = new FileInputStream(filePath);
								        InputStream excelFileToRead = new FileInputStream(filePath);
										XSSFWorkbook  wb = new XSSFWorkbook(excelFileToRead);
										XSSFSheet sheet = wb.getSheetAt(0);
										XSSFRow row; 
										XSSFCell cell;
										int rowCount=0;
										Iterator rows = sheet.rowIterator();
										while (rows.hasNext())
										{
											rowCount++;
											row=(XSSFRow) rows.next();
											Iterator cells = row.cellIterator();
											while (cells.hasNext())
											{
												cell=(XSSFCell) cells.next();
												if (cell.getCellType() == XSSFCell.CELL_TYPE_STRING)
												{
													System.out.print(cell.getStringCellValue()+" ");
													if(rowCount!=1){
														list.add(cell.getStringCellValue());
													}
												}
											}
										}
							String FinalMessage="";
							logger.info("row count : "+rowCount);
							logger.info("list size  : "+list.size());
						   if(rowCount<900){
							   if(list.size()>0){
									String xml = sendFaxDataResonse(list, "0", moduleUserId, formatId);
								
								logger.info("xml  : "+xml);
								logger.info("param  : "+param);
								int errorFlag =EnrollAuxDAOManager.insertWINDSelfCardDetails(xml,param);
								logger.info("errorFlag   "+errorFlag);
								if(errorFlag == 1)
								{
								    FinalMessage="Data inserted successfully<br/><br/>";
								}
								else
								{
									FinalMessage="Data insertion failed<br/><br/>";	
								}
								String FinalMessage1="Ecard generation request status";
								request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
								request.setAttribute("message","<center>"+FinalMessage+"</center>");
								rd = request.getRequestDispatcher("./status.jsp");
								rd.forward(request,response);
							   }else{
								   FinalMessage="Uploaded excel not proper format <br/><br/>";	
								   String FinalMessage1="Uploaded excel not proper format";
									request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
									request.setAttribute("message","<center>"+FinalMessage+"</center>");
									rd = request.getRequestDispatcher("./status.jsp");
									rd.forward(request,response);
								   
							   }
						   }else{
							   FinalMessage="excel Row count should be less than 900 records <br/><br/>";	
							   String FinalMessage1="Ecard generation request status";
								request.setAttribute("message1","<center>"+FinalMessage1+"</center>");
								request.setAttribute("message","<center>"+FinalMessage+"</center>");
								rd = request.getRequestDispatcher("./status.jsp");
								rd.forward(request,response);
						   }
							 }
								 }// file exten end
								 }// file part end
						} catch (Exception e) {
						    e.printStackTrace();
							request.setAttribute("message","<center>"+e.getMessage()+"</center>");
							rd = request.getRequestDispatcher("./status.jsp");
							rd.forward(request,response);
						} 
				}
				
				

			} else {
				if(session != null){
					session.invalidate();
				}
				response.sendRedirect("./index.jsp");
			}
		}catch(Exception e){
			e.printStackTrace();
			SendMail mailsend=new SendMail();
			try {
				mailsend.SendMailToUsers("ENROLLAUXTASKS",e.getMessage(),"",Constants.alertsEmailId,"","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(e);
		}
	}
	public String sendFaxDataResonse(List<String> selfCardList,String policyId,String userId,int fileFormat) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		// root elements
		Document doc = null;
		Element breakupElement = null;
		StringWriter resultAsString = new StringWriter();
        try{
        	docFactory = DocumentBuilderFactory.newInstance();
        	docBuilder = docFactory.newDocumentBuilder();
        	doc = docBuilder.newDocument();
        	breakupElement = doc.createElement("NODE");
        	doc.appendChild(breakupElement);
        	Element jobElement = doc.createElement("JOBS");
        	breakupElement.appendChild(jobElement);
        	Element PolicyIdElement = doc.createElement("POLICYID");
        	PolicyIdElement.appendChild(doc.createTextNode(policyId));
        	jobElement.appendChild(PolicyIdElement);
        	Element userIdElement = doc.createElement("USERID");
        	userIdElement.appendChild(doc.createTextNode(userId));
        	jobElement.appendChild(userIdElement);
        	Element fileFormatElement = doc.createElement("FORMATID");
        	fileFormatElement.appendChild(doc.createTextNode(fileFormat+""));
        	jobElement.appendChild(fileFormatElement);
        	if(selfCardList!=null && selfCardList.size()>0){
        		Element cardIdsElement = doc.createElement("GHCARDIDS");
        		jobElement.appendChild(cardIdsElement);
			    for(int i=0;i<selfCardList.size();i++){
		        	Element selfcardIdElement = doc.createElement("CARDID");
		        	selfcardIdElement.appendChild(doc.createTextNode(String.valueOf(selfCardList.get(i))));
		        	cardIdsElement.appendChild(selfcardIdElement);
				}
        	}
        	
        	/*Element CARD_FORMATElement = doc.createElement("CARD_FORMAT");
        	CARD_FORMATElement.appendChild(doc.createTextNode(param));
        	jobElement.appendChild(CARD_FORMATElement);*/
        	
        	TransformerFactory transformerFactory = TransformerFactory.newInstance();
    		Transformer transformer = transformerFactory.newTransformer();
    		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    		DOMSource source = new DOMSource(doc);
    		StreamResult result = new StreamResult(resultAsString);
    		// StreamResult streamResult = new StreamResult(new File(FaxData.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/ghplfax/faxresponse.xml")); 
    		transformer.transform(source, result);
            }catch(Exception e){
            	 logger.error(e);
            }finally{
            	 docFactory = null;
        		 docBuilder = null;
        		 doc = null;
        		 breakupElement = null;
            }
            return resultAsString.toString();
	}
	
	
	private String getFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
          if (cd.trim().startsWith("filename")) {
            return cd.substring(cd.indexOf('=') + 1).trim()
                    .replace("\"", "");
          }
        }
        return null;
      }

}
