package com.action;

import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.util.MyProperties;

/**
 * Servlet implementation class DownloadPhotoLotAction
 */
@WebServlet("/DownloadPhotoLotAction")
public class DownloadPhotoLotAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("DownloadPhotoLotAction"); 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DownloadPhotoLotAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		FileInputStream inputStream=null;
		try {
			MyProperties myResources = new MyProperties();
			String lotNumber = request.getParameter("lotNumber");
			String moduleId = request.getParameter("moduleId");
			
			String downloadslocal = myResources.getMyProperties("photodownload_file_path_zip");
			String inputFilepath="";
			//if(!moduleId.equals("2")){
				
				response.setContentType("application/zip");
			if(moduleId.equals("2")){
				inputFilepath= downloadslocal+lotNumber+".zip";
				response.setHeader("Content-Disposition", "attachment; filename="+lotNumber+".zip");
			}else{
				inputFilepath= downloadslocal+lotNumber+".zip";
				response.setHeader("Content-Disposition", "attachment; filename="+lotNumber+".zip");
			}
				/*else{
				String fileName="";
				if(formatId.equals("1")){
					fileName ="ABCards_Lot_"+lotNumber+"_vendor";
				}
				if(formatId.equals("2")){
					fileName ="ABCards_Lot_"+lotNumber;
				}
				if(formatId.equals("3")){
					fileName =lotNumber+"_plainpaper";
				}
				inputFilepath= downloadslocal+fileName+".pdf";
				response.setContentType("application/pdf");
				response.setHeader("Content-Disposition", "attachment; filename="+fileName+".pdf");
			}*/
		      inputStream = new FileInputStream(inputFilepath);
				ServletOutputStream output = response.getOutputStream();
				 byte[] bytesArray = new byte[4096];
			        int bytesRead = -1;
			        while ((bytesRead = inputStream.read(bytesArray)) != -1) {
			        	output.write(bytesArray, 0, bytesRead);
			        }
	        	  output.flush();
	        	  output.close();
        	
        } catch (Exception e) {
            e.printStackTrace();
        }finally{
        	if(inputStream!=null){
        		inputStream=null;
        	}
        }
	}
}
