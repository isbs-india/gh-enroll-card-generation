package com.action;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.model.LoginBean;
import com.model.PolicyDataBean;
import com.model.SinglePhotoBean;
import com.persistence.common.PhotoUploadDAOManager;

/**
 * Servlet implementation class UploadPhotoByEmpAction
 */
@WebServlet("/UploadPhotoByEmpAction")
public class UploadPhotoByEmpAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("UploadPhotoByEmpAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadPhotoByEmpAction() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try{
			HttpSession session= request.getSession();
			if(session != null && !session.isNew()) {
				//int userId=0;
				String moduleUserId= "";
				int moduleId =0;
				if(request.getParameter("moduleId")!=null){
					moduleId=Integer.parseInt(request.getParameter("moduleId"));
					//session.setAttribute("moduleId", 0);
				}
				
				if(session.getAttribute("user")!=null){
					LoginBean loginBean=(LoginBean)session.getAttribute("user");
					//userId =loginBean.getUid();
					moduleUserId =""+loginBean.getModuleUserId(moduleId);
					if(moduleUserId.equals("0") || moduleUserId.equals("")){
						getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
	            		return;
					}
				}else{
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				if(moduleUserId.equals("")){
					getServletContext().getRequestDispatcher("/sessionfile.jsp").forward(request, response);
            		return;
				}
				String policy = null;
				String empId = null;
				String bano = null;
				logger.info("policy split before ");
				if(request.getParameter("policy")!=null){
					policy = request.getParameter("policy").trim().toUpperCase();
					String[] policy1 = policy.split("~");
					policy = policy1[0];
				}
				logger.info("policy split after ");
				if(request.getParameter("empId")!=null && !request.getParameter("empId").equals("")){
					empId = request.getParameter("empId").trim().toUpperCase();
				}
				
				
				if(request.getParameter("bano")!=null && !request.getParameter("bano").equals("")){
					bano = request.getParameter("bano").trim().toUpperCase();
				}
				
				
				RequestDispatcher rd = null;
				List<SinglePhotoBean> empList= null;
				PolicyDataBean policyDataBean = null;
				if(policy != null && empId != null && moduleId != 0){
					logger.info("calling method before getEmployeeDetailsEmpId");
					policyDataBean = PhotoUploadDAOManager.getEmployeeDetailsEmpId(policy,empId, moduleId);
					logger.info("calling method after getEmployeeDetailsEmpId");
					if(policyDataBean!=null){
						if(policyDataBean.getStatusBean().getStatusFlag()==1 || policyDataBean.getStatusBean().getStatusFlag()==0){
							request.setAttribute("empList", policyDataBean.getSinglephotoList());
							 rd = request.getRequestDispatcher("./uploadPhotos.jsp");
							rd.forward(request,response);
						}else if(policyDataBean.getStatusBean().getStatusFlag()==0){
							 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
							 rd = request.getRequestDispatcher("/error.jsp");
							 rd.forward(request,response);
						}
					}else{
						request.setAttribute("empList", empList);
						 rd = request.getRequestDispatcher("./uploadPhotos.jsp");
						rd.forward(request,response);
					}
				}
				
				if(policy != null && bano != null && moduleId != 0){
					policyDataBean=PhotoUploadDAOManager.getEmployeeDetailsBANo(policy,bano, moduleId);
					if(policyDataBean!=null){
						if(policyDataBean.getStatusBean().getStatusFlag()==1 || policyDataBean.getStatusBean().getStatusFlag()==0){
							request.setAttribute("empList", policyDataBean.getSinglephotoList());
							 rd = request.getRequestDispatcher("./uploadPhotos.jsp");
							rd.forward(request,response);
						}else if(policyDataBean.getStatusBean().getStatusFlag()==0){
							 request.setAttribute("message", policyDataBean.getStatusBean().getStatusMessage()); 
							 rd = request.getRequestDispatcher("/error.jsp");
							 rd.forward(request,response);
						}
					}else{
						request.setAttribute("empList", empList);
						 rd = request.getRequestDispatcher("./uploadPhotos.jsp");
						rd.forward(request,response);
					}
				}
				
				/*if(policy != null && empId != null && moduleId != 0){
					empList=PhotoUploadDAOManager.getEmployeeDetailsEmpId(policy,empId, moduleId);
				}
				
				if(policy != null && bano != null && moduleId != 0){
					empList=PhotoUploadDAOManager.getEmployeeDetailsBANo(policy,bano, moduleId);
				}*/
				/*request.setAttribute("empList", empList);
				RequestDispatcher rd = request.getRequestDispatcher("./uploadPhotos.jsp");
				rd.forward(request,response);*/
			}else{
				Cookie[] cookies = request.getCookies();
		    	if(cookies != null){
		    	for(Cookie cookie : cookies){
		    		if(cookie.getName().equals("JSESSIONID")){
		    			cookie.setValue("");
		    			cookie.setPath("/");
		    			cookie.setMaxAge(0);
		    			response.addCookie(cookie);
		    			break;
		    		}
		    	}
		    	}
				if(session != null){
		    		session.invalidate();
		    	}
				 response.sendRedirect("./login.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e);
				getServletContext().getRequestDispatcher("/error.jsp").forward(request, response);
				//sendMail.SendMailToUsers(String subject, String messageText,String displayfilenme,String TO,String cc,String bcc)
				SendMail sendMail = new SendMail();
				//SendMail_preAuth mailsend=new SendMail_preAuth();
				try {
					sendMail.SendMailToUsers("ENROLL_AUXILLARY_TASKS_EXCEPTION_UploadPhotoByEmpAction",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
			}
	}

}
