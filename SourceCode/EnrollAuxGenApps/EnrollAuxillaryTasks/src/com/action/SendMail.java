package com.action;

//import java.net.URL;
import java.util.ArrayList;
//import java.util.HashMap;
import java.util.Properties;
//import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import javax.activation.DataHandler;
//import javax.activation.URLDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
//import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

//import org.apache.commons.net.ftp.FTPClient;
import org.apache.log4j.Logger;

//import com.action.SendMail.SMTPAuthenticator;
import com.util.MyProperties;
//import org.apache.commons.net.ftp.FTP;
//import org.apache.commons.net.ftp.FTPClient;


import com.util.Constants;

public class SendMail {


	private static Logger logger=Logger.getLogger("SendMail");
	// ResourceBundle myResources;
	String username;
	String password;
	String mail_transport_protocol;
	String mail_smtp_port;
	String mail_smtp_host;
	String mail_smtp_auth;
	String mail_messageText;
	String sec_username;
	String sec_password;
	String sec_mail_transport_protocol;
	String sec_mail_smtp_port;
	String sec_mail_smtp_host;
	String sec_mail_smtp_auth;
	String sec_mail_messageText;
	String bccMailID;
	String sec_bccMailID;
	String mail_from;
	String sec_mail_from;

	String preauthBcc=null;

	String fileName=null;
	//String extension=null;
	String filePath=null;
	String FTPHOST=null;
	String FTPUName=null;
	String FTPPwd=null;

	String docpath=null;
	String docpath_http=null;
	String FTP_IAUTH_PATH;  
	String ftpFolder;

	public int SendMailToUsers(String subject, String messageText,String displayfilenme,String TO,String cc,String bcc)
			throws MessagingException
			{
		// System.out.println("enter send mail"+messageText);
		int k=0;  
		MyProperties Gdbp = new MyProperties();   
		username = Gdbp.getMyProperties("preauth_username");
		password = Gdbp.getMyProperties("preauth_password");
		mail_from = Gdbp.getMyProperties("preauth_from");
		//mail_transport_protocol = myResources.getString("preauth_transport_protocol");
		mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
		mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
		mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
		mail_messageText = Gdbp.getMyProperties("preauth_messageText");
		mail_transport_protocol=Gdbp.getMyProperties("preauth_transport_protocol");
		//FTP_IAUTH_PATH = Gdbp.getMyProperties("FTP_IAUTH_PATH");
		ftpFolder = Gdbp.getMyProperties("ftp_folder");

		try
		{
			Properties props = new Properties();
			props.put("mail.transport.protocol", mail_transport_protocol);
			props.put("mail.smtp.port", mail_smtp_port);
			props.put("mail.smtp.host", mail_smtp_host);
			props.put("mail.smtp.auth", mail_smtp_auth);
			props.put("mail.smtp.ssl.trust", "*");
			props.put("mail.smtp.starttls.enable", "true");
			Authenticator auth = new SMTPAuthenticator();

			Session session = Session.getInstance(props, auth);
			MimeMessage message = new MimeMessage(session);
			message.setContent(messageText, mail_messageText);
			message.setSubject(subject);

			String toarray[] = TO.split(",");

			// Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

			for(int i = 0; i < toarray.length; i++)
			{
				InternetAddress fromAddress = new InternetAddress(mail_from);
				// System.out.println("fromAddress"+fromAddress);

				message.setFrom(fromAddress);
				Matcher g=p.matcher(toarray[i]);
				//Matcher m=p.matcher(args[0]);
				boolean b=g.matches();
				if(b==true)
				{
					InternetAddress toAddress = new InternetAddress(toarray[i]);
					message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);

					//CC*******************
					String toarraycc[] = cc.split(",");
					ArrayList ccemailidarr=new ArrayList();
					for(int l = 0; l < toarraycc.length; l++)
					{
						Matcher v=p.matcher(toarraycc[l]);
						boolean h=v.matches();
						if(h==true)
						{
							StringTokenizer st = new StringTokenizer(toarraycc[l],",");
							while (st.hasMoreTokens()) {
								ccemailidarr.add(st.nextToken());
							}//while
						}//if h

					}//for CC
					int sizecc = ccemailidarr.size(); 
					// System.out.println("SIZE CC"+sizecc);
					InternetAddress ccAddress[] = new InternetAddress[sizecc];
					for(int m = 0; m < sizecc; m++){
						ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());
					}
					message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);

					//CC END

					//BCC*******************
					String toarraybcc[] = bcc.split(",");
					ArrayList bccemailidarr=new ArrayList();
					for(int l = 0; l < toarraybcc.length; l++)
					{
						Matcher vh=p.matcher(toarraybcc[l]);
						boolean hh=vh.matches();
						if(hh==true)
						{
							StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
							while (st.hasMoreTokens()) {
								bccemailidarr.add(st.nextToken());
							}//while
						}//if h

					}//for BCC
					int sizebcc = bccemailidarr.size(); 
					// System.out.println("SIZE CC"+sizecc);
					InternetAddress bccAddress[] = new InternetAddress[sizebcc];
					for(int bm = 0; bm < sizebcc; bm++){
						bccAddress[bm] = new InternetAddress(bccemailidarr.get(bm).toString());
					}
					message.setRecipients(javax.mail.Message.RecipientType.BCC, bccAddress);
					//BCC END  
					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(messageText, mail_messageText);
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart);
					message.setContent(multipart);

					Transport.send(message);

					k=1;
				}//if b is true
			}//for  LOOP FOR TO EMAILID

		}
		catch(Exception ex)
		{
			logger.error((new StringBuilder("Error in sendmail from EnrollAuxillaryTasks Application ::")).append(ex).toString());
			k=2;
		}
		return k;
			}



	/**
	 * The Class SMTPAuthenticator.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}

	/**
	 * The Class Sec_SMTPAuthenticator.
	 */
	private class Sec_SMTPAuthenticator extends javax.mail.Authenticator {

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(sec_username, sec_password);
		}
	}


}
