package com.action;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.exception.DAOException;
import com.model.LoginBean;
import com.persistence.login.LoginDAOManager;
import com.util.Constants;
import com.util.Crypt;

/**
 * Servlet implementation class LoginAction
 */
@WebServlet("/LoginAction")
public class LoginAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
	Logger logger = Logger.getLogger("LoginAction");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginAction() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd =null;
		try{
			HttpSession session = request.getSession(true); 
			if(session != null && !session.isNew()) {
				String uname=null;
				String encriptPwd=null;
				String salt="am";
				int serverip=0;
				LoginBean user=null;
				String userName=null;
				String pwd=null;
				
				try{
					
					if(request.getParameter("userName")!=null){
						userName=request.getParameter("userName");
					}
					
					if(request.getParameter("userName")!=null){
						pwd=request.getParameter("pwd");
					}
					
					if(request.getParameter("serverip")!=null){
						serverip=Integer.parseInt(request.getParameter("serverip"));
					}
					//String hostName = request.getRemoteAddr();
					
					String hostName = request.getHeader("X-FORWARDED-FOR");  
					   if (hostName == null) {  
						   hostName = request.getRemoteAddr();  
					   }
					logger.info(userName   +"   ----   "+   pwd   +"  ---  "+  serverip   +"   ---    "+hostName);
					
					if(userName!=null && !userName.equals("")){
						uname=userName;
					}
					if(pwd!=null && !pwd.equals("")){
						// Crypt cryptobj = new Crypt();
						 encriptPwd = Crypt.crypt(salt, pwd);
					}
					
					if ((!uname.equals("") && uname !=null)&&(!encriptPwd.equals("") && encriptPwd !=null) && (serverip != 0)) { 
						try {
							user = new LoginBean();
							user= LoginDAOManager.getUserDetails(uname, encriptPwd, serverip, hostName);
							if(user!=null){
								if(user.getStatusBean().getStatusFlag()==1  || user.getStatusBean().getStatusFlag()==0){
								session.setAttribute("user", user);
								 session.setAttribute("userName", user.getLoginUserName());
								 session.setAttribute("roleId",user.getRoleId());
								 session.setAttribute("userId",user.getUid());
								 //session.setAttribute("moduleUserId", user.getgetModuleUserId())
								 
								 /*if(user.getRoleId()==51){
									 rd = request.getRequestDispatcher("/SearchHospitalAction");
									 rd.forward(request,response);
								 }
								 else if(user.getRoleId()==21){
									 rd = request.getRequestDispatcher("/SearchHospitalAction");
									 rd.forward(request,response);
								 }
								 else if(user.getRoleId()==18){
									 rd = request.getRequestDispatcher("/SearchHospitalAction");
									 rd.forward(request,response);
								 }
								 else if(user.getRoleId()==53){
										 rd = request.getRequestDispatcher("/viewcommuniationmaildetails.jsp");
										 rd.forward(request,response);		 
								 }else{*/
									 rd = request.getRequestDispatcher("/GetPolicyDDLDataAction");
									 rd.forward(request,response);
								/* }*/
								}else if(user.getStatusBean().getStatusFlag()==2){
									request.setAttribute("message", user.getStatusBean().getStatusMessage()); 
									 rd = request.getRequestDispatcher("/error.jsp");
									 rd.forward(request,response);
								}
							}else{
								if(session != null){
									session.invalidate();
						    	}
								 request.setAttribute("errorMessage", "Invalid username or password"); 
								 rd = request.getRequestDispatcher("/index.jsp");
								 rd.forward(request,response);
							}
						} catch (DAOException e) {
							e.printStackTrace();
						}
					}
					
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			} else {
				if(session != null){
		    		session.invalidate();
		    	}
			    response.sendRedirect("./index.jsp");
			}
			}catch(Exception e){
				e.printStackTrace();
				SendMail mailsend=new SendMail();
				try {
					mailsend.SendMailToUsers("IADMIN",e.getMessage(),"",Constants.alertsEmailId,"","");
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
				logger.error(e);
				 rd = request.getRequestDispatcher("/error.jsp");
				 rd.forward(request,response);
			}
	}

}
