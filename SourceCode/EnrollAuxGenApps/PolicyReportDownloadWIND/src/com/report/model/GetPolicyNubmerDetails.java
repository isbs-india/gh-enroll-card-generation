package com.report.model;

public class GetPolicyNubmerDetails {
	/*SELECT 
	IP.POLICY_NO,
	UPPER(IP.INSUREDNAME)    INSUREDNAME,
	UPPER(VWI.CARDID)        GHPLID,
	TO_CHAR(IP.INSUREDINCEPTIONDT, 'DD-MM-YYYY')    INSUREDINCEPTIONDT,
	IP.RENEWALSTATUS      RENEWALSTATUS,
	IP.CUSTOMER_ID        CUSTOMER_ID */
	
	private String policyNumber;
	private String insuredName;
	private String cardId;
	private String inceptionDate;
	private String renewalStatus;
	private String customerId;
		
	public String getPolicyNumber() {
		return policyNumber;
	}
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getInceptionDate() {
		return inceptionDate;
	}
	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}
	public String getRenewalStatus() {
		return renewalStatus;
	}
	public void setRenewalStatus(String renewalStatus) {
		this.renewalStatus = renewalStatus;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

}
