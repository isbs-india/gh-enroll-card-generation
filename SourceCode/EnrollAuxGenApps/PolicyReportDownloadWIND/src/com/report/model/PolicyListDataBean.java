package com.report.model;

import java.util.List;

public class PolicyListDataBean {
	private List<PolicyLotDetailsBean> lotListObj;
	private List<GetPolicyNubmerDetails> policyNumberDetailsObj;
	private List<GetPolicyNubmerStatus> policyNumberStatusObj;
	private int flag;
	
	public List<PolicyLotDetailsBean> getLotListObj() {
		return lotListObj;
	}

	public void setLotListObj(List<PolicyLotDetailsBean> lotListObj) {
		this.lotListObj = lotListObj;
	}

	public List<GetPolicyNubmerDetails> getPolicyNumberDetailsObj() {
		return policyNumberDetailsObj;
	}

	public void setPolicyNumberDetailsObj(
			List<GetPolicyNubmerDetails> policyNumberDetailsObj) {
		this.policyNumberDetailsObj = policyNumberDetailsObj;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public List<GetPolicyNubmerStatus> getPolicyNumberStatusObj() {
		return policyNumberStatusObj;
	}

	public void setPolicyNumberStatusObj(
			List<GetPolicyNubmerStatus> policyNumberStatusObj) {
		this.policyNumberStatusObj = policyNumberStatusObj;
	}

}
