package com.report.util;

public class ProcedureConstants { 
	
	public static final String PROC_FOR_GET_POLICY_NUMBER_DETAILS ="{call PKG_ECARDS_IN_PDF.GET_POLICY_DOC_LOT_DETAILS(?,?)}";
	public static final String PROC_FOR_GET_POLICY_COUNT_DETAILS ="{call PKG_ECARDS_IN_PDF.PROC_GET_POLICY_CNT(?,?)}";
	public static final String PROC_FOR_GET_LOT_POLICYDOC_XML_TEMPLATE ="{call PKG_ECARDS_IN_PDF.GET_LOT_POLICYDOC_XML_TEMPLATE(?,?,?)}";
	public static final String PROC_FOR_PUT_LOT_DETAILS ="{call PKG_ECARDS_IN_PDF.PUT_POLICY_DOC_LOT_DETAILS(?,?,?)}";
	
}

