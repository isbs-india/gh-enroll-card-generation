package com.report.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import com.report.exception.ServiceLocatorException;


 
public class ServiceLocator {
	private InitialContext ic;
    private Map<String,DataSource> cache;  
    private static ServiceLocator serviceLocator;
    
    static {
    	try {
    		serviceLocator = new ServiceLocator();
        } catch(ServiceLocatorException se) {         
        	se.printStackTrace(System.err);
        }
    }
    
    private ServiceLocator() throws ServiceLocatorException {
    	try {
        	ic = new InitialContext();
        	cache = Collections.synchronizedMap(new HashMap<String,DataSource>());
        } catch (NamingException ne) {
        	throw new ServiceLocatorException(ne);
        } catch (Exception e) {
        	throw new ServiceLocatorException(e);
        }
    }

    static public ServiceLocator getInstance() {
    	return serviceLocator;
    } 
    
    /**
     * This method obtains the datasource itself for a caller
     * @return the DataSource corresponding to the name parameter
     */
    public DataSource getDataSource(String dataSourceName) throws ServiceLocatorException {
    	DataSource dataSource = null;
    	try {
    		if (cache.containsKey(dataSourceName)) {
    			dataSource = (DataSource) cache.get(dataSourceName);
    		} else {
    			dataSource = (DataSource)ic.lookup(dataSourceName);
    			cache.put(dataSourceName, dataSource );
    		}
    	} catch (NamingException ne) {
    		throw new ServiceLocatorException(ne);
    	} catch (Exception e) {
    		throw new ServiceLocatorException(e);
    	}
    	return dataSource;
    }
}
