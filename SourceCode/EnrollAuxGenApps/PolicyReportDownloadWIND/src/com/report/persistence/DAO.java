package com.report.persistence;

import java.sql.Connection;

import com.report.exception.DAOException;

public interface DAO {
	public Connection getConnection() throws DAOException;
}
