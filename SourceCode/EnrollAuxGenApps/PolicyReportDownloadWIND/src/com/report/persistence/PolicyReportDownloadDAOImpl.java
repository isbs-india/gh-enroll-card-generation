package com.report.persistence;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import org.apache.log4j.Logger;

import com.report.model.GetPolicyNubmerDetails;
import com.report.model.GetPolicyNubmerStatus;
import com.report.model.PolicyListDataBean;
import com.report.exception.DAOException;
import com.report.model.PolicyLotDetailsBean;
import com.report.util.DBConn;
import com.report.util.ProcedureConstants;

public class PolicyReportDownloadDAOImpl extends DBConn implements PolicyReportDownloadDAO{
	Logger logger =(Logger) Logger.getInstance("UploadPhotosDAOImpl");
	
	@Override
	public PolicyListDataBean getPolicyLotDetails(int isCron, int moduleId)	throws DAOException {
		logger.info("ente getPolicyLotDetails method    isCron  "+isCron);
		PolicyLotDetailsBean insertLotDetailsBeanObj = null;
		List<PolicyLotDetailsBean> lotListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		PolicyListDataBean policyListDataBean= new PolicyListDataBean();
		ResultSet rs=null;
		int indexpos=0;
		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_POLICY_NUMBER_DETAILS);
			cstmt.setInt(++indexpos,isCron);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			lotListObj = new ArrayList<PolicyLotDetailsBean>();
			
			int serialNo=0;
			while(rs.next()) {
				insertLotDetailsBeanObj = new PolicyLotDetailsBean();
				insertLotDetailsBeanObj.setSerialNo(++serialNo);
				insertLotDetailsBeanObj.setLotNumber(rs.getString("LOTID"));
				//insertLotDetailsBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				
				String moduleName="Individual";
		    	
		    	 insertLotDetailsBeanObj.setModuleName(moduleName);
		    	 insertLotDetailsBeanObj.setModuleId(4);
		    	 insertLotDetailsBeanObj.setStatusDesc(rs.getString("STATUSDESC"));
		    	 logger.info("                STATUSDESC----    "+rs.getString("STATUSDESC") +"       LOTSTATUSFLAG    "+rs.getInt("LOTSTATUSFLAG"));
		    	 insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
		    	 insertLotDetailsBeanObj.setUserName(rs.getString("USERNAME"));
		    	 insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
		    	 insertLotDetailsBeanObj.setXml(rs.getString("LOTXML"));
		    	 lotListObj.add(insertLotDetailsBeanObj);
		    	 policyListDataBean.setLotListObj(lotListObj);
			}//while
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("ERROR IN getEndorDDLDetails:"+ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getPolicyLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return policyListDataBean;
	}

	@Override
	public int checkUploadedPolicyCount(String xml, int moduleId) throws DAOException {		
				
				Connection con = null;
				CallableStatement cstmt = null;
				PolicyListDataBean policyDataBean= new PolicyListDataBean();
				ResultSet rs=null;
				int indexpos=0;
				int result=0;
				String statusMessage="";
				try
				{
					//Retrieving the policies from CORP GoddHealth Module
					con =getMyConnection(moduleId);
					cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_POLICY_COUNT_DETAILS);
					cstmt.setString(++indexpos,xml);
					cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
					cstmt.execute();
					result = (Integer) cstmt.getInt(indexpos);
				}
				catch(Exception ex)
				{
					ex.printStackTrace();
					logger.error("ERROR IN getPolicyLotDetails:"+ex);
					throw new DAOException();
				}
				finally
				{
					try {
						if(con!=null){
							con.close();
						}
						if(cstmt!=null){
							cstmt.close();
						}
						if(rs!=null){
							rs.close();
						}
					} catch (SQLException ex) {
						ex.printStackTrace();
						logger.error("ERROR IN checkUploadedPolicyCount:"+ex);
						throw new DAOException();
					}
				}
				return result;
			}

	@Override
	public PolicyListDataBean getPolicyNumberLotDetails(int lotno, int moduleId) throws DAOException {
		
		GetPolicyNubmerDetails policyNumberDetailsBeanObj = null;
		GetPolicyNubmerStatus policyNumberStatusBeanObj = null;
		List<GetPolicyNubmerDetails> policyNumberDetailsListObj = null;
		List<GetPolicyNubmerStatus> policyNumberStatusListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		PolicyListDataBean policyListDataBean= new PolicyListDataBean();
		ResultSet rsExcep=null;
		ResultSet rsList=null;
		int indexpos=0;
		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_LOT_POLICYDOC_XML_TEMPLATE);
			cstmt.setInt(++indexpos,lotno);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rsExcep = (ResultSet) cstmt.getObject(indexpos);
			rsList = (ResultSet) cstmt.getObject(indexpos-1);
			policyNumberDetailsListObj = new ArrayList<GetPolicyNubmerDetails>();
			policyNumberStatusListObj = new ArrayList<GetPolicyNubmerStatus>();
			
			int serialNo=0;
			int flag=0;
			while(rsList.next()) {
				policyNumberDetailsBeanObj = new GetPolicyNubmerDetails();
		    	 
				policyNumberDetailsBeanObj.setPolicyNumber(rsList.getString("POLICY_NO"));
				policyNumberDetailsBeanObj.setInsuredName(rsList.getString("INSUREDNAME"));
				policyNumberDetailsBeanObj.setCardId(rsList.getString("GHPLID"));
				policyNumberDetailsBeanObj.setInceptionDate(rsList.getString("INSUREDINCEPTIONDT"));
				policyNumberDetailsBeanObj.setRenewalStatus(rsList.getString("RENEWALSTATUS"));
				policyNumberDetailsBeanObj.setCustomerId(rsList.getString("CUSTOMER_ID"));
				policyNumberDetailsListObj.add(policyNumberDetailsBeanObj);
			}//while
			policyListDataBean.setPolicyNumberDetailsObj(policyNumberDetailsListObj);
			while(rsExcep.next()) {
				policyNumberStatusBeanObj = new GetPolicyNubmerStatus();
				policyNumberStatusBeanObj.setPolicyNo(rsExcep.getString("POLICYNO"));
				policyNumberStatusListObj.add(policyNumberStatusBeanObj);
				flag++;
			}//while
			policyListDataBean.setPolicyNumberStatusObj(policyNumberStatusListObj); 
			policyListDataBean.setFlag(flag);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
			logger.error("ERROR IN getEndorDDLDetails:"+ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rsExcep!=null){
					rsExcep.close();
				}
				if(rsList!=null){
					rsList.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getPolicyLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return policyListDataBean;
	}

	@Override
	public int updatePolicyNumbersUploadLotUpdate(int lotno, int status,int moduleId) throws DAOException {
		logger.info("lotNumber putLotDetails --->  "+lotno);
		logger.info("lotStatus putLotDetails ---->"+status);
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		int outputStatus=0;
		try
		{
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_PUT_LOT_DETAILS);
			cstmt.setInt(++indexpos,lotno);
			cstmt.setInt(++indexpos,status);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			con.commit();
			outputStatus = cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN putLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return outputStatus;
	}
		
}