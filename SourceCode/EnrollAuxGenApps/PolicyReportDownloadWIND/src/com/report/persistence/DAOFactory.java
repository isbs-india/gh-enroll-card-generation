package com.report.persistence;



/**
 * 
* <h1>Class Name: com.ghpl.persistence.DAOFactory.java </h1>
* Basic Purpose of the class is to support the basic servlet implementation � 
* <p>
* <b>Note:</b> Giving proper comments in your program makes it more
* user friendly and it is assumed as a high quality code.
*
* @author  Prabhakar G
* @version 1.0
* @since   Apr 30, 2015
* @exception class-name description
* {@linkplain com.ghpl.persistence.DAOFactory.java}
 */
public class DAOFactory {

	private static DAOFactory instance;

	static {
		instance = new DAOFactory();
	}

	private DAOFactory() {

	}

	public static DAOFactory getInstance() {
		return instance;
	}
	
	
	public PolicyReportDownloadDAO getUploadDAO(){
		return  new PolicyReportDownloadDAOImpl();
	}
	
	
}
