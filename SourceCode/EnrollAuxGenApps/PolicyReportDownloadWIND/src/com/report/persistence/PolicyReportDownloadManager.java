
package com.report.persistence;

import com.report.exception.DAOException;
import com.report.model.PolicyListDataBean;
public class PolicyReportDownloadManager {

	public static PolicyListDataBean getPolicyLotDetails(int isCron ,int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().getPolicyLotDetails(isCron ,moduleId);

		}catch (DAOException e) {

		}
		return null;
		
	}
	public static int checkUploadedPolicyCount(String xml ,int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().checkUploadedPolicyCount(xml ,moduleId);

		}catch (DAOException e) {

		}
		return 0;
		
	}
	public static PolicyListDataBean getPolicyNumberLotDetails(int lotno ,int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().getPolicyNumberLotDetails(lotno ,moduleId);

		}catch (DAOException e) {

		}
		return null;
		
	}
	public static int updatePolicyNumbersUploadLotUpdate(int lotno ,int status,int moduleId) throws DAOException {

		try {
			return DAOFactory.getInstance().getUploadDAO().updatePolicyNumbersUploadLotUpdate(lotno ,status ,moduleId);

		}catch (DAOException e) {

		}
		return 0;
	}
	}
