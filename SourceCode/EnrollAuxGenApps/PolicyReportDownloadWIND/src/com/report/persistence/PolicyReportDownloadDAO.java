package com.report.persistence;

import com.report.exception.DAOException;
import com.report.model.PolicyListDataBean;


public interface PolicyReportDownloadDAO {
	public PolicyListDataBean getPolicyLotDetails(int isCron,int moduleId) throws DAOException;
	public int checkUploadedPolicyCount(String xml ,int moduleId) throws DAOException;
	public PolicyListDataBean getPolicyNumberLotDetails(int lotno ,int moduleId) throws DAOException;
	public int updatePolicyNumbersUploadLotUpdate(int lotno ,int status,int moduleId) throws DAOException;
	
}
