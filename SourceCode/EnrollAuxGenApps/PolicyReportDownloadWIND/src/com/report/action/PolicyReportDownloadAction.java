package com.report.action;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.report.model.PolicyListDataBean;
import com.report.persistence.PolicyReportDownloadManager;
import com.report.util.MyProperties;

public class PolicyReportDownloadAction {
	static Logger logger = Logger.getLogger("PolicyReportDownloadAction");

	public static void main(String[] args) {
		 MyProperties myResources = new MyProperties();
		 String uploadspolicylocal = myResources.getMyProperties("policyexcel_file_path");
	     String downloadspolicylocal = myResources.getMyProperties("policyexcel_file_path_zip");
		   logger = myResources.getLogger();
		     String LCKPATH = myResources.getMyProperties("policyNumbersValidLckWIND");
		     int status=0;
		     int statusCode=0;
		     int statuscode=0;
			File ff = new File(LCKPATH);
			if (ff.exists()) {
			//	System.out.println("#######   Please delete Leak file  ######################");
				logger.error("#############  Please delete Leak file is RETAIL ######################");
				return;
			} else {
				// step 1
				System.out.println("#######   enter else block  ######################");
				logger.info("#############  start POLICY NUMBERS UPLOAD RETAIL  ######################");
				File file = new File(LCKPATH);
				String srcFolder = "";
				
				int lotno=0;
				srcFolder=uploadspolicylocal+lotno+"_WIND";
				//System.out.println("file    "+file);
				try{
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				Date date = new Date();
				bw.write("" + date);
				bw.close();
				// setp1 end
				// setp2 start
					//String EXCELDOCS = myResources.getMyProperties("policyexcel_file_path");
				//	System.out.println("enter try block");
			
					int policyCount = 0;
					int isCron = 0;
					String FinalMessage="";
					PolicyListDataBean policyListDataBeanBean =null;
					policyListDataBeanBean=PolicyReportDownloadManager.getPolicyLotDetails(isCron, 4);
					System.out.println("policyListDataBeanBean.getLotListObj() : "+policyListDataBeanBean.getLotListObj());
				    lotno=Integer.parseInt(policyListDataBeanBean.getLotListObj().get(0).getLotNumber());				   				    
				   
					if(lotno!=0){
           			    srcFolder=uploadspolicylocal+lotno+"_WIND";
				    
				    //need to add functionality for check policy number exist (or) not
				  //PROC_GET_POLICY_CNT
					//policyCount =PolicyReportDownloadManager.checkUploadedPolicyCount(xml, 4);
				    policyListDataBeanBean=null;
				    
				    policyListDataBeanBean=PolicyReportDownloadManager.getPolicyNumberLotDetails(lotno, 4);
				    String policyNo = "";
					if(policyListDataBeanBean!=null){
				if(policyListDataBeanBean.getFlag() != 0){
					int m=1;
					String count="";
					Map<String, Object[]> data =null;
					data = new TreeMap<String, Object[]>();
					data.put("1", new Object[] { "Policy number","Status"});
					if(policyListDataBeanBean!=null){
					for (int j = 0; j < policyListDataBeanBean.getPolicyNumberDetailsObj().size(); j++) {
						policyNo = policyListDataBeanBean.getPolicyNumberDetailsObj().get(j).getPolicyNumber();
						
						m++;
						count = m+"";
									
									data.put(count,new Object[] { policyNo,"valid" });
					}
					}
					for (int j = 0; j < policyListDataBeanBean.getPolicyNumberStatusObj().size(); j++) {
						policyNo = policyListDataBeanBean.getPolicyNumberStatusObj().get(j).getPolicyNo();
						
						m++;
						count = m+"";
									
									data.put(count,new Object[] { policyNo,"not valid" });
					}
					// String srcFolder = EXCELDOCS;
					statuscode=  createExcelfie(data,lotno,srcFolder);
					 if(statuscode==1){
						 logger.info("excel file created");
					 }else{
						 logger.info("excel file not created");
					 }
					 

					  //write zip file from command fromt using unix or linux command
		 				String destZipFile= "";
		 					 destZipFile= downloadspolicylocal+lotno+"_WIND.zip";
		 					 logger.info("Destination zip file for Policy numbers report download WIND : "+destZipFile);
			        	
		 				 File file2 = new File(srcFolder);
							if (file2.exists() && file2.isDirectory()) {
						  
						 String command ="zip -rj " + destZipFile +  " " + srcFolder;
						  //System.out.println("command : "+command);
						  logger.info(command);
						  try {
						      Process proc = Runtime.getRuntime().exec(command);
						      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						      String line = null;
					            while ((line = in.readLine()) != null) {
					                //System.out.println(line);
					            }
					             status =5;
						      //System.out.println("status : "+status);
						      logger.info("status :"+status);
						   } catch (Exception e) {
							   status=4;
							   logger.error("status :"+status);
						      e.printStackTrace();
						      logger.error(e);
						   }
			        	
			        	logger.info("before calling put lot details  per Policy numbers report download lotnumber : "+lotno);
		 				logger.info("status   "+5);
			        	 statusCode = PolicyReportDownloadManager.updatePolicyNumbersUploadLotUpdate(lotno, status,4);
			        	 logger.info("statusCode   : "+statusCode);
				}
				}else{
				
					logger.info("getPolicyNumberDetailsObj   list size from DB  "+policyListDataBeanBean.getPolicyNumberDetailsObj().size());
					 if(policyListDataBeanBean.getPolicyNumberDetailsObj().size()>0){ 
						
	    			String policyNumber = "";
	    			String inceptionDate = "";
	    			String cardId = "";
	    			String insuredName = "";
	    			String renewalStatus = "";
	    			String customerId = "";
	    			int lotId = 0;
	    			int flag = 0;
					int n = 1;
					int m = 1;
					String count = "";
				     logger.info("before Loop  List size  : "+policyListDataBeanBean.getPolicyNumberDetailsObj().size());
				     if(policyListDataBeanBean.getPolicyNumberDetailsObj()!=null && policyListDataBeanBean.getPolicyNumberDetailsObj().size()>0){ 
				    	
							Map<String, Object[]> data =null;
							data = new TreeMap<String, Object[]>();
							data.put("1", new Object[] { "Policy number","Name of the Insured","GHPL ID","Member Inception Date","Renewal status","Customer ID","Others"});
					
					for (int j = 0; j < policyListDataBeanBean.getPolicyNumberDetailsObj().size(); j++) {
						policyNumber = policyListDataBeanBean.getPolicyNumberDetailsObj().get(j).getPolicyNumber();
						inceptionDate = policyListDataBeanBean.getPolicyNumberDetailsObj().get(j).getInceptionDate();
						cardId = policyListDataBeanBean.getPolicyNumberDetailsObj().get(j).getCardId();
						insuredName = policyListDataBeanBean.getPolicyNumberDetailsObj().get(j).getInsuredName();
						renewalStatus = policyListDataBeanBean.getPolicyNumberDetailsObj().get(j).getRenewalStatus();
						customerId = policyListDataBeanBean.getPolicyNumberDetailsObj().get(j).getCustomerId();
						
						m++;
						count = m+"";
									
									data.put(count,new Object[] { policyNumber,insuredName,cardId,inceptionDate,renewalStatus,customerId,"" });
									
									logger.info("flag : "+flag);
							}
					statuscode=  createExcelfie(data,lotno,srcFolder);
					 if(statuscode==1){
						 logger.info("excel file created");
					 }else{
						 logger.info("excel file not created");
					 }
					  
					 
					 //write zip file from command from using unix or linux command
		 				String destZipFile= "";
		 					 destZipFile= downloadspolicylocal+lotno+"_WIND.zip";
		 					 logger.info("Destination zip file for Policy Number Report download WIND : "+destZipFile);
		        		 File zipfile = new File(destZipFile);
		        		 if(zipfile.exists()){
		        			 zipfile.delete();
		 					 destZipFile= downloadspolicylocal+lotno+"_WIND.zip";
		 					 logger.info("Destination zip file for Policy Number Report download WIND : "+destZipFile);
		        			// destZipFile= downloadslocal+lotNumber+"_AB.zip";
		        			 zipfile = new File(destZipFile);
		        			 logger.info("Destination zip file for ECard : "+zipfile);
		        		 }
		        		 File file1 = new File(srcFolder);
						if (file1.exists() && file1.isDirectory()) {
					  //System.out.println("destZipFile  "+destZipFile);
					  //System.out.println("srcFolder  "+srcFolder);
					  String command ="zip -rj " + destZipFile +  " " + srcFolder;
					  //System.out.println("command : "+command);
					  logger.info(command);
					  try {
					      Process proc = Runtime.getRuntime().exec(command);
					      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
					      String line = null;
				            while ((line = in.readLine()) != null) {
				                //System.out.println(line);
				            }
				             status =3;
					      //System.out.println("status : "+status);
					      logger.info("status :"+status);
					   } catch (Exception e) {
						   status=4;
						   logger.error("status :"+status);
					      e.printStackTrace();
					      logger.error(e);
					   }
					  logger.info("before calling put lot details  per Policy numbers report download lotnumber : "+lotno);
		 				logger.info("status   "+5);
			        	 statusCode = PolicyReportDownloadManager.updatePolicyNumbersUploadLotUpdate(lotno, status,4);
			        	 logger.info("statusCode   : "+statusCode);
					 if (ff.exists()) {
							file.delete();
					}
				     }
					 }else{
						 if (ff.exists()) {
								file.delete();
						}
					 }
				}else{
					if (ff.exists()) {
						file.delete();
				}
				}
				}
				}else{
					if (ff.exists()) {
						file.delete();
				}
				}
				}
				else{
					if (ff.exists()) {
						file.delete();
				}
				}
				}catch(Exception e){
					
					try{
						 Document document = new Document(PageSize.A4);
				    	 Rectangle pagesize = new Rectangle(216f, 720f);
				    	 File theDir = new File(srcFolder);
						  if (theDir.exists()) {
							  theDir.delete();
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }else{
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }
						 String srcFolder1 = theDir+File.separator;
				    		 PdfWriter.getInstance(document, new FileOutputStream(srcFolder1+File.separator+"Exception.pdf"));
				         // step 3
				         document.open();
				         // step 4
				         document.add(new Paragraph(myResources.getMyProperties("PolicyNumberReportException")));
				         // step 5
				         document.close();
				         //write zip file from command fromt using unix or linux command
			 				String destZipFile= "";
			 					 destZipFile= downloadspolicylocal+lotno+"_WIND.zip";
			 					 logger.info("Destination zip file for Policy numbers report download WIND : "+destZipFile);
				        	
			 				 File file2 = new File(srcFolder);
								if (file2.exists() && file2.isDirectory()) {
							  
							 String command ="zip -rj " + destZipFile +  " " + srcFolder;
							  //System.out.println("command : "+command);
							  logger.info(command);
							  try {
							      Process proc = Runtime.getRuntime().exec(command);
							      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
							      String line = null;
						            while ((line = in.readLine()) != null) {
						                //System.out.println(line);
						            }
						             status =5;
							      //System.out.println("status : "+status);
							      logger.info("status :"+status);
							   } catch (Exception e1) {
								   status=4;
								   logger.error("status :"+status);
							      e1.printStackTrace();
							      logger.error(e1);
							   }
				        	
				        	logger.info("before calling put lot details  per Policy numbers report download lotnumber : "+lotno);
			 				logger.info("status   "+5);
				        	 statusCode = PolicyReportDownloadManager.updatePolicyNumbersUploadLotUpdate(lotno, status,4);
				        	 logger.info("statusCode   : "+statusCode);
						}
					}
				    catch(Exception ex){
				    	if (ff.exists()) {
							file.delete();
					      }
				    	ex.printStackTrace();
				    	}
					if (ff.exists()) {
						file.delete();
				}
				
				}finally{
					if (ff.exists()) {
						file.delete();
				}
			}
			}
			
	}
				     
	
	  private static List printFileDetails(FTPFile[] files) {
	      List<String> list = new ArrayList<String>();
	      for (FTPFile file : files) {
	          String details = file.getName();
	         // System.out.println(details);
	          list.add(details);
	      }
	      return list;
	  }
	
	static boolean checkFileExists(String filePath, FTPClient ftpClient) throws IOException {
        InputStream inputStream = ftpClient.retrieveFileStream(filePath);
       int returnCode = ftpClient.getReplyCode();
        if (inputStream == null || returnCode == 550) {
            return false;
        }
        return true;
    }
	
	public static int createExcelfie(Map<String, Object[]> data, int lotId,String srcFolder){
		logger.info("#############  Excel file creation start  ######################");
		 MyProperties myResources = new MyProperties();
		 int statuscode=0;
		//String EXCELDOCS = myResources.getMyProperties("policyexcel_file_path");
		 //File EXCELDOCS1 = new File(srcFolder);
		 File theDir = new File(srcFolder);
		  if (theDir.exists()) {
			  theDir.delete();
			  theDir = new File(srcFolder);
			  theDir.mkdir();
		  }else{
			  theDir = new File(srcFolder);
			  theDir.mkdir();
		  }
		 String EXCELDOCS = theDir+File.separator;
		
		 // make sure it's a directory
		XSSFWorkbook workbook = new XSSFWorkbook();
		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("POLICY REPORT DWNLOAD LOT");
	
		sheet.setColumnWidth(0, 7000);
		sheet.setColumnWidth(1, 7000);
		sheet.setColumnWidth(2, 5000);
		sheet.setColumnWidth(3, 5000);
		sheet.setColumnWidth(4, 4000);
		sheet.setColumnWidth(5, 4000);
		sheet.setColumnWidth(6, 4000);
		sheet.setVerticallyCenter(true);
		sheet.setTabColor(4);
		sheet.setDisplayRowColHeadings(true);
		// This data needs to be written (Object[])
		Set<String> keyset = data.keySet();
		XSSFCellStyle my_style = workbook.createCellStyle();
		 
         /* Create HSSFFont object from the workbook */
         XSSFFont my_font=workbook.createFont();
         /* set the weight of the font */
         my_font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
         /* attach the font to the style created earlier */
         my_style.setFont(my_font);
		int rownum = 0;
		int k = 1;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if(rownum==1){
					cell.setCellStyle(my_style);
				}
				if (obj instanceof String) {
					cell.setCellValue((String) obj);
				} else if (obj instanceof Integer) {
					cell.setCellValue(k++);
				}
			}
		}
		// Write the workbook in file system
		FileOutputStream out;
		try {
			out = new FileOutputStream( new File(EXCELDOCS+ lotId + ".xlsx"));
			workbook.write(out);
			out.close();
			statuscode=1;
			logger.info("#############  Excel file creation end  ######################");
			logger.info("Excel file  : "+EXCELDOCS+ lotId + ".xlsx");
			System.out.println("Excel file  : "+EXCELDOCS+ lotId + ".xlsx");
		} catch (Exception e) {
			statuscode=2;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return statuscode;
	}
		public static void deleteFile(File element) {
			if (element.isDirectory()) {
				for (File sub : element.listFiles()) {
					deleteFile(sub);
				}
			}
			element.delete();
		}
}
