package com.ghpl.model;

public class EnrollsBean {
	private String AGE;
	private String CARDID;
	private String CURRENTPOLICYID;
	private String DOB;
	private String DOJ;
	private String DOL;
	private String EMPID;
	private String FAMILYID;
	private String GENDER;
	private String GRADE;
	private String INSUREDNAME;
	private String INSURERCARDID;
    private String IPID;
    private String MODULEID;
    private String PLANTNAME;
    private String POLICYFROM;
    private String POLICYTO;
    private String POLICYHOLDERNAME;
    private String POLICYNO;
    private String RELATIONCODE;
    private String RELATIONSHIP;
	private String VALIDFROM;
	private String UWID;
	private String inceptionDate;
	private String bano;
	private String noofLeaves;
	private String policyStatus;
	private String branchCode;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String pincode;
	private String planName;
	private String rno;
	private String mobileno;
	private String topupsumInsured;
	private String sumInsured;
	private String serviceTax;
	private String topupServiceTax;
	private String premium;
	private String topupPremium;
	private String topupPolicyno;
	private String POLICYCARDNAME;
	private String insuredMail;
	private String bankAccoutNumber;
	private String BLOODGROUP;
	private String ADDRESS;
	private int serialNo;
	 
	
	
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getBLOODGROUP() {
		return BLOODGROUP;
	}
	public void setBLOODGROUP(String bLOODGROUP) {
		BLOODGROUP = bLOODGROUP;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getBankAccoutNumber() {
		return bankAccoutNumber;
	}
	public void setBankAccoutNumber(String bankAccoutNumber) {
		this.bankAccoutNumber = bankAccoutNumber;
	}
	public String getInsuredMail() {
		return insuredMail;
	}
	public void setInsuredMail(String insuredMail) {
		this.insuredMail = insuredMail;
	}
	public String getPOLICYCARDNAME() {
		return POLICYCARDNAME;
	}
	public void setPOLICYCARDNAME(String pOLICYCARDNAME) {
		POLICYCARDNAME = pOLICYCARDNAME;
	}
	public String getTopupPolicyno() {
		return topupPolicyno;
	}
	public void setTopupPolicyno(String topupPolicyno) {
		this.topupPolicyno = topupPolicyno;
	}
	public String getPremium() {
		return premium;
	}
	public void setPremium(String premium) {
		this.premium = premium;
	}
	public String getTopupPremium() {
		return topupPremium;
	}
	public void setTopupPremium(String topupPremium) {
		this.topupPremium = topupPremium;
	}
	public String getTopupServiceTax() {
		return topupServiceTax;
	}
	public void setTopupServiceTax(String topupServiceTax) {
		this.topupServiceTax = topupServiceTax;
	}
	public String getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}
	public String getTopupsumInsured() {
		return topupsumInsured;
	}
	public void setTopupsumInsured(String topupsumInsured) {
		this.topupsumInsured = topupsumInsured;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getRno() {
		return rno;
	}
	public void setRno(String rno) {
		this.rno = rno;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getNoofLeaves() {
		return noofLeaves;
	}
	public void setNoofLeaves(String noofLeaves) {
		this.noofLeaves = noofLeaves;
	}
	public String getPolicyStatus() {
		return policyStatus;
	}
	public void setPolicyStatus(String policyStatus) {
		this.policyStatus = policyStatus;
	}
	public String getBano() {
		return bano;
	}
	public void setBano(String bano) {
		this.bano = bano;
	}
	public String getInceptionDate() {
		return inceptionDate;
	}
	public void setInceptionDate(String inceptionDate) {
		this.inceptionDate = inceptionDate;
	}
	public String getAGE() {
		return AGE;
	}
	public void setAGE(String aGE) {
		AGE = aGE;
	}
	public String getCARDID() {
		return CARDID;
	}
	public void setCARDID(String cARDID) {
		CARDID = cARDID;
	}
	public String getCURRENTPOLICYID() {
		return CURRENTPOLICYID;
	}
	public void setCURRENTPOLICYID(String cURRENTPOLICYID) {
		CURRENTPOLICYID = cURRENTPOLICYID;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getDOJ() {
		return DOJ;
	}
	public void setDOJ(String dOJ) {
		DOJ = dOJ;
	}
	public String getDOL() {
		return DOL;
	}
	public void setDOL(String dOL) {
		DOL = dOL;
	}
	public String getEMPID() {
		return EMPID;
	}
	public void setEMPID(String eMPID) {
		EMPID = eMPID;
	}
	public String getFAMILYID() {
		return FAMILYID;
	}
	public void setFAMILYID(String fAMILYID) {
		FAMILYID = fAMILYID;
	}
	public String getGENDER() {
		return GENDER;
	}
	public void setGENDER(String gENDER) {
		GENDER = gENDER;
	}
	public String getGRADE() {
		return GRADE;
	}
	public void setGRADE(String gRADE) {
		GRADE = gRADE;
	}
	public String getINSUREDNAME() {
		return INSUREDNAME;
	}
	public void setINSUREDNAME(String iNSUREDNAME) {
		INSUREDNAME = iNSUREDNAME;
	}
	public String getINSURERCARDID() {
		return INSURERCARDID;
	}
	public void setINSURERCARDID(String iNSURERCARDID) {
		INSURERCARDID = iNSURERCARDID;
	}
	public String getIPID() {
		return IPID;
	}
	public void setIPID(String iPID) {
		IPID = iPID;
	}
	public String getMODULEID() {
		return MODULEID;
	}
	public void setMODULEID(String mODULEID) {
		MODULEID = mODULEID;
	}
	public String getPLANTNAME() {
		return PLANTNAME;
	}
	public void setPLANTNAME(String pLANTNAME) {
		PLANTNAME = pLANTNAME;
	}
	public String getPOLICYFROM() {
		return POLICYFROM;
	}
	public void setPOLICYFROM(String pOLICYFROM) {
		POLICYFROM = pOLICYFROM;
	}
	public String getPOLICYTO() {
		return POLICYTO;
	}
	public void setPOLICYTO(String pOLICYTO) {
		POLICYTO = pOLICYTO;
	}
	public String getPOLICYHOLDERNAME() {
		return POLICYHOLDERNAME;
	}
	public void setPOLICYHOLDERNAME(String pOLICYHOLDERNAME) {
		POLICYHOLDERNAME = pOLICYHOLDERNAME;
	}
	public String getPOLICYNO() {
		return POLICYNO;
	}
	public void setPOLICYNO(String pOLICYNO) {
		POLICYNO = pOLICYNO;
	}
	public String getRELATIONCODE() {
		return RELATIONCODE;
	}
	public void setRELATIONCODE(String rELATIONCODE) {
		RELATIONCODE = rELATIONCODE;
	}
	public String getRELATIONSHIP() {
		return RELATIONSHIP;
	}
	public void setRELATIONSHIP(String rELATIONSHIP) {
		RELATIONSHIP = rELATIONSHIP;
	}
	public String getVALIDFROM() {
		return VALIDFROM;
	}
	public void setVALIDFROM(String vALIDFROM) {
		VALIDFROM = vALIDFROM;
	}
	public String getUWID() {
		return UWID;
	}
	public void setUWID(String uWID) {
		UWID = uWID;
	}
	
	
		
	
	
	
	
}
