package com.ghpl.persistence;

import com.ghpl.exception.DAOException;
import com.ghpl.model.InsertLotDetailsBean;
import com.ghpl.model.LabelDataBean;

public interface EnrollCardsDAO {
	public LabelDataBean getEnrollLabelDetails(int lotNumber,int moduleId) throws DAOException;
	public InsertLotDetailsBean getLabelLotDetails(int isCron, int moduleId) throws DAOException;
	public  int putLabelLotDetails(int lotNumber, int LotStatus, int moduleId)throws DAOException;
}
