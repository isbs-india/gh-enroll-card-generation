package com.ghpl.persistence;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import oracle.jdbc.OracleTypes;

import com.ghpl.exception.DAOException;
import com.ghpl.model.InsertLotDetailsBean;
import com.ghpl.model.LabelDataBean;
import com.ghpl.util.DBConn;
import com.ghpl.util.ProcedureConstants;

public class EnrollCardsDAOImpl extends DBConn implements EnrollCardsDAO  {

	@Override
	public LabelDataBean getEnrollLabelDetails(int lotNumber,int moduleId) throws DAOException {
			Connection con = null;
			CallableStatement cstmt = null;
			ResultSet rsData=null;		
			int indexpos=0;
			LabelDataBean labelDataBean = new LabelDataBean();
			List<String[]> records =new LinkedList<String[]>();
			try {
					con =getMyConnection(moduleId);
					cstmt = con.prepareCall(ProcedureConstants.PROC_GET_LOT_LABLE_XML_TEMPLATE);
					cstmt.setInt(++indexpos,lotNumber);
					cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
					//cstmt.registerOutParameter(++indexpos, OracleTypes.VARCHAR);
					cstmt.execute();
					rsData = (ResultSet) cstmt.getObject(indexpos);
				
					List<String> columnNames = new ArrayList<String>();
					 ResultSetMetaData columns = rsData.getMetaData();
				        int i = 0;
				        while (i < columns.getColumnCount()) {
				          i++;
				          columnNames.add(columns.getColumnName(i).toUpperCase());
				        }
				        	int cols = rsData.getMetaData().getColumnCount();
				        	while(rsData.next()){
				        	    String[] arr = new String[cols];
				        	    for(int j=0; j<cols; j++){
				        	      arr[j] = rsData.getString(j+1);
				        	    }
				        	    records.add(arr);
				        	}
				        	labelDataBean.setDbheaderNamesList(columnNames);
				        	labelDataBean.setDbrecordsList(records);
			}catch (Exception ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}  finally {
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					
					if(rsData!=null){
						rsData.close();
					}
					} catch (SQLException ex) {
						ex.printStackTrace();
						logger.error(ex);
					throw new DAOException();
				}
			}
			return labelDataBean;
	}
	
	public int putLabelLotDetails(int lotNumber, int lotStatus, int moduleId)throws DAOException {
		logger.info("lotNumber putLotDetails --->  "+lotNumber);
		logger.info("lotStatus putLotDetails ---->"+lotStatus);
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		int outputStatus=0;
		try
		{
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_PUT_LABEL_LOT_DETAILS);
			cstmt.setInt(++indexpos,lotNumber);
			cstmt.setInt(++indexpos,lotStatus);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			con.commit();
			outputStatus = cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN putLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return outputStatus;
	}
	
	
	
	
	
	public InsertLotDetailsBean getLabelLotDetails(int isCron, int moduleId) throws DAOException
	{
		InsertLotDetailsBean insertLotDetailsBeanObj = null;
		List<InsertLotDetailsBean> lotListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;

		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_Label_LOT_DETAILS);
			cstmt.setInt(++indexpos,isCron);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			lotListObj = new ArrayList<InsertLotDetailsBean>();
			int serialNo=0;
			logger.info("PKG_ECARDS_IN_PDF.GET_LABELS_LOT_DETAILS ResultSet object is :       "+rs);
			if(rs.next()) {
				insertLotDetailsBeanObj = new InsertLotDetailsBean();
				insertLotDetailsBeanObj.setSerialNo(++serialNo);
				insertLotDetailsBeanObj.setLotNumber(rs.getInt("LOTID"));
				//insertLotDetailsBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				//insertLotDetailsBeanObj.setEndorsementNo(rs.getString("ENDORSEMENTNO"));
				/*String moduleName="";
				int modId=0;
		    	 if(rs.getInt("MODULEID")==1){
		    		 moduleName="GHPL";
		    		 modId=1;
		    	 }else if(rs.getInt("MODULEID")==2){
		    		 moduleName="AB";
		    		 modId=2;
		    	 }else if(rs.getInt("MODULEID")==3){
		    		 moduleName="VENIUS";
		    		 modId=3;
		    	 }else if(rs.getInt("MODULEID")==4){
		    		 moduleName="UNITED";
		    		 modId=4;
		    	 }else if(rs.getInt("MODULEID")==5){
		    		 moduleName="NEW INDIA";
		    		 modId=5;
		    	 }else if(rs.getInt("MODULEID")==6){
		    		 moduleName="ORIENTAL";
		    		 modId=6;
		    	 }else if(rs.getInt("MODULEID")==7){
		    		 moduleName="NATIONAL";
		    		 modId=7;
		    	 }else if(rs.getInt("MODULEID")==8){
		    		 moduleName="NAV";
		    		 modId=8;
		    	 }else if(rs.getInt("MODULEID")==9){
		    		 moduleName="XL-CORP";
		    		 modId=9;
		    	 }else if(rs.getInt("MODULEID")==10){
		    		 moduleName="XL-AB";
		    		 modId=10;
		    	 }else if(rs.getInt("MODULEID")==11){
		    		 moduleName="XL-VENUS";
		    		 modId=11;
		    	 }else if(rs.getInt("MODULEID")==12){
		    		 moduleName="XL-UNITED";
		    		 modId=12;
		    	 }else if(rs.getInt("MODULEID")==13){
		    		 moduleName="XL-NEW INDIA";
		    		 modId=13;
		    	 }else if(rs.getInt("MODULEID")==14){
		    		 moduleName="XL-ORIENTAL";
		    		 modId=14;
		    	 }else if(rs.getInt("MODULEID")==15){
		    		 moduleName="XL-NATIONAL";
		    		 modId=15;
		    	 }else if(rs.getInt("MODULEID")==16){
		    		 moduleName="XL-NAV";
		    		 modId=16;
		    	 }
		    	 insertLotDetailsBeanObj.setModuleName(moduleName);
		    	 insertLotDetailsBeanObj.setModuleId(modId);*/
		    	 insertLotDetailsBeanObj.setEmpName(rs.getString("USERNAME"));
		    	 insertLotDetailsBeanObj.setPolicyId(rs.getInt("POLICYID"));
		    	 //insertLotDetailsBeanObj.setEndorsementId(rs.getInt("ENDORSEMENTID"));
		    	 insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
		    	 //insertLotDetailsBeanObj.setStatusDesc(rs.getString("STATUSDESC"));
		    	 insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
		    	// insertLotDetailsBeanObj.setUserName(rs.getString("USERNAME"));
		    	// insertLotDetailsBeanObj.setContactEmailId(rs.getString("CONTACTEMAIL"));
		    	// insertLotDetailsBeanObj.setCardFormat(rs.getString("CARD_FORMAT"));
		    	 //lotListObj.add(insertLotDetailsBeanObj);

			}//while


		}
		catch(Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			insertLotDetailsBeanObj= null;
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				
				if(rs!=null){
					rs.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return insertLotDetailsBeanObj;
	}

	
	


	/*@Override
	public int putLotDetails(int lotNumber, int lotStatus, int moduleId)throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		int outputStatus=0;
		try
		{
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_PUT_LOT_DETAILS);
			cstmt.setInt(++indexpos,lotNumber);
			cstmt.setInt(++indexpos,lotStatus);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			con.commit();
			outputStatus = cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN putLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return outputStatus;
	}*/
	
	

}
