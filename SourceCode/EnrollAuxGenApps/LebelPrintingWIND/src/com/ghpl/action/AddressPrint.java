package com.ghpl.action;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import org.apache.log4j.Logger;

import com.ghpl.model.InsertLotDetailsBean;
import com.ghpl.model.LabelDataBean;
import com.ghpl.persistence.EnrollCardsManager;
import com.ghpl.util.MyProperties;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public class AddressPrint {
	 protected static Logger logger;
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		 MyProperties myResources = new MyProperties();
	     logger = myResources.getLogger();
	   
	     String uploadslocal = myResources.getMyProperties("uploadsLabel");
	     String downloadslocal = myResources.getMyProperties("downloadsLabel");
	    
	     String srcFolder= "";
	   
	     logger.info("=============JOB Start FOR  label WIND Cards==============");
		 try { 
			
			 	System.out.println("=============JOB Start FOR  Label WIND Cards==============");
			 	
			 	InsertLotDetailsBean insertLotDetailsBean =null;
			 	insertLotDetailsBean = EnrollCardsManager.getLabelLotDetails(1, 4);
			 
			 	logger.info("insertLotDetailsBean    "+insertLotDetailsBean);
			 	int lotNumber=0;
			 	int status=0;
			 	String records="";
			 	int statusCode=0;
			 	if(insertLotDetailsBean!=null){
			 		 lotNumber =insertLotDetailsBean.getLotNumber();
			 		 System.out.println("lotNumber    "+lotNumber);
			 		String contactEmail  = insertLotDetailsBean.getContactEmailId();
				  	 String cardFormat = insertLotDetailsBean.getCardFormat();
				  	 String userName = insertLotDetailsBean.getUserName();
				 	 LabelDataBean cardDataBean= new LabelDataBean();
				 	logger.info("lotNumber   "+lotNumber);
				 	logger.info("cardFormat   "+cardFormat);
			 		 cardDataBean = EnrollCardsManager.getEnrollLabelDetails(lotNumber,4);
			 		
			 		srcFolder=uploadslocal+File.separator+"WIND"+File.separator+lotNumber;
			 		
			 		logger.info("Source folder  : "+srcFolder);
			 		logger.info("lotNumber : "+lotNumber +"    DB records size :"+cardDataBean.getDbrecordsList().size());
			 		logger.info("records list : "+cardDataBean.getDbrecordsList());
			 		System.out.println("lotNumber : "+lotNumber +"    DB records size :"+cardDataBean.getDbrecordsList().size());
			 		if((cardDataBean.getDbrecordsList()!=null) && (cardDataBean.getDbrecordsList().size()>0)){
			 			
			 				try{
			 				File theDir = new File(srcFolder);
			 				 if (theDir.exists()) {
								  theDir.delete();
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }else{
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }
			 				 
					 					logger.info("pdf generation for WIND Labels : ");
					 					LabelEmbed.generateLabelFormat(cardDataBean, srcFolder+File.separator+"WIND_Lot_Labels");
					 		
			 				String destZipFile= "";
			 				
			 					 destZipFile= downloadslocal+lotNumber+"_WIND.zip";
			 					 logger.info("Destination zip file for Label WIND1 : "+destZipFile);
			 				System.out.println("destZipFile   "+destZipFile);
			        		 File zipfile = new File(destZipFile);
			        		 if(zipfile.exists()){
			        			 zipfile.delete();
			        			
					 					 destZipFile= downloadslocal+lotNumber+"_WIND.zip";
					 					logger.info("Destination zip file for Label WIND2 : "+destZipFile);
					 				
			        			 zipfile = new File(destZipFile);
			        			 logger.info("Destination zip file for PCard : "+zipfile);
			        		 }
			        		 File file = new File(srcFolder);
							if (file.exists() && file.isDirectory()) {
						  
						 String command ="zip -rj " + destZipFile +  " " + srcFolder;
						  System.out.println("command : "+command);
						  logger.info(command);
						  try {
						      Process proc = Runtime.getRuntime().exec(command);
						      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						      String line = null;
					            while ((line = in.readLine()) != null) {
					                //System.out.println(line);
					            }
					             status =3;
						      System.out.println("status : "+status);
						      logger.info("status :"+status);
						   } catch (Exception e) {
							   status=4;
							   logger.error("status :"+status);
						      e.printStackTrace();
						      logger.error(e);
						   }
							}else{
								status=4;
								   records=" no records and file creation failed ";
								   logger.info("status :"+status);
							}
			 				}catch (Exception e) {
								   status=4;
								   logger.error("status :"+status);
							      e.printStackTrace();
							      logger.error(e);
							   }
			 				logger.info("before calling put lot details  per Labels lotnumber : "+lotNumber);
			 				logger.info("status   "+status);
			 				//logger.info("module Id  : "+i);;
	     				 statusCode = EnrollCardsManager.putLabelLotDetails(lotNumber, status,4);
	     				 logger.info("lotNumber---  : "+lotNumber +"       statusCode  ------ :   "+statusCode);	
			        }else{
			        	File theDir = new File(srcFolder);
		 				 if (theDir.exists()) {
							  theDir.delete();
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }else{
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }
			        	
			        	try{
							 Document document = new Document(PageSize.A4);
					    	 Rectangle pagesize = new Rectangle(216f, 720f);
					         // step 2
					    
					    		 PdfWriter.getInstance(document, new FileOutputStream(srcFolder+File.separator+"WINDLabels_"+"Exception.pdf"));
					    	
					         document.open();
					         // step 4
					         document.add(new Paragraph(myResources.getMyProperties("NODataFound")));
					         // step 5
					         document.close();
							}catch(Exception ex){ex.printStackTrace();}
			        	
			        	//write zip file from command fromt using unix or linux command
		 				 //String destZipFile= downloadslocal+lotNumber+"_AB.zip";
		 				String destZipFile= "";
		 				
		 					 destZipFile= downloadslocal+lotNumber+"_WIND.zip";
		 					 logger.info("Destination zip file for Labels WIND1 : "+destZipFile);
		 				
			        	
		 				 File file = new File(srcFolder);
							if (file.exists() && file.isDirectory()) {
						  
						 String command ="zip -rj " + destZipFile +  " " + srcFolder;
						  //System.out.println("command : "+command);
						  logger.info(command);
						  try {
						      Process proc = Runtime.getRuntime().exec(command);
						      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						      String line = null;
					            while ((line = in.readLine()) != null) {
					                //System.out.println(line);
					            }
					             status =5;
						      //System.out.println("status : "+status);
						      logger.info("status :"+status);
						   } catch (Exception e) {
							   status=4;
							   logger.error("status :"+status);
						      e.printStackTrace();
						      logger.error(e);
						   }
			        	
			        	logger.info("before calling put lot details  per Labels lotnumber : "+lotNumber);
		 				logger.info("status   "+5);
		 				logger.info("module Id  : "+4);;
			        	 statusCode = EnrollCardsManager.putLabelLotDetails(lotNumber, status,4);
			        	 logger.info("statusCode   : "+statusCode);
			        
			 		}
			 }
			 		System.out.println("statusCode   : "+statusCode);
			 	System.out.println("=============JOB END FOR  Labels WIND ==============");
			 	logger.info("=============JOB END FOR  Labels WIND ==============");
			 	}
			 	/*}*/
			 //	}
		      } catch (Exception e) {  
		        e.printStackTrace();  
		 }  
	}

}
