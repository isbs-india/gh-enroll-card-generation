/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.photo.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



/**
 *
 * @author Surya Kiran
 */
public class MyProperties {
    
    static Logger logger;
    public String getMyProperties(String PropName)
    {
        logger = this.getLogger();        
        Properties prop = new Properties();
        try {
          /* // System.out.println("path of myproperties :"+MyProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        	final File f = new File(MyProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        	System.out.println(f);
        	System.out.println(f.getParent());
        	System.out.println(f.getAbsolutePath().indexOf(1));
            String configFilePath = URLDecoder.decode(f.getParentFile().getAbsolutePath()+ File.separator +Constants.DBConfig, "UTF-8");
           // System.out.println("config file path :"+configFilePath);
            InputStream inputStream = new FileInputStream(configFilePath);
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file 'Config properties' not found in the classpath");
            }
               //logger.info("to Get "+PropName+" Properties");
                //prop.load(getClass().getClassLoader().getResourceAsStream("config.properties"));          
                return prop.getProperty(PropName);*/
        	 
        	FileInputStream fin = new FileInputStream(Constants.DBConfig);
             prop.load(fin);
             return prop.getProperty(PropName);
        	
    	} catch (IOException ex) {
    		logger.error("Error DBConn - getting "+PropName+" Properties Set IO Exception"); 
    		logger.error("IN " + ex.getMessage());
                return "";
        }
    }     
    
    
    public Logger getLogger() {
        Properties prop = new Properties();
        try {

           /* final File f = new File(MyProperties.class.getProtectionDomain().getCodeSource().getLocation().getPath());
            //String LogFilePath = URLDecoder.decode(f.getParentFile().getAbsolutePath() + File.separator +"log4j.properties", "UTF-8");      
            String LogFilePath = URLDecoder.decode(f.getParentFile().getAbsolutePath()+ File.separator +Constants.LOG4JConfig, "UTF-8");
            InputStream inputStream = new FileInputStream(LogFilePath);
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file 'LOG 4 J' not found in the classpath");
            }
            PropertyConfigurator.configure(LogFilePath);
            logger = Logger.getLogger(MyProperties.class.getName());*/

        	 FileInputStream fin = new FileInputStream(Constants.LOG4JConfig);
             prop.load(fin);
             PropertyConfigurator.configure(Constants.LOG4JConfig);
             logger = Logger.getLogger(MyProperties.class.getName());
        } catch (IOException O) {
            System.out.println("Log File Config Error " + O.getMessage());
        }
        return logger;

    }
}
