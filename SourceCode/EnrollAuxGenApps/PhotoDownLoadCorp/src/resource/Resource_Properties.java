package resource;



import java.util.Locale;
//import java.util.MissingResourceException;
import java.util.ResourceBundle;



public class Resource_Properties 
{
	
	/**
	 * The main method.
	 * 
	 * @param ar
	 *            the arguments
	 */
	/*public static void main(String ar[])
    {        
        
      MessageLog mlog=null;
      mlog=MessageLog.getInstance();
      Locale currentLocale = new Locale("EN","IN");
      mlog.println(IMessage.DEBUG,"currentLocale -- "+currentLocale );      
      
      try 
      {
      ResourceBundle myResources = ResourceBundle.getBundle("resource.tpa_en_IN",currentLocale);
      
      mlog.println(IMessage.DEBUG,myResources.getString("pm_success_message"));  

      }
      catch(MissingResourceException e) 
      {
      mlog.println(IMessage.DEBUG,e.getMessage());
      }
            
    }*/
    
    /**
	 * Gets the main properties bundle
	 * 
	 * @return the bundle
	 */
    public ResourceBundle getBundle() 
    {
      Locale currentLocale = new Locale("EN","IN");     
      ResourceBundle myResources = ResourceBundle.getBundle("resource.tpa_en_IN",currentLocale);      
      return myResources;
    }
    
    /**
	 * Gets the properties bundle of collection module
	 * 
	 * @return the bundle_collection
	 */
   /* public ResourceBundle getBundle_collection() 
    {
      Locale currentLocale = new Locale("EN","IN");     
      ResourceBundle myResources_collection = ResourceBundle.getBundle("resource.tpa_collection_IN",currentLocale);      
      return myResources_collection;
    }*/
    
    /**
	 * Gets the properties bundle of mail configuration
	 * 
	 * @return the bundle_ mail
	 */
    public ResourceBundle getBundle_Mail() 
    {
      Locale currentLocale = new Locale("EN","IN");     
      ResourceBundle myResources_collection = ResourceBundle.getBundle("resource.tpa_mail_IN",currentLocale);      
      return myResources_collection;
    }
    
    /*public ResourceBundle getBundle_cofig() 
    {
      Locale currentLocale = new Locale("EN","IN");     
      ResourceBundle myResources_collection = ResourceBundle.getBundle("resource.tpa_config",currentLocale);      
      return myResources_collection;
    }*/
}
