package com.photo.model;

public class CardIDPhotoBean {
	private int lotId;
	private int moduleId;
	private String lotCreated;
	private String cantactEmail;
	private String userName;
	private int poicyId;
	private int lotStatusFlag;
	private String policyName;
	private String xmlString;
	public int getLotId() {
		return lotId;
	}
	public void setLotId(int lotId) {
		this.lotId = lotId;
	}
	public int getModuleId() {
		return moduleId;
	}
	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}
	public String getLotCreated() {
		return lotCreated;
	}
	public void setLotCreated(String lotCreated) {
		this.lotCreated = lotCreated;
	}
	public String getCantactEmail() {
		return cantactEmail;
	}
	public void setCantactEmail(String cantactEmail) {
		this.cantactEmail = cantactEmail;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getPoicyId() {
		return poicyId;
	}
	public void setPoicyId(int poicyId) {
		this.poicyId = poicyId;
	}
	public int getLotStatusFlag() {
		return lotStatusFlag;
	}
	public void setLotStatusFlag(int lotStatusFlag) {
		this.lotStatusFlag = lotStatusFlag;
	}
	public String getPolicyName() {
		return policyName;
	}
	public void setPolicyName(String policyName) {
		this.policyName = policyName;
	}
	public String getXmlString() {
		return xmlString;
	}
	public void setXmlString(String xmlString) {
		this.xmlString = xmlString;
	}
	
}
