package com.photo.persistence;

import java.util.List;

import com.photo.exception.DAOException;
import com.photo.model.CardIDPhotoBean;

public class PhotoDownLoadDAOManager {
	public static List<CardIDPhotoBean> getPhotoCardLotList(int moduleId) throws DAOException {
		try {
			return DAOFactory.getInstance().getPhotoDownLoadDAO().getPhotoCardLotList(moduleId);

		}catch (DAOException e) {

		}
		return null;
	}
	
	public static int updatePhotoCardLotStatus(int lotNumber, int status,int moduleId) throws DAOException {
		try {
			return DAOFactory.getInstance().getPhotoDownLoadDAO().updatePhotoCardLotStatus(lotNumber, status,moduleId);

		}catch (DAOException e) {

		}
		return 0;
	}
	
	
}
