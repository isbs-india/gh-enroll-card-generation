package com.photo.action;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.photo.model.GetPhotoPendingLot;
import com.photo.persistence.UploadPhotosManager;
import com.photo.util.MyProperties;
import com.photo.util.XmlGenerationAsString;

public class PhotoUploadAction {
	static Logger logger = Logger.getLogger("PhotoUploadAction");
	 static int statuscode =0;
	 /**
	  * 
	  * @param args
	  * @throws IOException
	  * this method cron will execution stated
	  * photo upload per crop in EnrollrollAuxillary tasks 
	  * we are uploaded images of .zip file with card id's after that display view photo cards no link for download button only display # 
	  * this cron executed after download button display
	  * at the time of execution start one lock file generated after that fetching the data photo pending lot cards list from database
	  * lot number and cards list to xml string 
	  * contact email 
	  * xml String is not empty split all cards to array object that object will
	  * connect ftp server 
	  * array object in loop valid  jpg file or not create .xlsx file s.no cardid jpgfile ftp status database status of headers in this file
	  * at that time writing .xlsx file all are jpg files in this lot generate lot number .zip file created
	  * the cards add to list object after that update .xlsx file already exist in ftp server and database inserted or not
	  * first appropriate database inserted if status is 1 after that crop module is inserted and xlsx file is completed 
	  *  
	  * this process will be repeated end of the list object of photo pending cards object
	  * after that delete the lock file
	  */
	public static void main(String[] args) {
		 MyProperties myResources = new MyProperties();
		   logger = myResources.getLogger();
		     String LCKPATH = myResources.getMyProperties("photoValidUploadPhotosLckWIND");
			File ff = new File(LCKPATH);
			if (ff.exists()) {
				System.out.println("#######   Please delete Leak file upload photos wind ######################");
				logger.error("#############  Please delete Leak upload photos wind file  ######################");
				return;
			} else {
				// step 1
				System.out.println("#######   enter else block  ######################");
				logger.info("#############  start PHOTO UPLOAD WIND  ######################");
				File file = new File(LCKPATH);
				System.out.println("file    "+file);
				try{
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				Date date = new Date();
				bw.write("" + date);
				bw.close();
				// setp1 end
				// setp2 start
					String EXCELDOCS = myResources.getMyProperties("photoexcel_file_path");
					String FtpFolder = myResources.getMyProperties("photoftp_file_download_path");
				//	System.out.println("enter try block");
					List<GetPhotoPendingLot> getPhotoPendingLotList=null;
					try{
						getPhotoPendingLotList = UploadPhotosManager.getPhotoPendingLot(4,ff,file);
					}catch(Exception e){
					//System.out.println("getPhotoPendingLotList   list size from DB ====== :    "+getPhotoPendingLotList.size());
						if (ff.exists()) {
							file.delete();
						}
					}
					logger.info("getPhotoPendingLotList   list size from DB         "+getPhotoPendingLotList.size());
					 if(getPhotoPendingLotList.size()>0){   
					FTPClient client = new FTPClient();
					
					String photodestinationftp_host=null;
					String photodestinationftp_username = null;
					String photodestinationftp_password = null;
					
					String FTPHOST=null;
					String FTPUName=null;
					String FTPPwd=null;
					String ftpupload_ftppath=null;
					String PHOTOUPLOADS_TEMP= null;
					boolean result=false;
					String dbFileName = null;
					String ftpFileName = null;
					MyProperties myResources1 = new MyProperties();
					
					FTPHOST = myResources1.getMyProperties("photosourceftp_host");
	    			FTPUName = myResources1.getMyProperties("photosourceftp_username");
	    			FTPPwd = myResources1.getMyProperties("photosourceftp_password");
	    			ftpupload_ftppath = myResources.getMyProperties("photosourcePHOTOUPLOADS");
	    			
	    			photodestinationftp_host = myResources1.getMyProperties("photodestinationftp_host");
					photodestinationftp_username = myResources1.getMyProperties("photodestinationftp_username");
					photodestinationftp_password = myResources1.getMyProperties("photodestinationftp_password");
	    			PHOTOUPLOADS_TEMP = myResources.getMyProperties("photodestination_PHOTOUPLOADS");
	    			String ftpdownloadfiles = myResources.getMyProperties("photodestination_local");
	    			int policyId = 0;
	    			int lotId = 0;
	    			 try{
			                client.connect(FTPHOST);
			                int replyCode = client.getReplyCode();
			                if (!FTPReply.isPositiveCompletion(replyCode)) {
			                	if (ff.exists()) {
									file.delete();
								}
			                	logger.info("Operation failed. FTP Server reply code: " + replyCode);
			                    return;
			                }
				            result = client.login(FTPUName, FTPPwd);
				            logger.info("FTP client  status    : "+result);
				            if (!result) {
				            	if (ff.exists()) {
									file.delete();
								}
				            	 logger.info("Could not login to the  FTP server");
				            	 return;
				            }
	    			 }catch(Exception e){
	    				    if (ff.exists()) {
								file.delete();
							}
	    				 	logger.error("ftp connection failed  Please check it:   ping "+FTPHOST +" -t if not ping ip address contact ITADMIN" );
						 	return;
	 			     }
				            client.setFileType(FTP.BINARY_FILE_TYPE);
				            client.enterLocalPassiveMode();
				          
				            FTPFile[] foldersList = client.listFiles(ftpupload_ftppath);  
				            System.out.println("FTP folder length    : "+foldersList.length);
				            
				            if((foldersList.length>0)  &&  (getPhotoPendingLotList.size()>0)){
				            	  for (int i = 0; i < foldersList.length; i++) {
				            		  String dbaseFileName="";
				            		  String currentFileName = foldersList[i].getName();
				            		  if (foldersList[i].isDirectory()) {
					                    	 String localDirectory = FtpFolder + currentFileName;
					                         
				            			for (int j = 0; j < getPhotoPendingLotList.size(); j++) {
				            		dbaseFileName = getPhotoPendingLotList.get(j).getFileName();
				    				if(dbaseFileName.equals(currentFileName)){
				    					File newDir = new File(localDirectory);
				    					boolean created = newDir.mkdirs();
				                         if (created) {
				                             System.out.println("CREATED the directory: " + localDirectory);
				                         } else {
				                             System.out.println("COULD NOT create the directory: " + localDirectory);
				                         }
				    					
					            		if (currentFileName.equals(".") || currentFileName.equals("..")) {
					                        // skip parent directory and the directory itself
					                        continue;
					                    }
					            		
				            		  String remoteFile1 = ftpupload_ftppath+foldersList[i].getName();
				            			  File uploadDir = new File(remoteFile1);
				            			  if (!uploadDir.exists()){
												String str[] =remoteFile1.split("/");
												for(int k=0;k<str.length;k++){
													client.changeWorkingDirectory(str[k]);
													client.printWorkingDirectory();
												}
											}
				            			 
			            		            FTPFile[] list2 = client.listFiles(remoteFile1);
			            		            File theDir = new File(FtpFolder+File.separator+foldersList[i].getName());
			            		            theDir.mkdir();
			            		            String remoteFile2 = null;
			            		            File downloadFile2 = null;
			            		            OutputStream outputStream2 = null;
				            		        for (int l = 0; l < list2.length; l++) {
				            		        	 String subcurrentFileName = list2[l].getName();
				            		             remoteFile2 = ftpupload_ftppath+foldersList[i].getName()+"/"+list2[l].getName();
				            		             if(list2[l].getName().equals(".") || list2[l].getName().equals("..")){
				            		            	 continue;
				            		             }
				            		             if(list2[l].isDirectory()){
				            		            	 String subDirectory = localDirectory + subcurrentFileName;
							                    	 File newsubDir = new File(subDirectory);
							                         boolean subcreated = newsubDir.mkdirs();
							                         if (subcreated) {
							                        	 logger.info("CREATED the sub directory: " + localDirectory);
							                         } else {
							                        	 logger.info("NOT create the sub directory: " + localDirectory);
							                         }
				            		             }
				            		             downloadFile2 = new File(FtpFolder+foldersList[i].getName()+"/"+list2[l].getName());
				            		             outputStream2 = new BufferedOutputStream(new FileOutputStream(downloadFile2));
				            		             client.retrieveFile(remoteFile2, outputStream2);
				            		             logger.info("file retrieve of reply code :"+client.getReplyCode());
				            		             outputStream2.close();
				            		        }
					                    }
				    						}
				            			}
				            		  }
				            }
				            client.logout();
				            System.out.println("before Loop  List size  : "+getPhotoPendingLotList.size());
				     logger.info("before Loop  List size  : "+getPhotoPendingLotList.size());
				     if(getPhotoPendingLotList!=null && getPhotoPendingLotList.size()>0){ 
				     
					for (int j = 0; j < getPhotoPendingLotList.size(); j++) {
						dbFileName = getPhotoPendingLotList.get(j).getFileName();
						policyId = getPhotoPendingLotList.get(j).getPoicyId();
						lotId = getPhotoPendingLotList.get(j).getLotId();
						int moduleId = getPhotoPendingLotList.get(j).getModuleId();
						String fileName = null;
						File dir = new File(FtpFolder);    //ftp files download to local folder name
						File[] subFiles = dir.listFiles();
						logger.info("in side  Loop   "+subFiles);
						File dir1 = null;
							for (int i = 0; i < subFiles.length; i++) {
								if(subFiles[i].isDirectory()) {
								ftpFileName = subFiles[i].getName();
								fileName = subFiles[i].getName().toString();
								dir1 = new File(FtpFolder+File.separator+ fileName);
									if (!dir1.exists()) {
										dir1.mkdir();
									}
								}
								//System.out.println(fileName   +"                  "+dbFileName);
								if (fileName.equals(dbFileName)) {
									logger.info("dbFileName ------    "+dbFileName  +"   ftp fileName    ----         "+fileName);
									int flag = 0;
									int n = 1;
									String count = "";
									List<String> cardList = new ArrayList<String>();
									List<String> cardListxml = new ArrayList<String>();
									Map<String, Object[]> data =null;
										if (dir1.isDirectory()) { 
											 data = new TreeMap<String, Object[]>();
											int m = 1;
											for (final File f : dir1.listFiles()) {
												String fileNameWithoutExt = FilenameUtils.getBaseName(f.getName());
												cardListxml.add(fileNameWithoutExt);
												cardList.add(f.getName());
												data.put("1", new Object[] { "ID", "NAME","VALID JPG" ," Ftp Status","Database Status"});
												Image image =null;
												try{
													 image 	= ImageIO.read(f);
													//image = readImage(f);
												}catch(Exception e){
													//e.printStackTrace();
													//System.out.println(e.getMessage()  +"         image Name : "+f);
													logger.error(e.getMessage()  +"         image Name : "+f);
													image=null;
													   /* if (ff.exists()) {
															file.delete();
														}*/
												}
												
												String extension = "";
												String ext = "";
												String replaceimg="";
												if (image != null) {
													int k = f.getName().lastIndexOf('.');
													if (k > 0) {
														extension = f.getName().substring(k + 1);
														ext = extension.toLowerCase();
														replaceimg =f.getName().replaceAll(extension, ext);
														if ((ext != "") && (!ext.equals("jpg"))) {
															flag++;
															n = n + 1;
															count = "" + n;
															data.put(count,new Object[] { m++,replaceimg,"Image ext not jpg","NO","NO" });
														} else {
															n = n + 1;
															count = "" + n;
															data.put(count,new Object[] { m++,replaceimg,"YES","NO","NO" });
														}
													}
												} else {
													flag++;
													n = n + 1;
													count = "" + n;
													data.put(count,new Object[] { m++,f.getName(),"NOT a Valid Image","NO","NO" });
												}
											}
										}
									System.out.println("flag status "+flag);
									
									
									String xmlString="";
									int cardslistSize=0;
									if(cardListxml!=null){
									   xmlString = XmlGenerationAsString.sendFaxDataResonse(cardListxml);
									   cardslistSize = cardListxml.size();
									}
									System.out.println("xmlString   " + xmlString);
									//logger.info("xmlString   " + xmlString);
									logger.info("flag : "+flag);
									
									int ipidCount=0;
									if(flag==0){
										try{
											ipidCount = UploadPhotosManager.getIPIDCountfromCardId(4,xmlString,policyId,ff,file);
											logger.info("ipidCount   "+ipidCount);
											if(ipidCount==0){
												if (ff.exists()) {
													file.delete();
												}
											}
										}catch(Exception e){
											if(ipidCount==0){
											 if (ff.exists()) {
													file.delete();
												}
											}
										}
										logger.info("ipidCount    :    "+ipidCount +"            cardslistSize   :  "+cardslistSize);
										System.out.println("ipidCount    :    "+ipidCount +"            cardslistSize   :  "+cardslistSize);
										if (ipidCount != cardslistSize){
											 Set<String> keyset = data.keySet();
												for (String key : keyset) {
													if(Integer.parseInt(key)!=1){
														Object[] objArr = data.get(key);
														//if(objArr[1].equals(list2[l].getName())){
															objArr[3]="Data not valid so not uploaded to ftp";
															data.put(key,objArr);
														// }
													}
												}
												statuscode=  createExcelfie(data,lotId);
												 if(statuscode==1){
													 logger.info("excel file created");
												 }else{
													 logger.info("excel file not created");
												 }
												 try{
												 UploadPhotosManager.updatePhotoUploadLotUpdate(4,lotId,4,ff,file);  //4 is photo upload failed
												 }catch(Exception e)
												 {
													 if (ff.exists()) {
															file.delete();
													}
												 }
												 Set<String> keyset1 = data.keySet();
												   for(int k = 0;k<cardList.size();k++){
														for (String key : keyset1) {
															if(Integer.parseInt(key)!=1){
																Object[] objArr = data.get(key);
																//if(objArr[1].equals(cardList.get(k))){
																	objArr[4]="Database updation failed";
																	data.put(key,objArr);
																//}
															}
														}
												   }
												 statuscode=  createExcelfie(data,lotId);
												 if(statuscode==1){
													 logger.info("excel file created");
												 }else{
													 logger.info("excel file not created");
												 }
												 if (ff.exists()) {
														file.delete();
												}
										}
									} // flag==0
									else{
										try{
											UploadPhotosManager.updatePhotoUploadLotUpdate(4,lotId,4,ff,file);  //4 is photo upload failed
										}catch(Exception e){
											e.printStackTrace();
											if (ff.exists()) {
												file.delete();
											}
										}
									 Set<String> keyset = data.keySet();
									   for(int k = 0;k<cardList.size();k++){
											for (String key : keyset) {
												if(Integer.parseInt(key)!=1){
													Object[] objArr = data.get(key);
													//if(objArr[1].equals(cardList.get(k))){
														objArr[4]="Database updation failed";
														data.put(key,objArr);
													//}
												}
											}
									   }
									 statuscode=  createExcelfie(data,lotId);
									 if(statuscode==1){
										 logger.info("excel file created");
									 }else{
										 logger.info("excel file not created");
									 }
								}
									
									logger.info(ipidCount +"          equal of db count and list size                      "+ cardslistSize);
									if (ipidCount == cardslistSize) {
										
										//  File localfolder = new File(FtpFolder+File.separator+ dbFileName);
										  File folder = new File(FtpFolder+File.separator+ dbFileName);
										  File[] listOfFiles = folder.listFiles();
										  for (File fileNamelocal : listOfFiles) {
											    if (fileNamelocal.isFile()) {
											    	
											    	FTPClient desftpclient = new FTPClient();
					            		             boolean filecheckstatus= false ;
					            		             try{
					            		             desftpclient.connect(photodestinationftp_host);
									                 int desreplyCodeftp = desftpclient.getReplyCode();
									                 logger.info("ftp client reply code: " + desreplyCodeftp);
									                 if (!FTPReply.isPositiveCompletion(desreplyCodeftp)) {
									                	logger.info("Operation failed. FTP Server reply code: " + desreplyCodeftp);
									                	if (ff.exists()) {
															file.delete();
														}
									                    return;
									                 }
											            //result = desftpclient.login(FTPUName, FTPPwd);
									                 result = desftpclient.login(photodestinationftp_username, photodestinationftp_password);
									                 logger.info("photodestinationftp_host  "+photodestinationftp_host +"   photodestinationftp_username   "+photodestinationftp_username  +"    photodestinationftp_password   "+photodestinationftp_password);;
											            logger.info("FTP client  status    : "+result);
											            //System.out.println("FTP client  status    : "+result);
											            if (!result) {
											            	if (ff.exists()) {
																file.delete();
															}
											            	 logger.info("Could not login to the  FTP server");
											            	 return;
											            }
					            		             }catch(Exception e){
					            		            	 logger.info("ftp connection failed  Please check it:   ping "+photodestinationftp_host +" -t if not ping ip address contact ITADMIN" );
					            		            	 if (ff.exists()) {
																file.delete();
															}
					            					 		return;
					            		             }
											            File desuploadDir = new File(PHOTOUPLOADS_TEMP);
											            if (!desuploadDir.exists()){
															String str[] =PHOTOUPLOADS_TEMP.split("/");
															for(int k=0;k<str.length;k++){
																desftpclient.changeWorkingDirectory(str[k]);
																desftpclient.printWorkingDirectory();
															}
														}
											            desftpclient.enterLocalPassiveMode();
											            desftpclient.setFileType(FTPClient.BINARY_FILE_TYPE);
											            desftpclient.setRemoteVerificationEnabled(false);
											            
											            
											            String image = "";
					            		                 String extension_img = "";
					            		            	 String ext_img = "";
					            		                 int num = fileNamelocal.getName().lastIndexOf('.');
															if (num > 0) {
																extension_img = fileNamelocal.getName().substring(num + 1);
																ext_img = extension_img.toLowerCase();
																if(extension_img == ext_img){
																	image = fileNamelocal.getName();
																}else{
																	image = fileNamelocal.getName().replaceAll(extension_img,ext_img);
																}
															}
															logger.info("checking the image in ftp folder or not");
												            //checking the image in ftp folder or not
					            		                 filecheckstatus =  checkFileExists(image, desftpclient);
					            		                 
					            		                 if(filecheckstatus){
						            		            	 Set<String> keyset = data.keySet();
																for (String key : keyset) {
																	if(Integer.parseInt(key)!=1){
																		Object[] objArr = data.get(key);
																		//if(objArr[1].equals(list2[l].getName())){
																			objArr[3]="Photo Already exists in ftp";
																			data.put(key,objArr);
																		 //}
																	}
																}
						            		             }else{
						            		            	 logger.info("insert image to ftp ");
						            		            	 InputStream inputStream = new FileInputStream(fileNamelocal);
						            		            	 String extension = "";
						            		            	 String ext = "";
						            		            	 String imageName = "";
						            		            	 int k = fileNamelocal.getName().lastIndexOf('.');
																if (k > 0) {
																	extension = fileNamelocal.getName().substring(k + 1);
																	ext = extension.toLowerCase();
																	imageName = fileNamelocal.getName().replaceAll(extension, ext);
																}
						            		            	 //String imageName = list2[l].getName().replaceAll("JPG", "jpg");
						            		            	 
						            		            	 desftpclient.storeFile(PHOTOUPLOADS_TEMP+imageName, inputStream);
						            		            	// desftpclient.storeFile(PHOTOUPLOADS_TEMP+list2[l].getName(), inputStream);
						            		            	 int desreplyCodeftpxx = desftpclient.getReplyCode();
						            		            	 logger.info("file upload in ftp server of client reply code  : " + desreplyCodeftpxx  +" and ReplyString : "+desftpclient.getReplyString() +" imageName   "+imageName);
											                 if (!FTPReply.isPositiveCompletion(desreplyCodeftpxx)) {
											                	logger.info("Operation failed. file wirte of FTP Server reply code: " + desreplyCodeftpxx);
											                   // return;
											                 }
											                 
							            		             Set<String> keyset = data.keySet();
							            		             for (String key : keyset) {
																	if(Integer.parseInt(key)!=1){
																		Object[] objArr = data.get(key);
																		//if(objArr[1].equals(list2[l].getName())){
																		
																		//if(objArr[1].equals(imageName)){
																			objArr[3]="Photo Uploaded in ftp";
																			data.put(key,objArr);
																		// }
																	}
																}
						            		             }
					            		                 desftpclient.disconnect();
											       
											    }
											}
										  
										
										 int status=0;
										try{ 
										 status = UploadPhotosManager.updatePhotoUploadLot(4,xmlString,policyId,ff,file);
										}catch(Exception e){
											e.printStackTrace();
											if (ff.exists()) {
												file.delete();
											}
										} 
										 if(status==1){
											 int lotupdatestatus=0;
											 try{
												 lotupdatestatus = UploadPhotosManager.updatePhotoUploadLotUpdate(4,lotId,3,ff,file); // 3 is success of status
											 }catch(Exception e){
													e.printStackTrace();
													if (ff.exists()) {
														file.delete();
													}
												} 
											 if(lotupdatestatus==0){
												 if (ff.exists()) {
														file.delete();
													}
											 }
											 Set<String> keyset = data.keySet();
											   for(int k = 0;k<cardList.size();k++){
													for (String key : keyset) {
														if(Integer.parseInt(key)!=1){
															Object[] objArr = data.get(key);
															//if(objArr[1].equals(cardList.get(k))){
																objArr[4]="Database updation Success";
																data.put(key,objArr);
															//}
														}
													}
											   }
											   statuscode = createExcelfie(data,lotId);
										 }else{
											 int lotupdatestatus=0;
											 try{
												 lotupdatestatus = UploadPhotosManager.updatePhotoUploadLotUpdate(4,lotId,5,ff,file); // 5 is error in appropriate database
											 }catch(Exception e){
													e.printStackTrace();
													if (ff.exists()) {
														file.delete();
													}
												} 
											 if(lotupdatestatus==0){
												 if (ff.exists()) {
														file.delete();
													}
											 }
											 Set<String> keyset = data.keySet();
											   for(int k = 0;k<cardList.size();k++){
													for (String key : keyset) {
														if(Integer.parseInt(key)!=1){
															Object[] objArr = data.get(key);
															//if(objArr[1].equals(cardList.get(k))){
																objArr[4]="Database updation failed";
																data.put(key,objArr);
															//}
														}
													}
											   }
											   statuscode = createExcelfie(data,lotId);
											   if(statuscode==1){
													 logger.info("excel file created");
												 }else{
													 logger.info("excel file not created");
												 }
										 }
									}
								}
							}
						if (ff.exists()) {
							file.delete();
						}
					}
				     }else{
				    	 if (ff.exists()) {
								file.delete();
							}
				     }
				    }else{
				    	if (ff.exists()) {
							file.delete();
						}
				    }
				}catch (Exception e) {
					if (ff.exists()) {
						file.delete();
					}
					e.printStackTrace();
				}
				 logger.info("#############  END PHOTO UPLOAD  ######################");
				 System.out.println("#######   end else block  ######################");
			}
			
			
		}
	
	  private static List printFileDetails(FTPFile[] files) {
	      List<String> list = new ArrayList<String>();
	      for (FTPFile file : files) {
	          String details = file.getName();
	         // System.out.println(details);
	          list.add(details);
	      }
	      return list;
	  }
	
	static boolean checkFileExists(String filePath, FTPClient ftpClient) throws IOException {
        InputStream inputStream = ftpClient.retrieveFileStream(filePath);
       int returnCode = ftpClient.getReplyCode();
        if (inputStream == null || returnCode == 550) {
            return false;
        }
        return true;
    }
	
	public static int createExcelfie(Map<String, Object[]> data, int lotId){
		logger.info("#############  Excel file creation start  ######################");
		 MyProperties myResources = new MyProperties();
		String EXCELDOCS = myResources.getMyProperties("photoexcel_file_path");
		
		 
		 // make sure it's a directory
		XSSFWorkbook workbook = new XSSFWorkbook();
		// Create a blank sheet
		XSSFSheet sheet = workbook.createSheet("PHOTO PENDING LOT");
		sheet.setColumnWidth(0, 1500);
		sheet.setColumnWidth(1, 5000);
		sheet.setColumnWidth(2, 2600);
		sheet.setVerticallyCenter(true);
		sheet.setTabColor(4);
		sheet.setDisplayRowColHeadings(true);
		// This data needs to be written (Object[])
		Set<String> keyset = data.keySet();
		int rownum = 0;
		int k = 1;
		for (String key : keyset) {
			Row row = sheet.createRow(rownum++);
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr) {
				Cell cell = row.createCell(cellnum++);
				if (obj instanceof String) {
					cell.setCellValue((String) obj);
				} else if (obj instanceof Integer) {
					cell.setCellValue(k++);
				}
			}
		}
		// Write the workbook in file system
		FileOutputStream out;
		try {
			out = new FileOutputStream( new File(EXCELDOCS+ lotId + ".xlsx"));
			workbook.write(out);
			out.close();
			statuscode=1;
			logger.info("#############  Excel file creation end  ######################");
			logger.info("Excel file  : "+EXCELDOCS+ lotId + ".xlsx");
			//System.out.println("Excel file  : "+EXCELDOCS+ lotId + ".xlsx");
		} catch (Exception e) {
			statuscode=2;
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return statuscode;
	}
		public static void deleteFile(File element) {
			if (element.isDirectory()) {
				for (File sub : element.listFiles()) {
					deleteFile(sub);
				}
			}
			element.delete();
		}
}
