package com.photo.persistence;

import java.sql.Connection;

import com.photo.exception.DAOException;

public interface DAO {
	public Connection getConnection() throws DAOException;
}
