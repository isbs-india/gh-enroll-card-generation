package com.photo.util;

import java.io.StringWriter;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import oracle.sql.CLOB;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XmlGenerationAsString {
	static Logger logger =Logger.getLogger("XmlGenerationAsString");
	public static String sendFaxDataResonse(List<String> selfCardList) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory docFactory = null;
		DocumentBuilder docBuilder = null;
		// root elements
		Document doc = null;
		Element breakupElement = null;
		StringWriter resultAsString = new StringWriter();
        try{
        	docFactory = DocumentBuilderFactory.newInstance();
        	docBuilder = docFactory.newDocumentBuilder();
        	doc = docBuilder.newDocument();
        	/*		breakupElement = doc.createElement("CARDIDS");
        	doc.appendChild(breakupElement);
        
        	Element jobElement = doc.createElement("CARDID");
        	breakupElement.appendChild(jobElement);
        	*/
        	if(selfCardList!=null && selfCardList.size()>0){
        		Element cardIdsElement = doc.createElement("CARDIDS");
        		doc.appendChild(cardIdsElement);
			    for(int i=0;i<selfCardList.size();i++){
		        	Element selfcardIdElement = doc.createElement("CARDID");
		        	selfcardIdElement.appendChild(doc.createTextNode(String.valueOf(selfCardList.get(i))));
		        	cardIdsElement.appendChild(selfcardIdElement);
				}
        	}
       
        	TransformerFactory transformerFactory = TransformerFactory.newInstance();
    		Transformer transformer = transformerFactory.newTransformer();
    		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
    		DOMSource source = new DOMSource(doc);
    		StreamResult result = new StreamResult(resultAsString);
    		// StreamResult streamResult = new StreamResult(new File(FaxData.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/ghplfax/faxresponse.xml")); 
    		transformer.transform(source, result);
            }catch(Exception e){
            	 logger.error(e);
            }finally{
            	 docFactory = null;
        		 docBuilder = null;
        		 doc = null;
        		 breakupElement = null;
            }
       // System.out.println("resultAsString  ::"+resultAsString);
            return resultAsString.toString();
	}

}
