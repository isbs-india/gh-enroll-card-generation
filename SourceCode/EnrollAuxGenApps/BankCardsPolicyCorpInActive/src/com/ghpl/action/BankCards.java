package com.ghpl.action;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.ghpl.model.CardDataBean;
import com.ghpl.model.EnrollsBean;
import com.ghpl.model.InsertLotDetailsBean;
import com.ghpl.persistence.EnrollCardsManager;
import com.ghpl.util.MyProperties;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;

public class BankCards {
	 protected static Logger logger;
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		 MyProperties myResources = new MyProperties();
	     logger = myResources.getLogger();
	     String uploadslocal = myResources.getMyProperties("uploadslocal");
	     String downloadslocal = myResources.getMyProperties("downloadslocal");
	     String srcFolder= "";
	     logger.info("=============JOB Start FOR  Bank Cards==============");
		 try { 
			// EnrollAuxillaryCards ecards = new EnrollAuxillaryCards();
			 	System.out.println("=============JOB Start FOR  Bank Cards CORP==============");
			 	InsertLotDetailsBean insertLotDetailsBean =null;
			 	insertLotDetailsBean = EnrollCardsManager.getLotDetails(1, 1);
			 	logger.info("insertLotDetailsBean    "+insertLotDetailsBean);
			 	int lotNumber=0;
			 	int status=0;
			 	String records="";
			 	int statusCode=0;
			 	if(insertLotDetailsBean!=null){
			 		 lotNumber =insertLotDetailsBean.getLotNumber();
			 	
			 		String contactEmail  = insertLotDetailsBean.getContactEmailId();
				  	 String cardFormat = insertLotDetailsBean.getCardFormat();
				  	 String userName = insertLotDetailsBean.getUserName();
				 	 CardDataBean cardDataBean= new CardDataBean();
				 	List<EnrollsBean> trackCardList=null;
				 	logger.info("lotNumber   "+lotNumber);
				 	logger.info("cardFormat   "+cardFormat);
			 		cardDataBean = EnrollCardsManager.getEnrollCardDetails(lotNumber,1);
			 		srcFolder=uploadslocal+lotNumber+"_CORP";
			 		logger.info("Source folder  : "+srcFolder);
			 		logger.info("lotNumber : "+lotNumber +"         cardFormat   "+cardFormat);
			 		
			 		System.out.println("Source folder  : "+srcFolder);
			 		System.out.println("lotNumber : "+lotNumber +"         cardFormat   "+cardFormat);
			 		if((cardDataBean.getDbrecordsList()!=null) && (cardDataBean.getDbrecordsList().size()>0)){
			 			if(cardFormat.equals("E")){
			 				try{
			 				File theDir = new File(srcFolder);
							  if (theDir.exists()) {
								  theDir.delete();
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }else{
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }
							
						 		 EnrollAuxillaryCards.generateECardFormat(cardDataBean,srcFolder+File.separator+"CORPCards_Lot");
						 	     logger.info("Generate ECard format for CORP : ");
							  
							 // insurer certificate generation code start
							  int insertStatusFlag=insertLotDetailsBean.getGenInsertStausFlag();
							  if(insertStatusFlag==1){
							  trackCardList =new ArrayList<EnrollsBean>(); 
							  String tempBano="";
							  int serialNo=0;
				        		 for(EnrollsBean enroleBean: cardDataBean.getInsurerList()){
				        			 if(enroleBean!=null){
				        			//tempBano = enroleBean.getBano();
				        				if(tempBano.equals(enroleBean.getBANO())){
				        					tempBano=""+enroleBean.getBANO();
				        					enroleBean.setSerialNo(++serialNo);
					        				 trackCardList.add(enroleBean);
					        			}else{
					        				if(trackCardList.size()>0){
					        					File theDir1 = new File(srcFolder);
												  if (!theDir1.exists()) {
													  theDir1.mkdir();
												  }
												  EnrollAuxillaryCards.generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
												  serialNo=0;
					        				}
					        				 trackCardList=null;
					        				 if(enroleBean.getBANO()!=null){
						        				 trackCardList =new ArrayList<EnrollsBean>();
						        				 tempBano=enroleBean.getBANO();
						        				 enroleBean.setSerialNo(++serialNo);
						        				 trackCardList.add(enroleBean);
					        				 }
					        				}
				        			 }//enroleBean null condition
				        		 }
				        		 if(trackCardList.size()>0){
				        			 EnrollAuxillaryCards.generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
			        			 }
							  }
							  // insurer certificate generation code end
							  
			 				 //write zip file from command from using unix or linux command
			 				String destZipFile= "";
			 					 destZipFile= downloadslocal+lotNumber+"_CORP.zip";
			 					 logger.info("Destination zip file for ECard CORP : "+destZipFile);
			        		 File zipfile = new File(destZipFile);
			        		 if(zipfile.exists()){
			        			 zipfile.delete();
			 					 destZipFile= downloadslocal+lotNumber+"_CORP.zip";
			 					 logger.info("Destination zip file for ECard CORP : "+destZipFile);
			        			// destZipFile= downloadslocal+lotNumber+"_AB.zip";
			        			 zipfile = new File(destZipFile);
			        			 logger.info("Destination zip file for ECard : "+zipfile);
			        		 }
			        		 File file = new File(srcFolder);
							if (file.exists() && file.isDirectory()) {
						  //System.out.println("destZipFile  "+destZipFile);
						  //System.out.println("srcFolder  "+srcFolder);
						  String command ="zip -rj " + destZipFile +  " " + srcFolder;
						  //System.out.println("command : "+command);
						  logger.info(command);
						  try {
						      Process proc = Runtime.getRuntime().exec(command);
						      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						      String line = null;
					            while ((line = in.readLine()) != null) {
					                //System.out.println(line);
					            }
					             status =3;
						      //System.out.println("status : "+status);
						      logger.info("status :"+status);
						   } catch (Exception e) {
							   status=4;
							   logger.error("status :"+status);
						      e.printStackTrace();
						      logger.error(e);
						   }
							}else{
								status=4;
								   records=" no records and file creation failed ";
								   logger.info("status :"+status);
							}
			 				}catch (Exception e) {
								   status=4;
								   logger.error("status :"+status);
							      e.printStackTrace();
							      logger.error(e);
							   }	
			 				
			 			logger.info("before calling put lot details  per Ecards   lotNumber---  : "+lotNumber +"status     "+status   );
	     				 statusCode = EnrollCardsManager.putLotDetails(lotNumber, status,1);
	     				 System.out.println("status     "+status   +"       statusCode:"+statusCode);
	     				 logger.info("lotNumber---  : "+lotNumber +"         cardFormat--- :  "+cardFormat+"      statusCode  ------ :   "+statusCode);
	     				 
			 			}else if(cardFormat.equals("P")){
			 				try{
			 				File theDir = new File(srcFolder);
			 				 if (theDir.exists()) {
								  theDir.delete();
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }else{
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }
			 				 
					 			logger.info("pdf generation for WIND Cards : ");
					 			EnrollAuxillaryCards.generatePhycalCardFormat(cardDataBean, srcFolder+File.separator+"CORPCards_Lot_PCard");
			 				 int insertStatusFlag=insertLotDetailsBean.getGenInsertStausFlag();
							  if(insertStatusFlag==1){
			 				trackCardList =new ArrayList<EnrollsBean>(); 
							  String tempBano="";
							  int serialNo=0;
				        		 for(EnrollsBean enroleBean: cardDataBean.getInsurerList()){
				        			 if(enroleBean!=null){
				        			//tempBano = enroleBean.getBano();
				        				if(tempBano.equals(enroleBean.getBANO())){
				        					tempBano=""+enroleBean.getBANO();
				        					enroleBean.setSerialNo(++serialNo);
					        				 trackCardList.add(enroleBean);
					        			}else{
					        				if(trackCardList.size()>0){
					        					File theDir1 = new File(srcFolder);
												  if (!theDir1.exists()) {
													  theDir1.mkdir();
												  }
												  EnrollAuxillaryCards.generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
												  serialNo=0;
					        				}
					        				 trackCardList=null;
					        				 if(enroleBean.getBANO()!=null){
						        				 trackCardList =new ArrayList<EnrollsBean>();
						        				 tempBano=enroleBean.getBANO();
						        				 enroleBean.setSerialNo(++serialNo);
						        				 trackCardList.add(enroleBean);
					        				 }
					        				}
				        			 }//enroleBean null condition
				        		 }
				        		 if(trackCardList.size()>0){
				        			 EnrollAuxillaryCards.generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
			        			 }
							  }
			 				 //write zip file from command fromt using unix or linux command
			 				String destZipFile= "";
			 					 destZipFile= downloadslocal+lotNumber+"_CORP.zip";
			 					 logger.info("Destination zip file for PCard CORP1 : "+destZipFile);
			        		 File zipfile = new File(destZipFile);
			        		 if(zipfile.exists()){
			        			 zipfile.delete();
					 				destZipFile= downloadslocal+lotNumber+"_CORP.zip";
					 				logger.info("Destination zip file for PCard CORP2 : "+destZipFile);
			        			 zipfile = new File(destZipFile);
			        			 logger.info("Destination zip file for PCard : "+zipfile);
			        		 }
			        		 File file = new File(srcFolder);
							if (file.exists() && file.isDirectory()) {
						  
						 String command ="zip -rj " + destZipFile +  " " + srcFolder;
						  //System.out.println("command : "+command);
						  logger.info(command);
						  try {
						      Process proc = Runtime.getRuntime().exec(command);
						      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						      String line = null;
					            while ((line = in.readLine()) != null) {
					                //System.out.println(line);
					            }
					             status =3;
						      //System.out.println("status : "+status);
						      logger.info("status :"+status);
						   } catch (Exception e) {
							   status=4;
							   logger.error("status :"+status);
						      e.printStackTrace();
						      logger.error(e);
						   }
							}else{
								status=4;
								   records=" no records and file creation failed ";
								   logger.info("status :"+status);
							}
			 				}catch (Exception e) {
								   status=4;
								   logger.error("status :"+status);
							      e.printStackTrace();
							      logger.error(e);
							   }
			 				logger.info("before calling put lot details  per Ecards lotnumber : "+lotNumber);
			 				logger.info("status   "+status);
			 				//logger.info("module Id  : "+i);;
	     				 statusCode = EnrollCardsManager.putLotDetails(lotNumber, status,1);
	     				 logger.info("lotNumber---  : "+lotNumber +"         cardFormat--- :  "+cardFormat+"      statusCode  ------ :   "+statusCode);	
			 				
			 			}/*else if(cardFormat.equals("V")){
			 				EnrollAuxillaryCards.generateVendorDetails(cardDataBean, srcFolder,contactEmail,userName);
			 			}*/
			        }else{
			        	File theDir = new File(srcFolder);
		 				 if (theDir.exists()) {
							  theDir.delete();
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }else{
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }
			        	
			        	try{
							 Document document = new Document(PageSize.A4);
					    	 Rectangle pagesize = new Rectangle(216f, 720f);
					    		 PdfWriter.getInstance(document, new FileOutputStream(srcFolder+File.separator+"CORPCards_"+"Exception.pdf"));
					         // step 3
					         document.open();
					         // step 4
					         document.add(new Paragraph(myResources.getMyProperties("NODataFound")));
					         // step 5
					         document.close();
							}catch(Exception ex){ex.printStackTrace();}
			        	
			        	//write zip file from command fromt using unix or linux command
		 				String destZipFile= "";
		 					 destZipFile= downloadslocal+lotNumber+"_CORP.zip";
		 					 logger.info("Destination zip file for PCard WIND : "+destZipFile);
			        	
		 				 File file = new File(srcFolder);
							if (file.exists() && file.isDirectory()) {
						  
						 String command ="zip -rj " + destZipFile +  " " + srcFolder;
						  //System.out.println("command : "+command);
						  logger.info(command);
						  try {
						      Process proc = Runtime.getRuntime().exec(command);
						      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
						      String line = null;
					            while ((line = in.readLine()) != null) {
					                //System.out.println(line);
					            }
					             status =5;
						      //System.out.println("status : "+status);
						      logger.info("status :"+status);
						   } catch (Exception e) {
							   status=4;
							   logger.error("status :"+status);
						      e.printStackTrace();
						      logger.error(e);
						   }
			        	
			        	logger.info("before calling put lot details  per Ecards lotnumber : "+lotNumber);
		 				logger.info("status   "+5);
			        	 statusCode = EnrollCardsManager.putLotDetails(lotNumber, status,1);
			        	 logger.info("statusCode   : "+statusCode);
			        
			 		}
			 }
			 	System.out.println("=============JOB END FOR  Bank Cards==============");
			 	logger.info("=============JOB END FOR  Bank Cards==============");
			 	}
			 //	}
			 	//}
		      } catch (Exception e) {  
		        e.printStackTrace();  
		 }  
	}
	
	
	
	

}
