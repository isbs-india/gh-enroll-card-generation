/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ghpl.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.log4j.Logger;

/**
 *
 * @author Surya Kiran
 * Module Definitions
 * 1 CORP
 * 2 AB
 * 3 VENUS
 * 4 RETAIL UIC
 * 5 RETAIL NIA
 * 6 RETAIL OIC
 * 7 RETAIL NIC
 */
public class DBConn {
	protected static Logger logger;
    
    public DBConn(){
        MyProperties Gdbp = new MyProperties();
        logger = Gdbp.getLogger();
    }    
    
    public static Connection getMyConnection(int Module) {
        
        Connection myConnection = null;
        
        switch (Module) {
            case 1:
            case 2:
            case 4:myConnection = getOracleConnection(Module); break;
        }
        
        return myConnection;
    }
    public static Connection getOracleConnection(int Module) {
                MyProperties Gdbp = new MyProperties();              
                String jdbcURL = "jdbc:oracle:thin:@"+ Gdbp.getMyProperties("GHIPREPORTS")+":1521:"+Gdbp.getMyProperties("GHSIDREPORTS");
                String dbUser = Gdbp.getMyProperties("GHUserREPORTS");
                String dbPwd = Gdbp.getMyProperties("GHPwdREPORTS");
                
                switch (Module) {
                    case 2 :
                        jdbcURL = "jdbc:oracle:thin:@"+ Gdbp.getMyProperties("ABIPREPORTS")+":1521:"+Gdbp.getMyProperties("ABSIDREPORTS");
                        dbUser = Gdbp.getMyProperties("ABUserREPORTS");
                        dbPwd = Gdbp.getMyProperties("ABPwdREPORTS");
                        break;
                    case 4 :
                        jdbcURL = "jdbc:oracle:thin:@"+ Gdbp.getMyProperties("WINDIPREPORTS")+":1521:"+Gdbp.getMyProperties("WINDSIDREPORTS");
                        dbUser = Gdbp.getMyProperties("WINDUserREPORTS");
                        dbPwd = Gdbp.getMyProperties("WINDPwdREPORTS");
                        break;
                }                                               
		Connection myConnection = null;		 
		try { 
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
		} catch (ClassNotFoundException e) { 
			logger.error("Error DBConn"); 
			logger.error("No Oracle JDBC Driver Found for "+Module+" Module");
			return null; 
		} catch ( InstantiationException e) { 
			logger.error("Error DBConn"); 
			logger.error("No Oracle JDBC Driver Found Cannot Instantiate for "+Module+" Module");
			return null; 
		} catch (IllegalAccessException e) { 
			logger.error("Error DBConn"); 
			logger.error("No Oracle JDBC Driver Found Illegal Access for "+Module+" Module" );
			return null; 
		} 
 
		//logger.info("Oracle JDBC Driver Registered! for "+Module+" Module");		
		
		try {
			 
			myConnection = DriverManager.getConnection(jdbcURL, dbUser, dbPwd);
                        myConnection.setAutoCommit(false);
 
		} catch (SQLException e) {
			logger.error("Error DBConn");
			logger.error("Connection Failed! Check output console for "+Module+" Module");
			e.printStackTrace();
			logger.error(e);
			return null; 
		}
		return myConnection;
		
	}
  
    public static Connection getMySQLConnection() {
	    //String jdbcURL = DBIP;
	    MyProperties Gdbp = new MyProperties();

        String jdbcURL = "jdbc:mysql://"+ Gdbp.getMyProperties("MSIP")+":3306/"+Gdbp.getMyProperties("MSSID");
	    String user =Gdbp.getMyProperties("MSUser");
	    String pwd= Gdbp.getMyProperties("MSPwd");
			Connection myConnection = null;
			try { 
				Class.forName("com.mysql.jdbc.Driver").newInstance();
			} catch (ClassNotFoundException e) { 
				logger.error("Error DBConn "+Gdbp.getMyProperties("MSIP")); 
				logger.error("No MYSQL JDBC Driver Found");
				return null; 
			} catch ( InstantiationException e) { 
				logger.error("Error DBConn   "+ Gdbp.getMyProperties("MSIP")); 
				logger.error("No MYSQL JDBC Driver Found Cannot Instantiate");
				return null; 
			} catch (IllegalAccessException e) { 
				logger.error("Error DBConn "+Gdbp.getMyProperties("MSIP")); 
				logger.error("No MYSQL JDBC Driver Found Illegal Access");
				return null; 
			} 
			
			//logger.info("MYSQL JDBC Driver Registered!");		
			
			try {
				 
				myConnection = DriverManager.getConnection(jdbcURL,user,pwd);
			
			} catch (SQLException e) {
				logger.error("Error DBConn  "+jdbcURL);
				logger.error("Connection Failed! Check output console");
				e.printStackTrace();
				return null; 
			}
			
			if (myConnection != null) {
				//logger.info(" MYSQL Connection Succuessfull....");
			} else {
				logger.error("Failed to make connection!");
			}
			return myConnection;
	}
}
