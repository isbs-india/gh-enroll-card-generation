package com.ghpl.model;

public class EnrollsBean {
	
private String CERTIFICATE;
	
	
	
	
	
	
	
	public String getCERTIFICATE() {
		return CERTIFICATE;
	}
	public void setCERTIFICATE(String cERTIFICATE) {
		CERTIFICATE = cERTIFICATE;
	}
	
	
	private int serialNo;
	private String DOJ;
	private String DOL;
	private String GRADE;
	private String RELATIONCODE;
	private String MODULEID;
	private String PLANTNAME;
	private String INSURERCARDID;
	private String topupsumInsured;
	private String sumInsured;
	private String serviceTax;
	private String topupServiceTax;
	private String premium;
	private String topupPremium;
	private String topupPolicyno;
	private String city;
	private String state;
	private String pincode;
	private String planName;
	private String rno;
	private String mobileno;
	 
	private String FAMILYID;
	private String IPID;
	private String INSUREDNAME;
	private String AGE;
	private String GENDER;
	private String EMPID;
	private String INSURER_CARDID;
	private String CARDID;
	private String INSUREDINCEPTIONDT;
	private String VALIDFROM;
	private String VALIDTO;
	private String POLICYCARDNAME;
	private String POLICYHOLDERNAME;
	private String POLICYFROM;
	private String POLICYTO;
	private String POLICYNO;
	private String TOPUPPOLICYNO;
	private String DOB;
	private String AGEFROMDOB;
	private String RELATIONSHIP;
	private String UWID;
	private String ID;
	private String CURRENTPOLICYID;
	private String TOPUPPOLICYID;
	private String PREMIUM;
	private String TOPUPPREMIUM;
	private String SERVICETAX;
	private String TOPUPSERVICETAX;
	private String SUMINSURED;
	private String TOPUPSUMINSURED;
	private String BANO;
	private String BANKACCOUNTNUMBER;
	private String BRANCHCODE;
	private String TO1;
	private String TO2;
	private String TO3;
	private String TO4;
	private String TO5;
	private String PLANNAME;
	private String RNO;
	private String MOBILENO;
	private String FCOUNT;
	private String INSURED_EMAIL;
	private String BLOODGROUP;
	private String ADDRESS;
	private String ECARD_TEMPLATE;
	private String BO_CODE;
	private String DO_CODE;
	private String CUMMULATIVE_BONUS;
	private String VB64_COMPLIANCE;
	private String PAYMENTMODE;
	private String ZONE_TYPE;
	private String CUSTOMER_ID;
	private String MEMBER_CODE;
	private String COPAY_REMARKS;
	private String PREEXISITNG;
	private String ROCODE;
	private String ID_TYPE;
	private String ID_NO;
	private String IPCODE;
	private String TOPUP_TEXT; 
	public String getTOPUP_TEXT() {
		return TOPUP_TEXT;
	}
	public void setTOPUP_TEXT(String tOPUP_TEXT) {
		TOPUP_TEXT = tOPUP_TEXT;
	}
	public String getIPCODE() {
		return IPCODE;
	}
	public void setIPCODE(String iPCODE) {
		IPCODE = iPCODE;
	}
	public int getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}
	public String getDOJ() {
		return DOJ;
	}
	public void setDOJ(String dOJ) {
		DOJ = dOJ;
	}
	public String getDOL() {
		return DOL;
	}
	public void setDOL(String dOL) {
		DOL = dOL;
	}
	public String getGRADE() {
		return GRADE;
	}
	public void setGRADE(String gRADE) {
		GRADE = gRADE;
	}
	public String getRELATIONCODE() {
		return RELATIONCODE;
	}
	public void setRELATIONCODE(String rELATIONCODE) {
		RELATIONCODE = rELATIONCODE;
	}
	public String getMODULEID() {
		return MODULEID;
	}
	public void setMODULEID(String mODULEID) {
		MODULEID = mODULEID;
	}
	public String getPLANTNAME() {
		return PLANTNAME;
	}
	public void setPLANTNAME(String pLANTNAME) {
		PLANTNAME = pLANTNAME;
	}
	public String getINSURERCARDID() {
		return INSURERCARDID;
	}
	public void setINSURERCARDID(String iNSURERCARDID) {
		INSURERCARDID = iNSURERCARDID;
	}
	public String getTopupsumInsured() {
		return topupsumInsured;
	}
	public void setTopupsumInsured(String topupsumInsured) {
		this.topupsumInsured = topupsumInsured;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}
	public String getTopupServiceTax() {
		return topupServiceTax;
	}
	public void setTopupServiceTax(String topupServiceTax) {
		this.topupServiceTax = topupServiceTax;
	}
	public String getPremium() {
		return premium;
	}
	public void setPremium(String premium) {
		this.premium = premium;
	}
	public String getTopupPremium() {
		return topupPremium;
	}
	public void setTopupPremium(String topupPremium) {
		this.topupPremium = topupPremium;
	}
	public String getTopupPolicyno() {
		return topupPolicyno;
	}
	public void setTopupPolicyno(String topupPolicyno) {
		this.topupPolicyno = topupPolicyno;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getRno() {
		return rno;
	}
	public void setRno(String rno) {
		this.rno = rno;
	}
	public String getMobileno() {
		return mobileno;
	}
	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}
	public String getFAMILYID() {
		return FAMILYID;
	}
	public void setFAMILYID(String fAMILYID) {
		FAMILYID = fAMILYID;
	}
	public String getIPID() {
		return IPID;
	}
	public void setIPID(String iPID) {
		IPID = iPID;
	}
	public String getINSUREDNAME() {
		return INSUREDNAME;
	}
	public void setINSUREDNAME(String iNSUREDNAME) {
		INSUREDNAME = iNSUREDNAME;
	}
	public String getAGE() {
		return AGE;
	}
	public void setAGE(String aGE) {
		AGE = aGE;
	}
	public String getGENDER() {
		return GENDER;
	}
	public void setGENDER(String gENDER) {
		GENDER = gENDER;
	}
	public String getEMPID() {
		return EMPID;
	}
	public void setEMPID(String eMPID) {
		EMPID = eMPID;
	}
	public String getINSURER_CARDID() {
		return INSURER_CARDID;
	}
	public void setINSURER_CARDID(String iNSURER_CARDID) {
		INSURER_CARDID = iNSURER_CARDID;
	}
	public String getCARDID() {
		return CARDID;
	}
	public void setCARDID(String cARDID) {
		CARDID = cARDID;
	}
	public String getINSUREDINCEPTIONDT() {
		return INSUREDINCEPTIONDT;
	}
	public void setINSUREDINCEPTIONDT(String iNSUREDINCEPTIONDT) {
		INSUREDINCEPTIONDT = iNSUREDINCEPTIONDT;
	}
	public String getVALIDFROM() {
		return VALIDFROM;
	}
	public void setVALIDFROM(String vALIDFROM) {
		VALIDFROM = vALIDFROM;
	}
	public String getVALIDTO() {
		return VALIDTO;
	}
	public void setVALIDTO(String vALIDTO) {
		VALIDTO = vALIDTO;
	}
	public String getPOLICYCARDNAME() {
		return POLICYCARDNAME;
	}
	public void setPOLICYCARDNAME(String pOLICYCARDNAME) {
		POLICYCARDNAME = pOLICYCARDNAME;
	}
	public String getPOLICYHOLDERNAME() {
		return POLICYHOLDERNAME;
	}
	public void setPOLICYHOLDERNAME(String pOLICYHOLDERNAME) {
		POLICYHOLDERNAME = pOLICYHOLDERNAME;
	}
	public String getPOLICYFROM() {
		return POLICYFROM;
	}
	public void setPOLICYFROM(String pOLICYFROM) {
		POLICYFROM = pOLICYFROM;
	}
	public String getPOLICYTO() {
		return POLICYTO;
	}
	public void setPOLICYTO(String pOLICYTO) {
		POLICYTO = pOLICYTO;
	}
	public String getPOLICYNO() {
		return POLICYNO;
	}
	public void setPOLICYNO(String pOLICYNO) {
		POLICYNO = pOLICYNO;
	}
	public String getTOPUPPOLICYNO() {
		return TOPUPPOLICYNO;
	}
	public void setTOPUPPOLICYNO(String tOPUPPOLICYNO) {
		TOPUPPOLICYNO = tOPUPPOLICYNO;
	}
	public String getDOB() {
		return DOB;
	}
	public void setDOB(String dOB) {
		DOB = dOB;
	}
	public String getAGEFROMDOB() {
		return AGEFROMDOB;
	}
	public void setAGEFROMDOB(String aGEFROMDOB) {
		AGEFROMDOB = aGEFROMDOB;
	}
	public String getRELATIONSHIP() {
		return RELATIONSHIP;
	}
	public void setRELATIONSHIP(String rELATIONSHIP) {
		RELATIONSHIP = rELATIONSHIP;
	}
	public String getUWID() {
		return UWID;
	}
	public void setUWID(String uWID) {
		UWID = uWID;
	}
	public String getID() {
		return ID;
	}
	public void setID(String iD) {
		ID = iD;
	}
	public String getCURRENTPOLICYID() {
		return CURRENTPOLICYID;
	}
	public void setCURRENTPOLICYID(String cURRENTPOLICYID) {
		CURRENTPOLICYID = cURRENTPOLICYID;
	}
	public String getTOPUPPOLICYID() {
		return TOPUPPOLICYID;
	}
	public void setTOPUPPOLICYID(String tOPUPPOLICYID) {
		TOPUPPOLICYID = tOPUPPOLICYID;
	}
	public String getPREMIUM() {
		return PREMIUM;
	}
	public void setPREMIUM(String pREMIUM) {
		PREMIUM = pREMIUM;
	}
	public String getTOPUPPREMIUM() {
		return TOPUPPREMIUM;
	}
	public void setTOPUPPREMIUM(String tOPUPPREMIUM) {
		TOPUPPREMIUM = tOPUPPREMIUM;
	}
	public String getSERVICETAX() {
		return SERVICETAX;
	}
	public void setSERVICETAX(String sERVICETAX) {
		SERVICETAX = sERVICETAX;
	}
	public String getTOPUPSERVICETAX() {
		return TOPUPSERVICETAX;
	}
	public void setTOPUPSERVICETAX(String tOPUPSERVICETAX) {
		TOPUPSERVICETAX = tOPUPSERVICETAX;
	}
	public String getSUMINSURED() {
		return SUMINSURED;
	}
	public void setSUMINSURED(String sUMINSURED) {
		SUMINSURED = sUMINSURED;
	}
	public String getTOPUPSUMINSURED() {
		return TOPUPSUMINSURED;
	}
	public void setTOPUPSUMINSURED(String tOPUPSUMINSURED) {
		TOPUPSUMINSURED = tOPUPSUMINSURED;
	}
	public String getBANO() {
		return BANO;
	}
	public void setBANO(String bANO) {
		BANO = bANO;
	}
	public String getBANKACCOUNTNUMBER() {
		return BANKACCOUNTNUMBER;
	}
	public void setBANKACCOUNTNUMBER(String bANKACCOUNTNUMBER) {
		BANKACCOUNTNUMBER = bANKACCOUNTNUMBER;
	}
	public String getBRANCHCODE() {
		return BRANCHCODE;
	}
	public void setBRANCHCODE(String bRANCHCODE) {
		BRANCHCODE = bRANCHCODE;
	}
	public String getTO1() {
		return TO1;
	}
	public void setTO1(String tO1) {
		TO1 = tO1;
	}
	public String getTO2() {
		return TO2;
	}
	public void setTO2(String tO2) {
		TO2 = tO2;
	}
	public String getTO3() {
		return TO3;
	}
	public void setTO3(String tO3) {
		TO3 = tO3;
	}
	public String getTO4() {
		return TO4;
	}
	public void setTO4(String tO4) {
		TO4 = tO4;
	}
	public String getTO5() {
		return TO5;
	}
	public void setTO5(String tO5) {
		TO5 = tO5;
	}
	public String getPLANNAME() {
		return PLANNAME;
	}
	public void setPLANNAME(String pLANNAME) {
		PLANNAME = pLANNAME;
	}
	public String getRNO() {
		return RNO;
	}
	public void setRNO(String rNO) {
		RNO = rNO;
	}
	public String getMOBILENO() {
		return MOBILENO;
	}
	public void setMOBILENO(String mOBILENO) {
		MOBILENO = mOBILENO;
	}
	public String getFCOUNT() {
		return FCOUNT;
	}
	public void setFCOUNT(String fCOUNT) {
		FCOUNT = fCOUNT;
	}
	public String getINSURED_EMAIL() {
		return INSURED_EMAIL;
	}
	public void setINSURED_EMAIL(String iNSURED_EMAIL) {
		INSURED_EMAIL = iNSURED_EMAIL;
	}
	public String getBLOODGROUP() {
		return BLOODGROUP;
	}
	public void setBLOODGROUP(String bLOODGROUP) {
		BLOODGROUP = bLOODGROUP;
	}
	public String getADDRESS() {
		return ADDRESS;
	}
	public void setADDRESS(String aDDRESS) {
		ADDRESS = aDDRESS;
	}
	public String getECARD_TEMPLATE() {
		return ECARD_TEMPLATE;
	}
	public void setECARD_TEMPLATE(String eCARD_TEMPLATE) {
		ECARD_TEMPLATE = eCARD_TEMPLATE;
	}
	public String getBO_CODE() {
		return BO_CODE;
	}
	public void setBO_CODE(String bO_CODE) {
		BO_CODE = bO_CODE;
	}
	public String getDO_CODE() {
		return DO_CODE;
	}
	public void setDO_CODE(String dO_CODE) {
		DO_CODE = dO_CODE;
	}
	public String getCUMMULATIVE_BONUS() {
		return CUMMULATIVE_BONUS;
	}
	public void setCUMMULATIVE_BONUS(String cUMMULATIVE_BONUS) {
		CUMMULATIVE_BONUS = cUMMULATIVE_BONUS;
	}
	public String getVB64_COMPLIANCE() {
		return VB64_COMPLIANCE;
	}
	public void setVB64_COMPLIANCE(String vB64_COMPLIANCE) {
		VB64_COMPLIANCE = vB64_COMPLIANCE;
	}
	public String getPAYMENTMODE() {
		return PAYMENTMODE;
	}
	public void setPAYMENTMODE(String pAYMENTMODE) {
		PAYMENTMODE = pAYMENTMODE;
	}
	public String getZONE_TYPE() {
		return ZONE_TYPE;
	}
	public void setZONE_TYPE(String zONE_TYPE) {
		ZONE_TYPE = zONE_TYPE;
	}
	public String getCUSTOMER_ID() {
		return CUSTOMER_ID;
	}
	public void setCUSTOMER_ID(String cUSTOMER_ID) {
		CUSTOMER_ID = cUSTOMER_ID;
	}
	public String getMEMBER_CODE() {
		return MEMBER_CODE;
	}
	public void setMEMBER_CODE(String mEMBER_CODE) {
		MEMBER_CODE = mEMBER_CODE;
	}
	public String getCOPAY_REMARKS() {
		return COPAY_REMARKS;
	}
	public void setCOPAY_REMARKS(String cOPAY_REMARKS) {
		COPAY_REMARKS = cOPAY_REMARKS;
	}
	public String getPREEXISITNG() {
		return PREEXISITNG;
	}
	public void setPREEXISITNG(String pREEXISITNG) {
		PREEXISITNG = pREEXISITNG;
	}
	public String getROCODE() {
		return ROCODE;
	}
	public void setROCODE(String rOCODE) {
		ROCODE = rOCODE;
	}
	public String getID_TYPE() {
		return ID_TYPE;
	}
	public void setID_TYPE(String iD_TYPE) {
		ID_TYPE = iD_TYPE;
	}
	public String getID_NO() {
		return ID_NO;
	}
	public void setID_NO(String iD_NO) {
		ID_NO = iD_NO;
	}
	 
	
	
	
	
	
		
	
	
	
	
}
