package com.ghpl.model;

import java.util.ArrayList;
import java.util.List;

public class CardJsonBean {
	private List<String> dbheaderNamesList;
	private List<String[]> dbrecordsList;
	private String cardTempxmlString;
    private String outputFileName;
    private List<EnrollsBean> insurerList = new ArrayList<EnrollsBean>();
    
	public List<EnrollsBean> getInsurerList() {
		return insurerList;
	}
	public void setInsurerList(List<EnrollsBean> insurerList) {
		this.insurerList = insurerList;
	}
	public List<String> getDbheaderNamesList() {
		return dbheaderNamesList;
	}
	public void setDbheaderNamesList(List<String> dbheaderNamesList) {
		this.dbheaderNamesList = dbheaderNamesList;
	}
	public List<String[]> getDbrecordsList() {
		return dbrecordsList;
	}
	public void setDbrecordsList(List<String[]> dbrecordsList) {
		this.dbrecordsList = dbrecordsList;
	}
	public String getCardTempxmlString() {
		return cardTempxmlString;
	}
	public void setCardTempxmlString(String cardTempxmlString) {
		this.cardTempxmlString = cardTempxmlString;
	}
	public String getOutputFileName() {
		return outputFileName;
	}
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}
}
