package com.ghpl.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement  
public class Card {
	private String cardtemplatename;
	private String cardfrontimage;
	private String cardbackimage;
	private String eformatType;
	private String etemplatexpossleft;
	private String etemplatexpossright;
	private String etemplateyposs;
	private String etableTotalWidth;
	private String etableFixedHeight;
	private String ebottomMargin;
	private String etemplateypossnextrow;
	
	private String pformatType;
	private String ptemplatexpossleft;
	private String ptemplatexpossright;
	private String ptemplateyposs;
	private String ptableTotalWidth;
	private String ptableFixedHeight;
	private String pbottomMargin;
	private String ptemplateypossnextrow;
	private List<row> row;
	

	public String getPtemplateypossnextrow() {
		return ptemplateypossnextrow;
	}

	public void setPtemplateypossnextrow(String ptemplateypossnextrow) {
		this.ptemplateypossnextrow = ptemplateypossnextrow;
	}

	public String getEtemplateypossnextrow() {
		return etemplateypossnextrow;
	}

	public void setEtemplateypossnextrow(String etemplateypossnextrow) {
		this.etemplateypossnextrow = etemplateypossnextrow;
	}

	public String getPtemplatexpossleft() {
		return ptemplatexpossleft;
	}

	public void setPtemplatexpossleft(String ptemplatexpossleft) {
		this.ptemplatexpossleft = ptemplatexpossleft;
	}

	public String getPtemplatexpossright() {
		return ptemplatexpossright;
	}

	public void setPtemplatexpossright(String ptemplatexpossright) {
		this.ptemplatexpossright = ptemplatexpossright;
	}

	public String getEtemplatexpossleft() {
		return etemplatexpossleft;
	}

	public void setEtemplatexpossleft(String etemplatexpossleft) {
		this.etemplatexpossleft = etemplatexpossleft;
	}

	public String getEtemplatexpossright() {
		return etemplatexpossright;
	}

	public void setEtemplatexpossright(String etemplatexpossright) {
		this.etemplatexpossright = etemplatexpossright;
	}

	public String getEformatType() {
		return eformatType;
	}

	public void setEformatType(String eformatType) {
		this.eformatType = eformatType;
	}

	

	public String getEtemplateyposs() {
		return etemplateyposs;
	}

	public void setEtemplateyposs(String etemplateyposs) {
		this.etemplateyposs = etemplateyposs;
	}

	public String getEtableTotalWidth() {
		return etableTotalWidth;
	}

	public void setEtableTotalWidth(String etableTotalWidth) {
		this.etableTotalWidth = etableTotalWidth;
	}

	public String getEtableFixedHeight() {
		return etableFixedHeight;
	}

	public void setEtableFixedHeight(String etableFixedHeight) {
		this.etableFixedHeight = etableFixedHeight;
	}

	public String getEbottomMargin() {
		return ebottomMargin;
	}

	public void setEbottomMargin(String ebottomMargin) {
		this.ebottomMargin = ebottomMargin;
	}

	public String getPformatType() {
		return pformatType;
	}

	public void setPformatType(String pformatType) {
		this.pformatType = pformatType;
	}

	

	public String getPtemplateyposs() {
		return ptemplateyposs;
	}

	public void setPtemplateyposs(String ptemplateyposs) {
		this.ptemplateyposs = ptemplateyposs;
	}

	public String getPtableTotalWidth() {
		return ptableTotalWidth;
	}

	public void setPtableTotalWidth(String ptableTotalWidth) {
		this.ptableTotalWidth = ptableTotalWidth;
	}

	public String getPtableFixedHeight() {
		return ptableFixedHeight;
	}

	public void setPtableFixedHeight(String ptableFixedHeight) {
		this.ptableFixedHeight = ptableFixedHeight;
	}

	public String getPbottomMargin() {
		return pbottomMargin;
	}

	public void setPbottomMargin(String pbottomMargin) {
		this.pbottomMargin = pbottomMargin;
	}

	
	
	 

	

	public String getCardtemplatename() {
		return cardtemplatename;
	}

	public void setCardtemplatename(String cardtemplatename) {
		this.cardtemplatename = cardtemplatename;
	}

	public String getCardfrontimage() {
		return cardfrontimage;
	}

	public void setCardfrontimage(String cardfrontimage) {
		this.cardfrontimage = cardfrontimage;
	}

	public String getCardbackimage() {
		return cardbackimage;
	}

	public void setCardbackimage(String cardbackimage) {
		this.cardbackimage = cardbackimage;
	}

	@XmlElement
	public List<row> getRow() {
		return row;
	}

	public void setRow(List<row> row) {
		this.row = row;
	}
	 
	

}
