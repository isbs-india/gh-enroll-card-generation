package com.ghpl.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CardBean {
	private String ipid;
	private String insuredName;
	private int age;
	private String gender;
	private String empId;
	private String insurerCardId;
	private String cardId;
	private String insuredInceptionDt;
	private String validFrom;
	private String validTo;
	private String policyCardName;
	private String policyHolderName;
	private String policyFrom;
	private String policyTo;
	private String policyNo;
	private String topupPolicyNo;
	private String dob;
	private String ageFromDob;
	private String relationShip;
	private String uwId;
	private int id;
	private String currentPolicyId;
	private String topupPolicyId;
	private String premium;
	private String topupPremium;
	private String serviceTax;
	private String topupServiceTax;
	private String sumInsured;
	private String topupSumInsured;
	private String bano;
	private String bankAccountNumber;
	private String branchCode;
	private String to1;
	private String to2;
	private String to3;
	private String to4;
	private String to5;
	private String planName;
	private String rno;
	private String mobileNo;
	private String fCount;
	private String insuredEmail;
	private String bloodGroup;
	private String address;
	private String TOPUP_TEXT;   //TOPUP_TEXT
	/*IPID	INSUREDNAME	AGE	GENDER	EMPID	INSURER_CARDID	CARDID	INSUREDINCEPTIONDT	VALIDFROM	VALIDUPTO	
	POLICYCARDNAME	POLICYHOLDERNAME	POLICYFROM	POLICYTO	POLICYNO	TOPUPPOLICYNO	DOB	AGEFROMDOB	RELATIONSHIP	
	UWID	ID	CURRENTPOLICYID	TOPUPPOLICYID	PREMIUM	TOPUPPREMIUM	SERVICETAX	TOPUPSERVICETAX	SUMINSURED	TOPUPSUMINSURED	BANO	
	BANKACCOUNTNUMBER	BRANCHCODE	TO1	TO2	TO3	TO4	TO5	PLANNAME	RNO	MOBILENO	FCOUNT	INSURED_EMAIL	BLOODGROUP	ADDRESS*/
	
	
	public String getIpid() {
		return ipid;
	}
	
	public String getTOPUP_TEXT() {
		return TOPUP_TEXT;
	}

	public void setTOPUP_TEXT(String tOPUP_TEXT) {
		if(TOPUP_TEXT!=null && !TOPUP_TEXT.equals("")){
			TOPUP_TEXT = tOPUP_TEXT;
			}else{
				TOPUP_TEXT="";
			}
	}

	public void setIpid(String ipid) {
		this.ipid = ipid;
	}
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getInsurerCardId() {
		return insurerCardId;
	}
	public void setInsurerCardId(String insurerCardId) {
		this.insurerCardId = insurerCardId;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public String getInsuredInceptionDt() {
		return insuredInceptionDt;
	}
	public void setInsuredInceptionDt(String insuredInceptionDt) {
		this.insuredInceptionDt = insuredInceptionDt;
	}
	public String getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	public String getValidTo() {
		return validTo;
	}
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	public String getPolicyCardName() {
		return policyCardName;
	}
	public void setPolicyCardName(String policyCardName) {
		this.policyCardName = policyCardName;
	}
	public String getPolicyHolderName() {
		return policyHolderName;
	}
	public void setPolicyHolderName(String policyHolderName) {
		this.policyHolderName = policyHolderName;
	}
	public String getPolicyFrom() {
		return policyFrom;
	}
	public void setPolicyFrom(String policyFrom) {
		this.policyFrom = policyFrom;
	}
	public String getPolicyTo() {
		return policyTo;
	}
	public void setPolicyTo(String policyTo) {
		this.policyTo = policyTo;
	}
	public String getPolicyNo() {
		return policyNo;
	}
	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}
	public String getTopupPolicyNo() {
		return topupPolicyNo;
	}
	public void setTopupPolicyNo(String topupPolicyNo) {
		this.topupPolicyNo = topupPolicyNo;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getAgeFromDob() {
		return ageFromDob;
	}
	public void setAgeFromDob(String ageFromDob) {
		this.ageFromDob = ageFromDob;
	}
	public String getRelationShip() {
		return relationShip;
	}
	public void setRelationShip(String relationShip) {
		this.relationShip = relationShip;
	}
	public String getUwId() {
		return uwId;
	}
	public void setUwId(String uwId) {
		this.uwId = uwId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCurrentPolicyId() {
		return currentPolicyId;
	}
	public void setCurrentPolicyId(String currentPolicyId) {
		this.currentPolicyId = currentPolicyId;
	}
	public String getTopupPolicyId() {
		return topupPolicyId;
	}
	public void setTopupPolicyId(String topupPolicyId) {
		this.topupPolicyId = topupPolicyId;
	}
	public String getPremium() {
		return premium;
	}
	public void setPremium(String premium) {
		this.premium = premium;
	}
	public String getTopupPremium() {
		return topupPremium;
	}
	public void setTopupPremium(String topupPremium) {
		this.topupPremium = topupPremium;
	}
	public String getServiceTax() {
		return serviceTax;
	}
	public void setServiceTax(String serviceTax) {
		this.serviceTax = serviceTax;
	}
	public String getTopupServiceTax() {
		return topupServiceTax;
	}
	public void setTopupServiceTax(String topupServiceTax) {
		this.topupServiceTax = topupServiceTax;
	}
	public String getSumInsured() {
		return sumInsured;
	}
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	public String getTopupSumInsured() {
		return topupSumInsured;
	}
	public void setTopupSumInsured(String topupSumInsured) {
		this.topupSumInsured = topupSumInsured;
	}
	public String getBano() {
		return bano;
	}
	public void setBano(String bano) {
		this.bano = bano;
	}
	public String getBankAccountNumber() {
		return bankAccountNumber;
	}
	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}
	public String getBranchCode() {
		return branchCode;
	}
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}
	public String getTo1() {
		return to1;
	}
	public void setTo1(String to1) {
		this.to1 = to1;
	}
	public String getTo2() {
		return to2;
	}
	public void setTo2(String to2) {
		this.to2 = to2;
	}
	public String getTo3() {
		return to3;
	}
	public void setTo3(String to3) {
		this.to3 = to3;
	}
	public String getTo4() {
		return to4;
	}
	public void setTo4(String to4) {
		this.to4 = to4;
	}
	public String getTo5() {
		return to5;
	}
	public void setTo5(String to5) {
		this.to5 = to5;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getRno() {
		return rno;
	}
	public void setRno(String rno) {
		this.rno = rno;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getfCount() {
		return fCount;
	}
	public void setfCount(String fCount) {
		this.fCount = fCount;
	}
	public String getInsuredEmail() {
		return insuredEmail;
	}
	public void setInsuredEmail(String insuredEmail) {
		this.insuredEmail = insuredEmail;
	}
	public String getBloodGroup() {
		return bloodGroup;
	}
	public void setBloodGroup(String bloodGroup) {
		this.bloodGroup = bloodGroup;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	

	
	

}
