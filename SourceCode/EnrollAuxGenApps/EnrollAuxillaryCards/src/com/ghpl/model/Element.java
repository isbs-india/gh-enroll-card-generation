package com.ghpl.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Element{
	private String dataset;
	private String font;
	private int fontsize;
	private String bold;
	private String italic;
	private String underline;
	private String isimage;
	private String xposition;
	private String yposition;
	private String length;
	private String label;
	private String isempty;
	private String issuedOn;
	public String getIssuedOn() {
		return issuedOn;
	}
	public void setIssuedOn(String issuedOn) {
		this.issuedOn = issuedOn;
	}
	public String getDataset() {
		return dataset;
	}
	public void setDataset(String dataset) {
		this.dataset = dataset;
	}
	public String getFont() {
		return font;
	}
	public void setFont(String font) {
		this.font = font;
	}
	public int getFontsize() {
		return fontsize;
	}
	public void setFontsize(int fontsize) {
		this.fontsize = fontsize;
	}
	public String getBold() {
		return bold;
	}
	public void setBold(String bold) {
		this.bold = bold;
	}
	public String getItalic() {
		return italic;
	}
	public void setItalic(String italic) {
		this.italic = italic;
	}
	public String getUnderline() {
		return underline;
	}
	public void setUnderline(String underline) {
		this.underline = underline;
	}
	public String getIsimage() {
		return isimage;
	}
	public void setIsimage(String isimage) {
		this.isimage = isimage;
	}
	public String getXposition() {
		return xposition;
	}
	public void setXposition(String xposition) {
		this.xposition = xposition;
	}
	public String getYposition() {
		return yposition;
	}
	public void setYposition(String yposition) {
		this.yposition = yposition;
	}
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getIsempty() {
		return isempty;
	}
	public void setIsempty(String isempty) {
		this.isempty = isempty;
	}
	
	
	
	
	

}
