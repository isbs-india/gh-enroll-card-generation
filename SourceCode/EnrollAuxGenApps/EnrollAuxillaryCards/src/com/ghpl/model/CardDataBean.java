package com.ghpl.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlRootElement(name = "CardDataBean")

public class CardDataBean {
	private List<String[]> dbrecordsList;
	private List<String> dbheaderNamesList;
	private String lotNumber;
	private String cardFormatType;
	private String cardTempxmlString;
    private String outputFileName;
    private List<EnrollsBean> insurerList = new ArrayList<EnrollsBean>();
    private List<Map<String,String>> certificateRecords;
	
	
	public List<Map<String, String>> getCertificateRecords() {
		return certificateRecords;
	}
	public void setCertificateRecords(List<Map<String, String>> certificateRecords) {
		this.certificateRecords = certificateRecords;
	}
	public List<EnrollsBean> getInsurerList() {
		return insurerList;
	}
	public void setInsurerList(List<EnrollsBean> insurerList) {
		this.insurerList = insurerList;
	}
	public String getCardTempxmlString() {
		return cardTempxmlString;
	}
	public void setCardTempxmlString(String cardTempxmlString) {
		this.cardTempxmlString = cardTempxmlString;
	}
	public String getOutputFileName() {
		return outputFileName;
	}
	public void setOutputFileName(String outputFileName) {
		this.outputFileName = outputFileName;
	}
	public List<String[]> getDbrecordsList() {
		return dbrecordsList;
	}
	@XmlElement
	public void setDbrecordsList(List<String[]> dbrecordsList) {
		this.dbrecordsList = dbrecordsList;
	}
	public List<String> getDbheaderNamesList() {
		return dbheaderNamesList;
	}
	@XmlElement
	public void setDbheaderNamesList(List<String> dbheaderNamesList) {
		this.dbheaderNamesList = dbheaderNamesList;
	}
	public String getLotNumber() {
		return lotNumber;
	}
	@XmlElement
	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}
	public String getCardFormatType() {
		return cardFormatType;
	}
	@XmlElement
	public void setCardFormatType(String cardFormatType) {
		this.cardFormatType = cardFormatType;
	}
	
	
	
	
	

}
