package com.ghpl.action;

import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.ghpl.util.MyProperties;



public class SendMail_preAuth
{
	private static Logger logger=Logger.getLogger("SendMail_preAuth");
	MyProperties myResources;
    String username;
    String password;
    String mail_transport_protocol;
    String mail_smtp_port;
    String mail_smtp_host;
    String mail_smtp_auth;
    String mail_messageText;
    String sec_username;
    String sec_password;
    String sec_mail_transport_protocol;
    String sec_mail_smtp_port;
    String sec_mail_smtp_host;
    String sec_mail_smtp_auth;
    String sec_mail_messageText;
    String bccMailID;
    String sec_bccMailID;
    String mail_from;
    String sec_mail_from;
    
    String fileName=null;
  String filePath=null;
  String FTPHOST=null;
  String FTPUName=null;
  String FTPPwd=null;
  
  String docpath=null;
  String docpath_http=null;
    
	public int SendMail_preauthcron1(String subject, String messageText,String displayfilenme,String TO,String cc,String bcc)
	        throws MessagingException
	    {
		int k=0;  
		    myResources = new MyProperties();
	       try
	        { 
                username = myResources.getMyProperties("mail_username_ecards");
                password = myResources.getMyProperties("mail_password_ecards");
                mail_transport_protocol = myResources.getMyProperties("mail_transport_protocol_ecards");
                mail_smtp_port = myResources.getMyProperties("mail_smtp_port_ecards");
                mail_smtp_host = myResources.getMyProperties("mail_smtp_host_ecards");
                mail_smtp_auth = myResources.getMyProperties("mail_smtp_auth_ecards");
                mail_messageText = myResources.getMyProperties("mail_messageText_ecards");
                mail_from = myResources.getMyProperties("mail_from_ecards");		        
	        
	            Properties props = new Properties();
	            props.put("mail.transport.protocol", mail_transport_protocol);
	            props.put("mail.smtp.port", mail_smtp_port);
	            props.put("mail.smtp.host", mail_smtp_host);
	            props.put("mail.smtp.auth", mail_smtp_auth);
	            props.put("mail.smtp.ssl.trust", "*");
	            props.put("mail.smtp.starttls.enable", "true");
	            Authenticator auth = new SMTPAuthenticator();
	            
	            Session session = Session.getInstance(props, auth);
	            MimeMessage message = new MimeMessage(session);
	            message.setContent(messageText, mail_messageText);
	            message.setSubject(subject);
	            
	            String toarray[] = TO.split(",");
	            
	           // Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
	            Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	            
	            for(int i = 0; i < toarray.length; i++)
	            {
	            	 InternetAddress fromAddress = new InternetAddress(mail_from);
	                // System.out.println("fromAddress"+fromAddress);
	                 
	                 message.setFrom(fromAddress);
	                // System.out.println((new StringBuilder("tomail=")).append(toarray[i]).toString());
	                 //VALIDATING TO EMAILID
	         		//Validating HR MAIL ID
	         		Matcher g=p.matcher(toarray[i]);
	         		//Matcher m=p.matcher(args[0]);
	         		boolean b=g.matches();
	         		if(b==true)
	         		{

	         		//System.out.println("EMAILID:"+toarray[i]);
	                 
	                 //END

	                 InternetAddress toAddress = new InternetAddress(toarray[i]);
	                 message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);
	                 
	                 //CC*******************
	                 String toarraycc[] = cc.split(",");
	                 ArrayList ccemailidarr=new ArrayList();
	                 for(int l = 0; l < toarraycc.length; l++)
	 	             {
	         		Matcher v=p.matcher(toarraycc[l]);
	         		boolean h=v.matches();
	         		if(h==true)
	         		{
	         			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
	       				while (st.hasMoreTokens()) {
	       					ccemailidarr.add(st.nextToken());
	       				}//while
	         		}//if h
	                 
	         		}//for CC
	              int sizecc = ccemailidarr.size(); 
	             // System.out.println("SIZE CC"+sizecc);
	              InternetAddress ccAddress[] = new InternetAddress[sizecc];
		                 for(int m = 0; m < sizecc; m++){
		                     ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());
		                 }
		                 message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);
	                 
	                 //CC END
		                 
		                 //BCC*******************
		                 String toarraybcc[] = bcc.split(",");
		                 ArrayList bccemailidarr=new ArrayList();
		                 for(int l = 0; l < toarraybcc.length; l++)
		 	             {
		         		Matcher vh=p.matcher(toarraybcc[l]);
		         		boolean hh=vh.matches();
		         		if(hh==true)
		         		{
		         			StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
		       				while (st.hasMoreTokens()) {
		       					bccemailidarr.add(st.nextToken());
		       				}//while
		         		}//if h
		                 
		         		}//for BCC
		              int sizebcc = bccemailidarr.size(); 
		             // System.out.println("SIZE CC"+sizecc);
		              InternetAddress bccAddress[] = new InternetAddress[sizebcc];
			                 for(int bm = 0; bm < sizebcc; bm++){
			                	 bccAddress[bm] = new InternetAddress(bccemailidarr.get(bm).toString());
			                 }
			                 message.setRecipients(javax.mail.Message.RecipientType.BCC, bccAddress);
		                 //BCC END  
	                BodyPart messageBodyPart = new MimeBodyPart();
	                messageBodyPart.setContent(messageText, mail_messageText);
	                Multipart multipart = new MimeMultipart();
	                multipart.addBodyPart(messageBodyPart);
	               // messageBodyPart = new MimeBodyPart();
	               // DataSource source = new FileDataSource(attachementPath);
	                //
	               //String filepath="/root/MyCrons/PreAuthTATExcelNew/"+displayfilenme;
	               //String filepath=SendMail_preAuth.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/transaction/"+displayfilenme;
		            //url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/PREAUTH/"+attachementname); 
	               // FileDataSource source = new FileDataSource(filepath);
	                //messageBodyPart.setDataHandler(new DataHandler(source));
	                //messageBodyPart.setFileName(displayfilenme);
	                //multipart.addBodyPart(messageBodyPart);
	                message.setContent(multipart);
	                
	                //END DOCS ATTACHMENT
	             
	             // System.out.println("BEFORE SENDING MSG");
	              
	                  Transport.send(message);
	             
	                   // System.out.println("k"+k);
	                    
	                    k=1;
	         		}//if b is true
	            }//for  LOOP FOR TO EMAILID

	        }
	        catch(Exception ex)
	        {
	            logger.error((new StringBuilder("Error in sendmail trying with the Primary configuratiion ::")).append(ex).toString());
	            k=2;
	        }
	        return k;
	    }
	
	
	public int preauth_InsurerMails(String subject, String messageText, String displayfilenme,String TO,String cc)
			throws MessagingException
			{
		
		int k=0;  
	    myResources = new MyProperties();
       try
        { 
            username = myResources.getMyProperties("mail_username");
            password = myResources.getMyProperties("mail_password");
            mail_transport_protocol = myResources.getMyProperties("mail_transport_protocol");
            mail_smtp_port = myResources.getMyProperties("mail_smtp_port");
            mail_smtp_host = myResources.getMyProperties("mail_smtp_host");
            mail_smtp_auth = myResources.getMyProperties("mail_smtp_auth");
            mail_messageText = myResources.getMyProperties("mail_messageText");
            mail_from = myResources.getMyProperties("mail_from");		        
        
            Properties props = new Properties();
            props.put("mail.transport.protocol", mail_transport_protocol);
            props.put("mail.smtp.port", mail_smtp_port);
            props.put("mail.smtp.host", mail_smtp_host);
            props.put("mail.smtp.auth", mail_smtp_auth);
            props.put("mail.smtp.ssl.trust", "*");
            props.put("mail.smtp.starttls.enable", "true");
            Authenticator auth = new SMTPAuthenticator();
            
            Session session = Session.getInstance(props, auth);
            MimeMessage message = new MimeMessage(session);
            message.setContent(messageText, mail_messageText);
            message.setSubject(subject);
            Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
            String toarray[] = TO.split(";");
            ArrayList toemailidarr=new ArrayList();
            String tempEmailId="";
            for(int i = 0; i < toarray.length; i++)
            {
            	String emailId="";
            	if(toarray[i]!=null && !toarray[i].equals("NA") && !toarray[i].equals("0")){
            		emailId=toarray[i];
            	}
            	if(!tempEmailId.equals(emailId)){
            	 InternetAddress fromAddress = new InternetAddress(mail_from);
                 message.setFrom(fromAddress);
         		Matcher g=p.matcher(toarray[i]);
         		boolean b=g.matches();
         		if(b==true)
         		{
         			StringTokenizer stto = new StringTokenizer(toarray[i],",");
      				while (stto.hasMoreTokens()) {
      					toemailidarr.add(stto.nextToken());
      				}//while
            	}
         		tempEmailId=toarray[i];
            	}
                	                
                 
        //CC*******************
                 String toarraycc[] = cc.split(";");
                 ArrayList ccemailidarr=new ArrayList();
                 for(int l = 0; l < toarraycc.length; l++)
 	             {
	         		Matcher v=p.matcher(toarraycc[l]);
	         		boolean h=v.matches();
	         		if(h==true)
	         		{
	         			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
	       				while (st.hasMoreTokens()) {
	       					ccemailidarr.add(st.nextToken());
	       				}//while
	         		}//if h
	                 
	         		}//for CC
	              int sizecc = ccemailidarr.size(); 
	             // System.out.println("SIZE CC"+sizecc);
	              InternetAddress ccAddress[] = new InternetAddress[sizecc];
	                 for(int m = 0; m < sizecc; m++){
	                     ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());
	                 }
	                 message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);
       //CC END
	                 
	    
          }
             
		   BodyPart messageBodyPart = new MimeBodyPart();
		   messageBodyPart.setContent(messageText, mail_messageText);
		   Multipart multipart = new MimeMultipart();
		   multipart.addBodyPart(messageBodyPart);
		   messageBodyPart.setFileName(displayfilenme);
		   multipart.addBodyPart(messageBodyPart);
		   message.setContent(multipart);      
           Transport.send(message);
           k=1;
            
        }
        catch(Exception ex)
        {
            logger.error((new StringBuilder("Error in sendmail trying with the Primary configuratiion ::")).append(ex).toString());
            k=2;
        }
        return k;
		
		
		
		
		
				/*int k=0;  
				    myResources = new MyProperties();
			       try
			        { 
		                username = myResources.getMyProperties("mail_username");
		                password = myResources.getMyProperties("mail_password");
		                mail_transport_protocol = myResources.getMyProperties("mail_transport_protocol");
		                mail_smtp_port = myResources.getMyProperties("mail_smtp_port");
		                mail_smtp_host = myResources.getMyProperties("mail_smtp_host");
		                mail_smtp_auth = myResources.getMyProperties("mail_smtp_auth");
		                mail_messageText = myResources.getMyProperties("mail_messageText");
		                mail_from = myResources.getMyProperties("mail_from");		        
			        
			            Properties props = new Properties();
			            props.put("mail.transport.protocol", mail_transport_protocol);
			            props.put("mail.smtp.port", mail_smtp_port);
			            props.put("mail.smtp.host", mail_smtp_host);
			            props.put("mail.smtp.auth", mail_smtp_auth);
			            props.put("mail.smtp.ssl.trust", "*");
			            props.put("mail.smtp.starttls.enable", "true");
			            Authenticator auth = new SMTPAuthenticator();
			            
			            Session session = Session.getInstance(props, auth);
			            MimeMessage message = new MimeMessage(session);
			            message.setContent(messageText, mail_messageText);
			            message.setSubject(subject);
			            
			            String toarray[] = TO.split(",");
			            
			           // Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			            Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
			 for(int i = 0; i < toarray.length; i++)
	            {
	            	 InternetAddress fromAddress = new InternetAddress(mail_from);
	                // System.out.println("fromAddress"+fromAddress);
	                 
	                 message.setFrom(fromAddress);
	                // System.out.println((new StringBuilder("tomail=")).append(toarray[i]).toString());
	                 //VALIDATING TO EMAILID
	         		//Validating HR MAIL ID
	         		Matcher g=p.matcher(toarray[i]);
	         		//Matcher m=p.matcher(args[0]);
	         		boolean b=g.matches();
	         		if(b==true)
	         		{

	         		//System.out.println("EMAILID:"+toarray[i]);
	                 
	                 //END

	                 InternetAddress toAddress = new InternetAddress(toarray[i]);
	                 message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);
	                 
	                 //CC*******************
	                 String toarraycc[] = cc.split(",");
	                 ArrayList ccemailidarr=new ArrayList();
	                 for(int l = 0; l < toarraycc.length; l++)
	 	             {
	         		Matcher v=p.matcher(toarraycc[l]);
	         		boolean h=v.matches();
	         		if(h==true)
	         		{
	         			StringTokenizer st = new StringTokenizer(toarraycc[l],",");
	       				while (st.hasMoreTokens()) {
	       					ccemailidarr.add(st.nextToken());
	       				}//while
	         		}//if h
	                 
	         		}//for CC
	              int sizecc = ccemailidarr.size(); 
	             // System.out.println("SIZE CC"+sizecc);
	              InternetAddress ccAddress[] = new InternetAddress[sizecc];
		                 for(int m = 0; m < sizecc; m++){
		                     ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());
		                 }
		                 message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);
	                 
	                 //CC END
		                 
		                 //BCC*******************
		                 String toarraybcc[] = bcc.split(",");
		                 ArrayList bccemailidarr=new ArrayList();
		                 for(int l = 0; l < toarraybcc.length; l++)
		 	             {
		         		Matcher vh=p.matcher(toarraybcc[l]);
		         		boolean hh=vh.matches();
		         		if(hh==true)
		         		{
		         			StringTokenizer st = new StringTokenizer(toarraybcc[l],",");
		       				while (st.hasMoreTokens()) {
		       					bccemailidarr.add(st.nextToken());
		       				}//while
		         		}//if h
		                 
		         		}//for BCC
		              int sizebcc = bccemailidarr.size(); 
		             // System.out.println("SIZE CC"+sizecc);
		              InternetAddress bccAddress[] = new InternetAddress[sizebcc];
			                 for(int bm = 0; bm < sizebcc; bm++){
			                	 bccAddress[bm] = new InternetAddress(bccemailidarr.get(bm).toString());
			                 }
			                 message.setRecipients(javax.mail.Message.RecipientType.BCC, bccAddress);
		                 //BCC END  
	                BodyPart messageBodyPart = new MimeBodyPart();
	                messageBodyPart.setContent(messageText, mail_messageText);
	                Multipart multipart = new MimeMultipart();
	                multipart.addBodyPart(messageBodyPart);
	               // messageBodyPart = new MimeBodyPart();
	               // DataSource source = new FileDataSource(attachementPath);
	                //
	               //String filepath="/root/MyCrons/PreAuthTATExcelNew/"+displayfilenme;
	               //String filepath=SendMail_preAuth.class.getProtectionDomain().getCodeSource().getLocation().getPath()+"/transaction/"+displayfilenme;
		            //url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/PREAUTH/"+attachementname); 
	               // FileDataSource source = new FileDataSource(filepath);
	                //messageBodyPart.setDataHandler(new DataHandler(source));
	                messageBodyPart.setFileName(displayfilenme);
	                multipart.addBodyPart(messageBodyPart);
	                message.setContent(multipart);
	                
	                //END DOCS ATTACHMENT
	             
	             // System.out.println("BEFORE SENDING MSG");
	              
	                  Transport.send(message);
	             
	                   // System.out.println("k"+k);
	                    
	                    k=1;
	         		}//if b is true
	            }//for  LOOP FOR TO EMAILID

		}
		catch(Exception ex)
		{
			logger.error("Error in sending mail to Insurer approvals ::"+ex);

			k=1;
		}
		return k;*/
			} 




	/*public int preauth_InsurerMails_Denial(String subject, String messageText, String attachementname,String displayfilenme,String TO,String cc,ArrayList arr,int moduleId)
			throws MessagingException
			{

		int k=0;  
		URL url=null;

		MyProperties Gdbp = new MyProperties();
		username = Gdbp.getMyProperties("preauth_username");
		password = Gdbp.getMyProperties("preauth_password");
		mail_from = Gdbp.getMyProperties("preauth_from");
		// mail_transport_protocol = myResources.getString("preauth_transport_protocol");
		mail_smtp_port = Gdbp.getMyProperties("preauth_smtp_port");
		mail_smtp_host = Gdbp.getMyProperties("preauth_smtp_host");
		mail_smtp_auth = Gdbp.getMyProperties("preauth_smtp_auth");
		mail_messageText = Gdbp.getMyProperties("preauth_messageText");
		preauthBcc = Gdbp.getMyProperties("preauth_bcc");


		try
		{
			Properties props = new Properties();
			props.put("mail.smtp.auth", mail_smtp_auth);
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host",  mail_smtp_host);
			props.put("mail.smtp.port", mail_smtp_port);


			Session session = Session.getInstance(props,new javax.mail.Authenticator() {
				protected PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(username, password);
				}
			});




			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mail_from)); 
			message.setText(messageText);
			message.setContent(messageText, mail_messageText);
			message.setSubject(subject);

			String toarray[] = TO.split(",");

			//Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
			Pattern p=Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");

			for(int i = 0; i < toarray.length; i++)
			{

				InternetAddress fromAddress = new InternetAddress(mail_from);


				message.setFrom(fromAddress);




				//VALIDATING TO EMAILID


				//Validating HR MAIL ID
				Matcher g=p.matcher(toarray[i]);
				//Matcher m=p.matcher(args[0]);
				boolean b=g.matches();

				if(b==true)
				{

					logger.info("EMAILID:"+toarray[i]);

					//END

					InternetAddress toAddress = new InternetAddress(toarray[i]);
					message.setRecipient(javax.mail.Message.RecipientType.TO, toAddress);


					 CC MAIL  
					String toarraycc[] = cc.split(",");
					ArrayList ccemailidarr=new ArrayList();



					for(int l = 0; l < toarraycc.length; l++)
					{

						logger.info("ccmail="+toarraycc[l]);
						//VALIDATING TO EMAILID


						//Validating HR MAIL ID
						Matcher v=p.matcher(toarraycc[l]);
						//Matcher m=p.matcher(args[0]);
						boolean h=v.matches();

						if(h==true)
						{

							//System.out.println("CC EMAILID:"+toarraycc[l]);

							//END


							StringTokenizer st = new StringTokenizer(toarraycc[l],",");

							while (st.hasMoreTokens()) {

								ccemailidarr.add(st.nextToken());

							}//while


						}//if h

					}//for CC

					 END CC EMAIL 	 


					int sizecc = ccemailidarr.size(); 

					logger.info("SIZE CC"+sizecc);

					InternetAddress ccAddress[] = new InternetAddress[sizecc];
					for(int m = 0; m < sizecc; m++)
						ccAddress[m] = new InternetAddress(ccemailidarr.get(m).toString());


					message.setRecipients(javax.mail.Message.RecipientType.CC, ccAddress);

					message.setRecipients(Message.RecipientType.BCC,InternetAddress.parse(preauthBcc));


					BodyPart messageBodyPart = new MimeBodyPart();
					messageBodyPart.setContent(messageText, mail_messageText);
					Multipart multipart = new MimeMultipart();
					multipart.addBodyPart(messageBodyPart);
					messageBodyPart = new MimeBodyPart();
					// DataSource source = new FileDataSource(attachementPath);
					//



					//ATTCHMENTS OF DOCS UPLOADED FROM ARRAY


					int index=0;
					String ftp_http_FileName;
					String fileName;

					while(index < arr.size())
					{
						HashMap data=(HashMap) arr.get(index);

						messageBodyPart = new MimeBodyPart();

						//DECIDING WHETHER TO DOWNLOAD FILE FROM FTP (IAUTH DOC) OR HTTP (MODULE UPLOADED DOC) based on FTP FLAG
						ftp_http_FileName="";
						fileName="";
						ftp_http_FileName=data.get("id"+index).toString();

						String MyFileNameArr[]=ftp_http_FileName.split("~");


						if("1".equals(MyFileNameArr[1]))
						{

							fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
							String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());

							// System.out.println("filename FTP:"+fileName);
							// System.out.println("extension FTP:"+extension); 
							// System.out.println("FTP_IAUTH_PATH:"+FTP_IAUTH_PATH);

							url = new URL("ftp://"+FTPUName+":"+FTPPwd+"@"+FTPHOST+"/"+FTP_IAUTH_PATH+MyFileNameArr[0]); 
							// System.out.println("FTPurl:"+url);
						}//IF FTP FILE
						else
						{

							url = new URL(docpath_http+""+MyFileNameArr[0]);

							fileName=MyFileNameArr[0].substring(fileName.lastIndexOf("/")+1,MyFileNameArr[0].length());
							String extension=MyFileNameArr[0].substring(MyFileNameArr[0].lastIndexOf(".")+1,MyFileNameArr[0].length());


							// System.out.println("filename HTTP:"+fileName);
							// System.out.println("extension HTTP:"+extension); 
							//System.out.println("HTTPurl:"+url);


						}

						logger.info("url:"+url); 


						URLDataSource sourcedocs = new URLDataSource(url);
						messageBodyPart.setDataHandler(new DataHandler(sourcedocs));
						//messageBodyPart.setFileName(fileName);
						messageBodyPart.setFileName(MyFileNameArr[2]);

						multipart.addBodyPart(messageBodyPart);

						//END

						// message.setContent(multipart);

						index++;
					}//while


					message.setContent(multipart);

					//END DOCS ATTACHMENT


					 System.out.println("BEFORE SENDING MSG");

					Transport.send(message); 
					 System.out.println("AFTER SENDING MSG");
					k=0;
				

				}//if b is true
			}//for

		}
		catch(Exception ex)
		{
			// System.out.println((new StringBuilder("Error in PreAuth Denial Mail to insurer ::")).append(ex).toString());
			logger.error((new StringBuilder("Error in sendmail from SendtoInsurer for DENIAL ::")).append(ex).toString());
			k=1;
		}
		return k;
			} */
	
	
	/**
	 * The Class SMTPAuthenticator.
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, password);
		}
	}
	
	/**
	 * The Class Sec_SMTPAuthenticator.
	 */
	private class Sec_SMTPAuthenticator extends javax.mail.Authenticator {
		
		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(sec_username, sec_password);
		}
	}
	
	
   
}