package com.ghpl.action;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPReply;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

import com.ghpl.model.Card;
import com.ghpl.model.CardDataBean;
import com.ghpl.model.CardJsonBean;
import com.ghpl.model.Element;
import com.ghpl.model.EnrollsBean;
import com.ghpl.model.InsertLotDetailsBean;
import com.ghpl.model.row;
import com.ghpl.persistence.EnrollCardsManager;
import com.ghpl.util.Constants;
import com.ghpl.util.MyProperties;
import com.ghpl.util.NumberToWords;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;




public class EnrollAuxillaryCards {
	protected static Logger logger;
	static MyProperties myResources = null;
		/*public static void main(String[] args) throws Exception {
			 MyProperties myResources = new MyProperties();
		     logger = myResources.getLogger();
		     String imgpath = myResources.getMyProperties("ANDHRABANKCARDS");
		     String uploadslocal = myResources.getMyProperties("uploadslocal");
		     String downloadslocal = myResources.getMyProperties("downloadslocal");
		   
		     String srcFolder= "";
		     int status=0;
		     logger.info("=============JOB Start FOR  Bank Cards==============");
			 try {  
				 	System.out.println("=============JOB Start FOR  Bank Cards==============");
				 	InsertLotDetailsBean insertLotDetailsBean =new InsertLotDetailsBean();
				 	insertLotDetailsBean = EnrollCardsManager.getLotDetails(1, 2);
				 	
				 	CardDataBean cardDataBean= new CardDataBean();
				 	
				 	//if(insertLotDetailsBean!=null){
					 	String cardFormat = "E";//insertLotDetailsBean.getCardFormat();
					 	int lotNumber = 6110;
					 	System.out.println("lotNumber   "+lotNumber);
					 	srcFolder=uploadslocal+lotNumber+"_Corp";
				 		cardDataBean = EnrollCardsManager.getEnrollCardDetails(lotNumber,1);
					 	//cardDataBean = EnrollCardsManager.getEnrollCardDetails(lotNumber,1);
					 	List<EnrollsBean> trackCardList=null;
				 	if(cardDataBean.getDbrecordsList().size()>0){
				 		CardJsonBean cjb=null;
				 		 File theDir = new File(srcFolder);
						  // this code is per card downloads per web method   start
						 
						   if (theDir.exists()) {
							  theDir.delete();
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }else{
							  theDir = new File(srcFolder);
							  theDir.mkdir();
						  }
						
				 			   
				 			// this code is per card downloads per web method   end
				 			if(cardFormat.equals("E")){
				 				 theDir = new File(srcFolder);
								  if (theDir.exists()) {
									  theDir.delete();
									  theDir = new File(srcFolder);
									  theDir.mkdir();
								  }else{
									  theDir = new File(srcFolder);
									  theDir.mkdir();
								  }
				 				generateECardFormat(cardDataBean, srcFolder+File.separator+lotNumber);
				 				//System.out.println("GenInsertStausFlag  :"+insertLotDetailsBean.getGenInsertStausFlag());
				 				if(insertLotDetailsBean.getGenInsertStausFlag()==1){
				 					 trackCardList =new ArrayList<EnrollsBean>(); 
									  String tempBano="";
									  int serialNo=0;
						        		 for(EnrollsBean enroleBean: cardDataBean.getInsurerList()){
						        			 if(enroleBean!=null){
						        			//tempBano = enroleBean.getBano();
						        				if(tempBano.equals(enroleBean.getBANO())){
						        					tempBano=""+enroleBean.getBANO();
						        					enroleBean.setSerialNo(++serialNo);
							        				 trackCardList.add(enroleBean);
							        			}else{
							        				if(trackCardList.size()>0){
							        					File theDir1 = new File(srcFolder);
														  if (!theDir1.exists()) {
															  theDir1.mkdir();
														  }
															 
														  int insertStatusFlag=insertLotDetailsBean.getGenInsertStausFlag();
														  if(insertStatusFlag==1){
															  generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
														  }
														  serialNo=0;
							        				}
							        				 trackCardList=null;
							        				 if(enroleBean.getBANO()!=null){
								        				 trackCardList =new ArrayList<EnrollsBean>();
								        				 tempBano=enroleBean.getBANO();
								        				 enroleBean.setSerialNo(++serialNo);
								        				 trackCardList.add(enroleBean);
							        				 }
							        				}
						        			 }//enroleBean null condition
						        		 }
						        		 if(trackCardList.size()>0){
											generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
					        			 }
				 					//generateInsuranceCertificate(cardDataBean, srcFolder+File.separator+"Insurance_Cert_Lot_");
				 				}
				 			}else if(cardFormat.equals("P")){
				 				 theDir = new File(srcFolder);
								  if (theDir.exists()) {
									  theDir.delete();
									  theDir = new File(srcFolder);
									  theDir.mkdir();
								  }else{
									  theDir = new File(srcFolder);
									  theDir.mkdir();
								  }
				 				//generatePhycalCardFormat(cardDataBean, srcFolder+File.separator+"ABCards_Lot");
								  System.out.println("gen_ins_status : "+insertLotDetailsBean.getGenInsertStausFlag());
				 				if(insertLotDetailsBean.getGenInsertStausFlag()==1){
				 					trackCardList =new ArrayList<EnrollsBean>(); 
									  String tempBano="";
									  int serialNo=0;
						        		 for(EnrollsBean enroleBean: cardDataBean.getInsurerList()){
						        			 if(enroleBean!=null){
						        			//tempBano = enroleBean.getBano();
						        				if(tempBano.equals(enroleBean.getBANO())){
						        					tempBano=""+enroleBean.getBANO();
						        					enroleBean.setSerialNo(++serialNo);
							        				 trackCardList.add(enroleBean);
							        			}else{
							        				if(trackCardList.size()>0){
							        					File theDir1 = new File(srcFolder);
														  if (!theDir1.exists()) {
															  theDir1.mkdir();
														  }
														  int insertStatusFlag=insertLotDetailsBean.getGenInsertStausFlag();
														  if(insertStatusFlag==1){
															  generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
														  }
														 
							        				}
							        				 trackCardList=null;
							        				 if(enroleBean.getBANO()!=null){
								        				 trackCardList =new ArrayList<EnrollsBean>();
								        				 tempBano=enroleBean.getBANO();
								        				 enroleBean.setSerialNo(++serialNo);
								        				 trackCardList.add(enroleBean);
							        				 }
							        				}
						        			 }//enroleBean null condition
						        		 }
						        		 if(trackCardList.size()>0){
											generateInsuranceCertificate(trackCardList, srcFolder+File.separator+"Insurance_Cert_Lot_");
					        			 }
				 				}
				 			}else if(cardFormat.equals("V")){
				 				//generateVendorDetails(cardDataBean, uploadslocal,contactEmail,userName);
				 			}
				        }
				 
				 	
				 	
				 	
				 	// commit start when open this commit
				 	
				 	else{

				        	File theDir = new File(srcFolder);
			 				 if (theDir.exists()) {
								  theDir.delete();
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }else{
								  theDir = new File(srcFolder);
								  theDir.mkdir();
							  }
				        	
				        	try{
								 Document document = new Document(PageSize.A4);
						    	 Rectangle pagesize = new Rectangle(216f, 720f);
						         // step 2
						    	// if(i==1){
						        // PdfWriter.getInstance(document, new FileOutputStream(srcFolder+File.separator+"CORPCards_"+"Exception.pdf"));
						    	// }else if(i==2){
						    		 PdfWriter.getInstance(document, new FileOutputStream(srcFolder+File.separator+"ABCards_"+"Exception.pdf"));
						    		
						    	// }
						         // step 3
						         document.open();
						         // step 4
						         document.add(new Paragraph(myResources.getMyProperties("NODataFound")));
						         // step 5
						         document.close();
								}catch(Exception ex){ex.printStackTrace();}
				        	
				        	//write zip file from command fromt using unix or linux command
			 				String destZipFile= "";
				        	
			 				 File file = new File(srcFolder);
								if (file.exists() && file.isDirectory()) {
							  
							 String command ="zip -rj " + destZipFile +  " " + srcFolder;
							  //System.out.println("command : "+command);
							  logger.info(command);
							  try {
							      Process proc = Runtime.getRuntime().exec(command);
							      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
							      String line = null;
						            while ((line = in.readLine()) != null) {
						                //System.out.println(line);
						            }
						             status =5;
							      //System.out.println("status : "+status);
							      logger.info("status :"+status);
							   } catch (Exception e) {
								   status=4;
								   logger.error("status :"+status);
							      e.printStackTrace();
							      logger.error(e);
							   }
				        	
				        	logger.info("before calling put lot details  per Ecards lotnumber : "+lotNumber);
			 				logger.info("status   "+5);
			 			
				        	
				        
				 		}
				 
				        }
				 	
				 		// commit start when open this commit
				 	
				 	
				 //}
				 	System.out.println("=============JOB END FOR  Bank Cards==============");
				 	logger.info("=============JOB END FOR  Bank Cards==============");
			      } catch (Exception e) {  
			        e.printStackTrace();  
			      }  
		}*/
		
		
		public static void  generateECardFormat(CardDataBean cardDataBean,String lotNumber)
		{

			  myResources = new MyProperties();
		     // logger = myResources.getLogger();
		     Logger logger =(Logger) Logger.getInstance("generateECardFormat");
		     logger.info("=============JOB Start FOR generateECardFormat ==============");
		     String insurencetext = myResources.getMyProperties("insurencetext");
			System.out.println("enter ecard format");
			//  String fileCreatepath = myResources.getMyProperties("uploadslocal");
			try{
				int recordlistSize=0;
				if(cardDataBean.getDbrecordsList()!=null){
					recordlistSize=cardDataBean.getDbrecordsList().size();
				}
				 
				 String imgpath = myResources.getMyProperties("ANDHRABANKCARDS");
				// String frontImage = myResources.getMyProperties("ANDHRABANKFRONTCARD");
				 String photopath = myResources.getMyProperties("photopath");
				 String imgpathextn = myResources.getMyProperties("ANDHRABANKCARDSEXTN");
				 String ERRORPDFTEXT = myResources.getMyProperties("IMAGEERRORPDFTEXT");
				 if(recordlistSize>0){
					 Image image = null;
				    String tempfamilyId="";
				    Card cardTemp=null;
			    	  String XMLObj =""; 
			    	  String  IMAGE = null;
				    List<String[]> records =new LinkedList<String[]>();
			    	  for(int i=0;i<recordlistSize;i++){
			    		  String[] card =cardDataBean.getDbrecordsList().get(i);
			    		 /*
			    		  * newly added code checking front image is available or not
			    		  */
			    		  String[] recordcard =null;;
			    		  recordcard = card;
						  if(image==null){
				    	  for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
				    		  if(cardDataBean.getDbheaderNamesList().get(x).equals("ECARD_TEMPLATE")){
				    			  XMLObj= recordcard[x];
				    			  break;
				    		  }
				          }
				    	  if(XMLObj!=null){
				    	  StringReader sr = new StringReader(XMLObj+"");
				    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
				    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
				    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
				    	  }
				    	 
				    	 String  cardfrontimage1=cardTemp.getCardfrontimage();
				    	 IMAGE = imgpath+cardfrontimage1;
						    if(IMAGE!=null){
						    	try{
						     image = Image.getInstance(IMAGE);
						    }catch(Exception e){
						    	 Document document = new Document(PageSize.A4);
						    	 Rectangle pagesize = new Rectangle(216f, 720f);
						         
						         // step 2
						         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
						         // step 3
						         document.open();
						         // step 4
						         document.add(new Paragraph(ERRORPDFTEXT));
						             
						         // step 5
						         document.close();
						         return;
						    }
						    }
						  }
			    		  /*  newly added code end*/
			    		  if((card!=null) && (image!=null)){
					    	  String familyId = card[0];
					    	  if(tempfamilyId.equals(familyId)){
					    		  	tempfamilyId=card[0];
					    		  	records.add(card);
					    	  }else{
								    if(records.size()>0){
								    	 Document document = new Document(PageSize.A4);
								    	 /* Updated the code on 03AUG2019 for rectifying CARDNAME when retrievng from card service (GCM-1036) */
										  PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"_"+tempfamilyId+".pdf"));
								    	  //  PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"_ECard.pdf"));
										  document.open();
									      PdfContentByte cb = writer.getDirectContent();
									      cb.beginText();
									      PdfPTable table = new PdfPTable(1);
							            	table.setTotalWidth(527);
							                table.setLockedWidth(true);
							                table.getDefaultCell().setBorder(1);
										    PdfPCell cellinsurencetext = new PdfPCell(new Phrase(insurencetext,  new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK)));
										    cellinsurencetext.setBorder(Rectangle.NO_BORDER);
										    table.addCell(cellinsurencetext);
									      
									      /*  float bottom_margin =83.0f;
									        float xPoss=55f;
								            float yPoss=808.0f;
										    float yPossImage = 843.0f;
										    float xPossImage = 20;*/
										    float bottom_margin =0;
										    float xPoss=0;
									        float xPossLeft=0;
									        float xPossRight=0;
									        float yPossNextRow=0;
								            float yPoss=0;
										    float yPossImage = 0;
										    float xPossImage = 0;
										     IMAGE = null;
										     //image = null;
										    float tableTotalWidth=0;
										    float tableFixedHeight=0;
								    	for(int j=0;j<records.size();j++){
								    	  String cardtemplatename="";
										 // String cardfrontimage="";
										  //String cardbackimage="";
										   recordcard =records.get(j);
										  
										  /* cardTemp=null;
								    	   XMLObj =""; 
								    	  for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
								    		  if(cardDataBean.getDbheaderNamesList().get(x).equals("ECARD_TEMPLATE")){
								    			  XMLObj= recordcard[x];
								    			  break;
								    		  }
								          }
								    	  if(XMLObj!=null){
								    	  StringReader sr = new StringReader(XMLObj+"");
								    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
								    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
								    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
								    	  }*/
								    	  int rowSize=0;
											if(cardTemp!=null){
												cardtemplatename=cardTemp.getCardtemplatename();
												//cardfrontimage=cardTemp.getCardfrontimage();
												//cardbackimage=cardTemp.getCardbackimage();
												bottom_margin =  Float.parseFloat(cardTemp.getEbottomMargin());
												xPossLeft  = Float.parseFloat(cardTemp.getEtemplatexpossleft());
												xPossRight = Float.parseFloat(cardTemp.getEtemplatexpossright());
												yPossNextRow = Float.parseFloat(cardTemp.getEtemplateypossnextrow());
												yPoss = Float.parseFloat(cardTemp.getEtemplateyposs());
												tableTotalWidth= Float.parseFloat(cardTemp.getEtableTotalWidth());
												tableFixedHeight=Float.parseFloat(cardTemp.getEtableFixedHeight());
												rowSize = cardTemp.getRow().size();
												   /* IMAGE = imgpath+cardfrontimage;
												    if(IMAGE!=null){
												    	try{
												     image = Image.getInstance(IMAGE);
												    }catch(Exception e){
												    	IMAGE=imgpath+"images.png";
												    	image = Image.getInstance(IMAGE);
												    }
												    }*/
											        image.setBorder(0);
											}
										  
										  
								    	  PdfPTable backgroundImageTable = new PdfPTable(1);
								    		backgroundImageTable.setWidths(new float[]{0.43f});
										    backgroundImageTable.setSplitLate(false);
										    backgroundImageTable.setTotalWidth(275);
										    backgroundImageTable.setLockedWidth(true);
										    backgroundImageTable.getDefaultCell().setBorder(0);
										    backgroundImageTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
										    backgroundImageTable.setSpacingBefore(100f);
										    backgroundImageTable.getDefaultCell().setFixedHeight(170.2f);
										    backgroundImageTable.setWidthPercentage(100);
										    backgroundImageTable.getDefaultCell().setPadding(5);
										    
										    PdfPTable dataTable = new PdfPTable(4);
										    dataTable.setWidths(new float[]{1.6f,1.3f,1f,1.5f});
										    dataTable.setSplitLate(false);
										   // dataTable.setTotalWidth(200f);
										    dataTable.setTotalWidth(tableTotalWidth);
										    dataTable.setLockedWidth(true);
										    dataTable.getDefaultCell().setBorder(0);
										    dataTable.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
										    dataTable.setSpacingBefore(100f);
										   // dataTable.getDefaultCell().setFixedHeight(160.2f);
										    dataTable.getDefaultCell().setFixedHeight(tableFixedHeight);
										    PdfPCell cell;
										   
												 for(int k=0;k<rowSize;k++){
													 row rowObj = cardTemp.getRow().get(k);
												int elementSize = 	rowObj.getElement().size(); 
												Element element= null;
												String outMessage="";
												String outMessageelementSize4label="";
												 String outMessageelementSize4dataset="";
												for(int l=0;l<elementSize;l++){	 
													String label="";
													Object dataset="";
													element = rowObj.getElement().get(l);
												 if(element.getIsimage().equals("y")){
													 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
											        		 dataset = recordcard[x];
											        		 break;
														  }
										        	 }
													 
													  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
													 
													 String url= null; 
													 String imgEx=null;
													 Image imageSign=null;
													 try{
													 url= photopath+dataset+imgpathextn;
													 if(url!=null){
														 imgEx = getMimeType(url);
													 }
													 }catch(Exception e){
											        	 e.printStackTrace();
											        	 if(imgEx==null){
												    			url=imgpath+"images.png";
											    			}
											         }
													 if(imgEx==null){
											    			url=imgpath+"images.png";
										    		 }
								                	 imageSign= Image.getInstance(url);
								                	 imageSign.scaleToFit(10f,50f);
								                	 cell = new PdfPCell(imageSign,true);
								                	
								                	
										  		     cell.setBorder(Rectangle.NO_BORDER);
										  		     cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
										  		     cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
										  		     cell.setColspan(4);
										  		     cell.setRowspan(rowSize-1);
												     //cell.setPadding(0);
												     dataTable.addCell(cell);
										 	         
										 	         
										         }else{
										        	  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
										        	 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
										        			 if(recordcard[x]!=null){
											        			 dataset = recordcard[x];
											        		 }else  if(recordcard[x]==null){
										        				 dataset=" ";
										        			 }else if(recordcard[x].equals("")){
										        				 dataset=" ";
										        			 }else {
										        				 dataset="NA";
										        			 }
											        		 break;
														  }
										        	 }
										        	/* if(element.getBold().equals("y")){
														 label = element.getLabel().toUpperCase();
													 }*/
										        	 
										        	     String font ="";
										        		 int fontsize=0;
										        		 int element4labelfontType=0;
										        		 String bold="";
										        		 String italic="";
										        		 String underline="";
										        		 int fontType= 0;
										        		 Font.FontFamily fontText= null;
										        	  	 if(element.getFont()!=null){
										        	  		 font = element.getFont().toUpperCase();
										        	  		 switch (font) {
										        	  	         case "HELVETICA":
										        	  	        	fontText = Font.FontFamily.HELVETICA;
										        	  	             break;
										        	  	         case "COURIER":
										        	  	        	fontText = Font.FontFamily.COURIER;
										        	  	             break;
										        	  	         case "TIMES_ROMAN":
										        	  	        	fontText = Font.FontFamily.TIMES_ROMAN;
										        	  	             break;
										        	  	         case "UNDEFINED":
										        	  	        	fontText = Font.FontFamily.UNDEFINED;
										        	  	             break;
										        	  	          case "SYMBOL":
										        	  	        	fontText = Font.FontFamily.SYMBOL;
										        	  	             break;
										        	  	          case "ZAPFDINGBATS":
										        	  	        	fontText = Font.FontFamily.ZAPFDINGBATS;
										        	  	             break;
										        	  	           default : fontText = Font.FontFamily.HELVETICA;
										        	  	     }
										        	  		
														 }
										        	  	 if(element.getBold().equals("y")){
										        	  		 bold = element.getBold().toUpperCase();
										        	  		fontType = Font.BOLD;
														 }
										        	  	 if(element.getItalic().equals("y")){
										        	  		italic = element.getItalic().toUpperCase();
										        	  		fontType = Font.ITALIC;
														 }
										        	  	 if(element.getUnderline().equals("y")){
										        	  		underline = element.getUnderline().toUpperCase();
										        	  		fontType = Font.UNDERLINE;
														 }
										        	  	 
										        	  	 if((bold.equals("Y")) && (italic.equals("Y"))){
										        	  		fontType = Font.BOLDITALIC;
										        	  	 }
										        	  	 if((bold.equals("Y")) && (italic.equals("Y"))   && (underline.equals("Y"))){
										        	  		fontType = Font.ITALIC + Font.UNDERLINE;
										        	  	 }
										        	  	fontsize = element.getFontsize();
										        	  	element4labelfontType=Font.NORMAL;
										        	  	if(elementSize ==4) {
										        	  		if(dataset!=null && !dataset.equals("")) {
									        	  				outMessageelementSize4dataset=outMessageelementSize4dataset+dataset+"~~";
									        	  			}
										        	  		
										        	  		if(label!=null && !label.equals("")) {
										        	  			outMessageelementSize4label=outMessageelementSize4label+label+"~~";
										        	  		}
										        	  	}
										        	  	
											        	 if(elementSize==1){
											        		 outMessage = label+ dataset;
											        	 }
										        		 if(elementSize>1){
										        			 outMessage = outMessage+label+ dataset+" ";
										        			 if(outMessage.trim()!=""){
													        		
												        		 if(l==1 && elementSize ==2){
												        				 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
												        				 cell.setBorder(Rectangle.NO_BORDER);
												        				 cell.setColspan(4);
												        				 dataTable.addCell(cell);
										        				 }else if(l==2 && elementSize ==3){
										        					 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);
									        					 }else if(l==3 && elementSize ==4){
									        						 if(outMessageelementSize4label!=null && !outMessageelementSize4label.equals("")) {
										        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4label.trim(),  new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
											        					 cell.setBorder(Rectangle.NO_BORDER);
											        					 //#0088cc
											        					 cell.setBackgroundColor(new BaseColor(0, 136, 204));
											        					 cell.setPadding(1);
											        					 cell.setColspan(4);
											        					 dataTable.addCell(cell);*/
									        							 
									        							 String[] StrArray = outMessageelementSize4label.split("~~");
														        			for(int n=0;n<StrArray.length;n++){
												        		            	cell = new PdfPCell();
											        		                    cell.setBackgroundColor(new BaseColor(0, 136, 204));
												        		                cell.setUseAscender(true);
												        		                cell.setBorder(Rectangle.NO_BORDER);
												        		                //cell.setMinimumHeight(22);
												        		                //cell.setPaddingLeft(10);                    
												        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
												        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
												        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
												        		                dataTable.addCell(cell);
														        		   }
										        					 }
												        		 if(outMessageelementSize4dataset!=null && !outMessageelementSize4dataset.equals("")) {
									        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4dataset.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setBackgroundColor(new BaseColor(235, 235, 224));
										        					 cell.setPadding(1);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);*/
												        			 String[] StrArray = outMessageelementSize4dataset.split("~~");
													        			for(int n=0;n<StrArray.length;n++){
											        		            	cell = new PdfPCell();
										        		                    cell.setBackgroundColor(new BaseColor(235, 235, 224));
											        		                cell.setUseAscender(true);
											        		                cell.setBorder(Rectangle.NO_BORDER);
											        		                //cell.setMinimumHeight(22);
											        		                //cell.setPaddingLeft(10);                    
											        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
											        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
											        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.BLACK)));
											        		                dataTable.addCell(cell);
													        		   }
									        					 }
										        				}
										        			 }
										        		 }else{
										        			/* if(element.getLabel().equals("issue on:")){
																 cell = new PdfPCell(new Phrase(outMessage,  new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
																 cell.setBorder(Rectangle.BOX);
													 	         cell.setColspan(4);
													 	         cardTable.addCell(cell);
															 }else{*/
																 //cell = new PdfPCell(new Phrase(outMessage ,new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
										        			 	cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
																 cell.setBorder(Rectangle.NO_BORDER);
																 //if((rowSize-1)==k){
																	 cell.setColspan(4);
																// }
																 dataTable.addCell(cell);
															// }
										        		 }
										        	 
										         }
												
												}
												 
											}
								    	  
										   if(j%2!=0){
											 
											    backgroundImageTable.addCell(image);
								    			/*
								    			xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
								    			xPoss = (int)backgroundImageTable.getTotalWidth()+51;*/
											   
											   if(j==1){
												   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
												   xPoss = xPossImage+xPossLeft;
											   }else{
												   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
												   xPoss = xPossImage+xPossLeft;
												   yPoss= yPossImage-yPossNextRow;
											   }
										    }else if(j%2==0){
								    			backgroundImageTable.addCell(image);
								    			if(j==0){
								    					xPoss =xPossRight;
								    				// yPoss=808.0f;
								    				 xPossImage = 20;
												    yPossImage = 843.0f;
												    }else{
												    	xPoss =xPossRight;
												    	xPossImage = 20;
												    	yPossImage =  yPossImage - (int)backgroundImageTable.getTotalHeight()+2;
												    	//yPoss= yPossImage-35;
												    	yPoss= yPossImage-yPossNextRow;
													}
										    }
									
										    if(yPossImage <= bottom_margin)
									        {
								            	  cb.endText();
										          document.newPage();
										          cb.beginText();
										         // xPoss = 55f;
										         // yPoss=808.0f;
										          xPossImage = 20;
										          yPossImage = 843.0f;
									         }
											 //yPossImage= yPossImage-(policyDataTable.getTotalHeight()+2);
									        // backgroundImageTable.writeSelectedRows(0, 34, 30, yPossImage, cb);
										    
										    ArrayList tmp = dataTable.getRows(0, dataTable.getRows().size());
										    dataTable.getRows().clear();
										    dataTable.getRows().addAll(tmp);
									        backgroundImageTable.writeSelectedRows(0, -1, xPossImage, yPossImage, writer.getDirectContent());
									        dataTable.writeSelectedRows(0, 40, xPoss, yPoss, cb);
									        /*if((records.size()-1)==j){
									        	 yPossImage= yPossImage-190;
									        	 table.writeSelectedRows(0, 40, 30, yPossImage, cb);
									        }*/
								   }
								    	
								    	 //26-11-2019 added by prabhakar gogula for back image start
								    	 String  cardBackimage1=cardTemp.getCardbackimage();
								    	String IMAGEBACK = imgpath+cardBackimage1;
								    	 System.out.println(" first back Image"+IMAGEBACK);
								    	Image imageback = Image.getInstance(IMAGEBACK);
								    	 PdfPTable backImagetable = new PdfPTable(1);
								    	 backImagetable.setTotalWidth(275);
								    	 backImagetable.setLockedWidth(true);
								    	 backImagetable.getDefaultCell().setBorder(0);
								    	 backImagetable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
								    	 backImagetable.getDefaultCell().setFixedHeight(170.2f);
								    	 backImagetable.addCell(imageback);
								    	 //(int)backgroundImageTable.getTotalWidth()-15
								    	 backImagetable.writeSelectedRows(0, 150, 160, yPossImage-190, cb);
								        
								        System.out.println("yposs : "+(int)backImagetable.getTotalHeight());
								       // if((records.size()-1)==j){
								        	 table.writeSelectedRows(0, 40, 30, (int)backImagetable.getTotalHeight(), cb);
								       // }
								        	 //26-11-2019 added by prabhakar gogula for back image end
								        if(backImagetable!=null) {
								        	backImagetable=null;
								        }
								        if(table!=null) {
								        	table=null;
								        }
								        if(imageback!=null) {
								        	imageback=null;
								        }
								        if(IMAGEBACK!=null) {
								        	IMAGEBACK=null;
								        }
								        	 
							    	  cb.endText();
								      document.close();
								    }
								    records= null;
					    		   records =new LinkedList<String[]>();
					    		   records.add(card);  
					    		   tempfamilyId=card[0];
					    	  }// else end
					      }//if card not null
			    	  }
			    	  if(records.size()>0){
					    	 Document document = new Document(PageSize.A4);
					    	 /* Updated the code on 03AUG2019 for rectifying CARDNAME when retrievng from card service (GCM-1036) */
					    	 PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"_"+tempfamilyId+".pdf"));
					    	  // PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"_ECard.pdf"));
							  document.open();
						      PdfContentByte cb = writer.getDirectContent();
						      cb.beginText();
						      PdfPTable table = new PdfPTable(1);
				            	table.setTotalWidth(527);
				                table.setLockedWidth(true);
				                table.getDefaultCell().setBorder(1);
							    PdfPCell cellinsurencetext = new PdfPCell(new Phrase(insurencetext,  new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK)));
							    cellinsurencetext.setBorder(Rectangle.NO_BORDER);
							    table.addCell(cellinsurencetext);
						      
						      /*  float bottom_margin =83.0f;
						        float xPoss=55f;
					            float yPoss=808.0f;
							    float yPossImage = 843.0f;
							    float xPossImage = 20;*/
							    float bottom_margin =0;
							    float yPossNextRow = 0;
							    float xPossLeft  = 0;
							    float xPossRight = 0;
						        float xPoss=0;
					            float yPoss=0;
							    float yPossImage = 0;
							    float xPossImage = 0;
							   
							    //image = null;
							    float tableTotalWidth=0;
							    float tableFixedHeight=0;
					    	for(int j=0;j<records.size();j++){
					    	  String cardtemplatename="";
							  //String cardfrontimage="";
							 // String cardbackimage="";
							  String[] recordcard =records.get(j);
							  
							 /* Card cardTemp=null;
					    	  String XMLObj =""; 
					    	  for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
					    		  if(cardDataBean.getDbheaderNamesList().get(x).equals("ECARD_TEMPLATE")){
					    			  XMLObj= recordcard[x];
					    			  break;
					    		  }
					          }
					    	  if(XMLObj!=null){
					    	  StringReader sr = new StringReader(XMLObj+"");
					    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
					    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
					    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
					    	  }*/
					    	  int rowSize=0;
								if(cardTemp!=null){
									cardtemplatename=cardTemp.getCardtemplatename();
									//cardfrontimage=cardTemp.getCardfrontimage();
									//cardbackimage=cardTemp.getCardbackimage();
									bottom_margin =  Float.parseFloat(cardTemp.getEbottomMargin());
									xPossLeft  = Float.parseFloat(cardTemp.getEtemplatexpossleft());
									xPossRight = Float.parseFloat(cardTemp.getEtemplatexpossright());
									yPossNextRow = Float.parseFloat(cardTemp.getEtemplateypossnextrow());
									yPoss = Float.parseFloat(cardTemp.getEtemplateyposs());
									tableTotalWidth= Float.parseFloat(cardTemp.getEtableTotalWidth());
									tableFixedHeight=Float.parseFloat(cardTemp.getEtableFixedHeight());
									rowSize = cardTemp.getRow().size();
									    /*IMAGE = imgpath+cardfrontimage;
									    if(IMAGE!=null){
									    	try{
									     image = Image.getInstance(IMAGE);
									    }catch(Exception e){
									    	IMAGE=imgpath+"images.png";
									    	image = Image.getInstance(IMAGE);
									    }
									    }*/
								        image.setBorder(0);
								}
							  
							  
					    	  PdfPTable backgroundImageTable = new PdfPTable(1);
					    		backgroundImageTable.setWidths(new float[]{0.43f});
							    backgroundImageTable.setSplitLate(false);
							    backgroundImageTable.setTotalWidth(275);
							    backgroundImageTable.setLockedWidth(true);
							    backgroundImageTable.getDefaultCell().setBorder(0);
							    backgroundImageTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
							    backgroundImageTable.setSpacingBefore(100f);
							    backgroundImageTable.getDefaultCell().setFixedHeight(170.2f);
							    backgroundImageTable.setWidthPercentage(100);
							    backgroundImageTable.getDefaultCell().setPadding(5);
							    
							    PdfPTable dataTable = new PdfPTable(4);
							    dataTable.setWidths(new float[]{1.6f,1.3f,1f,1.5f});
							    dataTable.setSplitLate(false);
							   // dataTable.setTotalWidth(200f);
							    dataTable.setTotalWidth(tableTotalWidth);
							    dataTable.setLockedWidth(true);
							    dataTable.getDefaultCell().setBorder(0);
							    dataTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
							    dataTable.setSpacingBefore(100f);
							   // dataTable.getDefaultCell().setFixedHeight(160.2f);
							    dataTable.getDefaultCell().setFixedHeight(tableFixedHeight);
							    PdfPCell cell;
							   
									 for(int k=0;k<rowSize;k++){
										 row rowObj = cardTemp.getRow().get(k);
									int elementSize = 	rowObj.getElement().size(); 
									Element element= null;
									String outMessage="";
									String outMessageelementSize4label="";
									 String outMessageelementSize4dataset="";
									for(int l=0;l<elementSize;l++){	 
										String label="";
										Object dataset="";
										element = rowObj.getElement().get(l);
									 if(element.getIsimage().equals("y")){
										 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
							        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
							        			 if(recordcard[x]!=null){
							        				 dataset = recordcard[x];
							        			 }
								        		 break;
											  }
							        	 }
										 
										  if(element.getLabel()!=null){
											 label = element.getLabel().toUpperCase();
										  }
										 
										 String url= null; 
										 String imgEx=null;
										 Image imageSign=null;
										 try{
										 url= photopath+dataset+imgpathextn;
										 if(url!=null){
											 imgEx = getMimeType(url);
										 }
										 }catch(Exception e){
								        	 e.printStackTrace();
								        	 if(imgEx==null){
									    			url=imgpath+"images.png";
								    			}
								         }
										 if(imgEx==null){
								    			url=imgpath+"images.png";
							    			}
					                	 imageSign= Image.getInstance(url);
					                	 imageSign.scaleToFit(10f,50f);
					                	 cell = new PdfPCell(imageSign,true);
					                	
					                	
							  		     cell.setBorder(Rectangle.NO_BORDER);
							  		     cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
							  		     cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
							  		     cell.setColspan(4);
									     cell.setRowspan(rowSize-1);
									     //cell.setPadding(0);
									     dataTable.addCell(cell);
							 	         
							 	         
							         }else{
							        	 
							        	  if(element.getLabel()!=null){
											 label = element.getLabel().toUpperCase();
										  }
							        	 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
							        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
							        			 if(recordcard[x]!=null){
							        				 dataset = recordcard[x];
							        			 }else{
							        				 dataset = " ";
							        			 }
								        		 break;
											  }
							        	 }
							        	/* if(element.getBold().equals("y")){
											 label = element.getLabel().toUpperCase();
										 }*/
							        	 
							        	     String font ="";
							        		 int fontsize=0;
							        		 int element4labelfontType=0;
							        		 String bold="";
							        		 String italic="";
							        		 String underline="";
							        		 int fontType= 0;
							        		 Font.FontFamily fontText= null;
							        	  	 if(element.getFont()!=null){
							        	  		 font = element.getFont().toUpperCase();
							        	  		 switch (font) {
							        	  	         case "HELVETICA":
							        	  	        	fontText = Font.FontFamily.HELVETICA;
							        	  	             break;
							        	  	         case "COURIER":
							        	  	        	fontText = Font.FontFamily.COURIER;
							        	  	             break;
							        	  	         case "TIMES_ROMAN":
							        	  	        	fontText = Font.FontFamily.TIMES_ROMAN;
							        	  	             break;
							        	  	         case "UNDEFINED":
							        	  	        	fontText = Font.FontFamily.UNDEFINED;
							        	  	             break;
							        	  	          case "SYMBOL":
							        	  	        	fontText = Font.FontFamily.SYMBOL;
							        	  	             break;
							        	  	          case "ZAPFDINGBATS":
							        	  	        	fontText = Font.FontFamily.ZAPFDINGBATS;
							        	  	             break;
							        	  	           default : fontText = Font.FontFamily.HELVETICA;
							        	  	     }
							        	  		
											 }
							        	  	 if(element.getBold().equals("y")){
							        	  		 bold = element.getBold().toUpperCase();
							        	  		fontType = Font.BOLD;
											 }
							        	  	 if(element.getItalic().equals("y")){
							        	  		italic = element.getItalic().toUpperCase();
							        	  		fontType = Font.ITALIC;
											 }
							        	  	 if(element.getUnderline().equals("y")){
							        	  		underline = element.getUnderline().toUpperCase();
							        	  		fontType = Font.UNDERLINE;
											 }
							        	  	 
							        	  	 if((bold.equals("Y")) && (italic.equals("Y"))){
							        	  		fontType = Font.BOLDITALIC;
							        	  	 }
							        	  	 if((bold.equals("Y")) && (italic.equals("Y"))   && (underline.equals("Y"))){
							        	  		fontType = Font.ITALIC + Font.UNDERLINE;
							        	  	 }
							        	  	element4labelfontType=Font.NORMAL;
							        	  	fontsize = element.getFontsize();
							        	  	
							        		if(elementSize ==4) {
							        			if(dataset!=null && !dataset.equals("")) {
						        	  				outMessageelementSize4dataset=outMessageelementSize4dataset+dataset+"~~";
						        	  				
						        	  			}
							        	  		
							        	  		if(label!=null && !label.equals("")) {
							        	  			outMessageelementSize4label=outMessageelementSize4label+label+"~~";
							        	  		}
							        		}
							        	  	 
								        	 if(elementSize==1){
								        		 outMessage = label+ dataset;
								        	 }
							        		 if(elementSize>1){
							        			 outMessage = outMessage+label+ dataset+" ";
							        			 if(outMessage.trim()!=""){
										        		
									        		 if(l==1 && elementSize ==2){
									        				 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
									        				 cell.setBorder(Rectangle.NO_BORDER);
									        				 cell.setColspan(4);
									        				 dataTable.addCell(cell);
							        				 }else if(l==2 && elementSize ==3){
							        					 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
							        					 cell.setBorder(Rectangle.NO_BORDER);
							        					 cell.setColspan(4);
							        					 dataTable.addCell(cell);
						        					 }else if(l==3 && elementSize ==4){
						        						 if(outMessageelementSize4label!=null && !outMessageelementSize4label.equals("")) {
							        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4label.trim(),  new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
								        					 cell.setBorder(Rectangle.NO_BORDER);
								        					 //#0088cc
								        					 cell.setBackgroundColor(new BaseColor(0, 136, 204));
								        					 cell.setPadding(1);
								        					 cell.setColspan(4);
								        					 dataTable.addCell(cell);*/
						        							 
						        							 String[] StrArray = outMessageelementSize4label.split("~~");
											        			for(int n=0;n<StrArray.length;n++){
									        		            	cell = new PdfPCell();
								        		                    cell.setBackgroundColor(new BaseColor(0, 136, 204));
									        		                cell.setUseAscender(true);
									        		                cell.setBorder(Rectangle.NO_BORDER);
									        		                //cell.setMinimumHeight(22);
									        		                //cell.setPaddingLeft(10);                    
									        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
									        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
									        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
									        		                dataTable.addCell(cell);
											        		   }
							        					 }
									        		 if(outMessageelementSize4dataset!=null && !outMessageelementSize4dataset.equals("")) {
						        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4dataset.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
							        					 cell.setBorder(Rectangle.NO_BORDER);
							        					 cell.setBackgroundColor(new BaseColor(235, 235, 224));
							        					 cell.setPadding(1);
							        					 cell.setColspan(4);
							        					 dataTable.addCell(cell);*/
									        			 String[] StrArray = outMessageelementSize4dataset.split("~~");
										        			for(int n=0;n<StrArray.length;n++){
								        		            	cell = new PdfPCell();
							        		                    cell.setBackgroundColor(new BaseColor(235, 235, 224));
								        		                cell.setUseAscender(true);
								        		                cell.setBorder(Rectangle.NO_BORDER);
								        		                //cell.setMinimumHeight(22);
								        		                //cell.setPaddingLeft(10);                    
								        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
								        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
								        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.BLACK)));
								        		                dataTable.addCell(cell);
										        		   }
						        					 }
							        				}
							        			 }
							        		 }else{
							        			/* if(element.getLabel().equals("issue on:")){
													 cell = new PdfPCell(new Phrase(outMessage,  new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
													 cell.setBorder(Rectangle.BOX);
										 	         cell.setColspan(4);
										 	         cardTable.addCell(cell);
												 }else{*/
													 //cell = new PdfPCell(new Phrase(outMessage ,new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
							        			 	cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
													 cell.setBorder(Rectangle.NO_BORDER);
													 //if((rowSize-1)==k){
														 cell.setColspan(4);
													 //}
													 dataTable.addCell(cell);
												// }
							        		 }
							        	 
							         }
									
									}
									 
								}
					    	  
							   if(j%2!=0){
								 
								    backgroundImageTable.addCell(image);
					    			/*
					    			xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
					    			xPoss = (int)backgroundImageTable.getTotalWidth()+51;*/
								   
								   if(j==1){
									   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
									  // xPoss = xPossImage+35;
									   xPoss = xPossImage+xPossLeft;
								   }else{
									   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
									   //xPoss = xPossImage+35;
									   xPoss = xPossImage+xPossLeft;
									   yPoss= yPossImage-yPossNextRow;
								   }
							    }else if(j%2==0){
					    			backgroundImageTable.addCell(image);
					    			if(j==0){
					    				xPoss = xPossRight;
					    				// yPoss=808.0f;
					    				 xPossImage = 20;
									    yPossImage = 843.0f;
									    }else{
									    	xPoss = xPossRight;
									    	xPossImage = 20;
									    	yPossImage =  yPossImage - (int)backgroundImageTable.getTotalHeight()+2;
									    	yPoss= yPossImage-yPossNextRow;
										}
							    }
						
							    if(yPossImage <= bottom_margin)
						        {
					            	  cb.endText();
							          document.newPage();
							          cb.beginText();
							          xPoss = xPossLeft;
							         // yPoss=808.0f;
							          xPossImage = 20;
							          yPossImage = 843.0f;
						         }
								 //yPossImage= yPossImage-(policyDataTable.getTotalHeight()+2);
						        // backgroundImageTable.writeSelectedRows(0, 34, 30, yPossImage, cb);
							    
							    ArrayList tmp = dataTable.getRows(0, dataTable.getRows().size());
							    dataTable.getRows().clear();
							    dataTable.getRows().addAll(tmp);
						        backgroundImageTable.writeSelectedRows(0, -1, xPossImage, yPossImage, writer.getDirectContent());
						        dataTable.writeSelectedRows(0, 40, xPoss, yPoss, cb);
						      //20-05-2019 commented by prabhakar gogula for back image start
							       /* if((records.size()-1)==j){
							        	 yPossImage= yPossImage-190;
							        	 table.writeSelectedRows(0, 40, 30, yPossImage, cb);
							        }*/
							        //20-05-2019 commented by prabhakar gogula for back image end
						       
					   }
					    	
					    	//20-05-2020 added by prabhakar gogula for back image start
					    	 String  cardBackimage1=cardTemp.getCardbackimage();
					    	String IMAGEBACK = imgpath+cardBackimage1;
					    	 System.out.println(" second back Image"+IMAGEBACK);
					    	Image imageback = Image.getInstance(IMAGEBACK);
					    	 PdfPTable backImagetable = new PdfPTable(1);
					    	 backImagetable.setTotalWidth(275);
					    	 backImagetable.setLockedWidth(true);
					    	 backImagetable.getDefaultCell().setBorder(0);
					    	 backImagetable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
					    	 backImagetable.getDefaultCell().setFixedHeight(170.2f);
					    	 backImagetable.addCell(imageback);
					    	 //(int)backgroundImageTable.getTotalWidth()-15
					    	 backImagetable.writeSelectedRows(0, 150, 160, yPossImage-190, cb);
					        
					        System.out.println("yposs : "+(int)backImagetable.getTotalHeight());
					       // if((records.size()-1)==j){
					        	 table.writeSelectedRows(0, 40, 30, (int)backImagetable.getTotalHeight(), cb);
					       // }
					        	//20-05-2020 added by prabhakar gogula for back image end
					        if(backImagetable!=null) {
					        	backImagetable=null;
					        }
					        if(table!=null) {
					        	table=null;
					        }
					        if(imageback!=null) {
					        	imageback=null;
					        }
					        if(IMAGEBACK!=null) {
					        	IMAGEBACK=null;
					        }
					    	
				    	  cb.endText();
					      document.close();
					    }
			    	  	
				     // System.out.println("JOB END FOR generateECardFormat ");
				      logger.info("=============JOB END FOR generateECardFormat ==============");
				
				 }//if listSize >0
			}catch(Exception e){
				e.printStackTrace();
				System.out.println("ex:msg :_________>"+e.getMessage());
				try{
				 Document document = new Document(PageSize.A4);
		    	 Rectangle pagesize = new Rectangle(216f, 720f);
		         
		         // step 2
		         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
		         // step 3
		         document.open();
		         // step 4
		         document.add(new Paragraph(myResources.getMyProperties("ERRORPDFTEXTTech")+"error message :"+e.getMessage()+"erro cause : "+e.getCause()));
		         // step 5
		         document.close();
				}catch(Exception ex){ex.printStackTrace();}
				
				logger.error(e.getMessage());
			}
			finally {
				if(myResources!=null){
					myResources=null;
					}
			}
			//return 0;
		}
		
		
		public static void  generatePhycalCardFormat(CardDataBean cardDataBean, String lotNumber)
		{

			  myResources = new MyProperties();
		     // logger = myResources.getLogger();
		     Logger logger =(Logger) Logger.getInstance("generatePhycalCardFormat");
		     logger.info("=============JOB Start FOR generatePhycalCardFormat ==============");
			try{
				int recordlistSize=0;
				if(cardDataBean.getDbrecordsList()!=null){
					recordlistSize=cardDataBean.getDbrecordsList().size();
				}
				 
				 String imgpath = myResources.getMyProperties("ANDHRABANKCARDS");
				 String photopath = myResources.getMyProperties("photopath");
				 String imgpathextn = myResources.getMyProperties("ANDHRABANKCARDSEXTN");
				 if(recordlistSize>0){
				    Document document = new Document(PageSize.A4);
				    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+".pdf"));
					document.open();
				   // PdfContentByte cb = writer.getDirectContent();
				   // cb.beginText();
					PdfContentByte cb =null;
				    
				        float bottom_margin =0;
				        float xPoss=0;
				        float yPoss=0;
				        float tyPoss=0;
				        float tableTotalWidth=0;
					    float tableFixedHeight=0;
					    float xPossLeft  = 0;
					    float xPossRight = 0;
					    float yPossNextRow =0;
					    float newpage=0;
					    //String  IMAGE = null;
					   // Image image = null;
			    	  for(int i=0;i<recordlistSize;i++){
			    		 // if(i==0 || i==1){
			    		  String[] card =cardDataBean.getDbrecordsList().get(i);
					      if(card!=null){
					    	  cb = writer.getDirectContent();
							    cb.beginText();
					    	 // String familyId = card[0];
								    	  //String cardtemplatename="";
										  //String cardfrontimage="";
										  //String cardbackimage="";
										  Card cardTemp=null;
								    	  String XMLObj =""; 
								    	  for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
								    		  if(cardDataBean.getDbheaderNamesList().get(x).equals("ECARD_TEMPLATE")){
								    			  XMLObj= card[x];
								    			  break;
								    		  }
								          }
								    	  if(XMLObj!=null && !XMLObj.equals("")){
								    	  StringReader sr = new StringReader(XMLObj+"");
								    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
								    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
								    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
								    	  }
								    	  int rowSize=0;
											if(cardTemp!=null){
												//cardtemplatename=cardTemp.getCardtemplatename();
												//cardfrontimage=cardTemp.getCardfrontimage();
												//cardbackimage=cardTemp.getCardbackimage();
												bottom_margin =  Float.parseFloat(cardTemp.getPbottomMargin());
												xPossLeft  = Float.parseFloat(cardTemp.getPtemplatexpossleft());
												xPossRight = Float.parseFloat(cardTemp.getPtemplatexpossright());
												yPossNextRow = Float.parseFloat(cardTemp.getPtemplateypossnextrow());
												if((yPoss==0) || (yPoss <= bottom_margin)&& (i%2==0)){
													yPoss = Float.parseFloat(cardTemp.getPtemplateyposs());
													tyPoss= Float.parseFloat(cardTemp.getPtemplateyposs());
												}
												tableTotalWidth= Float.parseFloat(cardTemp.getPtableTotalWidth());
												tableFixedHeight=Float.parseFloat(cardTemp.getPtableFixedHeight());
												rowSize = cardTemp.getRow().size();
											}
										  
										    PdfPTable dataTable = new PdfPTable(4);
										    dataTable.setWidths(new float[]{1.6f,1.3f,1f,1.5f});
										    dataTable.setSplitLate(false);
										    dataTable.setTotalWidth(tableTotalWidth);
										    dataTable.setLockedWidth(true);
										    dataTable.getDefaultCell().setBorder(0);
										    dataTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
										    dataTable.setSpacingBefore(100f);
										    dataTable.getDefaultCell().setFixedHeight(tableFixedHeight);
										    PdfPCell cell;
										    
											for(int k=0;k<rowSize;k++){
												row rowObj = cardTemp.getRow().get(k);
												int elementSize = 	rowObj.getElement().size(); 
												Element element= null;
												String outMessage="";
												String outMessageelementSize4dataset="";
												String outMessageelementSize4label="";
												/*int previousspaces=0;
												int templabelspaces=0;
												int tempdataspaces=0;*/
												for(int l=0;l<elementSize;l++){	 
													String label="";
													Object dataset="";
													element = rowObj.getElement().get(l);
												 if(element.getIsimage().equals("y")){
													 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
										        			 if(card[x]!=null && !card[x].equals("null")){
										        				 dataset = card[x];
										        			 }
											        		 break;
														  }
										        	 }
													 
													  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
													 
													 String url= null; 
													 String imgEx=null;
													 Image imageSign=null;
													 cb.endText();
													 try{
													 url= photopath+dataset+imgpathextn;
													 if(url!=null){
														 imgEx = getMimeType(url);
													 }
													 }catch(Exception e){
											        	 e.printStackTrace();
											        	 logger.error("IMGE not found : "+url);
											        	 if(imgEx==null){
												    			url=imgpath+"images.png";
											    			}
											         }
													 if(imgEx==null){
											    			url=imgpath+"images.png";
										    			}
													 
								                	 imageSign= Image.getInstance(url);
								                	 imageSign.scaleToFit(100f,100f);
								                	 cell = new PdfPCell(imageSign,true);
										  		     cell.setBorder(Rectangle.NO_BORDER);
										  		     cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										  		     cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
										  		     cell.setColspan(4);
										  		     cell.setRowspan(rowSize-1);
												     //cell.setPadding(0);
												     dataTable.addCell(cell);
												     cb.beginText();
										         }else{
										        	  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
										        	 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
										        			 if(card[x]!=null){
										        				 dataset = card[x];
										        			 }else  if(card[x]==null){
										        				 dataset=" ";
										        			 }else if(card[x].equals("")){
										        				 dataset=" ";
										        			 }else{
										        				 dataset="NA";
										        			 }
											        		 break;
														  }
										        	 }
										        	
										        	     String font ="";
										        		 int fontsize=0;
										        		 String bold="";
										        		 String italic="";
										        		 String underline="";
										        		 int fontType= 0;
										        		 int element4labelfontType=0;
										        		 Font.FontFamily fontText= null;
										        	  	 if(element.getFont()!=null){
										        	  		 font = element.getFont().toUpperCase();
										        	  		 switch (font) {
										        	  	         case "HELVETICA":
										        	  	        	fontText = Font.FontFamily.HELVETICA;
										        	  	             break;
										        	  	         case "COURIER":
										        	  	        	fontText = Font.FontFamily.COURIER;
										        	  	             break;
										        	  	         case "TIMES_ROMAN":
										        	  	        	fontText = Font.FontFamily.TIMES_ROMAN;
										        	  	             break;
										        	  	         case "UNDEFINED":
										        	  	        	fontText = Font.FontFamily.UNDEFINED;
										        	  	             break;
										        	  	          case "SYMBOL":
										        	  	        	fontText = Font.FontFamily.SYMBOL;
										        	  	             break;
										        	  	          case "ZAPFDINGBATS":
										        	  	        	fontText = Font.FontFamily.ZAPFDINGBATS;
										        	  	             break;
										        	  	           default : fontText = Font.FontFamily.HELVETICA;
										        	  	     }
										        	  		
														 }
										        	  	 if(element.getBold().equals("y")){
										        	  		 bold = element.getBold().toUpperCase();
										        	  		fontType = Font.BOLD;
														 }
										        	  	 if(element.getItalic().equals("y")){
										        	  		italic = element.getItalic().toUpperCase();
										        	  		fontType = Font.ITALIC;
														 }
										        	  	 if(element.getUnderline().equals("y")){
										        	  		underline = element.getUnderline().toUpperCase();
										        	  		fontType = Font.UNDERLINE;
														 }
										        	  	 
										        	  	 if((bold.equals("Y")) && (italic.equals("Y"))){
										        	  		fontType = Font.BOLDITALIC;
										        	  	 }
										        	  	if((bold.equals("Y")) && (italic.equals("Y"))   && (underline.equals("Y"))){
										        	  		fontType = Font.ITALIC + Font.UNDERLINE;
										        	  	 }
										        	  	element4labelfontType=Font.NORMAL;
										        	  	fontsize = element.getFontsize();
										        	  	/*int datasetsize=0;
										        	  	int labelsize=0;
										        	  	int labelspaces= 0;
										        	  	int dataspaces= 0;
										        	  	int empty= 4;
										        	  	StringBuilder result = new StringBuilder();
										        	  	StringBuilder dataresult = new StringBuilder();*/
										        	  	if(elementSize ==4) {
										        	  		/*if(dataset!=null && !dataset.equals("") && label!=null && !label.equals("")) {
										        	  			char[] datasetletter = dataset.toString().toCharArray();
										        	  			char[] labelletter = label.toString().toCharArray();
										        	  			datasetsize =datasetletter.length;
										        	  			labelsize=	labelletter.length;
										        	  			
										        	  			if(previousspaces==0) {
											        	  			if(datasetsize<labelsize) {
											        	  				previousspaces =labelsize-datasetsize;
											        	  				dataspaces=previousspaces+empty;
											        	  				labelspaces=empty;
											        	  				tempdataspaces=(previousspaces)+empty;
											        	  				templabelspaces=empty;
											        	  				
										        	  				}else if(datasetsize>labelsize) {
										        	  					previousspaces=datasetsize-labelsize;
										        	  					dataspaces=empty;
											        	  				labelspaces=previousspaces+empty;
											        	  				tempdataspaces=empty;
										        	  					templabelspaces=(previousspaces)+empty;
											        	  				
										        	  				}else if(datasetsize==labelsize) {
										        	  					dataspaces=empty;
											        	  				labelspaces=empty;
										        	  					previousspaces=0;
											        	  				tempdataspaces=empty;
										        	  					templabelspaces=empty;
										        	  				}
										        	  			}else {
										        	  				if(tempdataspaces<templabelspaces) {
											        	  				dataspaces=(previousspaces)+empty;
											        	  				labelspaces=empty;
										        	  				}else if(tempdataspaces>templabelspaces) {
											        	  				dataspaces=empty;
											        	  				labelspaces=(previousspaces)+empty;
										        	  				}else if(tempdataspaces==templabelspaces) {
											        	  				dataspaces=empty;
											        	  				labelspaces=(previousspaces)+empty;
										        	  				}
										        	  				if(datasetsize<labelsize) {
											        	  				previousspaces =labelsize-datasetsize;
											        	  				tempdataspaces=previousspaces+empty;
											        	  				templabelspaces=empty;
											        	  				
										        	  				}else if(datasetsize>labelsize) {
										        	  					previousspaces=datasetsize-labelsize;
										        	  					tempdataspaces=empty;
										        	  					templabelspaces=previousspaces+empty;
										        	  					
										        	  				}else if(datasetsize==labelsize) {
										        	  					previousspaces=0;
										        	  					tempdataspaces=empty;
										        	  					templabelspaces=empty;
										        	  				}
										        	  			}
										        	  			
										        	  			for (int ss = 0; ss < labelspaces; ss++) {
													        	  	   if (ss > 0) {
													        	  	      result.append(new Chunk("\u0000 "));
													        	  	    }
													        	  	}
										        	  			
										        	  			for (int ss = 0; ss < dataspaces; ss++) {
													        	  	   if (ss > 0) {
													        	  		 dataresult.append(new Chunk("\u0000 "));
													        	  		
													        	  	    }
													        	  	}
										        	  		}*/
										        	  		
										        	  		//System.out.println(label +"    "+labelspaces);
										        	  		//System.out.println(dataset +"    "+dataspaces);
									        	  			if(dataset!=null && !dataset.equals("")) {
									        	  				outMessageelementSize4dataset=outMessageelementSize4dataset+dataset+"~~";
									        	  			}
										        	  		
										        	  		if(label!=null && !label.equals("")) {
										        	  			outMessageelementSize4label=outMessageelementSize4label+label+"~~";
										        	  		}
										        	  		
										        	  	}
										        	  	
										        	  	/*if(l==3) {
										        	  		System.out.println(outMessageelementSize4label.trim());
										        	  		System.out.println(outMessageelementSize4dataset.trim());
										        	  	}*/
										        	  	
										        	  	 
											        	 if(elementSize==1){
											        		 outMessage = label+ dataset;
											        	 }
										        		 if(elementSize>1){
										        			 outMessage = outMessage+label+ dataset+" ";
												        	 if(outMessage!=""){
												        		/* if(l==1){
													        	// cell = new PdfPCell(new Phrase(outMessage ,new Font(font, fontsize, Font.BOLD, BaseColor.BLACK)));
												        		 cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
													        	 cell.setBorder(Rectangle.NO_BORDER);
													        	 dataTable.addCell(cell);
												        		 }*/
												        		 
												        		 if(l==1 && elementSize ==2){
												        				// cell = new PdfPCell(new Phrase(outMessage ,new Font(font, fontsize, Font.BOLD, BaseColor.BLACK)));
												        				 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
												        				 cell.setBorder(Rectangle.NO_BORDER);
												        				 cell.setColspan(4);
												        				 dataTable.addCell(cell);
										        				 }else if(l==2 && elementSize ==3){
										        						// cell = new PdfPCell(new Phrase(outMessage ,new Font(font, fontsize, Font.BOLD, BaseColor.BLACK)));
										        					 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);
										        				 }else if(l==3 && elementSize ==4){
										        					 if(outMessageelementSize4label!=null && !outMessageelementSize4label.equals("")) {
										        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4label.trim(),  new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
											        					 cell.setBorder(Rectangle.NO_BORDER);
											        					 //#0088cc
											        					 cell.setBackgroundColor(new BaseColor(0, 136, 204));
											        					 cell.setPadding(1);
											        					 cell.setColspan(4);
											        					 dataTable.addCell(cell);*/
									        							 
									        							 String[] StrArray = outMessageelementSize4label.split("~~");
														        			for(int n=0;n<StrArray.length;n++){
												        		            	cell = new PdfPCell();
											        		                    cell.setBackgroundColor(new BaseColor(0, 136, 204));
												        		                cell.setUseAscender(true);
												        		                cell.setBorder(Rectangle.NO_BORDER);
												        		                //cell.setMinimumHeight(22);
												        		                //cell.setPaddingLeft(10);                    
												        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
												        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
												        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
												        		                dataTable.addCell(cell);
														        		   }
										        					 }
												        		 if(outMessageelementSize4dataset!=null && !outMessageelementSize4dataset.equals("")) {
									        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4dataset.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setBackgroundColor(new BaseColor(235, 235, 224));
										        					 cell.setPadding(1);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);*/
												        			 String[] StrArray = outMessageelementSize4dataset.split("~~");
													        			for(int n=0;n<StrArray.length;n++){
											        		            	cell = new PdfPCell();
										        		                    cell.setBackgroundColor(new BaseColor(235, 235, 224));
											        		                cell.setUseAscender(true);
											        		                cell.setBorder(Rectangle.NO_BORDER);
											        		                //cell.setMinimumHeight(22);
											        		                //cell.setPaddingLeft(10);                    
											        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
											        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
											        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.BLACK)));
											        		                dataTable.addCell(cell);
													        		   }
									        					 }
										        				}
											        	 }
									        		 }else{
									        			 /*if((rowSize-1)==k){
									        			// if(element.getLabel().equals("issue on:")){
															 cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
															 cell.setBorder(Rectangle.NO_BORDER);
												 	         cell.setColspan(2);
												 	         dataTable.addCell(cell);
														 //}else{
									        			 }else{*/
															 //cell = new PdfPCell(new Phrase(outMessage ,new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
									        			 	cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
															 cell.setBorder(Rectangle.NO_BORDER);
															 cell.setColspan(4);
															 if((rowSize-1)==k){
																 cell.setColspan(4);
																 }
																 dataTable.addCell(cell);
															 //}
										        		 }
										        	 
										         }
												
												}// row inside elements list size loop
												 
											}// in side card template row size loop
								    	  
										   if(i%2!=0){
												   //xPoss = (int)dataTable.getTotalWidth()+125;
											   		xPoss = (int)dataTable.getTotalWidth()+xPossLeft;
										   }else if(i%2==0){
											   if(i==0){
												   xPoss =xPossRight;
										    	}else{
										    		 xPoss =xPossRight;
										    		 if(newpage != bottom_margin){
										    			 yPoss= yPoss-(dataTable.getTotalHeight()+yPossNextRow);
										    		 }else{
										    			 newpage =0;
										    			 yPoss= tyPoss;
										    		 }
										    	}
										    }
										   
										   //System.out.println("i val : "+i +"     xPoss  :  "+xPoss+"     yPoss  : "+yPoss +"   table height : "+dataTable.getTotalHeight() +"     bottom_margin  "+bottom_margin);
										    ArrayList tmp = new ArrayList();				    
										    tmp = dataTable.getRows(0, dataTable.getRows().size());
										    dataTable.getRows().clear();
										    dataTable.getRows().addAll(tmp);
										    tmp = null;
									        dataTable.writeSelectedRows(0, 40, xPoss, yPoss, cb);
									        dataTable= null;
									        
									        if(i%2!=0){
									        	 if(yPoss <= bottom_margin)
											     {
									        		 	  cb.endText();
									        		 	  document.newPage();
									        		 	  //cb = writer.getDirectContent();
									        		 	  cb.beginText();
									        		 	  newpage = bottom_margin;
												          xPoss =xPossLeft;
												          yPoss= tyPoss;
												  }
									        }
									        cb.endText();
								    }//card null
			    	  //}
					    	  }// records for loop end
			    	  			//cb.endText();
			    	  			document.close();
					    	  
					    /*  }//if Records size >0
							 else{
								 try{
									 Document document = new Document(PageSize.A4);
							    	 Rectangle pagesize = new Rectangle(216f, 720f);
							         
							         // step 2
							         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
							         // step 3
							         document.open();
							         // step 4
							         document.add(new Paragraph(myResources.getMyProperties("NODataFound")));
							         // step 5
							         document.close();
									}catch(Exception ex){ex.printStackTrace();}
							 }*/
				}
				 else{
					 try{
						 Document document = new Document(PageSize.A4);
				    	 Rectangle pagesize = new Rectangle(216f, 720f);
				         
				         // step 2
				         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
				         // step 3
				         document.open();
				         // step 4
				         document.add(new Paragraph(myResources.getMyProperties("NODataFound")));
				         // step 5
				         document.close();
						}catch(Exception ex){ex.printStackTrace();}
				 }
			 		   	
				     // System.out.println("JOB END FOR generateECardFormat ");
				      logger.info("=============JOB END FOR generateECardFormat ==============");
				
				
			}catch(Exception e){
				System.out.println(e.getMessage());
				logger.error(e.getMessage());
				try{
					 Document document = new Document(PageSize.A4);
			    	 Rectangle pagesize = new Rectangle(216f, 720f);
			         
			         // step 2
			         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
			         // step 3
			         document.open();
			         // step 4
			         document.add(new Paragraph(myResources.getMyProperties("ERRORPDFTEXTTech")+"error message :"+e.getMessage()+"zero cause : "+e.getCause()));
			         // step 5
			         document.close();
					}catch(Exception ex){ex.printStackTrace();}
				e.printStackTrace();
			}
			finally {
				if(myResources!=null){
					myResources=null;
					}
			}
			//return 0;
		}
		
		
		public static void  generateECardFormat(CardDataBean cardDataBean,int lotNumber)
		{
			  myResources = new MyProperties();
		     // logger = myResources.getLogger();
		     Logger logger =(Logger) Logger.getInstance("generateECardFormat");
		     logger.info("=============JOB Start FOR generateECardFormat ==============");
		     String insurencetext = myResources.getMyProperties("insurencetext");
			System.out.println("enter ecard format");
			//  String fileCreatepath = myResources.getMyProperties("uploadslocal");
			try{
				int recordlistSize=0;
				if(cardDataBean.getDbrecordsList()!=null){
					recordlistSize=cardDataBean.getDbrecordsList().size();
				}
				 
				 String imgpath = myResources.getMyProperties("ANDHRABANKCARDS");
				// String frontImage = myResources.getMyProperties("ANDHRABANKFRONTCARD");
				 String photopath = myResources.getMyProperties("photopath");
				 String imgpathextn = myResources.getMyProperties("ANDHRABANKCARDSEXTN");
				 String ERRORPDFTEXT = myResources.getMyProperties("IMAGEERRORPDFTEXT");
				 if(recordlistSize>0){
					 Image image = null;
				    String tempfamilyId="";
				    Card cardTemp=null;
			    	  String XMLObj =""; 
			    	  String  IMAGE = null;
				    List<String[]> records =new LinkedList<String[]>();
			    	  for(int i=0;i<recordlistSize;i++){
			    		  String[] card =cardDataBean.getDbrecordsList().get(i);
			    		 /*
			    		  * newly added code checking front image is available or not
			    		  */
			    		  String[] recordcard =null;;
			    		  recordcard = card;
						  if(image==null){
				    	  for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
				    		  if(cardDataBean.getDbheaderNamesList().get(x).equals("ECARD_TEMPLATE")){
				    			  XMLObj= recordcard[x];
				    			  break;
				    		  }
				          }
				    	  if(XMLObj!=null){
				    	  StringReader sr = new StringReader(XMLObj+"");
				    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
				    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
				    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
				    	  }
				    	 
				    	 String  cardfrontimage1=cardTemp.getCardfrontimage();
				    	 IMAGE = imgpath+cardfrontimage1;
						    if(IMAGE!=null){
						    	try{
						     image = Image.getInstance(IMAGE);
						    }catch(Exception e){
						    	 Document document = new Document(PageSize.A4);
						    	 Rectangle pagesize = new Rectangle(216f, 720f);
						         
						         // step 2
						         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
						         // step 3
						         document.open();
						         // step 4
						         document.add(new Paragraph(ERRORPDFTEXT));
						             
						         // step 5
						         document.close();
						         return;
						    }
						    }
						  }
			    		  /*  newly added code end*/
			    		  if((card!=null) && (image!=null)){
					    	  String familyId = card[0];
					    	  if(tempfamilyId.equals(familyId)){
					    		  	tempfamilyId=card[0];
					    		  	records.add(card);
					    	  }else{
								    if(records.size()>0){
								    	 Document document = new Document(PageSize.A4);
								    	 PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+tempfamilyId+"_ECard.pdf"));
										//  PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"_"+tempfamilyId+".pdf"));
										  document.open();
									      PdfContentByte cb = writer.getDirectContent();
									      cb.beginText();
									      PdfPTable table = new PdfPTable(1);
							            	table.setTotalWidth(527);
							                table.setLockedWidth(true);
							                table.getDefaultCell().setBorder(1);
										    PdfPCell cellinsurencetext = new PdfPCell(new Phrase(insurencetext,  new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK)));
										    cellinsurencetext.setBorder(Rectangle.NO_BORDER);
										    table.addCell(cellinsurencetext);
									      
									      /*  float bottom_margin =83.0f;
									        float xPoss=55f;
								            float yPoss=808.0f;
										    float yPossImage = 843.0f;
										    float xPossImage = 20;*/
										    float bottom_margin =0;
										    float xPoss=0;
									        float xPossLeft=0;
									        float xPossRight=0;
									        float yPossNextRow=0;
								            float yPoss=0;
										    float yPossImage = 0;
										    float xPossImage = 0;
										     IMAGE = null;
										     //image = null;
										    float tableTotalWidth=0;
										    float tableFixedHeight=0;
								    	for(int j=0;j<records.size();j++){
								    	  String cardtemplatename="";
										 // String cardfrontimage="";
										  //String cardbackimage="";
										   recordcard =records.get(j);
										  
										  /* cardTemp=null;
								    	   XMLObj =""; 
								    	  for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
								    		  if(cardDataBean.getDbheaderNamesList().get(x).equals("ECARD_TEMPLATE")){
								    			  XMLObj= recordcard[x];
								    			  break;
								    		  }
								          }
								    	  if(XMLObj!=null){
								    	  StringReader sr = new StringReader(XMLObj+"");
								    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
								    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
								    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
								    	  }*/
								    	  int rowSize=0;
											if(cardTemp!=null){
												cardtemplatename=cardTemp.getCardtemplatename();
												//cardfrontimage=cardTemp.getCardfrontimage();
												//cardbackimage=cardTemp.getCardbackimage();
												bottom_margin =  Float.parseFloat(cardTemp.getEbottomMargin());
												xPossLeft  = Float.parseFloat(cardTemp.getEtemplatexpossleft());
												xPossRight = Float.parseFloat(cardTemp.getEtemplatexpossright());
												yPossNextRow = Float.parseFloat(cardTemp.getEtemplateypossnextrow());
												yPoss = Float.parseFloat(cardTemp.getEtemplateyposs());
												tableTotalWidth= Float.parseFloat(cardTemp.getEtableTotalWidth());
												tableFixedHeight=Float.parseFloat(cardTemp.getEtableFixedHeight());
												rowSize = cardTemp.getRow().size();
												   /* IMAGE = imgpath+cardfrontimage;
												    if(IMAGE!=null){
												    	try{
												     image = Image.getInstance(IMAGE);
												    }catch(Exception e){
												    	IMAGE=imgpath+"images.png";
												    	image = Image.getInstance(IMAGE);
												    }
												    }*/
											        image.setBorder(0);
											}
										  
										  
								    	  PdfPTable backgroundImageTable = new PdfPTable(1);
								    		backgroundImageTable.setWidths(new float[]{0.43f});
										    backgroundImageTable.setSplitLate(false);
										    backgroundImageTable.setTotalWidth(275);
										    backgroundImageTable.setLockedWidth(true);
										    backgroundImageTable.getDefaultCell().setBorder(0);
										    backgroundImageTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
										    backgroundImageTable.setSpacingBefore(100f);
										    backgroundImageTable.getDefaultCell().setFixedHeight(170.2f);
										    backgroundImageTable.setWidthPercentage(100);
										    backgroundImageTable.getDefaultCell().setPadding(5);
										    
										    PdfPTable dataTable = new PdfPTable(4);
										    dataTable.setWidths(new float[]{1.6f,1.3f,1f,1.5f});
										    dataTable.setSplitLate(false);
										   // dataTable.setTotalWidth(200f);
										    dataTable.setTotalWidth(tableTotalWidth);
										    dataTable.setLockedWidth(true);
										    dataTable.getDefaultCell().setBorder(0);
										    dataTable.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
										    dataTable.setSpacingBefore(100f);
										   // dataTable.getDefaultCell().setFixedHeight(160.2f);
										    dataTable.getDefaultCell().setFixedHeight(tableFixedHeight);
										    PdfPCell cell;
										   
												 for(int k=0;k<rowSize;k++){
													 row rowObj = cardTemp.getRow().get(k);
												int elementSize = 	rowObj.getElement().size(); 
												Element element= null;
												String outMessage="";
												String outMessageelementSize4dataset="";
												String outMessageelementSize4label="";
												for(int l=0;l<elementSize;l++){	 
													String label="";
													Object dataset="";
													element = rowObj.getElement().get(l);
												 if(element.getIsimage().equals("y")){
													 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
											        		 dataset = recordcard[x];
											        		 break;
														  }
										        	 }
													 
													  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
													 
													 String url= null; 
													 String imgEx=null;
													 Image imageSign=null;
													 try{
													 url= photopath+dataset+imgpathextn;
													 if(url!=null){
														 imgEx = getMimeType(url);
													 }
													 }catch(Exception e){
											        	 e.printStackTrace();
											        	 if(imgEx==null){
												    			url=imgpath+"images.png";
											    			}
											         }
													 if(imgEx==null){
											    			url=imgpath+"images.png";
										    		 }
								                	 imageSign= Image.getInstance(url);
								                	 imageSign.scaleToFit(10f,50f);
								                	 cell = new PdfPCell(imageSign,true);
								                	
								                	
										  		     cell.setBorder(Rectangle.NO_BORDER);
										  		     cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
										  		     cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
										  		     cell.setColspan(4);
										  		     cell.setRowspan(rowSize-1);
												     //cell.setPadding(0);
												     dataTable.addCell(cell);
										 	         
										 	         
										         }else{
										        	  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
										        	 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
										        			 if(recordcard[x]!=null){
											        			 dataset = recordcard[x];
											        		 }else  if(recordcard[x]==null){
										        				 dataset=" ";
										        			 }else if(recordcard[x].equals("")){
										        				 dataset=" ";
										        			 }else {
										        				 dataset="NA";
										        			 }
											        		 break;
														  }
										        	 }
										        	/* if(element.getBold().equals("y")){
														 label = element.getLabel().toUpperCase();
													 }*/
										        	 
										        	     String font ="";
										        		 int fontsize=0;
										        		 int element4labelfontType=0;
										        		 String bold="";
										        		 String italic="";
										        		 String underline="";
										        		 int fontType= 0;
										        		 Font.FontFamily fontText= null;
										        	  	 if(element.getFont()!=null){
										        	  		 font = element.getFont().toUpperCase();
										        	  		 switch (font) {
										        	  	         case "HELVETICA":
										        	  	        	fontText = Font.FontFamily.HELVETICA;
										        	  	             break;
										        	  	         case "COURIER":
										        	  	        	fontText = Font.FontFamily.COURIER;
										        	  	             break;
										        	  	         case "TIMES_ROMAN":
										        	  	        	fontText = Font.FontFamily.TIMES_ROMAN;
										        	  	             break;
										        	  	         case "UNDEFINED":
										        	  	        	fontText = Font.FontFamily.UNDEFINED;
										        	  	             break;
										        	  	          case "SYMBOL":
										        	  	        	fontText = Font.FontFamily.SYMBOL;
										        	  	             break;
										        	  	          case "ZAPFDINGBATS":
										        	  	        	fontText = Font.FontFamily.ZAPFDINGBATS;
										        	  	             break;
										        	  	           default : fontText = Font.FontFamily.HELVETICA;
										        	  	     }
										        	  		
														 }
										        	  	 if(element.getBold().equals("y")){
										        	  		 bold = element.getBold().toUpperCase();
										        	  		fontType = Font.BOLD;
														 }
										        	  	 if(element.getItalic().equals("y")){
										        	  		italic = element.getItalic().toUpperCase();
										        	  		fontType = Font.ITALIC;
														 }
										        	  	 if(element.getUnderline().equals("y")){
										        	  		underline = element.getUnderline().toUpperCase();
										        	  		fontType = Font.UNDERLINE;
														 }
										        	  	 
										        	  	 if((bold.equals("Y")) && (italic.equals("Y"))){
										        	  		fontType = Font.BOLDITALIC;
										        	  	 }
										        	  	 if((bold.equals("Y")) && (italic.equals("Y"))   && (underline.equals("Y"))){
										        	  		fontType = Font.ITALIC + Font.UNDERLINE;
										        	  	 }
										        	  	element4labelfontType=Font.NORMAL;
										        	  	fontsize = element.getFontsize();
										        	  	
										        		if(elementSize ==4) {
										        			if(dataset!=null && !dataset.equals("")) {
									        	  				outMessageelementSize4dataset=outMessageelementSize4dataset+dataset+"~~";
									        	  				
									        	  			}
										        	  		
										        	  		if(label!=null && !label.equals("")) {
										        	  			outMessageelementSize4label=outMessageelementSize4label+label+"~~";
										        	  		}
										        		}
										        	  	 
											        	 if(elementSize==1){
											        		 outMessage = label+ dataset;
											        	 }
										        		 if(elementSize>1){
										        			 outMessage = outMessage+label+ dataset+" ";
										        			 if(outMessage.trim()!=""){
													        		
												        		 if(l==1 && elementSize ==2){
												        				 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
												        				 cell.setBorder(Rectangle.NO_BORDER);
												        				 cell.setColspan(4);
												        				 dataTable.addCell(cell);
										        				 }else if(l==2 && elementSize ==3){
										        					 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);
									        					 }else if(l==3 && elementSize ==4){
									        						 if(outMessageelementSize4label!=null && !outMessageelementSize4label.equals("")) {
										        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4label.trim(),  new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
											        					 cell.setBorder(Rectangle.NO_BORDER);
											        					 //#0088cc
											        					 cell.setBackgroundColor(new BaseColor(0, 136, 204));
											        					 cell.setPadding(1);
											        					 cell.setColspan(4);
											        					 dataTable.addCell(cell);*/
									        							 
									        							 String[] StrArray = outMessageelementSize4label.split("~~");
														        			for(int n=0;n<StrArray.length;n++){
												        		            	cell = new PdfPCell();
											        		                    cell.setBackgroundColor(new BaseColor(0, 136, 204));
												        		                cell.setUseAscender(true);
												        		                cell.setBorder(Rectangle.NO_BORDER);
												        		                //cell.setMinimumHeight(22);
												        		                //cell.setPaddingLeft(10);                    
												        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
												        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
												        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
												        		                dataTable.addCell(cell);
														        		   }
										        					 }
												        		 if(outMessageelementSize4dataset!=null && !outMessageelementSize4dataset.equals("")) {
									        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4dataset.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setBackgroundColor(new BaseColor(235, 235, 224));
										        					 cell.setPadding(1);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);*/
												        			 String[] StrArray = outMessageelementSize4dataset.split("~~");
													        			for(int n=0;n<StrArray.length;n++){
											        		            	cell = new PdfPCell();
										        		                    cell.setBackgroundColor(new BaseColor(235, 235, 224));
											        		                cell.setUseAscender(true);
											        		                cell.setBorder(Rectangle.NO_BORDER);
											        		                //cell.setMinimumHeight(22);
											        		                //cell.setPaddingLeft(10);                    
											        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
											        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
											        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.BLACK)));
											        		                dataTable.addCell(cell);
													        		   }
									        					 }
										        				}
										        			 }
										        		 }else{
										        			/* if(element.getLabel().equals("issue on:")){
																 cell = new PdfPCell(new Phrase(outMessage,  new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
																 cell.setBorder(Rectangle.BOX);
													 	         cell.setColspan(4);
													 	         cardTable.addCell(cell);
															 }else{*/
																 //cell = new PdfPCell(new Phrase(outMessage ,new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
										        			 	cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
																 cell.setBorder(Rectangle.NO_BORDER);
																 //if((rowSize-1)==k){
																	 cell.setColspan(4);
																// }
																 dataTable.addCell(cell);
															// }
										        		 }
										        	 
										         }
												
												}
												 
											}
								    	  
										   if(j%2!=0){
											 
											    backgroundImageTable.addCell(image);
								    			/*
								    			xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
								    			xPoss = (int)backgroundImageTable.getTotalWidth()+51;*/
											   
											   if(j==1){
												   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
												   xPoss = xPossImage+xPossLeft;
											   }else{
												   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
												   xPoss = xPossImage+xPossLeft;
												   yPoss= yPossImage-yPossNextRow;
											   }
										    }else if(j%2==0){
								    			backgroundImageTable.addCell(image);
								    			if(j==0){
								    					xPoss =xPossRight;
								    				// yPoss=808.0f;
								    				 xPossImage = 20;
												    yPossImage = 843.0f;
												    }else{
												    	xPoss =xPossRight;
												    	xPossImage = 20;
												    	yPossImage =  yPossImage - (int)backgroundImageTable.getTotalHeight()+2;
												    	//yPoss= yPossImage-35;
												    	yPoss= yPossImage-yPossNextRow;
													}
										    }
									
										    if(yPossImage <= bottom_margin)
									        {
								            	  cb.endText();
										          document.newPage();
										          cb.beginText();
										         // xPoss = 55f;
										         // yPoss=808.0f;
										          xPossImage = 20;
										          yPossImage = 843.0f;
									         }
											 //yPossImage= yPossImage-(policyDataTable.getTotalHeight()+2);
									        // backgroundImageTable.writeSelectedRows(0, 34, 30, yPossImage, cb);
										    
										    ArrayList tmp = dataTable.getRows(0, dataTable.getRows().size());
										    dataTable.getRows().clear();
										    dataTable.getRows().addAll(tmp);
									        backgroundImageTable.writeSelectedRows(0, -1, xPossImage, yPossImage, writer.getDirectContent());
									        dataTable.writeSelectedRows(0, 40, xPoss, yPoss, cb);
									       /* if((records.size()-1)==j){
									        	 yPossImage= yPossImage-190;
									        	 table.writeSelectedRows(0, 40, 30, yPossImage, cb);
									        }*/
								   }
								    	
								    	 //20-05-2020 added by prabhakar gogula for back image start
								    	 String  cardBackimage1=cardTemp.getCardbackimage();
								    	String IMAGEBACK = imgpath+cardBackimage1;
								    	 System.out.println(" first back Image"+IMAGEBACK);
								    	Image imageback = Image.getInstance(IMAGEBACK);
								    	 PdfPTable backImagetable = new PdfPTable(1);
								    	 backImagetable.setTotalWidth(275);
								    	 backImagetable.setLockedWidth(true);
								    	 backImagetable.getDefaultCell().setBorder(0);
								    	 backImagetable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
								    	 backImagetable.getDefaultCell().setFixedHeight(170.2f);
								    	 backImagetable.addCell(imageback);
								    	 //(int)backgroundImageTable.getTotalWidth()-15
								    	 backImagetable.writeSelectedRows(0, 150, 160, yPossImage-190, cb);
								        
								        System.out.println("yposs : "+(int)backImagetable.getTotalHeight());
								       // if((records.size()-1)==j){
								        	 table.writeSelectedRows(0, 40, 30, (int)backImagetable.getTotalHeight(), cb);
								       // }
								        	//20-05-2020 added by prabhakar gogula for back image end
								        if(backImagetable!=null) {
								        	backImagetable=null;
								        }
								        if(table!=null) {
								        	table=null;
								        }
								        if(imageback!=null) {
								        	imageback=null;
								        }
								        if(IMAGEBACK!=null) {
								        	IMAGEBACK=null;
								        }
							    	  cb.endText();
								      document.close();
								    }
								    records= null;
					    		   records =new LinkedList<String[]>();
					    		   records.add(card);  
					    		   tempfamilyId=card[0];
					    	  }// else end
					      }//if card not null
			    	  }
			    	  if(records.size()>0){
					    	 Document document = new Document(PageSize.A4);
					    	 PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+tempfamilyId+"_ECard.pdf"));
							  document.open();
						      PdfContentByte cb = writer.getDirectContent();
						      cb.beginText();
						      PdfPTable table = new PdfPTable(1);
				            	table.setTotalWidth(527);
				                table.setLockedWidth(true);
				                table.getDefaultCell().setBorder(1);
							    PdfPCell cellinsurencetext = new PdfPCell(new Phrase(insurencetext,  new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK)));
							    cellinsurencetext.setBorder(Rectangle.NO_BORDER);
							    table.addCell(cellinsurencetext);
						      
						      /*  float bottom_margin =83.0f;
						        float xPoss=55f;
					            float yPoss=808.0f;
							    float yPossImage = 843.0f;
							    float xPossImage = 20;*/
							    float bottom_margin =0;
							    float yPossNextRow = 0;
							    float xPossLeft  = 0;
							    float xPossRight = 0;
						        float xPoss=0;
					            float yPoss=0;
							    float yPossImage = 0;
							    float xPossImage = 0;
							   
							    //image = null;
							    float tableTotalWidth=0;
							    float tableFixedHeight=0;
					    	for(int j=0;j<records.size();j++){
					    	  String cardtemplatename="";
							  //String cardfrontimage="";
							 // String cardbackimage="";
							  String[] recordcard =records.get(j);
							  
							 /* Card cardTemp=null;
					    	  String XMLObj =""; 
					    	  for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
					    		  if(cardDataBean.getDbheaderNamesList().get(x).equals("ECARD_TEMPLATE")){
					    			  XMLObj= recordcard[x];
					    			  break;
					    		  }
					          }
					    	  if(XMLObj!=null){
					    	  StringReader sr = new StringReader(XMLObj+"");
					    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
					    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
					    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
					    	  }*/
					    	  int rowSize=0;
								if(cardTemp!=null){
									cardtemplatename=cardTemp.getCardtemplatename();
									//cardfrontimage=cardTemp.getCardfrontimage();
									//cardbackimage=cardTemp.getCardbackimage();
									bottom_margin =  Float.parseFloat(cardTemp.getEbottomMargin());
									xPossLeft  = Float.parseFloat(cardTemp.getEtemplatexpossleft());
									xPossRight = Float.parseFloat(cardTemp.getEtemplatexpossright());
									yPossNextRow = Float.parseFloat(cardTemp.getEtemplateypossnextrow());
									yPoss = Float.parseFloat(cardTemp.getEtemplateyposs());
									tableTotalWidth= Float.parseFloat(cardTemp.getEtableTotalWidth());
									tableFixedHeight=Float.parseFloat(cardTemp.getEtableFixedHeight());
									rowSize = cardTemp.getRow().size();
									    /*IMAGE = imgpath+cardfrontimage;
									    if(IMAGE!=null){
									    	try{
									     image = Image.getInstance(IMAGE);
									    }catch(Exception e){
									    	IMAGE=imgpath+"images.png";
									    	image = Image.getInstance(IMAGE);
									    }
									    }*/
								        image.setBorder(0);
								}
							  
							  
					    	  PdfPTable backgroundImageTable = new PdfPTable(1);
					    		backgroundImageTable.setWidths(new float[]{0.43f});
							    backgroundImageTable.setSplitLate(false);
							    backgroundImageTable.setTotalWidth(275);
							    backgroundImageTable.setLockedWidth(true);
							    backgroundImageTable.getDefaultCell().setBorder(0);
							    backgroundImageTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
							    backgroundImageTable.setSpacingBefore(100f);
							    backgroundImageTable.getDefaultCell().setFixedHeight(170.2f);
							    backgroundImageTable.setWidthPercentage(100);
							    backgroundImageTable.getDefaultCell().setPadding(5);
							    
							    PdfPTable dataTable = new PdfPTable(4);
							    dataTable.setWidths(new float[]{1.6f,1.3f,1f,1.5f});
							    dataTable.setSplitLate(false);
							   // dataTable.setTotalWidth(200f);
							    dataTable.setTotalWidth(tableTotalWidth);
							    dataTable.setLockedWidth(true);
							    dataTable.getDefaultCell().setBorder(0);
							    dataTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
							    dataTable.setSpacingBefore(100f);
							   // dataTable.getDefaultCell().setFixedHeight(160.2f);
							    dataTable.getDefaultCell().setFixedHeight(tableFixedHeight);
							    PdfPCell cell;
							   
									 for(int k=0;k<rowSize;k++){
										 row rowObj = cardTemp.getRow().get(k);
									int elementSize = 	rowObj.getElement().size(); 
									Element element= null;
									String outMessage="";
									String outMessageelementSize4dataset="";
									String outMessageelementSize4label="";
									for(int l=0;l<elementSize;l++){	 
										String label="";
										Object dataset="";
										element = rowObj.getElement().get(l);
									 if(element.getIsimage().equals("y")){
										 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
							        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
							        			 if(recordcard[x]!=null){
							        				 dataset = recordcard[x];
							        			 }
								        		 break;
											  }
							        	 }
										 
										  if(element.getLabel()!=null){
											 label = element.getLabel().toUpperCase();
										  }
										 
										 String url= null; 
										 String imgEx=null;
										 Image imageSign=null;
										 try{
										 url= photopath+dataset+imgpathextn;
										 if(url!=null){
											 imgEx = getMimeType(url);
										 }
										 }catch(Exception e){
								        	 e.printStackTrace();
								        	 if(imgEx==null){
									    			url=imgpath+"images.png";
								    			}
								         }
										 if(imgEx==null){
								    			url=imgpath+"images.png";
							    			}
					                	 imageSign= Image.getInstance(url);
					                	 imageSign.scaleToFit(10f,50f);
					                	 cell = new PdfPCell(imageSign,true);
					                	
					                	
							  		     cell.setBorder(Rectangle.NO_BORDER);
							  		     cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
							  		     cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
							  		     cell.setColspan(4);
									     cell.setRowspan(rowSize-1);
									     //cell.setPadding(0);
									     dataTable.addCell(cell);
							 	         
							 	         
							         }else{
							        	 
							        	  if(element.getLabel()!=null){
											 label = element.getLabel().toUpperCase();
										  }
							        	 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
							        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
							        			 if(recordcard[x]!=null){
							        				 dataset = recordcard[x];
							        			 }else{
							        				 dataset = " ";
							        			 }
								        		 break;
											  }
							        	 }
							        	/* if(element.getBold().equals("y")){
											 label = element.getLabel().toUpperCase();
										 }*/
							        	 
							        	     String font ="";
							        		 int fontsize=0;
							        		 int element4labelfontType=0;
							        		 String bold="";
							        		 String italic="";
							        		 String underline="";
							        		 int fontType= 0;
							        		 Font.FontFamily fontText= null;
							        	  	 if(element.getFont()!=null){
							        	  		 font = element.getFont().toUpperCase();
							        	  		 switch (font) {
							        	  	         case "HELVETICA":
							        	  	        	fontText = Font.FontFamily.HELVETICA;
							        	  	             break;
							        	  	         case "COURIER":
							        	  	        	fontText = Font.FontFamily.COURIER;
							        	  	             break;
							        	  	         case "TIMES_ROMAN":
							        	  	        	fontText = Font.FontFamily.TIMES_ROMAN;
							        	  	             break;
							        	  	         case "UNDEFINED":
							        	  	        	fontText = Font.FontFamily.UNDEFINED;
							        	  	             break;
							        	  	          case "SYMBOL":
							        	  	        	fontText = Font.FontFamily.SYMBOL;
							        	  	             break;
							        	  	          case "ZAPFDINGBATS":
							        	  	        	fontText = Font.FontFamily.ZAPFDINGBATS;
							        	  	             break;
							        	  	           default : fontText = Font.FontFamily.HELVETICA;
							        	  	     }
							        	  		
											 }
							        	  	 if(element.getBold().equals("y")){
							        	  		 bold = element.getBold().toUpperCase();
							        	  		fontType = Font.BOLD;
											 }
							        	  	 if(element.getItalic().equals("y")){
							        	  		italic = element.getItalic().toUpperCase();
							        	  		fontType = Font.ITALIC;
											 }
							        	  	 if(element.getUnderline().equals("y")){
							        	  		underline = element.getUnderline().toUpperCase();
							        	  		fontType = Font.UNDERLINE;
											 }
							        	  	 
							        	  	 if((bold.equals("Y")) && (italic.equals("Y"))){
							        	  		fontType = Font.BOLDITALIC;
							        	  	 }
							        	  	 if((bold.equals("Y")) && (italic.equals("Y"))   && (underline.equals("Y"))){
							        	  		fontType = Font.ITALIC + Font.UNDERLINE;
							        	  	 }
							        	  	element4labelfontType=Font.NORMAL;
							        	  	fontsize = element.getFontsize();
							        	  	
							        		if(elementSize ==4) {
							        			if(dataset!=null && !dataset.equals("")) {
						        	  				outMessageelementSize4dataset=outMessageelementSize4dataset+dataset+"~~";
						        	  				
						        	  			}
							        	  		
							        	  		if(label!=null && !label.equals("")) {
							        	  			outMessageelementSize4label=outMessageelementSize4label+label+"~~";
							        	  		}
							        		}
							        	  	 
								        	 if(elementSize==1){
								        		 outMessage = label+ dataset;
								        	 }
							        		 if(elementSize>1){
							        			 outMessage = outMessage+label+ dataset+" ";
							        			 if(outMessage.trim()!=""){
										        		
									        		 if(l==1 && elementSize ==2){
									        				 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
									        				 cell.setBorder(Rectangle.NO_BORDER);
									        				 cell.setColspan(4);
									        				 dataTable.addCell(cell);
							        				 }else if(l==2 && elementSize ==3){
							        					 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
							        					 cell.setBorder(Rectangle.NO_BORDER);
							        					 cell.setColspan(4);
							        					 dataTable.addCell(cell);
						        					 }else if(l==3 && elementSize ==4){
						        						 if(outMessageelementSize4label!=null && !outMessageelementSize4label.equals("")) {
							        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4label.trim(),  new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
								        					 cell.setBorder(Rectangle.NO_BORDER);
								        					 //#0088cc
								        					 cell.setBackgroundColor(new BaseColor(0, 136, 204));
								        					 cell.setPadding(1);
								        					 cell.setColspan(4);
								        					 dataTable.addCell(cell);*/
						        							 
						        							 String[] StrArray = outMessageelementSize4label.split("~~");
											        			for(int n=0;n<StrArray.length;n++){
									        		            	cell = new PdfPCell();
								        		                    cell.setBackgroundColor(new BaseColor(0, 136, 204));
									        		                cell.setUseAscender(true);
									        		                cell.setBorder(Rectangle.NO_BORDER);
									        		                //cell.setMinimumHeight(22);
									        		                //cell.setPaddingLeft(10);                    
									        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
									        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
									        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
									        		                dataTable.addCell(cell);
											        		   }
							        					 }
									        		 if(outMessageelementSize4dataset!=null && !outMessageelementSize4dataset.equals("")) {
						        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4dataset.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
							        					 cell.setBorder(Rectangle.NO_BORDER);
							        					 cell.setBackgroundColor(new BaseColor(235, 235, 224));
							        					 cell.setPadding(1);
							        					 cell.setColspan(4);
							        					 dataTable.addCell(cell);*/
									        			 String[] StrArray = outMessageelementSize4dataset.split("~~");
										        			for(int n=0;n<StrArray.length;n++){
								        		            	cell = new PdfPCell();
							        		                    cell.setBackgroundColor(new BaseColor(235, 235, 224));
								        		                cell.setUseAscender(true);
								        		                cell.setBorder(Rectangle.NO_BORDER);
								        		                //cell.setMinimumHeight(22);
								        		                //cell.setPaddingLeft(10);                    
								        		                cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
								        		                cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
								        		                cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.BLACK)));
								        		                dataTable.addCell(cell);
										        		   }
						        					 }
							        				}
							        			 }
							        		 }else{
							        			/* if(element.getLabel().equals("issue on:")){
													 cell = new PdfPCell(new Phrase(outMessage,  new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
													 cell.setBorder(Rectangle.BOX);
										 	         cell.setColspan(4);
										 	         cardTable.addCell(cell);
												 }else{*/
													 //cell = new PdfPCell(new Phrase(outMessage ,new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
							        			 	cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
													 cell.setBorder(Rectangle.NO_BORDER);
													 //if((rowSize-1)==k){
														 cell.setColspan(4);
													// }
													 dataTable.addCell(cell);
												// }
							        		 }
							        	 
							         }
									
									}
									 
								}
					    	  
							   if(j%2!=0){
								 
								    backgroundImageTable.addCell(image);
					    			/*
					    			xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
					    			xPoss = (int)backgroundImageTable.getTotalWidth()+51;*/
								   
								   if(j==1){
									   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
									  // xPoss = xPossImage+35;
									   xPoss = xPossImage+xPossLeft;
								   }else{
									   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
									   //xPoss = xPossImage+35;
									   xPoss = xPossImage+xPossLeft;
									   yPoss= yPossImage-yPossNextRow;
								   }
							    }else if(j%2==0){
					    			backgroundImageTable.addCell(image);
					    			if(j==0){
					    				xPoss = xPossRight;
					    				// yPoss=808.0f;
					    				 xPossImage = 20;
									    yPossImage = 843.0f;
									    }else{
									    	xPoss = xPossRight;
									    	xPossImage = 20;
									    	yPossImage =  yPossImage - (int)backgroundImageTable.getTotalHeight()+2;
									    	yPoss= yPossImage-yPossNextRow;
										}
							    }
						
							    if(yPossImage <= bottom_margin)
						        {
					            	  cb.endText();
							          document.newPage();
							          cb.beginText();
							          xPoss = xPossLeft;
							         // yPoss=808.0f;
							          xPossImage = 20;
							          yPossImage = 843.0f;
						         }
								 //yPossImage= yPossImage-(policyDataTable.getTotalHeight()+2);
						        // backgroundImageTable.writeSelectedRows(0, 34, 30, yPossImage, cb);
							    
							    ArrayList tmp = dataTable.getRows(0, dataTable.getRows().size());
							    dataTable.getRows().clear();
							    dataTable.getRows().addAll(tmp);
						        backgroundImageTable.writeSelectedRows(0, -1, xPossImage, yPossImage, writer.getDirectContent());
						        dataTable.writeSelectedRows(0, 40, xPoss, yPoss, cb);
						       /* if((records.size()-1)==j){
						        	 yPossImage= yPossImage-190;
						        	 table.writeSelectedRows(0, 40, 30, yPossImage, cb);
						        }*/
					   }
					    	
					    	
					    	//20-05-2020 added by prabhakar gogula for back image start
					    	 String  cardBackimage1=cardTemp.getCardbackimage();
					    	String IMAGEBACK = imgpath+cardBackimage1;
					    	 System.out.println(" second back Image"+IMAGEBACK);
					    	Image imageback = Image.getInstance(IMAGEBACK);
					    	 PdfPTable backImagetable = new PdfPTable(1);
					    	 backImagetable.setTotalWidth(275);
					    	 backImagetable.setLockedWidth(true);
					    	 backImagetable.getDefaultCell().setBorder(0);
					    	 backImagetable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
					    	 backImagetable.getDefaultCell().setFixedHeight(170.2f);
					    	 backImagetable.addCell(imageback);
					    	 //(int)backgroundImageTable.getTotalWidth()-15
					    	 backImagetable.writeSelectedRows(0, 150, 160, yPossImage-190, cb);
					        
					        System.out.println("yposs : "+(int)backImagetable.getTotalHeight());
					       // if((records.size()-1)==j){
					        	 table.writeSelectedRows(0, 40, 30, (int)backImagetable.getTotalHeight(), cb);
					       // }
					        	//20-05-2020 added by prabhakar gogula for back image end
					        if(backImagetable!=null) {
					        	backImagetable=null;
					        }
					        if(table!=null) {
					        	table=null;
					        }
					        if(imageback!=null) {
					        	imageback=null;
					        }
					        if(IMAGEBACK!=null) {
					        	IMAGEBACK=null;
					        }
				    	  cb.endText();
					      document.close();
					    }
			    	  	
				      logger.info("=============JOB END FOR generateECardFormat ==============");
				
				 }//if listSize >0
			}catch(Exception e){
				try{
				 Document document = new Document(PageSize.A4);
		    	 Rectangle pagesize = new Rectangle(216f, 720f);
		         
		         // step 2
		         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
		         // step 3
		         document.open();
		         // step 4
		         document.add(new Paragraph(myResources.getMyProperties("ERRORPDFTEXTTech")+"error message :"+e.getMessage()+"erro cause : "+e.getCause()));
		         // step 5
		         document.close();
				}catch(Exception ex){ex.printStackTrace();}
				e.printStackTrace();
			}
			finally {
				if(myResources!=null){
					myResources=null;
					}
			}
			//return 0;
		}
		
		
		
		public static void  generateECardFormatWeb(CardJsonBean cardDataBean,String cardTemplate, String lotNumber)
		{
			  myResources = new MyProperties();
		     // logger = myResources.getLogger();
		     Logger logger =(Logger) Logger.getInstance("generateECardFormat");
		     logger.info("=============JOB Start FOR generateECardFormat ==============");
		     String insurencetext = myResources.getMyProperties("insurencetext");
			  String fileCreatepath = myResources.getMyProperties("uploadslocal");
			try{
				int recordlistSize=0;
				if(cardDataBean.getDbrecordsList()!=null){
					recordlistSize=cardDataBean.getDbrecordsList().size();
				}
				 Card cardTemp=null;
				 String cardfrontimage= null;
				 String imgpath = myResources.getMyProperties("ANDHRABANKCARDS");
				// String frontImage = myResources.getMyProperties("ANDHRABANKFRONTCARD");
				 String photopath = myResources.getMyProperties("photopath");
				 String imgpathextn = myResources.getMyProperties("ANDHRABANKCARDSEXTN");
				 String ERRORPDFTEXT = myResources.getMyProperties("IMAGEERRORPDFTEXT");
				 if(recordlistSize>0){
					 Image image=null;
					 String IMAGE=null;
					 if(cardTemplate!=null){
				    	  StringReader sr = new StringReader(cardTemplate+"");
				    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
				    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
				    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
				      }
						if(cardTemp!=null){
							cardfrontimage=cardTemp.getCardfrontimage();
							IMAGE = imgpath+cardfrontimage;
					    if(IMAGE!=null){
					    	try{
					    		 image = Image.getInstance(IMAGE);
							    }catch(Exception e){
							    	 Document document = new Document(PageSize.A4);
							    	 Rectangle pagesize = new Rectangle(216f, 720f);
							         // step 2
							         PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"Exception.pdf"));
							         // step 3
							         document.open();
							         // step 4
							         document.add(new Paragraph(ERRORPDFTEXT));
							         // step 5
							         document.close();
							         return;
							    }
							    } 
							}
					 
				    String tempfamilyId="";
				    List<String[]> records =new LinkedList<String[]>();
			    	  for(int i=0;i<recordlistSize;i++){
			    		  String[] card =cardDataBean.getDbrecordsList().get(i);
					      if((card!=null) && (image!=null) ){
					    	  String familyId = card[0];
					    	  if(tempfamilyId.equals(familyId)){
					    		  	tempfamilyId=card[0];
					    		  	records.add(card);
					    	  }else{
								    if(records.size()>0){
								    	 String XMLObj =""; 
								         XMLObj= cardTemplate;
								    	 Document document = new Document(PageSize.A4);
								    	 PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileCreatepath+File.separator+lotNumber+File.separator+lotNumber+".pdf"));
										//  PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"_"+tempfamilyId+".pdf"));
										  document.open();
									      PdfContentByte cb = writer.getDirectContent();
									      cb.beginText();
									      PdfPTable table = new PdfPTable(1);
							            	table.setTotalWidth(527);
							                table.setLockedWidth(true);
							                table.getDefaultCell().setBorder(1);
										    PdfPCell cellinsurencetext = new PdfPCell(new Phrase(insurencetext,  new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK)));
										    cellinsurencetext.setBorder(Rectangle.NO_BORDER);
										    table.addCell(cellinsurencetext);
									      
									     
										    float bottom_margin =0;
										    float xPoss=0;
									        float xPossLeft=0;
									        float xPossRight=0;
									        float yPossNextRow=0;
								            float yPoss=0;
										    float yPossImage = 0;
										    float xPossImage = 0;
										    IMAGE = null;
										    
										    float tableTotalWidth=0;
										    float tableFixedHeight=0;
								    	for(int j=0;j<records.size();j++){
								    	  String cardtemplatename="";
										  // cardfrontimage="";
										  String cardbackimage="";
										  String[] recordcard =records.get(j);
										  
										   cardTemp=null;
								    	 
								    	  if(XMLObj!=null){
								    	  StringReader sr = new StringReader(XMLObj+"");
								    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
								    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
								    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
								    	  }
								    	  int rowSize=0;
											if(cardTemp!=null){
												cardtemplatename=cardTemp.getCardtemplatename();
												//cardfrontimage=cardTemp.getCardfrontimage();
												cardbackimage=cardTemp.getCardbackimage();
												bottom_margin =  Float.parseFloat(cardTemp.getEbottomMargin());
												xPossLeft  = Float.parseFloat(cardTemp.getEtemplatexpossleft());
												xPossRight = Float.parseFloat(cardTemp.getEtemplatexpossright());
												yPossNextRow = Float.parseFloat(cardTemp.getEtemplateypossnextrow());
												yPoss = Float.parseFloat(cardTemp.getEtemplateyposs());
												tableTotalWidth= Float.parseFloat(cardTemp.getEtableTotalWidth());
												tableFixedHeight=Float.parseFloat(cardTemp.getEtableFixedHeight());
												rowSize = cardTemp.getRow().size();
												   /* IMAGE = imgpath+cardfrontimage;
												    if(IMAGE!=null){
												    	try{
												     image = Image.getInstance(IMAGE);
												    }catch(Exception e){
												    	IMAGE=imgpath+"images.png";
												    	image = Image.getInstance(IMAGE);
												    }
												    }*/
											        image.setBorder(0);
											}
										  
										  
								    	  PdfPTable backgroundImageTable = new PdfPTable(1);
								    		backgroundImageTable.setWidths(new float[]{0.43f});
										    backgroundImageTable.setSplitLate(false);
										    backgroundImageTable.setTotalWidth(275);
										    backgroundImageTable.setLockedWidth(true);
										    backgroundImageTable.getDefaultCell().setBorder(0);
										    backgroundImageTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
										    backgroundImageTable.setSpacingBefore(100f);
										    backgroundImageTable.getDefaultCell().setFixedHeight(170.2f);
										    backgroundImageTable.setWidthPercentage(100);
										    backgroundImageTable.getDefaultCell().setPadding(5);
										    
										    PdfPTable dataTable = new PdfPTable(2);
										    dataTable.setWidths(new float[]{7.3f,1.5f});
										    dataTable.setSplitLate(false);
										   // dataTable.setTotalWidth(200f);
										    dataTable.setTotalWidth(tableTotalWidth);
										    dataTable.setLockedWidth(true);
										    dataTable.getDefaultCell().setBorder(0);
										    dataTable.setHorizontalAlignment(PdfPTable.ALIGN_LEFT);
										    dataTable.setSpacingBefore(100f);
										   // dataTable.getDefaultCell().setFixedHeight(160.2f);
										    dataTable.getDefaultCell().setFixedHeight(tableFixedHeight);
										    PdfPCell cell;
										   
												 for(int k=0;k<rowSize;k++){
													 row rowObj = cardTemp.getRow().get(k);
												int elementSize = 	rowObj.getElement().size(); 
												Element element= null;
												String outMessage="";
												String outMessageelementSize4dataset="";
												String outMessageelementSize4label="";
												for(int l=0;l<elementSize;l++){	 
													String label="";
													Object dataset="";
													element = rowObj.getElement().get(l);
												 if(element.getIsimage().equals("y")){
													 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
										        			 if(recordcard[x]!=null){
										        				 dataset = recordcard[x];
										        			 }
											        		 break;
														  }
										        	 }
													 
													  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
													 
													 String url= null; 
													 String imgEx=null;
													 Image imageSign=null;
													 try{
													 url= photopath+dataset+imgpathextn;
													 if(url!=null){
														 imgEx = getMimeType(url);
													 }
													 }catch(Exception e){
											        	 e.printStackTrace();
											        	 if(imgEx==null){
												    			url=imgpath+"images.png";
											    			}
											         }
													 if(imgEx==null){
											    			url=imgpath+"images.png";
										    			}
								                	 imageSign= Image.getInstance(url);
								                	 imageSign.scaleToFit(10f,50f);
								                	 cell = new PdfPCell(imageSign,true);
								                	
								                	
										  		     cell.setBorder(Rectangle.NO_BORDER);
										  		     cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
										  		     cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_LEFT);
										  		     cell.setColspan(4);
										  		     cell.setRowspan(rowSize-1);
												     //cell.setPadding(0);
												     dataTable.addCell(cell);
										 	         
										 	         
										         }else{
										        	 
										        	  if(element.getLabel()!=null){
														 label = element.getLabel().toUpperCase();
													  }
										        	 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
										        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
											        		 if(recordcard[x]!=null){
											        			 dataset = recordcard[x];
											        		 }else  if(recordcard[x]==null){
										        				 dataset=" ";
										        			 }else if(recordcard[x].equals("")){
										        				 dataset=" ";
										        			 }else{
										        				 dataset="NA";
										        			 }
											        		 break;
														  }
										        	 }
										        	/* if(element.getBold().equals("y")){
														 label = element.getLabel().toUpperCase();
													 }*/
										        	 
										        	     String font ="";
										        		 int fontsize=0;
										        		 int element4labelfontType=0;
										        		 String bold="";
										        		 String italic="";
										        		 String underline="";
										        		 int fontType= 0;
										        		 Font.FontFamily fontText= null;
										        	  	 if(element.getFont()!=null){
										        	  		 font = element.getFont().toUpperCase();
										        	  		 switch (font) {
										        	  	         case "HELVETICA":
										        	  	        	fontText = Font.FontFamily.HELVETICA;
										        	  	             break;
										        	  	         case "COURIER":
										        	  	        	fontText = Font.FontFamily.COURIER;
										        	  	             break;
										        	  	         case "TIMES_ROMAN":
										        	  	        	fontText = Font.FontFamily.TIMES_ROMAN;
										        	  	             break;
										        	  	         case "UNDEFINED":
										        	  	        	fontText = Font.FontFamily.UNDEFINED;
										        	  	             break;
										        	  	          case "SYMBOL":
										        	  	        	fontText = Font.FontFamily.SYMBOL;
										        	  	             break;
										        	  	          case "ZAPFDINGBATS":
										        	  	        	fontText = Font.FontFamily.ZAPFDINGBATS;
										        	  	             break;
										        	  	           default : fontText = Font.FontFamily.HELVETICA;
										        	  	     }
										        	  		
														 }
										        	  	 if(element.getBold().equals("y")){
										        	  		 bold = element.getBold().toUpperCase();
										        	  		fontType = Font.BOLD;
														 }
										        	  	 if(element.getItalic().equals("y")){
										        	  		italic = element.getItalic().toUpperCase();
										        	  		fontType = Font.ITALIC;
														 }
										        	  	 if(element.getUnderline().equals("y")){
										        	  		underline = element.getUnderline().toUpperCase();
										        	  		fontType = Font.UNDERLINE;
														 }
										        	  	 
										        	  	 if((bold.equals("Y")) && (italic.equals("Y"))){
										        	  		fontType = Font.BOLDITALIC;
										        	  	 }
										        	  	 if((bold.equals("Y")) && (italic.equals("Y"))   && (underline.equals("Y"))){
										        	  		fontType = Font.ITALIC + Font.UNDERLINE;
										        	  	 }
										        	  	element4labelfontType=Font.NORMAL;
										        	  	fontsize = element.getFontsize();

										        	  	if(elementSize ==4) {
										        	  		if(dataset!=null && !dataset.equals("")) {
										        	  			outMessageelementSize4dataset=outMessageelementSize4dataset+dataset+"~~";
										        	  			
										        	  		}
										        	  		
										        	  		if(label!=null && !label.equals("")) {
										        	  			outMessageelementSize4label=outMessageelementSize4label+label+"~~";
										        	  		}
										        	  	}
										        	  	 
											        	 if(elementSize==1){
											        		 outMessage = label+ dataset;
											        	 }
										        		 if(elementSize>1){
										        			 outMessage = outMessage+label+ dataset+" ";
										        			
										        			 if(outMessage.trim()!=""){
													        		
										        				 if(l==1 && elementSize ==2){
										        						 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        						 cell.setBorder(Rectangle.NO_BORDER);
										        						 cell.setColspan(4);
										        						 dataTable.addCell(cell);
										        				 }else if(l==2 && elementSize ==3){
										        					 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);
										        				 }else if(l==3 && elementSize ==4){
										        					 if(outMessageelementSize4label!=null && !outMessageelementSize4label.equals("")) {
										        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4label.trim(),  new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
										        						 cell.setBorder(Rectangle.NO_BORDER);
										        						 //#0088cc
										        						 cell.setBackgroundColor(new BaseColor(0, 136, 204));
										        						 cell.setPadding(1);
										        						 cell.setColspan(4);
										        						 dataTable.addCell(cell);*/
										        						 
										        						 String[] StrArray = outMessageelementSize4label.split("~~");
										        							for(int n=0;n<StrArray.length;n++){
										        								cell = new PdfPCell();
										        								cell.setBackgroundColor(new BaseColor(0, 136, 204));
										        								cell.setUseAscender(true);
										        								cell.setBorder(Rectangle.NO_BORDER);
										        								//cell.setMinimumHeight(22);
										        								//cell.setPaddingLeft(10);                    
										        								cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
										        								cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
										        								cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
										        								dataTable.addCell(cell);
										        						   }
										        					 }
										        				 if(outMessageelementSize4dataset!=null && !outMessageelementSize4dataset.equals("")) {
										        					 /*cell = new PdfPCell(new Phrase(outMessageelementSize4dataset.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
										        					 cell.setBorder(Rectangle.NO_BORDER);
										        					 cell.setBackgroundColor(new BaseColor(235, 235, 224));
										        					 cell.setPadding(1);
										        					 cell.setColspan(4);
										        					 dataTable.addCell(cell);*/
										        					 String[] StrArray = outMessageelementSize4dataset.split("~~");
										        						for(int n=0;n<StrArray.length;n++){
										        							cell = new PdfPCell();
										        							cell.setBackgroundColor(new BaseColor(235, 235, 224));
										        							cell.setUseAscender(true);
										        							cell.setBorder(Rectangle.NO_BORDER);
										        							//cell.setMinimumHeight(22);
										        							//cell.setPaddingLeft(10);                    
										        							cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
										        							cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
										        							cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.BLACK)));
										        							dataTable.addCell(cell);
										        					   }
										        				 }
										        				}
										        				}

										        		 }else{
										        			/* if(element.getLabel().equals("issue on:")){
																 cell = new PdfPCell(new Phrase(outMessage,  new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
																 cell.setBorder(Rectangle.BOX);
													 	         cell.setColspan(4);
													 	         cardTable.addCell(cell);
															 }else{*/
																 //cell = new PdfPCell(new Phrase(outMessage ,new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
										        			 	cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
																 cell.setBorder(Rectangle.NO_BORDER);
																 //if((rowSize-1)==k){
																	 cell.setColspan(4);
																 //}
																 dataTable.addCell(cell);
															// }
										        		 }
										        	 
										         }
												
												}
												 
											}
								    	  
										   if(j%2!=0){
											 
											    backgroundImageTable.addCell(image);
								    			/*
								    			xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
								    			xPoss = (int)backgroundImageTable.getTotalWidth()+51;*/
											   
											   if(j==1){
												   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
												   xPoss = xPossImage+xPossLeft;
											   }else{
												   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
												   xPoss = xPossImage+xPossLeft;
												   yPoss= yPossImage-yPossNextRow;
											   }
										    }else if(j%2==0){
								    			backgroundImageTable.addCell(image);
								    			if(j==0){
								    					xPoss =xPossRight;
								    				// yPoss=808.0f;
								    				 xPossImage = 20;
												    yPossImage = 843.0f;
												    }else{
												    	xPoss =xPossRight;
												    	xPossImage = 20;
												    	yPossImage =  yPossImage - (int)backgroundImageTable.getTotalHeight()+2;
												    	//yPoss= yPossImage-35;
												    	yPoss= yPossImage-yPossNextRow;
													}
										    }
									
										    if(yPossImage <= bottom_margin)
									        {
								            	  cb.endText();
										          document.newPage();
										          cb.beginText();
										         // xPoss = 55f;
										         // yPoss=808.0f;
										          xPossImage = 20;
										          yPossImage = 843.0f;
									         }
											 //yPossImage= yPossImage-(policyDataTable.getTotalHeight()+2);
									        // backgroundImageTable.writeSelectedRows(0, 34, 30, yPossImage, cb);
										    
										    ArrayList tmp = dataTable.getRows(0, dataTable.getRows().size());
										    dataTable.getRows().clear();
										    dataTable.getRows().addAll(tmp);
									        backgroundImageTable.writeSelectedRows(0, -1, xPossImage, yPossImage, writer.getDirectContent());
									        dataTable.writeSelectedRows(0, 40, xPoss, yPoss, cb);
									        /*if((records.size()-1)==j){
									        	 yPossImage= yPossImage-190;
									        	 table.writeSelectedRows(0, 40, 30, yPossImage, cb);
									        }*/
								   }
								    	//20-05-2020 added by prabhakar gogula for back image start
								    	 String  cardBackimage1=cardTemp.getCardbackimage();
								    	String IMAGEBACK = imgpath+cardBackimage1;
								    	 System.out.println(" first back Image"+IMAGEBACK);
								    	Image imageback = Image.getInstance(IMAGEBACK);
								    	 PdfPTable backImagetable = new PdfPTable(1);
								    	 backImagetable.setTotalWidth(275);
								    	 backImagetable.setLockedWidth(true);
								    	 backImagetable.getDefaultCell().setBorder(0);
								    	 backImagetable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
								    	 backImagetable.getDefaultCell().setFixedHeight(170.2f);
								    	 backImagetable.addCell(imageback);
								    	 //(int)backgroundImageTable.getTotalWidth()-15
								    	 backImagetable.writeSelectedRows(0, 150, 160, yPossImage-190, cb);
								        
								        System.out.println("yposs : "+(int)backImagetable.getTotalHeight());
								       // if((records.size()-1)==j){
								        	 table.writeSelectedRows(0, 40, 30, (int)backImagetable.getTotalHeight(), cb);
								       // }
								        	//20-05-2020 added by prabhakar gogula for back image end
								        if(backImagetable!=null) {
								        	backImagetable=null;
								        }
								        if(table!=null) {
								        	table=null;
								        }
								        if(imageback!=null) {
								        	imageback=null;
								        }
								        if(IMAGEBACK!=null) {
								        	IMAGEBACK=null;
								        }
							    	  cb.endText();
								      document.close();
								    }
								    records= null;
					    		   records =new LinkedList<String[]>();
					    		   records.add(card);  
					    		   tempfamilyId=card[0];
					    	  }// else end
					      }//if card not null
			    	  }
			    	  if(records.size()>0){
					    	 Document document = new Document(PageSize.A4);
					    	 PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(fileCreatepath+File.separator+lotNumber+File.separator+lotNumber+".pdf"));
							  document.open();
						      PdfContentByte cb = writer.getDirectContent();
						      cb.beginText();
						      PdfPTable table = new PdfPTable(1);
				            	table.setTotalWidth(527);
				                table.setLockedWidth(true);
				                table.getDefaultCell().setBorder(1);
							    PdfPCell cellinsurencetext = new PdfPCell(new Phrase(insurencetext,  new Font(FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK)));
							    cellinsurencetext.setBorder(Rectangle.NO_BORDER);
							    table.addCell(cellinsurencetext);
							    float bottom_margin =0;
							    float yPossNextRow = 0;
							    float xPossLeft  = 0;
							    float xPossRight = 0;
						        float xPoss=0;
					            float yPoss=0;
							    float yPossImage = 0;
							    float xPossImage = 0;
							    
							    float tableTotalWidth=0;
							    float tableFixedHeight=0;
					    	for(int j=0;j<records.size();j++){
					    	  String cardtemplatename="";
							   cardfrontimage="";
							  String cardbackimage="";
							  String[] recordcard =records.get(j);
							  
							   cardTemp=null;
					    	  String XMLObj =""; 
					    	  XMLObj = cardTemplate;
					    	 
					    	  if(XMLObj!=null){
					    	  StringReader sr = new StringReader(XMLObj+"");
					    	  JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
					    	  Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
					    	  cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr);
					    	  }
					    	  int rowSize=0;
								if(cardTemp!=null){
									cardtemplatename=cardTemp.getCardtemplatename();
									//cardfrontimage=cardTemp.getCardfrontimage();
									cardbackimage=cardTemp.getCardbackimage();
									bottom_margin =  Float.parseFloat(cardTemp.getEbottomMargin());
									xPossLeft  = Float.parseFloat(cardTemp.getEtemplatexpossleft());
									xPossRight = Float.parseFloat(cardTemp.getEtemplatexpossright());
									yPossNextRow = Float.parseFloat(cardTemp.getEtemplateypossnextrow());
									yPoss = Float.parseFloat(cardTemp.getEtemplateyposs());
									tableTotalWidth= Float.parseFloat(cardTemp.getEtableTotalWidth());
									tableFixedHeight=Float.parseFloat(cardTemp.getEtableFixedHeight());
									rowSize = cardTemp.getRow().size();
									   /* IMAGE = imgpath+cardfrontimage;
									    image = Image.getInstance(IMAGE);*/
								        image.setBorder(0);
								}
							  
							  
					    	  PdfPTable backgroundImageTable = new PdfPTable(1);
					    		backgroundImageTable.setWidths(new float[]{0.43f});
							    backgroundImageTable.setSplitLate(false);
							    backgroundImageTable.setTotalWidth(275);
							    backgroundImageTable.setLockedWidth(true);
							    backgroundImageTable.getDefaultCell().setBorder(0);
							    backgroundImageTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
							    backgroundImageTable.setSpacingBefore(100f);
							    backgroundImageTable.getDefaultCell().setFixedHeight(170.2f);
							    backgroundImageTable.setWidthPercentage(100);
							    backgroundImageTable.getDefaultCell().setPadding(5);
							    
							    PdfPTable dataTable = new PdfPTable(2);
							    dataTable.setWidths(new float[]{7.3f,1.5f});
							    dataTable.setSplitLate(false);
							   // dataTable.setTotalWidth(200f);
							    dataTable.setTotalWidth(tableTotalWidth);
							    dataTable.setLockedWidth(true);
							    dataTable.getDefaultCell().setBorder(0);
							    dataTable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
							    dataTable.setSpacingBefore(100f);
							   // dataTable.getDefaultCell().setFixedHeight(160.2f);
							    dataTable.getDefaultCell().setFixedHeight(tableFixedHeight);
							    PdfPCell cell;
							   
									 for(int k=0;k<rowSize;k++){
										 row rowObj = cardTemp.getRow().get(k);
									int elementSize = 	rowObj.getElement().size(); 
									Element element= null;
									String outMessage="";
									String outMessageelementSize4dataset="";
									String outMessageelementSize4label="";
									for(int l=0;l<elementSize;l++){	 
										String label="";
										Object dataset="";
										element = rowObj.getElement().get(l);
									 if(element.getIsimage().equals("y")){
										 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
							        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
								        		 dataset = recordcard[x];
								        		 break;
											  }
							        	 }
										 
										  if(element.getLabel()!=null){
											 label = element.getLabel().toUpperCase();
										  }
										 
										 String url= null; 
										 String imgEx=null;
										 Image imageSign=null;
										 try{
										 url= photopath+dataset+imgpathextn;
										 if(url!=null){
											 imgEx = getMimeType(url);
										 }
										 }catch(Exception e){
								        	 e.printStackTrace();
								        	 if(imgEx==null){
									    			url=imgpath+"images.png";
								    			}
								         }
										 if(imgEx==null){
								    			url=imgpath+"images.png";
							    			}
					                	 imageSign= Image.getInstance(url);
					                	 imageSign.scaleToFit(10f,50f);
					                	 cell = new PdfPCell(imageSign,true);
					                	
					                	
							  		     cell.setBorder(Rectangle.NO_BORDER);
							  		     cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
							  		     cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_CENTER);
							  		     cell.setColspan(4);
							  		     cell.setRowspan(rowSize-1);
									     //cell.setPadding(0);
									     dataTable.addCell(cell);
							 	         
							 	         
							         }else{
							        	 
							        	  if(element.getLabel()!=null){
											 label = element.getLabel().toUpperCase();
										  }
							        	 for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
							        		 if(element.getDataset().toUpperCase().equals(cardDataBean.getDbheaderNamesList().get(x))){
							        			 if(recordcard[x]!=null){
							        				 dataset = recordcard[x];
							        			 }else  if(recordcard[x]==null){
							        				 dataset=" ";
							        			 }else if(recordcard[x].equals("")){
							        				 dataset=" ";
							        			 }else{
							        				 dataset="NA";
							        			 }
								        		 break;
											  }
							        	 }
							        	/* if(element.getBold().equals("y")){
											 label = element.getLabel().toUpperCase();
										 }*/
							        	 
							        	     String font ="";
							        		 int fontsize=0;
							        		 int element4labelfontType=0;
							        		 String bold="";
							        		 String italic="";
							        		 String underline="";
							        		 int fontType= 0;
							        		 Font.FontFamily fontText= null;
							        	  	 if(element.getFont()!=null){
							        	  		 font = element.getFont().toUpperCase();
							        	  		 switch (font) {
							        	  	         case "HELVETICA":
							        	  	        	fontText = Font.FontFamily.HELVETICA;
							        	  	             break;
							        	  	         case "COURIER":
							        	  	        	fontText = Font.FontFamily.COURIER;
							        	  	             break;
							        	  	         case "TIMES_ROMAN":
							        	  	        	fontText = Font.FontFamily.TIMES_ROMAN;
							        	  	             break;
							        	  	         case "UNDEFINED":
							        	  	        	fontText = Font.FontFamily.UNDEFINED;
							        	  	             break;
							        	  	          case "SYMBOL":
							        	  	        	fontText = Font.FontFamily.SYMBOL;
							        	  	             break;
							        	  	          case "ZAPFDINGBATS":
							        	  	        	fontText = Font.FontFamily.ZAPFDINGBATS;
							        	  	             break;
							        	  	           default : fontText = Font.FontFamily.HELVETICA;
							        	  	     }
							        	  		
											 }
							        	  	 if(element.getBold().equals("y")){
							        	  		 bold = element.getBold().toUpperCase();
							        	  		fontType = Font.BOLD;
											 }
							        	  	 if(element.getItalic().equals("y")){
							        	  		italic = element.getItalic().toUpperCase();
							        	  		fontType = Font.ITALIC;
											 }
							        	  	 if(element.getUnderline().equals("y")){
							        	  		underline = element.getUnderline().toUpperCase();
							        	  		fontType = Font.UNDERLINE;
											 }
							        	  	 
							        	  	 if((bold.equals("Y")) && (italic.equals("Y"))){
							        	  		fontType = Font.BOLDITALIC;
							        	  	 }
							        	  	 if((bold.equals("Y")) && (italic.equals("Y"))   && (underline.equals("Y"))){
							        	  		fontType = Font.ITALIC + Font.UNDERLINE;
							        	  	 }
							        	  	element4labelfontType=Font.NORMAL;
							        	  	fontsize = element.getFontsize();

							        	  	if(elementSize ==4) {
							        	  		if(dataset!=null && !dataset.equals("")) {
							        	  			outMessageelementSize4dataset=outMessageelementSize4dataset+dataset+"~~";
							        	  			
							        	  		}
							        	  		
							        	  		if(label!=null && !label.equals("")) {
							        	  			outMessageelementSize4label=outMessageelementSize4label+label+"~~";
							        	  		}
							        	  	}
							        	  	 
								        	 if(elementSize==1){
								        		 outMessage = label+ dataset;
								        	 }
							        		 if(elementSize>1){
							        			 outMessage = outMessage+label+ dataset+" ";
							        			 if(outMessage.trim()!=""){
										        		
							        				 if(l==1 && elementSize ==2){
							        						 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
							        						 cell.setBorder(Rectangle.NO_BORDER);
							        						 cell.setColspan(4);
							        						 dataTable.addCell(cell);
							        				 }else if(l==2 && elementSize ==3){
							        					 cell = new PdfPCell(new Phrase(outMessage.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
							        					 cell.setBorder(Rectangle.NO_BORDER);
							        					 cell.setColspan(4);
							        					 dataTable.addCell(cell);
							        				 }else if(l==3 && elementSize ==4){
							        					 if(outMessageelementSize4label!=null && !outMessageelementSize4label.equals("")) {
							        						 /*cell = new PdfPCell(new Phrase(outMessageelementSize4label.trim(),  new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
							        						 cell.setBorder(Rectangle.NO_BORDER);
							        						 //#0088cc
							        						 cell.setBackgroundColor(new BaseColor(0, 136, 204));
							        						 cell.setPadding(1);
							        						 cell.setColspan(4);
							        						 dataTable.addCell(cell);*/
							        						 
							        						 String[] StrArray = outMessageelementSize4label.split("~~");
							        							for(int n=0;n<StrArray.length;n++){
							        								cell = new PdfPCell();
							        								cell.setBackgroundColor(new BaseColor(0, 136, 204));
							        								cell.setUseAscender(true);
							        								cell.setBorder(Rectangle.NO_BORDER);
							        								//cell.setMinimumHeight(22);
							        								//cell.setPaddingLeft(10);                    
							        								cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
							        								cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
							        								cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.WHITE)));
							        								dataTable.addCell(cell);
							        						   }
							        					 }
							        				 if(outMessageelementSize4dataset!=null && !outMessageelementSize4dataset.equals("")) {
							        					 /*cell = new PdfPCell(new Phrase(outMessageelementSize4dataset.trim(),  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
							        					 cell.setBorder(Rectangle.NO_BORDER);
							        					 cell.setBackgroundColor(new BaseColor(235, 235, 224));
							        					 cell.setPadding(1);
							        					 cell.setColspan(4);
							        					 dataTable.addCell(cell);*/
							        					 String[] StrArray = outMessageelementSize4dataset.split("~~");
							        						for(int n=0;n<StrArray.length;n++){
							        							cell = new PdfPCell();
							        							cell.setBackgroundColor(new BaseColor(235, 235, 224));
							        							cell.setUseAscender(true);
							        							cell.setBorder(Rectangle.NO_BORDER);
							        							//cell.setMinimumHeight(22);
							        							//cell.setPaddingLeft(10);                    
							        							cell.setHorizontalAlignment(com.itextpdf.text.Element.ALIGN_RIGHT);
							        							cell.setVerticalAlignment(com.itextpdf.text.Element.ALIGN_MIDDLE);
							        							cell.addElement(new Phrase(StrArray[n],new Font(fontText, fontsize, element4labelfontType, BaseColor.BLACK)));
							        							dataTable.addCell(cell);
							        					   }
							        				 }
							        				}
							        				}

							        		 }else{
							        			/* if(element.getLabel().equals("issue on:")){
													 cell = new PdfPCell(new Phrase(outMessage,  new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
													 cell.setBorder(Rectangle.BOX);
										 	         cell.setColspan(4);
										 	         cardTable.addCell(cell);
												 }else{*/
													 //cell = new PdfPCell(new Phrase(outMessage ,new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK)));
							        			 	cell = new PdfPCell(new Phrase(outMessage,  new Font(fontText, fontsize, fontType, BaseColor.BLACK)));
													 cell.setBorder(Rectangle.NO_BORDER);
													 //if((rowSize-1)==k){
														 cell.setColspan(4);
													 //}
													 dataTable.addCell(cell);
												// }
							        		 }
							        	 
							         }
									
									}
									 
								}
					    	  
							   if(j%2!=0){
								 
								    backgroundImageTable.addCell(image);
					    			/*
					    			xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
					    			xPoss = (int)backgroundImageTable.getTotalWidth()+51;*/
								   
								   if(j==1){
									   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
									  // xPoss = xPossImage+35;
									   xPoss = xPossImage+xPossLeft;
								   }else{
									   xPossImage = (int)backgroundImageTable.getTotalWidth()+15;
									   //xPoss = xPossImage+35;
									   xPoss = xPossImage+xPossLeft;
									   yPoss= yPossImage-yPossNextRow;
								   }
							    }else if(j%2==0){
					    			backgroundImageTable.addCell(image);
					    			if(j==0){
					    				xPoss = xPossRight;
					    				// yPoss=808.0f;
					    				 xPossImage = 20;
									    yPossImage = 843.0f;
									    }else{
									    	xPoss = xPossRight;
									    	xPossImage = 20;
									    	yPossImage =  yPossImage - (int)backgroundImageTable.getTotalHeight()+2;
									    	yPoss= yPossImage-yPossNextRow;
										}
							    }
						
							    if(yPossImage <= bottom_margin)
						        {
					            	  cb.endText();
							          document.newPage();
							          cb.beginText();
							          xPoss = xPossLeft;
							         // yPoss=808.0f;
							          xPossImage = 20;
							          yPossImage = 843.0f;
						         }
								 //yPossImage= yPossImage-(policyDataTable.getTotalHeight()+2);
						        // backgroundImageTable.writeSelectedRows(0, 34, 30, yPossImage, cb);
							    
							    ArrayList tmp = dataTable.getRows(0, dataTable.getRows().size());
							    dataTable.getRows().clear();
							    dataTable.getRows().addAll(tmp);
						        backgroundImageTable.writeSelectedRows(0, -1, xPossImage, yPossImage, writer.getDirectContent());
						        dataTable.writeSelectedRows(0, 40, xPoss, yPoss, cb);
						       /* if((records.size()-1)==j){
						        	 yPossImage= yPossImage-190;
						        	 table.writeSelectedRows(0, 40, 30, yPossImage, cb);
						        }*/
					   }
					    	//20-05-2020 added by prabhakar gogula for back image start
					    	 String  cardBackimage1=cardTemp.getCardbackimage();
					    	String IMAGEBACK = imgpath+cardBackimage1;
					    	 System.out.println(" first back Image"+IMAGEBACK);
					    	Image imageback = Image.getInstance(IMAGEBACK);
					    	 PdfPTable backImagetable = new PdfPTable(1);
					    	 backImagetable.setTotalWidth(275);
					    	 backImagetable.setLockedWidth(true);
					    	 backImagetable.getDefaultCell().setBorder(0);
					    	 backImagetable.setHorizontalAlignment(PdfPTable.ALIGN_CENTER);
					    	 backImagetable.getDefaultCell().setFixedHeight(170.2f);
					    	 backImagetable.addCell(imageback);
					    	 //(int)backgroundImageTable.getTotalWidth()-15
					    	 backImagetable.writeSelectedRows(0, 150, 160, yPossImage-190, cb);
					        
					        System.out.println("yposs : "+(int)backImagetable.getTotalHeight());
					       // if((records.size()-1)==j){
					        	 table.writeSelectedRows(0, 40, 30, (int)backImagetable.getTotalHeight(), cb);
					       // }
					        	//20-05-2020 added by prabhakar gogula for back image end
					        if(backImagetable!=null) {
					        	backImagetable=null;
					        }
					        if(table!=null) {
					        	table=null;
					        }
					        if(imageback!=null) {
					        	imageback=null;
					        }
					        if(IMAGEBACK!=null) {
					        	IMAGEBACK=null;
					        }
				    	  cb.endText();
					      document.close();
					    }
			    	  	
				      System.out.println("JOB END FOR generateECardFormat ");
				      logger.info("=============JOB END FOR generateECardFormat ==============");
				
				 }//if listSize >0
			}catch(Exception e){
				e.printStackTrace();
			}
			finally {
				if(myResources!=null){
					myResources=null;
					}
			}
			//return 0;
		}
		
		public static void generateInsuranceCertificate(List<EnrollsBean> enrolesList, String lotNumber)
		{
			 myResources = new MyProperties();
			 String imgpath = myResources.getMyProperties("ANDHRABANKCARDS");
			 String insurencetext = myResources.getMyProperties("insurencetext");
			 String fileCreatepath = myResources.getMyProperties("uploadslocal");
			try{
				String fileName=""; 
	            if(enrolesList.get(0).getBANO()!=null){
	            	fileName = enrolesList.get(0).getBANO();
	            }else if(enrolesList.get(0).getCARDID()!=null){
	            	fileName = enrolesList.get(0).getCARDID();
	            }
	            	Document document = new Document(PageSize.A4);   //lotNumber+tempfamilyId+"_ECard.pdf"
		    	    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(lotNumber+"_"+fileName+".pdf"));
				    document.open();
			        float yPoss=728.0f;
		    		 int listSize=0;
					    listSize=enrolesList.size();
					    if(listSize>0){
					    	//int i=0;
					    	PdfContentByte cb=null;
						     
			    			 PdfPTable billsTable=null;
			    			 String tempBano="";
			    			 int noLeaves=0;
					    	for(EnrollsBean enrollsBean:enrolesList){
					    		if(enrollsBean.getBANO()!=null && !enrollsBean.getBANO().equals("")){
					    			
					    			
					    			
					    			 String certificateName=enrollsBean.getCERTIFICATE();
					    			 System.out.println("certificateName="+certificateName);
					    			 System.out.println("current poilicyid="+enrollsBean.getCURRENTPOLICYID());
					    			 if(certificateName!=null && certificateName!=""){
					    			// final String  IMAGE = imgpath+"inscert_2018.png";
					    			  String  IMAGE = imgpath+certificateName;
					    			
					    			
					    			if(tempBano.equals(enrollsBean.getBANO())){
					    				PdfPCell cellserialNo = new PdfPCell(new Paragraph(enrollsBean.getSerialNo()+".",Constants.fontParaTextins));
					    				cellserialNo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
					    				//cellserialNo.setPaddingLeft(6);
					    				cellserialNo.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellserialNo);
					    				
	    		   		               PdfPCell cellcardId = new PdfPCell(new Paragraph(enrollsBean.getCARDID(),Constants.fontParaTextins));
	    		   		                cellcardId.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                cellcardId.setPaddingLeft(2);
	    		   		                cellcardId.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellcardId);
	    		   		               
	    		   		                PdfPCell cellInsuredName = new PdfPCell(new Paragraph(enrollsBean.getINSUREDNAME(),Constants.fontParaTextins));
	    		   		                cellInsuredName.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                //cellInsuredName.setPaddingLeft(2);
	    		   		                cellInsuredName.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellInsuredName);
	    		   		                
	    		   		                PdfPCell cellInceptionDt = new PdfPCell(new Paragraph(enrollsBean.getINSUREDINCEPTIONDT(),Constants.fontParaTextins));
	    		   		                cellInceptionDt.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		             	//cellInceptionDt.setPaddingLeft(4);
	    		   		             	cellInceptionDt.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellInceptionDt);
	    		   		               
	    		   		                PdfPCell cellRelation = new PdfPCell(new Paragraph(enrollsBean.getRELATIONSHIP(),Constants.fontParaTextins));
	    		   		                cellRelation.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                cellRelation.setBorder(Rectangle.NO_BORDER);
	    		   		                //cellRelation.setPaddingLeft(2);
	    		   		                billsTable.addCell(cellRelation);
	    		   		               
	    		   		                PdfPCell cellSumInsured = new PdfPCell(new Paragraph("",Constants.fontParaTextins));
	    		   		                cellSumInsured.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                cellSumInsured.setBorder(Rectangle.NO_BORDER);
	    		   		                cellSumInsured.setPaddingLeft(2);
	    		   		                billsTable.addCell(cellSumInsured);
	    		   		                
	    		   		                PdfPCell cellTopUpSumInsured = new PdfPCell(new Paragraph("",Constants.fontParaTextins));
	    		   		                cellTopUpSumInsured.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                cellTopUpSumInsured.setBorder(Rectangle.NO_BORDER);
	    		   		                cellTopUpSumInsured.setPaddingLeft(2);
	    		   		                billsTable.addCell(cellTopUpSumInsured);
					    				tempBano=enrollsBean.getBANO();
					    				++noLeaves;
					    				
					    			}else{
					    				if(!tempBano.equals("") && cb!=null && billsTable!=null){
					    					//billsTable.writeSelectedRows(0, 34, 42, yPoss-208, cb);
					    					billsTable.writeSelectedRows(0, 34, 49, yPoss-238, cb);
					    					//cb.endText();
							    			document.newPage();
							    			yPoss=728.0f;
							    			//cb.beginText();
					    				}
					    				
					    				 	cb = writer.getDirectContentUnder();
					    			        Image image = Image.getInstance(IMAGE);
					    			        image.scaleAbsolute(527, 785);
					    			        image.setAbsolutePosition(33, 27);
					    			        cb.addImage(image);
					    			        
					    			        PdfPTable baTable = new PdfPTable(1);
					    			        baTable.setTotalWidth(400);
					    			        baTable.setLockedWidth(true);
					    			        baTable.getDefaultCell().setBorder(0);
					    			        baTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
									   
									     
									    PdfPCell cellbano = new PdfPCell(new Paragraph(enrollsBean.getBANO()  ,Constants.fontParaTextins));
									    cellbano.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    cellbano.setBorder(Rectangle.NO_BORDER);
									    baTable.addCell(cellbano);
									    
									    baTable.writeSelectedRows(0, 34, 90, yPoss, cb);
									    
									    PdfPTable brcodeTable = new PdfPTable(1);
				    			        brcodeTable.setTotalWidth(400);
				    			        brcodeTable.setLockedWidth(true);
				    			        brcodeTable.getDefaultCell().setBorder(0);
				    			        brcodeTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
									    
									    PdfPCell cellbrcode = new PdfPCell(new Paragraph(enrollsBean.getBRANCHCODE()  ,Constants.fontParaTextins));
									    cellbrcode.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    cellbrcode.setBorder(Rectangle.NO_BORDER);
									    brcodeTable.addCell(cellbrcode);
									    
									    brcodeTable.writeSelectedRows(0, 34, 210, yPoss, cb);
									    
									    PdfPTable addressTable = new PdfPTable(1);
									     addressTable.setTotalWidth(400);
									     addressTable.setLockedWidth(true);
									     addressTable.getDefaultCell().setBorder(0);
									     addressTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
									    
									    PdfPCell cellinsName = new PdfPCell(new Paragraph(enrollsBean.getINSUREDNAME(),Constants.fontParaTextinsBold));
									    cellinsName.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    cellinsName.setBorder(Rectangle.NO_BORDER);
									    addressTable.addCell(cellinsName);
									    
									    PdfPCell celladd1 = new PdfPCell(new Paragraph(enrollsBean.getTO1(),Constants.fontParaTextins));
									    celladd1.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    celladd1.setBorder(Rectangle.NO_BORDER);
									    addressTable.addCell(celladd1);
									    
									    PdfPCell celladd2 = new PdfPCell(new Paragraph(enrollsBean.getTO2(),Constants.fontParaTextins));
									    celladd2.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    celladd2.setBorder(Rectangle.NO_BORDER);
									    addressTable.addCell(celladd2);
									    
									    PdfPCell cellcity = new PdfPCell(new Paragraph(enrollsBean.getTO3(),Constants.fontParaTextins));
									    cellcity.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    cellcity.setBorder(Rectangle.NO_BORDER);
									    addressTable.addCell(cellcity);
									    
									    PdfPCell cellstate = new PdfPCell(new Paragraph(enrollsBean.getTO4(),Constants.fontParaTextins));
									    cellstate.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    cellstate.setBorder(Rectangle.NO_BORDER);
									    addressTable.addCell(cellstate);
									    
									    PdfPCell cellPincode = new PdfPCell(new Paragraph("PINCODE : "+enrollsBean.getTO5(),Constants.fontParaTextins));
									    cellPincode.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    cellPincode.setBorder(Rectangle.NO_BORDER);
									    addressTable.addCell(cellPincode);
									    
									    PdfPCell cellphone = new PdfPCell(new Paragraph("PHONE NO : "+enrollsBean.getMOBILENO(),Constants.fontParaTextins));
									    cellphone.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
									    cellphone.setBorder(Rectangle.NO_BORDER);
									    addressTable.addCell(cellphone);
						               
						                addressTable.writeSelectedRows(0, 34, 57, yPoss-24, cb);
						               
						                PdfPTable tabledear = new PdfPTable(1);
						                tabledear.setTotalWidth(527);
						                PdfPCell celldear = new PdfPCell(new Paragraph(String.format(enrollsBean.getPOLICYNO()),Constants.fontGeneralText));
						                celldear.setBorder(PdfPCell.NO_BORDER);
						                celldear.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tabledear.addCell(celldear);
						                PdfPCell cellwe = new PdfPCell(new Paragraph(String.format(enrollsBean.getTOPUPPOLICYNO()),Constants.fontGeneralText));
						                cellwe.setBorder(PdfPCell.NO_BORDER);
						                cellwe.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tabledear.addCell(cellwe);
						                tabledear.writeSelectedRows(0, 34, 140, yPoss-179, cb);
						                
						                billsTable = new PdfPTable(7);
				    	                float[] columnWidths = new float[] {2.0f,8.2f, 13.0f, 4.7f,5.0f, 7.0f,7.0f};
				    	                billsTable.setWidths(columnWidths);
				    	                billsTable.setTotalWidth(520);
				    	                billsTable.setLockedWidth(true);
				    	                
				    	                billsTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				    	               	PdfPCell cellserialNo = new PdfPCell(new Paragraph(enrollsBean.getSerialNo()+".",Constants.fontParaTextins));
					    				cellserialNo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
					    				//cellserialNo.setPaddingLeft();
					    				cellserialNo.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellserialNo);
				    	                
				    	                
				    	                billsTable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
				    	                PdfPCell cellcardId = new PdfPCell(new Paragraph(enrollsBean.getCARDID(),Constants.fontParaTextins));
				    	                cellcardId.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
				    	                cellcardId.setPaddingLeft(2);
				    	                cellcardId.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellcardId);
	    		   		               
	    		   		                PdfPCell cellInsuredName = new PdfPCell(new Paragraph(enrollsBean.getINSUREDNAME(),Constants.fontParaTextins));
	    		   		                cellInsuredName.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                //cellInsuredName.setPaddingLeft(2);
	    		   		                cellInsuredName.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellInsuredName);
	    		   		                
	    		   		                PdfPCell cellInceptionDt = new PdfPCell(new Paragraph(enrollsBean.getINSUREDINCEPTIONDT(),Constants.fontParaTextins));
	    		   		                cellInceptionDt.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		             	//cellInceptionDt.setPaddingLeft(4);
	    		   		             	cellInceptionDt.setBorder(Rectangle.NO_BORDER);
	    		   		                billsTable.addCell(cellInceptionDt);
	    		   		               
	    		   		                PdfPCell cellRelation = new PdfPCell(new Paragraph(enrollsBean.getRELATIONSHIP(),Constants.fontParaTextins));
	    		   		                cellRelation.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                cellRelation.setBorder(Rectangle.NO_BORDER);
	    		   		                //cellRelation.setPaddingLeft(2);
	    		   		                billsTable.addCell(cellRelation);
	    		   		               
	    		   		                PdfPCell cellSumInsured = new PdfPCell(new Paragraph("Rs."+enrollsBean.getSUMINSURED(),Constants.fontParaTextins));
	    		   		                cellSumInsured.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                cellSumInsured.setBorder(Rectangle.NO_BORDER);
	    		   		                //cellSumInsured.setPaddingLeft(6);
	    		   		                billsTable.addCell(cellSumInsured);
	    		   		                
	    		   		                PdfPCell cellTopUpSumInsured = new PdfPCell(new Paragraph("Rs."+enrollsBean.getTOPUPSUMINSURED(),Constants.fontParaTextins));
	    		   		                cellTopUpSumInsured.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
	    		   		                cellTopUpSumInsured.setBorder(Rectangle.NO_BORDER);
	    		   		                //cellTopUpSumInsured.setPaddingLeft(6);
	    		   		                billsTable.addCell(cellTopUpSumInsured);
				    	                
						                tempBano=enrollsBean.getBANO();
						                
						                
						                

						                //suresh added code start
						                if(enrollsBean.getINSURER_CARDID()!=null && enrollsBean.getINSURER_CARDID()!=""){
						                PdfPTable tablecardid = new PdfPTable(1);
						                tablecardid.setTotalWidth(237);
						                PdfPCell cellcardid= new PdfPCell(new Paragraph(String.format(enrollsBean.getINSURER_CARDID()),Constants.fontTableCellText));
						                cellcardid.setBorder(PdfPCell.NO_BORDER);
						                cellcardid.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tablecardid.addCell(cellcardid);
						                tablecardid.writeSelectedRows(0, 24, 37, yPoss-515, cb);
						                }
						                
						                
						                
						                
						            /*    PdfPTable tablepolicyperiod = new PdfPTable(1);
						                tablepolicyperiod.setTotalWidth(237);
						                PdfPCell cellpolicyperiod= new PdfPCell(new Paragraph(String.format(enrollsBean.getUWID()),Constants.fontTableCellText));
						                cellpolicyperiod.setBorder(PdfPCell.NO_BORDER);
						                cellpolicyperiod.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tablepolicyperiod.addCell(cellpolicyperiod);
						                tablepolicyperiod.writeSelectedRows(0, 34, 140, yPoss-559, cb);*/
						                //suresh added code end
						                
						                
						                PdfPTable tableinsuredName = new PdfPTable(1);
				    					tableinsuredName.setTotalWidth(237);
						                PdfPCell cellinsuredName = new PdfPCell(new Paragraph(String.format(enrollsBean.getINSUREDNAME()),Constants.fontGeneralText));
						                cellinsuredName.setBorder(PdfPCell.NO_BORDER);
						                cellinsuredName.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tableinsuredName.addCell(cellinsuredName);
						                tableinsuredName.writeSelectedRows(0, 34, 130, yPoss-537, cb);
						                
						                PdfPTable tabletopupservicetax = new PdfPTable(1);
						                tabletopupservicetax.setTotalWidth(314);
						                double premiumTot =0;
						                double premium=0;
						                if(enrollsBean.getPREMIUM()!=null && enrollsBean.getPREMIUM()!=""){
						                	premium=Double.parseDouble(enrollsBean.getPREMIUM());
						                }
						                double topupPremium=0;
						                if(enrollsBean.getTOPUPPREMIUM()!=null && enrollsBean.getTOPUPPREMIUM()!=""){
						                	topupPremium=Double.parseDouble(enrollsBean.getTOPUPPREMIUM());
						                }
						                premiumTot=	premium+topupPremium;
						                String chequekAmtOrginal= NumberToWords.convert(new Double(premiumTot).intValue()).toUpperCase();
						                PdfPTable tabletopuppremiumTot = new PdfPTable(1);
						                tabletopuppremiumTot.setTotalWidth(200);
						                PdfPCell celltopuppremiumTot = new PdfPCell(new Paragraph("( Rs. "+NumberToWords.setPrecission(premiumTot)+" )",Constants.fontGeneralText));
						                celltopuppremiumTot.setBorder(PdfPCell.NO_BORDER);
						                celltopuppremiumTot.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tabletopuppremiumTot.addCell(celltopuppremiumTot);
						                tabletopuppremiumTot.writeSelectedRows(0, 34, 280, yPoss-537, cb);
						                int premiumTotLength= ("( Rs. "+NumberToWords.setPrecission(premiumTot)+" )").length();
						                PdfPCell celltopupserviceTax = new PdfPCell(new Paragraph(chequekAmtOrginal,Constants.fontGeneralText));
						                celltopupserviceTax.setBorder(PdfPCell.NO_BORDER);
						                celltopupserviceTax.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tabletopupservicetax.addCell(celltopupserviceTax);
						                tabletopupservicetax.writeSelectedRows(0, 34, (320+premiumTotLength), yPoss-537, cb);
						                
						                PdfPTable tableservicetax = new PdfPTable(1);
						                tableservicetax.setTotalWidth(314);
						                double sereviceTaxTot =0;
						                double serviceTax=0;
						                if(enrollsBean.getSERVICETAX()!=null && enrollsBean.getSERVICETAX()!=""){
						                	serviceTax=Double.parseDouble(enrollsBean.getSERVICETAX());
						                }
						                double topupServiceTax=0;
						                if(enrollsBean.getTOPUPSERVICETAX()!=null && enrollsBean.getTOPUPSERVICETAX()!=""){
						                	topupServiceTax=Double.parseDouble(enrollsBean.getTOPUPSERVICETAX());
						                }
						                 sereviceTaxTot = serviceTax+topupServiceTax;
						                 PdfPTable tableservicetaxAmt = new PdfPTable(1);
						                 tableservicetaxAmt.setTotalWidth(200);
						                 PdfPCell cellserviceTaxAmt = new PdfPCell(new Paragraph("( Rs. "+NumberToWords.setPrecission(sereviceTaxTot)+" )",Constants.fontGeneralText));
						                 cellserviceTaxAmt.setBorder(PdfPCell.NO_BORDER);
						                 cellserviceTaxAmt.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                 tableservicetaxAmt.addCell(cellserviceTaxAmt);
						                 tableservicetaxAmt.writeSelectedRows(0, 34, 70, yPoss-548, cb);
						                 
						                 int sereviceTaxTotLength= ("( Rs. "+NumberToWords.setPrecission(sereviceTaxTot)+" )").length();
						                 String sereviceTaxTotOrginal= NumberToWords.convert(new Double(sereviceTaxTot).intValue()).toUpperCase();
						                PdfPCell cellserviceTax = new PdfPCell(new Paragraph(sereviceTaxTotOrginal,Constants.fontGeneralText));
						                cellserviceTax.setBorder(PdfPCell.NO_BORDER);
						                cellserviceTax.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tableservicetax.addCell(cellserviceTax);
						                tableservicetax.writeSelectedRows(0, 34, (110+sereviceTaxTotLength), yPoss-548, cb);
						                
						                PdfPTable tabledateEntry = new PdfPTable(1);
						                tabledateEntry.setTotalWidth(70);
						                DateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY");
						                Date date = new Date();
						                String sysDate = dateFormat.format(date);

						                //PdfPCell celldateEntry = new PdfPCell(new Paragraph(enrollsBean.getPOLICYFROM(),Constants.fontGeneralText));
						                PdfPCell celldateEntry = new PdfPCell(new Paragraph(sysDate,Constants.fontGeneralText));
						                celldateEntry.setBorder(PdfPCell.NO_BORDER);
						                celldateEntry.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
						                tabledateEntry.addCell(celldateEntry);
						                tabledateEntry.writeSelectedRows(0, 34, 75, yPoss-675, cb);
					    			}
					    			
					    			 }
					    			 else {
					    				 System.out.println(" certificate field is empty in policies table");
					    			 }
					    		}
					    		
					    	}
					    	billsTable.writeSelectedRows(0, 34, 49, yPoss-252, cb);
					    	//objectTable.addCell(cell);
					    }
			        document.close();
			}catch(Exception e){
				e.printStackTrace();
				SendMail_preAuth  mailsend=new SendMail_preAuth();
				try {
					mailsend.SendMail_preauthcron1("BankPolicy_EXCEPTION",e.getMessage(),"","alerts@isharemail.in","","");
				} catch (MessagingException e1) {
					e1.printStackTrace();
				}
			}
			
			finally {
				if(myResources!=null){
					myResources=null;
					}
			}
		}
		
		
	
		
		public static void  generateVendorDetails(CardDataBean cardDataBean,  String lotNumber,String contactEmail,String userName)
		{
			try{
				  myResources = new MyProperties();
				 Logger logger =(Logger) Logger.getInstance("generateVendorDetails");
			     logger.info("=============JOB Start FOR generateVendorDetails ==============");
				  String srcFolder = myResources.getMyProperties("uploadslocal");
				 String photopath = myResources.getMyProperties("photopath");
				 String imgpathextn = myResources.getMyProperties("ANDHRABANKCARDSEXTN");
			 	HSSFWorkbook hwb = new HSSFWorkbook();
				CellStyle style = hwb.createCellStyle();
				style.setAlignment(CellStyle.ALIGN_LEFT);
				Sheet sheet = hwb.createSheet("CardHolderDetails");
				Row rowHeader = sheet.createRow((short)0);
			    for(int x=0;x<cardDataBean.getDbheaderNamesList().size();x++){
	    		  	cardDataBean.getDbheaderNamesList().get(x);
	    		  	rowHeader.createCell(x).setCellValue(cardDataBean.getDbheaderNamesList().get(x));
	    	    }
			    String  fileName = lotNumber+".xlsx";
			    File theDir = new File(srcFolder+lotNumber);
				  if (!theDir.exists()) {
					  theDir.mkdir();
				  } 
			    
			    int rowCount=0;
				 for(int i=0;i<cardDataBean.getDbrecordsList().size();i++){
					 String [] s2 = cardDataBean.getDbrecordsList().get(i);
					 int x = s2.length;
					 Row rowcardId = sheet.createRow((short)++rowCount);
					 for(int j=0;j<x;j++){
    					rowcardId.createCell((short)j).setCellValue(s2[j]);
    					String url =null;
				      		 if(j==6){
				        		url = photopath+s2[j]+imgpathextn;
				        		 byte[] rawData = getRawBytesFromFile(url);
				        		 if(rawData!=null){
				        		 File f = new File(theDir+s2[j]+imgpathextn);  //output file path
				        		 FileOutputStream fos = new FileOutputStream(f, false);
				        	        fos.write(rawData);
				        	        fos.flush();
				        	        fos.close();
				        		 }
				      		 }
				        
					 }
				 }
				       
        	       String  filePath = srcFolder+lotNumber + File.separator + fileName;
        	       FileOutputStream out1 = new FileOutputStream (filePath);
        	       hwb.write(out1);
        	       out1.close();
        	       out1.close();
        		 
        		 //write zip file from command fromt using unix or linux command
        		 String destZipFile= srcFolder+lotNumber+".zip";
        		 File file = new File(srcFolder);
				if (file.exists() && file.isDirectory()) {
			  String command ="zip -rj " + destZipFile +  " " + srcFolder;
			  logger.info(command);
			  
			      Process proc = Runtime.getRuntime().exec(command);
			      BufferedReader  in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
			      String line = null;
		            while ((line = in.readLine()) != null) {
		              //  System.out.println(line);
		            }
				}  
				
        	    boolean storefileStatuspart1 =false;
				String FTPHOST=null;
				String FTPUName=null;
				String FTPPwd=null;
				boolean result=false;    
					FTPHOST = myResources.getMyProperties("ftp_host");
					FTPUName = myResources.getMyProperties("ftp_username");
					FTPPwd = myResources.getMyProperties("ftp_password");
			
				    FTPClient client = new FTPClient();
				    BufferedInputStream bis=null;
			                client.connect(FTPHOST);
			                int replyCode = client.getReplyCode();
			                if (!FTPReply.isPositiveCompletion(replyCode)) {
			                	logger.info("Operation failed. FTP Server reply code: " + replyCode);
			                    return;
			                }
				            result = client.login(FTPUName, FTPPwd);
				            if (!result) {
				            	 logger.info("Could not login to the  FTP server");
				            	 return;
				            }
				            client.setFileType(FTP.BINARY_FILE_TYPE);
				            client.enterLocalPassiveMode();
				            String  ftpUploadPath= myResources.getMyProperties("uploadZipfilepath"); 
				            String pathcurrent = ftpUploadPath+lotNumber;
				        	File uploadDir = new File(pathcurrent);
				            if (!uploadDir.exists()){
								String str[] =pathcurrent.split("/");
								for(int i=0;i<str.length;i++){
									client.makeDirectory(str[i]);
									client.changeWorkingDirectory(str[i]);
								}
							}
				            File uploadedFile = new File(filePath);
	                        bis = new BufferedInputStream( new FileInputStream(uploadedFile) );
	                        storefileStatuspart1 = client.storeFile(ftpUploadPath+"/"+lotNumber, bis);
		            if(storefileStatuspart1){
							  SendMail_preAuth sendMailPreauth = new SendMail_preAuth();
							  String subject ="JOB Andhra bank  Vedor details # "+lotNumber;
							  String messageText="Dear "+userName+", <br/><br/>";
								  subject = subject+" Lot Number "+lotNumber+" of  details ";
								  messageText = messageText+"&nbsp;&nbsp;&nbsp;&nbsp;The job ID "+lotNumber+ " created is now completed and you can Now Download the name. <br/>"
								  		+"Number of records in xlsx file  "+rowCount+ "<br/><br/><br/><br/><br/>Thanks & Regards<br/>IT Team.";
							  String displayfilenme="";
							  if(contactEmail!=null && !contactEmail.trim().equals("")){
								  int mailStatus =sendMailPreauth.SendMail_preauthcron1(subject, messageText, displayfilenme, contactEmail, "", "");
								  logger.info("Mail Sent status : "+mailStatus);
							  }
					 }
		            logger.info("=============JOB END FOR generateVendorDetails ==============");
			}catch(Exception e){
				e.printStackTrace();
			}
			finally {
				if(myResources!=null){
					myResources=null;
					}
			}
			//return 0;
		}
		
		
		
		
		
		
		private static byte[] getRawBytesFromFile(String path) throws FileNotFoundException, IOException {

	        byte[] image =null;
	        File file = new File(path);
	        if(file.length()>0){
	        image = new byte[(int)file.length()];
	        FileInputStream fileInputStream = new FileInputStream(file);
	        fileInputStream.read(image);
	        }
	        return image;
	    }
		
		
		
		
		
		
		
		
		
		public static String getMimeType(String fileUrl)
			    throws java.io.IOException, MalformedURLException
			  {
			    String type = null;
			    String fileEx=null;
			    URL u = new URL(fileUrl);
			    URLConnection uc = null;
			    uc = u.openConnection();
			    type = uc.getContentType();
			    
			    if(type!=null){
		    		if(type.equals("image/gif")){   fileEx="gif"; }
		    		if(type.equals("image/jpeg")){  fileEx="jpeg,jpg";}
		    		if(type.equals("image/png")){   fileEx="png";}
		    		if(type.equals("image/bmp")){   fileEx="bmp";}
			    }
	    		
			    return fileEx;
			  }

}
