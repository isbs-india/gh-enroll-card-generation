package com.ghpl.action;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.ExceptionConverter;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPCellEvent;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPTableEvent;

public class ImageBackgroundEvent implements PdfPTableEvent, PdfPCellEvent {
	 
    protected Image image;

    public ImageBackgroundEvent(Image image) {
        this.image = image;
    }

    public void cellLayout(PdfPCell cell, Rectangle position,
            PdfContentByte[] canvases) {
        try {
            PdfContentByte cb = canvases[PdfPTable.BACKGROUNDCANVAS];
            image.scaleAbsolute(100, 200);
            image.setAbsolutePosition(position.getLeft(), position.getBottom());
            cb.addImage(image);
        } catch (DocumentException e) {
            throw new ExceptionConverter(e);
        }
    }

	@Override
	public void tableLayout(PdfPTable arg0, float[][] arg1, float[] arg2,
			int arg3, int arg4, PdfContentByte[] arg5) {
		// TODO Auto-generated method stub
		
	}

}
