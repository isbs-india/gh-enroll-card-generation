package com.ghpl.util;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.IIOException;
import javax.imageio.ImageIO;



public class Util {
	
	public static boolean getValidImage(String name) throws IOException {
	    try {
	    	File image = new File(name);
	        BufferedImage bi = ImageIO.read(image);
	        bi.flush();
	    } catch (IIOException e) {
	        return false;
	    }
	    return true;
	}
	
	/*public static boolean getValidImage(String name){
		boolean flag =false;
		try {
		    Image image = ImageIO.read(new File(name));
		    if (image == null) {
		    	flag = false;
		        System.out.println("The file"+name+"could not be opened , it is not an image");
		    }
		} catch(IOException ex) {
			flag = false;
		    System.out.println("The file"+name+"could not be opened , an error occurred.");
		}
		
		return flag;
	}*/
	
	
	/*public static boolean getValidImage(String path) {
	    try (RandomAccessFile fh = new RandomAccessFile(path, "r")) {
	        long length = fh.length();
	        if (length < 10L) { // Or whatever
	            return false;
	        }
	        fh.seek(length - 2);
	        byte[] eoi = new byte[2];
	        fh.readFully(eoi);
	        return eoi[0] == -1 && eoi[1] == -23; // DD D9
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
	    return 
	}*/
	
	/*public static boolean check(File image)  {
	    JpegImageParser parser = new JpegImageParser();
	    ByteSourceFile bs = new ByteSourceFile(image);
	    try {
	        BufferedImage bi = parser.getBufferedImage(bs, null);
	        bi.flush();

	        return true;
	    } catch (ImageReadException e) {
	        return false;
	    }
	}*/
	
	
	/*public static boolean getValidImage(String filename)   {
		boolean flag = false; 
		DataInputStream ins=null;
		try {
			 URL urlObj = new URL(filename);
		     URLConnection con = urlObj.openConnection();
		     //ins = new DataInputStream(new BufferedInputStream(new FileInputStream(con.getInputStream())));
		     BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
	        if (in.read() == 0xffd8ffe0) {
	        	flag =true;
	        } else {
	        	flag =false;
	        }
	    } catch(Exception ex) {
			flag = false;
		    System.out.println("The file"+filename+"could not be opened , an error occurred.");
		}
	     finally {
	    	if(ins!=null){
	         try {
				ins.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	}
	    }
		return flag;
	}*/

}
