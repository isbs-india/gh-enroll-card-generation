package com.ghpl.util;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;



public class Constants {
	
	public static final Font fontDateText= new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontGeneralText = new Font(Font.FontFamily.HELVETICA, 7, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontPintText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontTableCellText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontBoldParaTextRed = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.RED);
	public static final Font fontNoteText = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontItalicParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC, BaseColor.BLACK);
	public static final Font fontItalicBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC, BaseColor.BLACK);
	public static final Font fontItalicUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.ITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontItalicBoldUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLDITALIC + Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineBoldParaText = new Font(Font.FontFamily.HELVETICA, 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	
	public static final Font fontParaTextverdana = FontFactory.getFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontBoldParaTextverdana = FontFactory.getFont("Verdana", 8, Font.BOLD, BaseColor.BLACK);
	public static final Font fontGeneralTextverdana = FontFactory.getFont("Verdana", 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontUnderlineParaTextverdana = FontFactory.getFont("Verdana", 8, Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineBoldParaTextverdana = FontFactory.getFont("Verdana", 8, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	public static final Font fontParaTextins = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontParaTextinsBold = new Font(Font.FontFamily.HELVETICA, 8, Font.BOLD, BaseColor.BLACK);
	
	public static final Font fontBoldParaText12 = FontFactory.getFont("Verdana", 11, Font.BOLD, BaseColor.BLACK);
	public static final Font fontGeneralText12 = FontFactory.getFont("Verdana", 11, Font.NORMAL, BaseColor.BLACK);
	public static final Font fontUnderlineParaText12 = FontFactory.getFont("Verdana", 11, Font.UNDERLINE, BaseColor.BLACK);
	public static final Font fontUnderlineBoldParaText12 = FontFactory.getFont("Verdana", 11, Font.UNDERLINE + Font.BOLD, BaseColor.BLACK);
	
	   // for live application web only service 
		/*public static final String LOG4JConfig="/home/ec2-user/MyCrons/Ecardlog4j.properties";
		public static final String DBConfig="/home/ec2-user/MyCrons/config.properties";
		public static final String PDFConfig="/home/ec2-user/MyCrons/pdf.properties";*/
	
	// for corn applications internal
	public static final String LOG4JConfig="/root/MyCrons/keys/ECardslog4j.properties";
	public static final String DBConfig="/root/MyCrons/keys/config.properties";
	public static final String PDFConfig="/root/MyCrons/keys/pdf.properties";
	
	/*public static final String LOG4JConfig="E:/appconfig/Ecardslog4j.properties";
	public static final String DBConfig="E:/appconfig/config.properties";
	public static final String PDFConfig="E:/appconfig/pdf.properties";*/
		
		
	  
}
