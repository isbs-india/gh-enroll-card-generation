package com.ghpl.persistence;

import java.io.StringReader;
import java.lang.reflect.Method;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.mail.MessagingException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import oracle.jdbc.OracleTypes;

import com.ghpl.action.SendMail_preAuth;
import com.ghpl.exception.DAOException;
import com.ghpl.model.Card;
import com.ghpl.model.CardBean;
import com.ghpl.model.CardDataBean;
import com.ghpl.model.CardJsonBean;
import com.ghpl.model.Element;
import com.ghpl.model.EnrollsBean;
import com.ghpl.model.InsertLotDetailsBean;
import com.ghpl.model.row;
import com.ghpl.util.DBConn;
import com.ghpl.util.ProcedureConstants;

public class EnrollCardsDAOImpl extends DBConn implements EnrollCardsDAO  {

	@Override
	public CardDataBean getEnrollCardDetails(int lotNumber,int moduleId) throws DAOException {
			Connection con = null;
			CallableStatement cstmt = null;
			//Card enrollsBean=null;
			ResultSet rsData=null;
			String rsXML=null;
			int indexpos=0;
			CardDataBean cardDataBean = new CardDataBean();
			List<EnrollsBean> enroleList = new ArrayList<EnrollsBean>();
			List<String[]> records =new LinkedList<String[]>();
			List<Map<String,String>> certificateRecords =new LinkedList<Map<String,String>>();
			EnrollsBean enrollsBean=null;
			
			try {
					con =getMyConnection(moduleId);
					cstmt = con.prepareCall(ProcedureConstants.PROC_GET_LOT_CARDS_XML_TEMPLATE);
					cstmt.setInt(++indexpos,lotNumber);
					cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
					//cstmt.registerOutParameter(++indexpos, OracleTypes.VARCHAR);
					cstmt.execute();
					rsData = (ResultSet) cstmt.getObject(indexpos);
				System.out.println("hello");
					List<String> columnNames = new ArrayList<String>();
					 ResultSetMetaData columns = rsData.getMetaData();
				        int i = 0;
				        while (i < columns.getColumnCount()) {
				          i++;
				          columnNames.add(columns.getColumnName(i).toUpperCase());
				        }
				        	int cols = rsData.getMetaData().getColumnCount();
				        	while(rsData.next()){
				        		 String methodName = "";
				        		enrollsBean=new EnrollsBean();
				        		//Map<String,String> map = new LinkedHashMap<String,String>();
				        	    String[] arr = new String[cols];
				        	    for(int j=0; j<cols; j++){
				        	      arr[j] = rsData.getString(j+1);
				        	      methodName = "set"+(columnNames.get(j));
				        	     // System.out.println("column name :"+columnNames.get(j)+"     val : "+rsData.getString((columnNames.get(j))));
				        	      Method setNameMethod = enrollsBean.getClass().getMethod(methodName, String.class);
								  setNameMethod.invoke(enrollsBean,rsData.getString((columnNames.get(j)))); // explicit cast
				        	      
				        	    }
				        	    
				        	    enroleList.add(enrollsBean);
				        	    records.add(arr);
				        	}
				        	cardDataBean.setInsurerList(enroleList);    
				        	cardDataBean.setDbheaderNamesList(columnNames);
				        	cardDataBean.setDbrecordsList(records);
				        	
				        	
			}catch (Exception ex) {
				ex.printStackTrace();
				logger.error(ex);
				throw new DAOException();
			}  finally {
				try {
					if(con!=null){
						con.close();
					}
					if(cstmt!=null){
						cstmt.close();
					}
					
					if(rsData!=null){
						rsData.close();
					}
					} catch (SQLException ex) {
						ex.printStackTrace();
						logger.error(ex);
					throw new DAOException();
				}
			}
			return cardDataBean;
	}
	
	public CardJsonBean getEnrollCardDetailsjson(int lotNumber,int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		Card enrollsBean=null;
		ResultSet rsData=null;
		String rsXML=null;
		int indexpos=0;
		CardJsonBean cardDataBean = new CardJsonBean();
		List<Card> enroleList = new ArrayList<Card>();
		List<String[]> records =new LinkedList<String[]>();
		String cardTemplate=null;
		try {
				con =getMyConnection(moduleId);
				cstmt = con.prepareCall(ProcedureConstants.PROC_GET_LOT_CARDS_XML_TEMPLATE);
				cstmt.setInt(++indexpos,lotNumber);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				//cstmt.registerOutParameter(++indexpos, OracleTypes.VARCHAR);
				cstmt.execute();
				rsData = (ResultSet) cstmt.getObject(indexpos);
			    
				List<String> columnNames = new ArrayList<String>();
				 ResultSetMetaData columns = rsData.getMetaData();
			        int i = 0;
			        while (i < columns.getColumnCount()) {
			          i++;
			          columnNames.add(columns.getColumnName(i).toUpperCase());
			        }
			        	int cols = rsData.getMetaData().getColumnCount();
			        	while(rsData.next()){
			        		if(cardTemplate==null){
				        	      if(rsData.getString("ECARD_TEMPLATE")!=null){
				        	    	  cardTemplate=rsData.getString("ECARD_TEMPLATE");
				        	      }
			        	      }
			        		for(int k=0;k<cols;k++){
				        		if(!columns.getColumnName(k).equals("ECARD_TEMPLATE")){
				        	    String[] arr = new String[cols];
				        	    for(int j=0; j<cols; j++){
				        	      arr[j] = rsData.getString(j+1);
				        	    }
				        	    records.add(arr);
				        		}
			        		}
			        	
			        	}
			        	//cardDataBean.setDbheaderNamesList(columnNames);
			        	cardDataBean.setDbrecordsList(records);
			        	 if(cardTemplate!=null){
			        		 cardDataBean.setCardTempxmlString(cardTemplate);
			        	 }
			        	
			        	
		}catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				
				if(rsData!=null){
					rsData.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return cardDataBean;
}
	
	@Override
	public List<EnrollsBean> getLotInsurerCertificationDetails(int lotId, int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		EnrollsBean enrollsBean=null;
		ResultSet rs=null;
		int indexpos=0;
		List<EnrollsBean> enroleList = new ArrayList<EnrollsBean>();
		try {
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
				cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_PROC_GET_LOT_INSCERT);
				cstmt.setInt(++indexpos,lotId);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.execute();
				rs = (ResultSet) cstmt.getObject(indexpos);
				while(rs.next()){
					enrollsBean=new EnrollsBean();
					enrollsBean.setAGE(rs.getString("AGE"));
					enrollsBean.setCARDID(rs.getString("CARDID"));
					enrollsBean.setCURRENTPOLICYID(rs.getString("CURRENTPOLICYID"));
					enrollsBean.setDOB(rs.getString("DOB"));
					enrollsBean.setDOJ(rs.getString("VALIDFROM"));
					enrollsBean.setDOL(rs.getString("DOL"));
					enrollsBean.setEMPID(rs.getString("EMPID"));
					enrollsBean.setFAMILYID(rs.getString("FAMILYID"));
					enrollsBean.setGENDER(rs.getString("GENDER"));
					enrollsBean.setGRADE(rs.getString("GRADE"));
					enrollsBean.setINSUREDNAME(rs.getString("INSUREDNAME"));
					enrollsBean.setINSURERCARDID(rs.getString("INSURER_CARDID"));
					enrollsBean.setIPID(rs.getString("IPID"));
					enrollsBean.setMODULEID(rs.getString("MODULE"));
					enrollsBean.setPLANTNAME(rs.getString("PLANTNAME"));
					enrollsBean.setPOLICYFROM(rs.getString("POLICYFROM"));
					enrollsBean.setPOLICYHOLDERNAME(rs.getString("POLICYHOLDERNAME"));
					enrollsBean.setPOLICYNO(rs.getString("POLICYNO"));
					enrollsBean.setPOLICYTO(rs.getString("POLICYTO"));
					enrollsBean.setRELATIONCODE(rs.getString("RELATION_CODE"));
					enrollsBean.setRELATIONSHIP(rs.getString("RELATIONSHIP"));
					enrollsBean.setUWID(rs.getString("UWID"));
					enroleList.add(enrollsBean);
				}
		}
		catch (Exception ex) {
			ex.printStackTrace();
			SendMail_preAuth mailsend=new SendMail_preAuth();
     		try {
				mailsend.SendMail_preauthcron1("SendToInsurerCommMails_Exception_getClaimDetails",ex.getMessage(),"","alerts@isharemail.in","","");
			} catch (MessagingException e1) {
				e1.printStackTrace();
			}
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				if(rs!=null){
					rs.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return enroleList;
	}
	
	public List<CardDataBean> getEnrollCardDetailsold(int lotNumber,int moduleId) throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		//Card enrollsBean=null;
		ResultSet rsData=null;
		String rsXML=null;
		int indexpos=0;
		Card cardTemp= null;
		//CardDataBean cardDataBean = new CardDataBean();
		List<CardDataBean> enroleList = new ArrayList<CardDataBean>();
		//List<Object[]> records =new LinkedList<Object[]>();
		try {
				con =getMyConnection(moduleId);
				cstmt = con.prepareCall(ProcedureConstants.PROC_GET_LOT_CARDS_XML_TEMPLATE);
				cstmt.setInt(++indexpos,lotNumber);
				cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
				cstmt.registerOutParameter(++indexpos, OracleTypes.VARCHAR);
				cstmt.execute();
				rsData = (ResultSet) cstmt.getObject(indexpos-1);
				rsXML =    cstmt.getString(indexpos);
					StringReader sr = new StringReader(rsXML);
					JAXBContext jaxbContext = JAXBContext.newInstance(Card.class);  
		        	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();  
		        	cardTemp = (Card) jaxbUnmarshaller.unmarshal(sr); 
		        	
		       // Card cardobj = null;
		        row rowObj= null;
		    	Element elemntObj = null;
		    	
		    	CardDataBean cardobj = null;
		      
				Vector<String> columnNames = new Vector<String>();
				 ResultSetMetaData columns = rsData.getMetaData();
			        int i = 0;
			        while (i < columns.getColumnCount()) {
			          i++;
			         // System.out.print(columns.getColumnName(i) + "\t");
			          columnNames.add(columns.getColumnName(i));
			        }
			      //  System.out.print("\n");

			        int rowsize = 0;
			       
			        CardBean cardBean= null;
			       
			        	
			      while (rsData.next()) {
			    	  cardobj = new CardDataBean();
			        	 List<row> rowList = new ArrayList<row>();
			        	 rowsize = cardTemp.getRow().size();
			        for(int j=0;j<rowsize;j++){
			        	//System.out.println("row val  : "+j);
			        	 rowObj = new row();
			        	 rowObj = cardTemp.getRow().get(j);
				        	 if(cardTemp.getRow().get(j).getElement()!=null){
					        	 int elemetsize = 0;
					        	 elemetsize = cardTemp.getRow().get(j).getElement().size();
					        	  List<Element> elementList = new ArrayList<Element>();
					        	 for(int k=0;k<elemetsize;k++){
					        	//	 System.out.println("element :"+k);	 
					        		 elemntObj = new Element();
					        		 elemntObj = cardTemp.getRow().get(j).getElement().get(k);
							        	for (int m = 0; m < columnNames.size(); m++) {
								        	  if(cardTemp.getRow().get(j).getElement().get(k).getDataset().equals(columnNames.get(m).toLowerCase())){
								        		  if(columnNames.get(m).equals("IPID")){
								        			  elemntObj.setDataset("http://docs.icaretpa.com:8087/photoupload/2251467.jpg");
									        		  break;
								        		  }else{
								        			  System.out.println("dataset  : "+cardTemp.getRow().get(j).getElement().get(k).getDataset()  +"     val : "+rsData.getString(columnNames.get(m)));
								        			  elemntObj.setDataset(rsData.getString(columnNames.get(m)));
								        		  break;
								        		  }
								        	  }
							        	}
							        	elementList.add(elemntObj);
							        	elemntObj= null;
					        	 }
					        	 rowObj.setElement(elementList);
					        	 elementList=null;
					        	 
				        	 }
			        	// rowList.add(rowObj);
			        	 rowObj= null;
			        	 //cardobj.setRow(rowList);
				        }
			        rowsize=0;
			        rowList= null;
			        enroleList.add(cardobj);
			        cardobj= null;
			        }
			    
				
				
		}catch (Exception ex) {
			ex.printStackTrace();
			logger.error(ex);
			throw new DAOException();
		}  finally {
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
				
				if(rsData!=null){
					rsData.close();
				}
				} catch (SQLException ex) {
					ex.printStackTrace();
					logger.error(ex);
				throw new DAOException();
			}
		}
		return enroleList;
}
	
	
	public InsertLotDetailsBean getLotDetails(int isCron, int moduleId) throws DAOException
	{
		InsertLotDetailsBean insertLotDetailsBeanObj = null;
		List<InsertLotDetailsBean> lotListObj = null;
		Connection con = null;
		CallableStatement cstmt = null;
		ResultSet rs=null;
		int indexpos=0;

		try
		{
			//Retrieving the policies from CORP GoddHealth Module
			con =getMyConnection(moduleId);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_GET_LOT_DETAILS);
			cstmt.setInt(++indexpos,isCron);
			cstmt.registerOutParameter(++indexpos, OracleTypes.CURSOR);
			cstmt.execute();
			rs = (ResultSet) cstmt.getObject(indexpos);
			lotListObj = new ArrayList<InsertLotDetailsBean>();
			int serialNo=0;
			if(rs.next()) {
				insertLotDetailsBeanObj = new InsertLotDetailsBean();
				insertLotDetailsBeanObj.setSerialNo(++serialNo);
				insertLotDetailsBeanObj.setLotNumber(rs.getInt("LOTID"));
				//insertLotDetailsBeanObj.setPolicyHolderName(rs.getString("POLICYHOLDERNAME"));
				//insertLotDetailsBeanObj.setEndorsementNo(rs.getString("ENDORSEMENTNO"));
				/*String moduleName="";
				int modId=0;
		    	 if(rs.getInt("MODULEID")==1){
		    		 moduleName="GHPL";
		    		 modId=1;
		    	 }else if(rs.getInt("MODULEID")==2){
		    		 moduleName="AB";
		    		 modId=2;
		    	 }else if(rs.getInt("MODULEID")==3){
		    		 moduleName="VENIUS";
		    		 modId=3;
		    	 }else if(rs.getInt("MODULEID")==4){
		    		 moduleName="UNITED";
		    		 modId=4;
		    	 }else if(rs.getInt("MODULEID")==5){
		    		 moduleName="NEW INDIA";
		    		 modId=5;
		    	 }else if(rs.getInt("MODULEID")==6){
		    		 moduleName="ORIENTAL";
		    		 modId=6;
		    	 }else if(rs.getInt("MODULEID")==7){
		    		 moduleName="NATIONAL";
		    		 modId=7;
		    	 }else if(rs.getInt("MODULEID")==8){
		    		 moduleName="NAV";
		    		 modId=8;
		    	 }else if(rs.getInt("MODULEID")==9){
		    		 moduleName="XL-CORP";
		    		 modId=9;
		    	 }else if(rs.getInt("MODULEID")==10){
		    		 moduleName="XL-AB";
		    		 modId=10;
		    	 }else if(rs.getInt("MODULEID")==11){
		    		 moduleName="XL-VENUS";
		    		 modId=11;
		    	 }else if(rs.getInt("MODULEID")==12){
		    		 moduleName="XL-UNITED";
		    		 modId=12;
		    	 }else if(rs.getInt("MODULEID")==13){
		    		 moduleName="XL-NEW INDIA";
		    		 modId=13;
		    	 }else if(rs.getInt("MODULEID")==14){
		    		 moduleName="XL-ORIENTAL";
		    		 modId=14;
		    	 }else if(rs.getInt("MODULEID")==15){
		    		 moduleName="XL-NATIONAL";
		    		 modId=15;
		    	 }else if(rs.getInt("MODULEID")==16){
		    		 moduleName="XL-NAV";
		    		 modId=16;
		    	 }
		    	 insertLotDetailsBeanObj.setModuleName(moduleName);
		    	 insertLotDetailsBeanObj.setModuleId(modId);*/
		    	 insertLotDetailsBeanObj.setEmpName(rs.getString("USERNAME"));
		    	 insertLotDetailsBeanObj.setPolicyId(rs.getInt("POLICYID"));
		    	 //insertLotDetailsBeanObj.setEndorsementId(rs.getInt("ENDORSEMENTID"));
		    	 insertLotDetailsBeanObj.setLotStatus(rs.getInt("LOTSTATUSFLAG"));
		    	 //insertLotDetailsBeanObj.setStatusDesc(rs.getString("STATUSDESC"));
		    	 insertLotDetailsBeanObj.setCreatedOn(rs.getString("LOTCREATED"));
		    	// insertLotDetailsBeanObj.setUserName(rs.getString("USERNAME"));
		    	 insertLotDetailsBeanObj.setContactEmailId(rs.getString("CONTACTEMAIL"));
		    	 insertLotDetailsBeanObj.setCardFormat(rs.getString("CARD_FORMAT"));
		    	 insertLotDetailsBeanObj.setGenInsertStausFlag(rs.getInt("gen_ins_cer"));
		    	 //lotListObj.add(insertLotDetailsBeanObj);

			}//while


		}
		catch(Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN getEndorDDLDetails:"+ex);
				throw new DAOException();
			}
		}
		return insertLotDetailsBeanObj;
	}

	

	


	/*@Override
	public int putLotDetails(int lotNumber, int lotStatus, int moduleId)throws DAOException {
		Connection con = null;
		CallableStatement cstmt = null;
		int indexpos=0;
		int outputStatus=0;
		try
		{
			con =getMyConnection(moduleId);
			con.setAutoCommit(false);
			cstmt = con.prepareCall(ProcedureConstants.PROC_FOR_PUT_LOT_DETAILS);
			cstmt.setInt(++indexpos,lotNumber);
			cstmt.setInt(++indexpos,lotStatus);
			cstmt.registerOutParameter(++indexpos, OracleTypes.INTEGER);
			cstmt.execute();
			con.commit();
			outputStatus = cstmt.getInt(indexpos);
		}
		catch(Exception ex)
		{
			logger.error(ex);
			throw new DAOException();
		}
		finally
		{
			try {
				if(con!=null){
					con.close();
				}
				if(cstmt!=null){
					cstmt.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
				logger.error("ERROR IN putLotDetails:"+ex);
				throw new DAOException();
			}
		}
		return outputStatus;
	}*/
	
	

}
