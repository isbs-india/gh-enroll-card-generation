package com.ghpl.persistence;

import java.util.List;









import com.ghpl.exception.DAOException;
import com.ghpl.model.Card;
import com.ghpl.model.CardDataBean;
import com.ghpl.model.CardJsonBean;
import com.ghpl.model.EnrollsBean;
import com.ghpl.model.InsertLotDetailsBean;

public interface EnrollCardsDAO {
	public CardDataBean getEnrollCardDetails(int lotNumber,int moduleId) throws DAOException;
	public List<CardDataBean> getEnrollCardDetailsold(int lotNumber,int moduleId) throws DAOException;
	public InsertLotDetailsBean getLotDetails(int isCron, int moduleId) throws DAOException;
	public List<EnrollsBean> getLotInsurerCertificationDetails(int lotId, int moduleId) throws DAOException;
	public CardJsonBean getEnrollCardDetailsjson(int lotNumber,int moduleId) throws DAOException;
	
	
		
	
}
