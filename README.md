EnrollAuxillary tasks web applicaiton has been deployed first in Jan 2016 and further enhanced as it goes.  EnrollAuxillary tasks provides the following features through leftmenu.

Developed in Java
Team: Mr. Prabhakar, Mr. Anand, Mr. Raviteja (Database), Ms Ratnam (Database) and Mr. Surya Kiran

Current Maintainance Team:
Mr. Rajesh Pudi (Database)
Mr. Prabhakar & Mr. Anand

URL for Live : http://act3.goodhealthtpa.net:8085/EnrollAuxillaryTasks/
URL for Stage : http://stage.goodhealthtpa.net:8080/EnrollAuxillaryTasks/
Login : http://act3.goodhealthtpa.net:8085/EnrollAuxillaryTasks/LoginAction
Database : corp 100.73

Folder Structure

/SourceCode : contains the actual source code (Eclipse Workbench)
/Deploy : Deployment files of various version
/Resources : Various documents and other referential assets used for this project
/DB : Various database scripts

LeftMenu Options:

Upload Single Photo
Valid JPG
 
Bulk Operations
CARD Generation
Corp policies
BA policies
Individual policies
 
CARD Generation Job List
Corp policies
BA policies
Individual policies
 
Bulk Photo Upload
Corp & BA Policies
Individual policies
View Bulk Photo Upload Job
 
Bulk Photo Download
Corp & BA Policies
Individual policies
View Bulk Photo Download Job
 
Create Print Address Label Job
Corp BA & Individual Policies
View Print Address Label Job
In Active Policies
CARD Generation
Corp policies
BA policies
Individual policies
CARD Generation Job List
Corp policies
BA policies
Individual policies
 
Bulk Photo Download
Corp & BA Policies
Individual policies
View Bulk Photo Download Job
Logout
